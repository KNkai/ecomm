---
to: lib/src/repositories/<%= h.inflection.underscore(name, true) %>.repo.dart
---
import 'package:genie/src/models/<%= h.inflection.underscore(name) %>.model.dart';
import 'package:genie/src/services/graphql/graph-repository.dart';

class <%= h.inflection.camelize(name) %>Repository extends GraphRepository<<%= h.inflection.camelize(name) %>> {
  final String shortFragment = "id name";
  final String fullFragment = "id name";
  final String apiName = "<%= h.inflection.camelize(name) %>";

  <%= h.inflection.camelize(name) %> fromJson(Map<String, dynamic> json) => <%= h.inflection.camelize(name) %>.fromJson(json);
}
