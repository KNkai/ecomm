import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:genie/src/components/app_navbar.dart';
import 'package:genie/src/controllers/auth_controller.dart';
import 'package:genie/src/helper/cart_data.dart';
import 'package:genie/src/helper/no_splash_factory.dart';
import 'package:genie/src/pages/detail_product/controllers/detail_product_controller.dart';
import 'package:genie/src/pages/member_ship_page/controller/member_ship_controller.dart';
import 'package:genie/src/pages/notification/notify_controller.dart';
import 'package:genie/src/pages/profile/controllers/profile_controller.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:provider/provider.dart';

import 'src/utils/size_config.dart';

void main() {
  runApp(MyApp());
  if (Platform.isIOS) {
    // Android is fixed in native, but iOS can not rigid in portrait because youtupe page need to change to landscape.
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarBrightness: Brightness.light));
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final notifyController = NotifyController();
  @override
  Widget build(BuildContext context) {
    SizeConfig.init();
    AppClient.instance.init();
    // Auth.init();
    Firebase.initializeApp();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthController(_)),
        ChangeNotifierProvider(create: (_) => ProfileController(_)),
        Provider<CartData>(create: (_) => CartData()),
        ChangeNotifierProvider(create: (_) => ProductMasterModel()),
        ChangeNotifierProvider(create: (_) => MemberShipController()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Genie',
        theme: ThemeData(
          splashFactory: const NoSplashFactory(),
          bottomNavigationBarTheme: const BottomNavigationBarThemeData(
              selectedItemColor: AppColor.primary,
              unselectedItemColor: Color(0xff9B9B9B),
              selectedLabelStyle: TextStyle(fontSize: 13.0),
              unselectedLabelStyle: TextStyle(fontSize: 13.0),
              showSelectedLabels: true),
          textTheme: const TextTheme(bodyText2: TextStyle(fontSize: 17)),
          brightness: Brightness.light,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: AppNavBar(),
      ),
    );
  }
}
