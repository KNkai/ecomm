import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:genie/src/utils/app_key.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../pages/cart/cart_controller.dart';

abstract class CartProtocol {
  void eventChangeProductQuantity(
      {@required String productId, @required int quantity});

  void eventChangeProductNote(
      {@required String productId, @required String note});

  void eventAddProudct({
    @required String productId,
    @required int quantity,
    @required String note,
  });

  void eventRemoveProduct({@required String productId});

  void eventReset();
}

class CartData extends CartProtocol {
  static CartData getCtl(BuildContext context) {
    return Provider.of<CartData>(context, listen: false);
  }

  SharedPreferences _sharePrefInstance;

  final _mapCart = LinkedHashMap<String, ModelCart>();
  final stateNumberCart = ValueNotifier<int>(0);

  LinkedHashMap<String, ModelCart> get mapCart => _mapCart;

  CartData() {
    // get data from local and decode to _mapCart
    SharedPreferences.getInstance().then((instance) {
      _sharePrefInstance = instance;
      final jsonPersist = instance.getString(AppKey.cartData);
      int count = 0;
      if (jsonPersist != null) {
        (jsonDecode(jsonPersist)['persist'] as List).forEach((element) {
          final d = ModelCart.fromJson(element);
          _mapCart[d.productId] = ModelCart.fromJson(element);
          count++;
        });
        print(jsonPersist);
        _mapCart.forEach((key, value) {
          print(
              "key: $key - value.id: ${value.productId} - value.note: ${value.note} - value.quantity: ${value.quantity}");
        });

        stateNumberCart.value = count;
      }
    });
  }

  @override
  void eventChangeProductQuantity(
      {@required String productId, @required int quantity}) {
    final oldQuantity = _mapCart[productId].quantity;
    if (quantity > 0 && oldQuantity != quantity) {
      _mapCart[productId].quantity = quantity;
      _updateDataToLocal();
    }
  }

  void eventChangeProductNote(
      {@required String productId, @required String note}) {
    _mapCart[productId].note = note;
    _updateDataToLocal();
  }

  @override
  void eventAddProudct({
    @required String productId,
    @required int quantity,
    @required String note,
  }) {
    final product = _mapCart[productId];
    if (product != null)
      _mapCart[productId]
        ..quantity += quantity
        ..note = note;
    else
      _mapCart[productId] = ModelCart(
        quantity: quantity,
        productId: productId,
        note: note,
      );
    _updateDataToLocal();
    _updateNumberState();
  }

  void eventRemoveProduct({@required String productId}) {
    _mapCart.remove(productId);
    _updateDataToLocal();
    _updateNumberState();
  }

  void eventReset() {
    _mapCart.clear();
    _updateDataToLocal();
    _updateNumberState();
  }

  void _updateNumberState() {
    stateNumberCart.value = mapCart.length;
  }

  void _updateDataToLocal() {
    if (_sharePrefInstance != null) {
      final persist = List<Map<String, dynamic>>(_mapCart.length);
      int c = 0;
      _mapCart.forEach((_, value) {
        persist[c++] = value.toJson();
      });
      _sharePrefInstance.setString(
          AppKey.cartData, jsonEncode({"persist": persist}));
    }
  }
}
