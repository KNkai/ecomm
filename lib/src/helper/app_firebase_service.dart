import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/app_webview.dart';
import 'package:genie/src/pages/category/category_page.dart';
import 'package:genie/src/pages/default_screen/default_screen_page.dart';
import 'package:genie/src/pages/detail_product/detail_product_page.dart';
import 'package:genie/src/pages/order_detail/order_detail_page.dart';

class AppFirebaseService {
  AppFirebaseService._();

  static final AppFirebaseService _instance = AppFirebaseService._();

  static AppFirebaseService get instance => _instance;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  BuildContext _context;

  Future<String> get deviceToken {
    return _firebaseMessaging.getToken();
  }

  void init(BuildContext context) {
    if (_context == null) {
      _firebaseMessaging
        ..configure(
          onLaunch: _onLaunch,
          onResume: _onResume,
        )
        ..requestNotificationPermissions();
    }
    _context = context;
  }

  Future<bool> deleteInstanceID() {
    return _firebaseMessaging.deleteInstanceID();
  }

  Future<void> _onResume(Map<String, dynamic> message) async {
    _fcmClick(message: message);
  }

  Future<void> _onLaunch(Map<String, dynamic> message) async {
    _fcmClick(message: message);
  }

  Future<String> getDeviceToken() {
    return _firebaseMessaging.getToken();
  }

  void _fcmClick({Map<String, dynamic> message}) {
    if (Platform.isAndroid) {
      final data = message['data'];
      _filterPushPlatform(message: data, type: data['type']);
    } else {
      _filterPushPlatform(message: message, type: message['type']);
    }
  }

  void _filterPushPlatform({@required dynamic message, @required String type}) {
    if (type == 'ORDER') {
      final orderId = message['orderId'];
      if (orderId != null)
        OrderDetailPage.pushNew(context: _context, id: orderId);
    } else if (type == 'PRODUCT') {
      final productId = message['productId'];
      if (productId != null)
        DetailProductPage.push(context: _context, productId: productId);
    } else if (type == 'CATEGORY') {
      final categoryId = message['categoryId'];
      if (categoryId != null)
        CategoryPage.push(_context, categoryId: categoryId, title: '');
    } else if (type == 'WEBSITE') {
      final link = message['link'];
      if (link != null)
        AppWebView.push(context: _context, title: '', url: link);
    } else if (type == 'SCREEN') {
      final screenId = message['screenId'];
      if (screenId != null)
        DefaultScreenPage.push(context: _context, screenId: screenId);
    }
  }
}
