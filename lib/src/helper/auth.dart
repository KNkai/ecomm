import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:genie/src/pages/login/login_page.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/utils/app_key.dart';
import 'package:jose/jose.dart';

class Auth {
  Auth._();

  static void init() {
    const FlutterSecureStorage()
        .read(key: AppKey.xToken)
        .then((xTokenFromStorage) {
      if (xTokenFromStorage == null || xTokenFromStorage.isEmpty) return;
      final jws =
          JsonWebSignature.fromCompactSerialization(xTokenFromStorage ?? "");
      final contentJwt = jws?.unverifiedPayload?.stringContent;
      if (contentJwt != null) {
        final timeExp = DateTime.fromMillisecondsSinceEpoch(
                jsonDecode(contentJwt)['exp'] * 1000)
            .toLocal();

        if (timeExp.isAfter(DateTime.now().add(const Duration(minutes: 30)))) {
          AppClient.instance.installToken(xToken: xTokenFromStorage);
        }
      }
    });
  }

  static void checkExistUser(
      {@required BuildContext context,
      @required VoidCallback onLoginSuccess,
      VoidCallback onAlreadyLogin}) {
    if (AppClient.instance.checkExistToken()) {
      if (onAlreadyLogin != null)
        onAlreadyLogin();
      else
        onLoginSuccess();
    } else {
      LoginPage.push(context, onLoginSuccess: onLoginSuccess);
    }
  }

  static void removeUser() {}
}
