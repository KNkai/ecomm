import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:genie/src/helper/app_firebase_service.dart';
import 'package:genie/src/models/customer.model.dart';
import 'package:genie/src/repositories/customer.repo.dart';
import 'package:genie/src/repositories/reward.repo.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/utils/app_key.dart';
import 'package:genie/src/utils/util.dart';
import 'package:jose/jose.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthController extends ChangeNotifier {
  final BuildContext context;
  final customerRepo = new CustomerRepository();
  final rewardRepo = new RewardRepository();

  ValueNotifier<Customer> customerNotifer = ValueNotifier(null);

  AuthController(this.context) {
    getXToken().then((xTokenFromStorage) {
      if (xTokenFromStorage == null || xTokenFromStorage.isEmpty) return;
      final jws =
          JsonWebSignature.fromCompactSerialization(xTokenFromStorage ?? "");
      final contentJwt = jws?.unverifiedPayload?.stringContent;
      if (contentJwt != null) {
        final timeExp = DateTime.fromMillisecondsSinceEpoch(
                jsonDecode(contentJwt)['exp'] * 1000)
            .toLocal();

        if (timeExp.isAfter(DateTime.now().add(const Duration(minutes: 30)))) {
          setXToken(xTokenFromStorage);
          customerRepo.getCust().then((customer) {
            setCustomer(customer);
          });
        }
      }
    });
  }
  Future<String> getXToken() {
    return const FlutterSecureStorage().read(key: AppKey.xToken);
  }

  setXToken(String token) {
    AppClient.instance.installToken(xToken: token);
    // Save xToken to secure storage.
    const FlutterSecureStorage().write(key: AppKey.xToken, value: token);
  }

  setCustomer(Customer customer) {
    // Save customerId to local
    SharedPreferences.getInstance().then((instance) {
      instance.setString(AppKey.customerId, customer.id);
    });
    customerNotifer.value = customer;
    customerNotifer.notifyListeners();
  }

  getCustomer() {
    customerRepo.getCust().then((customer) {
      setCustomer(customer);
    });
  }

  getReward(String idReward) {
    rewardRepo.getReward(idReward).then((value) {
      getCustomer();
    });
  }

  Future<Customer> loginWithFirebaseToken(String token) async {
    List<String> result = await Future.wait(
        [AppFirebaseService.instance.getDeviceToken(), getUniqueDeviceId()]);
    String deviceToken = result[0];
    String deviceId = result[1];
    var loginData =
        await customerRepo.loginByFirebaseToken(token, deviceToken, deviceId);
    setXToken(loginData.token);
    setCustomer(loginData.customer);
    return loginData.customer;
  }

  Future logout() async {
    String deviceId = await getUniqueDeviceId();
    await AppClient.instance
        .execute('mutation { unRegisDevice(deviceId: "$deviceId") }');
    AppClient.instance.unInstallToken();
    // Delete xToken to secure storage.
    const FlutterSecureStorage().delete(key: AppKey.xToken);
    // Delete customerId from local
    SharedPreferences.getInstance().then((instance) {
      instance.remove(
        AppKey.customerId,
      );
    });
    customerNotifer.value = null;
    customerNotifer.notifyListeners();
  }

  Future updateCustomerProfile(dynamic data) {
    return customerRepo.updateMeCustomer(data).then((customer) {
      customerNotifer.value = customer;
      customerNotifer.notifyListeners();
    });
  }
}
