import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/services/graphql.dart';
import 'package:genie/src/utils/util.dart';

class FavoriteRepository extends GraphQL<FavoriteProduct> {
  Future addFavoriteProduct(String idProduct) async {
    String fragment = '''
        mutation{
  createFavoriteProduct(data:{productId:"${idProduct}"}){
    product{
      id
    }
  }
}
  ''';

    return AppClient.instance.execute(fragment).then((value) {
      return value.data;
    }).catchError((e) {
      return e;
    });
  }

  deleteOneFavouriteProd(String id) {
    print(id);
    deleteOne(id: id);
  }

  @override
  FavoriteProduct fromJson(Map<String, dynamic> json) =>
      FavoriteProduct.fromJson(json);

  @override
  String get modelName => 'FavoriteProduct';
}

class FavoriteStackRepository extends FavoriteRepository
    with InitListProtocol, LoadMoreProtocol {
  num _limit = 10;
  String _query = '''
    id
    productId
    product{
      id
      blockItem {
        title
        image
        product {
          basePrice 
          hasSale 
          saleValue 
          saleType 
          saleQty
        }
        action {
          type
          screenId
          categoryId
          productId
          link
        }
      }
    }
  ''';

  @override
  Future<ResultInitList> init({dynamic parameter}) {
    return getAll(
        query: _query,
        queryInput: GraphQLQueryInput(
          limit: 6,
          page: 1,
          order: {"updatedAt": -1},
        )).then((value) {
      if (appCheckListEmpty(value?.data) || value?.pagination?.total == null) {
        return null;
      }

      return ResultInitListSuccess<FavoriteProduct>(
          value.pagination.total, value?.data);
    }).catchError((_) => print("ERRPR ${_.toString()}"));
  }

  @override
  Future<ResultLoadMoreSuccess> loadMore({int page, dynamic parameter}) {
    return getAll(
        query: _query,
        queryInput: GraphQLQueryInput(
          limit: _limit,
          page: page,
          order: {"_id": -1},
        )).then((value) {
      if (!appCheckListEmpty(value?.data))
        return ResultLoadMoreSuccess<FavoriteProduct>(value.data);

      return null;
    });
  }

  Future<FavoriteProduct> checkFavoritesProd({String id}) async {
    final _query = '''{
           getAllFavoriteProduct
           (q: {filter:{productId:"$id"}})
            { 
              data {     
                      id
                      productId
                      
                    }
            }}
    ''';
    return await AppClient.instance.execute(_query).then((value) {
      final list = List<FavoriteProduct>.from(
          value.data['getAllFavoriteProduct']['data'].map((e) {
        return FavoriteProduct.fromJson(e);
      })).toList();
      if (list != null) {
        return list[0];
      }

      return null;
    }).catchError((onError) {
      print(onError);
      return null;
    });
    // return false;
  }
}
