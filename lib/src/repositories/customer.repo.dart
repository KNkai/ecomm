import 'package:genie/src/models/customer.model.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/services/graphql.dart';

class CustomerRepository extends GraphQL<Customer> {
  @override
  Customer fromJson(Map<String, dynamic> json) {
    return Customer.fromJson(json);
  }

  @override
  String get modelName => 'Customer';

  final String fragment = """
    name
    id
    rank
    avatar
    companyName
    rewardPoint
    cumulativePoint
    rewardPoint
    phone
    email
    birthday
    unseenNotify
    gender
    address
    recentKeyWords
    province
    ward
    district
    provinceId
    wardId
    districtId
  """;
  Future<Customer> getCust() async {
    return await AppClient.instance
        .execute("query { getMeCustomer { $fragment } }")
        .then((value) {
      if (value == null) {
        return null;
      }
      Customer cus = Customer.fromJson(value.data['getMeCustomer']);
      return cus;
    });
  }

  Future<LoginCustomerData> loginByFirebaseToken(
      String token, String deviceToken, String deviceId) async {
    return await AppClient.instance.execute(
        """mutation { loginCustomerByToken(idToken: "$token", deviceId: "$deviceId", deviceToken: "$deviceToken") { token , customer { $fragment }} }""").then((value) {
      return LoginCustomerData(
          token: value.data["loginCustomerByToken"]["token"],
          customer: Customer.fromJson(
              value.data["loginCustomerByToken"]["customer"]));
    });
  }

  Future<Customer> updateMeCustomer(dynamic data) async {
    // print(fragment);
    return await AppClient.instance.execute(
        '''mutation Update(\$data: UpdateCustomerInput!){ customerUpdateMe(data: \$data){ $fragment } }''',
        variables: {"data": data}).then((value) {
      return Customer.fromJson(value.data["customerUpdateMe"]);
    }).catchError((onError) {
      print("Error $onError");
      return null;
    });
  }
}

class LoginCustomerData {
  final String token;
  final Customer customer;

  LoginCustomerData({this.token, this.customer});
}
