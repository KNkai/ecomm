import 'package:genie/src/models/keyword.model.dart';
import 'package:genie/src/models/membership.model.dart';
import 'package:genie/src/services/app_client.dart';

class KeyWordRepository {
  final String fragment = """
     {
  getAllKeyWord{
    data{
      keyWord
      count
    }
  }
}
  """;

  Future<List<KeyWordClass>> getAllKeyWord() async {
    List<KeyWordClass> listAllKeyWord = List<KeyWordClass>();
    return await AppClient.instance.execute(fragment).then((value) {
      if (value == null) return null;
      listAllKeyWord = List<KeyWordClass>.from(value.data['getAllKeyWord']
              ['data']
          .map((x) => KeyWordClass.fromJson(x))).toList();
      return listAllKeyWord;
    }).catchError((e) {
      print(e.toString());
      return null;
    });
  }
}
