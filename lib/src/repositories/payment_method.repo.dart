import 'package:genie/src/models/payment_method.model.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/services/graphql.dart';

class PaymentMethodRepository extends GraphQL<PaymentMethodModel> {
  @override
  PaymentMethodModel fromJson(Map<String, dynamic> json) =>
      PaymentMethodModel.fromJson(json);

  @override
  String get modelName => 'PaymentMethod';

  String _query = '''
  
    {
      getAllPaymentMethod{
        value
        label
      }
    }
  
  ''';

  Future getAllPayMentMethod() async {
    List<PaymentMethodModel> listPaymentMethod = [];
    return await AppClient.instance.execute(_query).then((value) {
      value.data['getAllPaymentMethod'].forEach((e) {
        listPaymentMethod.add(PaymentMethodModel.fromJson(e));
      });
      return listPaymentMethod;
    });
  }
}
