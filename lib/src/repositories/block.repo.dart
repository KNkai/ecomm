import 'package:genie/src/models/block.model.dart';
import 'package:genie/src/models/pagination.model.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/services/graphql.dart';

class BlockRepository extends GraphQL<Block> {
  @override
  Block fromJson(Map<String, dynamic> json) => Block.fromJson(json);

  @override
  String get modelName => 'Block';
  String blockQuery = '''
        id
      screenId
      name
      code
      type
      priority
      title
      subtitle
      backgroundColor
      textColor
      gridCol
      hidden
      items {
        id
        blockId
        image
        title
        subtitle
        backgroundColor
        textColor
        hidden
        campaign{
          name
          type
          taken
          code
          description
          startAt
          endAt
          details{
            title
            content
          }
        }
        action {
          type
          screenId
          categoryId
          productId
          link
        }
        product {
          id
          saleQty
          name
          basePrice
          rating
          saleType
          saleQty
          saleValue
          hasSale
        }
      }
''';

  // null
  // List<Block>
  Future getAllBlockDashboard(String screenId) {
    return getAll(
        query: blockQuery,
        queryInput: GraphQLQueryInput(
            limit: 100,
            page: 1,
            filter: {"screenId": screenId},
            order: {"priority": -1})).then((res) {
      if (res?.data == null && res.data.isEmpty) return null;

      return res.data;
    }).catchError((_) => null);
  }

  Future<List<Block>> getAllHomeBlock() {
    var queryInput =
        GraphQLQueryInput(limit: 100, page: 1, order: {"priority": -1});
    const apiName = 'getAllHomeBlock';
    return AppClient.instance.execute('''
        query GetAll(\$q: QueryGetListInput) {
          $apiName(q: \$q) { data { $blockQuery } pagination { limit page total } }
        }
        ''', variables: {'q': queryInput.toJson()}).then((response) {
      return GraphQLList(
          List<Block>.from(
              response.data[apiName]["data"].map((x) => fromJson(x))),
          Pagination.fromJson(response.data[apiName]["pagination"]));
    }).then((res) {
      if (res?.data == null && res.data.isEmpty) return null;
      return res.data;
    }).catchError((_) {
      print('ERROR $_');
    });
  }

  Future<List<Block>> getAllPromotionBlock() {
    var queryInput =
        GraphQLQueryInput(limit: 100, page: 1, order: {"priority": -1});
    const apiName = 'getAllPromotionBlock';
    return AppClient.instance.execute('''
        query GetAll(\$q: QueryGetListInput) {
          $apiName(q: \$q) { data { $blockQuery } pagination { limit page total } }
        }
        ''', variables: {'q': queryInput.toJson()}).then((response) {
      return GraphQLList(
          List<Block>.from(
              response.data[apiName]["data"].map((x) => fromJson(x))),
          Pagination.fromJson(response.data[apiName]["pagination"]));
    }).then((res) {
      if (res?.data == null && res.data.isEmpty) return null;
      return res.data;
    }).catchError((_) {
      print('ERROR $_');
    });
  }
}
