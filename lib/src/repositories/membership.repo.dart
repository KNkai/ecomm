import 'package:genie/src/models/membership.model.dart';
import 'package:genie/src/services/app_client.dart';

class MemberShipClassRepository {
  final String fragment = """
      {
        getAllMembershipClass{
          data{
            id
            name
            code
            color
            requiredPoint
            requiredOrder
            requiredSales
            privileges
          }
        }
      }
  """;

  Future<List<MemberShipClass>> getAllMemberShipClass() async {
    List<MemberShipClass> listMemberShipClass = List<MemberShipClass>();
    return await AppClient.instance.execute(fragment).then((value) {
      if (value == null) return null;
      listMemberShipClass = List<MemberShipClass>.from(value
          .data['getAllMembershipClass']['data']
          .map((x) => MemberShipClass.fromJson(x))).toList();
      return listMemberShipClass;
    }).catchError((e) {
      print(e.toString());
      return null;
    });
  }
}
