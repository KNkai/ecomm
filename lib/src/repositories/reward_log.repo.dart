import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/reward_log.model.dart';
import 'package:genie/src/services/graphql.dart';

class RewardLogRepository extends GraphQL<RewardLog> with InitListProtocol {
  @override
  RewardLog fromJson(Map<String, dynamic> json) {
    return RewardLog.fromJson(json);
  }

  @override
  String get modelName => 'RewardLog';

  @override
  Future<ResultInitList> init({parameter}) {
    return getAll(query: '''id reward { id
            name
            point  
            image
            content
            issueNumber
            qtyExchanged
            type
            isActivated
            startAt
            endAt
            condition
            campaign {
              image,
              name,
              type
              id
              endAt
              code
              details {
                title
                content
              }
            } }''', queryInput: GraphQLQueryInput(limit: 1000)).then((value) {
      if (value?.data == null || value.data.isEmpty) return null;

      return ResultInitListSuccess(value.pagination.total, value.data);
    }).catchError((_) => null);
  }
}
