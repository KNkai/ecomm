import 'package:genie/src/models/category.model.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/services/graphql.dart';

class StoreRepository extends GraphQL<Category> {
  @override
  Category fromJson(Map<String, dynamic> json) => Category.fromJson(json);

  @override
  String get modelName => 'Category';

  String fragment = '''{
      getAllShopCategory{
        id
        name
        priority
        showOnShop
      }
    }''';

  Future getAllShopCategory() async {
    List<Category> listCategories = [];
    return await AppClient.instance.execute(fragment).then((value) {
      value.data['getAllShopCategory'].forEach((e) {
        listCategories.add(Category.fromJson(e));
      });
      return listCategories;
    });
  }
}
