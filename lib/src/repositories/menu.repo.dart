import 'package:genie/src/models/menu.model.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/services/graphql.dart';

class MenuRepository extends GraphQL<Menu> {
  @override
  Menu fromJson(Map<String, dynamic> json) {
    return Menu.fromJson(json);
  }

  @override
  String get modelName => 'Menu';

  String fragment = '''
        query{
          getActivatedMenu{
              id
              name
              activated
              sections{
                title
                subMenus{
                  title
                  icon
                  action{
                    type
                    screenId
                    categoryId
                    productId
                    link
                  }
              }
            }
          }
        }
  ''';

  Future getAllMenu() async {
    return await AppClient.instance.execute(fragment).then((value) {
      if (value == null) {
        return null;
      }
      return value.data['getActivatedMenu'];
    }).catchError((onError) {
      return null;
    });
  }
}
