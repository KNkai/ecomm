import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/notifycation.model.dart';
import 'package:genie/src/services/graphql.dart';
import 'package:genie/src/utils/util.dart';

class NotificationRepository extends GraphQL<NotifcationModel>
    with LoadMoreProtocol, InitListProtocol {
  final _query = '''
            id
            title
            body
            type
            seen
            link
            orderId
            createdAt
            image
            type
            link
            screen{
              id
              name
            }
            category{
              id
              name
            }
            campaign {
                image,
                name,
                type
                id
                endAt
                code
                details {
                  title
                  content
                }
            },
            productId
            categoryId
            campaignId
  ''';

  @override
  Future<ResultInitList> init({parameter}) {
    return getAll(
        query: _query,
        queryInput: GraphQLQueryInput(
          limit: 10,
          page: 1,
          order: {"_id": -1},
          filter: {
            "${parameter[0]}": parameter.getRange(1, parameter.length).toList()
          },
        )).then((response) {
      if (response?.pagination?.total == null) return ResultInitListFailed();

      return ResultInitListSuccess<NotifcationModel>(
          response.pagination.total, response?.data);
    }).catchError((_) => ResultInitListFailed());
  }

  @override
  Future<ResultLoadMoreSuccess> loadMore({int page, parameter}) {
    return getAll(
        query: _query,
        queryInput: GraphQLQueryInput(
          page: page,
          order: {"_id": -1},
          filter: {
            "${parameter[0]}": parameter.getRange(1, parameter.length).toList()
          },
        )).then((value) {
      if (!appCheckListEmpty(value?.data))
        return ResultLoadMoreSuccess<NotifcationModel>(value.data);

      return null;
    });
  }

  @override
  NotifcationModel fromJson(Map<String, dynamic> json) {
    return NotifcationModel.fromJson(json);
  }

  @override
  String get modelName => 'Notification';
}
