import 'package:genie/src/models/screen.model.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/services/graphql.dart';

class ScreenRepository extends GraphQL<Screen> {
  String screenFragment = '''
  id 
  name 
  showSuggestProduct 
  isHome 
  isPromotion
  popup { 
    image 
    enabled
    action { 
      type 
      screenId 
      categoryId 
      productId 
      link 
      label 
      screen{
        name
      }
      product{
        id
        name
      }
      campaign{
        name
      }
      category{
        name
      } 
    }
  }
  ''';
  @override
  Screen fromJson(Map<String, dynamic> json) => Screen.fromJson(json);

  @override
  String get modelName => 'Screen';

  Future<Screen> getHomeScreen() {
    String apiName = 'getHomeScreen';
    return AppClient.instance.execute('''
      query {
        $apiName { $screenFragment }
      }
    ''').then((res) {
      return Screen.fromJson(res.data[apiName]);
    }).catchError((_) => null);
  }

  Future<Screen> getPromotionScreen() {
    String apiName = 'getPromotionScreen';
    return AppClient.instance.execute('''
      query {
        $apiName { $screenFragment }
      }
    ''').then((res) {
      return Screen.fromJson(res.data[apiName]);
    }).catchError((_) => null);
  }

  Future getCampaign(String idCampaign) {
    String apiName = 'takeEvoucher';
    return AppClient.instance.execute('''
    mutation{
        $apiName(campaignId:"$idCampaign"){
          campaign{
            name
            taken
          }
      }
    }
    ''');
  }

  Future<List<Screen>> getAllScreen() async {
    const _screenQuery = """
    {
      getAllScreen(q:{limit:10000}){
        data{
          id
          name
          code
          type
          isHome
          isPromotion
          popup{
            image
          }
          showSuggestProduct
        }
      }
    }
    """;

    return await AppClient.instance.execute(_screenQuery).then((value) {
      if (value.data != null)
        return List<Screen>.from(value.data['getAllScreen']['data']
            .map((e) => Screen.fromJson(e))).toList();
      return null;
    });
  }
}
