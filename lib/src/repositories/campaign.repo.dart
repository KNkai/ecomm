import 'package:genie/src/models/campaign.model.dart';
import 'package:genie/src/services/app_client.dart';

class CampaignRepository {
  String fragment = """
            {
        getAllCampaign(q:{filter:{popularCampaign:"true"}}){
          data{
            id
            name
            code
          }
        }
      }
  """;

  Future<List<Campaign>> getCampaignPopular() async {
    return await AppClient.instance.execute(fragment).then((value) {
      if (value.data == null) {
        return null;
      }
      List<Campaign> popularCampagin = List<Campaign>.from(value
          .data['getAllCampaign']['data']
          .map((e) => Campaign.fromJson(e))).toList();
      return popularCampagin;
    }).catchError((onError) => print(onError));
  }
}
