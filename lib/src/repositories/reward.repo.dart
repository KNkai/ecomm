import 'package:genie/src/models/reward.model.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/services/graphql.dart';

class RewardRepository extends GraphQL {
  @override
  Reward fromJson(Map<String, dynamic> json) {
    return Reward.fromJson(json);
  }

  @override
  String get modelName => 'Reward';

  final String fragment = '''{
        getAllReward{
          data{
            id
            name
            point  
            image
            content
            issueNumber
            qtyExchanged
            type
            isActivated
            startAt
            endAt
            condition
            campaign {
              image,
              name,
              type
              id
              endAt
              code
              details {
                title
                content
              }
            },
          }
        }
      }
  ''';
  Future<List<Reward>> getAllReward() async {
    List<Reward> listReward = [];
    return await AppClient.instance.execute(fragment).then((value) {
      value.data['getAllReward']['data'].forEach((e) {
        listReward.add(Reward.fromJson(e));
      });
      return listReward;
    });
  }

  Future getReward(String idReward) async {
    await AppClient.instance.execute('''mutation{
      exchangeReward(rewardId:"${idReward}"){
        id
      }
    }''');
  }
}
