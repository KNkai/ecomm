import 'package:genie/src/services/app_client.dart';

class ProfileRepository {
  final String fragment = """
      {
      getOneSettingByKey(key:"APP_CHAT_LINK"){
          id
          name
          value
      }
    }
  """;

  Future getOneSettingByKey(String key) async {
    return await AppClient.instance.execute(
      """
          {
          getOneSettingByKey(key:"$key"){
              id
              name
              value
            }
          }
  """,
    ).then((value) {
      return value.data['getOneSettingByKey'];
    });
  }

  Future upLoadAvatar(String imgUrl) async {
    return await AppClient.instance.execute('''
                  mutation {
          customerUpdateMe(data:{
            avatar:"$imgUrl"
          }){
            avatar
          }
        }
    ''').catchError((onError) {
      print(onError);
    });
  }

  Future updateMeCustomer(String key, String value) async {
    String fragment = '''
    mutation Update{
          customerUpdateMe(data:{
            $key:"${value}",
          }){
    $key
          }
        }
    ''';
    // print(fragment);
    await AppClient.instance.execute(fragment).then((value) {
      return value.data;
    }).catchError((onError) {
      return null;
    });
  }
}
