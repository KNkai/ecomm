import 'package:genie/src/models/province_district_ward.model.dart';
import 'package:genie/src/services/app_client.dart';

class ProvinceDistrictWardRepository {
  Future getAllProvince() {
    return AppClient.instance.execute('''
query {
  getProvince {
    id
    province
  }
}
    ''').then((province) {
      if (province?.data == null ||
          (province.data['getProvince'] as List).isEmpty) return null;

      return (province.data['getProvince'] as List)
          .map((e) => Province.fromJson(e))
          .toList();
    }).catchError((_) => null);
  }

  Future getAllDistrict(String provinceId) {
    return AppClient.instance.execute('''
query {
  getDistrict(provinceId: "$provinceId") {
    id
    district
  }
}
    ''').then((district) {
      if (district?.data == null ||
          (district.data['getDistrict'] as List).isEmpty) return null;

      return (district.data['getDistrict'] as List)
          .map((e) => District.fromJson(e))
          .toList();
    }).catchError((_) => null);
  }

  Future getAllWard(String districtId) {
    return AppClient.instance.execute('''
query {
  getWard(districtId: "$districtId") {
    id
    ward
  }
}
    ''').then((ward) {
      if (ward?.data == null || (ward.data['getWard'] as List).isEmpty)
        return null;

      return (ward.data['getWard'] as List)
          .map((e) => Ward.fromJson(e))
          .toList();
    }).catchError((_) => null);
  }
}
