import 'package:flutter/cupertino.dart';
import 'package:genie/src/models/evoucher.model.dart';
import 'package:genie/src/models/evoucher_group.model.dart';
import 'package:genie/src/pages/payment/components/voucher_confirm/voucher_confirm_controller.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/services/graphql.dart';
import 'package:genie/src/utils/app_key.dart';
import 'package:shared_preferences/shared_preferences.dart';

const _kVoucherQuery = '''
            id
            campaign {
                image,
                name,
                type
                id
                endAt
                code
                
                details {
                  title
                  content
                }
            },
            effectiveAt
            expiredAt
            status
            almostExpired
    ''';

class EvoucherRepository extends GraphQL<Evoucher> {
  @override
  Evoucher fromJson(Map<String, dynamic> json) {
    return Evoucher.fromJson(json);
  }

  @override
  String get modelName => 'EVoucher';

  Future getAllEvoucher() {
    return SharedPreferences.getInstance().then((instance) {
      final customerId = instance.getString(AppKey.customerId);
      return AppClient.instance.execute('''
            query {
          getAllEVoucherByGroup(customerId: "$customerId") {
            title
            evouchers {
                $_kVoucherQuery
            }
          }
        }
    ''').then((response) {
        if (response?.data == null ||
            response.data['getAllEVoucherByGroup'] == null ||
            (response.data['getAllEVoucherByGroup'] as List).isEmpty)
          return null;

        final List<EvoucherModel> list = [];

        (response.data['getAllEVoucherByGroup'] as List).forEach((element) {
          String title = element['title'];
          list.add(EvoucherModel.title(title: title));
          (element['evouchers'] as List).forEach((voucher) {
            list.add(EvoucherModel.item(
                item: Evoucher.fromJson(voucher)..groupTitle = title));
          });
        });

        // final List<GroupEvoucherModel> listEvoucher = [];
        // (response.data['getAllEVoucherByGroup'] as List).forEach((element) {
        //   listEvoucher.add(GroupEvoucherModel.fromJson(element));
        // });

        // return listEvoucher;
        return list;
      });
    }).catchError((_) {
      return null;
    });
  }

  Future<List<GroupEvoucher>> getAllEvoucherByGroup() {
    return SharedPreferences.getInstance().then((instance) {
      final customerId = instance.getString(AppKey.customerId);
      return AppClient.instance.execute('''
            query {
          getAllEVoucherByGroup(customerId: "$customerId") {
            title
            evouchers {
                $_kVoucherQuery
            }
          }
        }
    ''').then((response) {
        if (response?.data == null ||
            response.data['getAllEVoucherByGroup'] == null ||
            (response.data['getAllEVoucherByGroup'] as List).isEmpty)
          return null;

        List<GroupEvoucher> list = [];

        (response.data['getAllEVoucherByGroup'] as List).forEach((element) {
          list.add(GroupEvoucher.fromJson(element));
        });
        return list;
      });
    }).catchError((_) {
      return null;
    });
  }

  Future takeEVoucherByCode(String code) {
    return AppClient.instance
        .executeWithError(
            'mutation { takeEvoucherByCode(code: "$code") { $_kVoucherQuery }}')
        .then((response) {
      if (response?.errors != null && response.errors[0] != null) {
        return response.errors[0].message;
      }

      return Evoucher.fromJson(response.data['takeEvoucherByCode']);
    }).catchError((_) {
      return null;
    });
  }
}

Future<List<GroupEvoucher>> get() {
  return SharedPreferences.getInstance().then((instance) {
    final customerId = instance.getString(AppKey.customerId);
    return AppClient.instance.execute('''
            query {
          getAllEVoucherByGroup(customerId: "$customerId") {
            title
            evouchers {
                $_kVoucherQuery
            }
          }
        }
    ''').then((response) {
      if (response?.data == null ||
          response.data['getAllEVoucherByGroup'] == null ||
          (response.data['getAllEVoucherByGroup'] as List).isEmpty)
        return null;

      List<GroupEvoucher> list = [];

      (response.data['getAllEVoucherByGroup'] as List).forEach((element) {
        list.add(GroupEvoucher.fromJson(element));
      });
      return list;
    });
  }).catchError((_) {
    return null;
  });
}

// class GroupEvoucherModel {
//   final String title;
//   final List<Evoucher> listEvoucher;

//   GroupEvoucherModel({this.title, this.listEvoucher});

//   factory GroupEvoucherModel.fromJson(Map<String, dynamic> json) {
//     return GroupEvoucherModel(
//       title: json["title"] == null ? null : json["title"],
//       listEvoucher: json["evouchers"] == null
//           ? null
//           : List<Evoucher>.from(
//               json["evouchers"].map((e) => Evoucher.fromJson(e))),
//     );
//   }
// }

class EvoucherModel {
  final EvoucherModelType type;
  final dynamic data;
  bool select;
  EvoucherModel._(
      {@required this.type, @required this.data, this.select = false});

  factory EvoucherModel.title({@required String title}) {
    return EvoucherModel._(type: EvoucherModelType.title, data: title);
  }

  factory EvoucherModel.item({@required Evoucher item}) {
    return EvoucherModel._(type: EvoucherModelType.item, data: item);
  }
}
