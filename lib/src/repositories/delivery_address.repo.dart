import 'package:flutter/cupertino.dart';
import 'package:genie/src/models/delivery_address.model.dart';
import 'package:genie/src/services/graphql.dart';

class DeliveryAddressRepository extends GraphQL<DeliveryAddress> {
  @override
  DeliveryAddress fromJson(Map<String, dynamic> json) {
    return DeliveryAddress.fromJson(json);
  }

  @override
  String get modelName => 'DeliveryAddress';

// null
// DeliveryAdress
// EmptyDeliveryAddress
  Future getDefaultDeliveryAddress() {
    return getAll(query: '''
          id
          isDefault,
          name,
          address
          province
          district
          ward
          provinceId
          districtId
          wardId
          phone
    ''', queryInput: GraphQLQueryInput(filter: {'isDefault': true}))
        .then((response) {
      if (response == null || response.data == null) return null;

      if (response.data.isEmpty) return EmptyDeliveryAddress();

      return response.data[0];
    }).catchError((_) => null);
  }

// null
// List<DeliveryAddress>
  Future getAllDeliveryAddress() {
    return getAll(query: '''
          id
          isDefault,
          name,
          address,
          province,
          district,
          ward
          phone
    ''', queryInput: GraphQLQueryInput(limit: 100)).then((response) {
      if (response == null || response.data == null) return null;

      return response.data;
    });
  }

  Future<bool> createDeliveryAddress(
      {@required String name,
      @required String phone,
      @required String address,
      @required String provinceId,
      @required String districtId,
      @required String wardId,
      @required bool isDefault}) {
    return create(query: 'province', variables: {
      'name': name,
      'phone': phone,
      'address': address,
      'provinceId': provinceId,
      'districtId': districtId,
      'wardId': wardId,
      'isDefault': isDefault
    }).then((value) {
      return (value?.province != null);
    }).catchError((_) => false);
  }
}

class EmptyDeliveryAddress {}
