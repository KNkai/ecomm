import 'package:flutter/material.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/services/graphql.dart';
import 'package:genie/src/utils/util.dart';

class ProductRepository extends GraphQL<Product> {
  @override
  Product fromJson(Map<String, dynamic> json) => Product.fromJson(json);

  @override
  String get modelName => 'Product';

  Future getProductsCart({@required List<String> ids}) {
    return getAll(
        query: 'id name hasSale saleValue saleType basePrice listImage',
        queryInput: GraphQLQueryInput(filter: {"_id": ids})).then((value) {
      if (value == null) return null;

      return value.data;
    }).catchError((_) => null);
  }

  Future getCampaigns({@required String productId}) {
    return getOne(
            id: productId,
            query: '''
           campaigns {
                image
                name
                taken
                endAt
                createdAt
                id
            }
    ''',
            queryInput: GraphQLQueryInput(limit: 1000))
        .then((value) {
      if (value == null) return null;

      return value.campaigns..sort((a, b) => a.taken ? 1 : -1);
    }).catchError((_) => null);
  }

  // Proudct
  // null
  Future getOneProductDetail({@required String productId}) {
    return getOne(id: productId, query: '''
           campaigns {
                id
                code
            }
            id
            name
            productMasterId
            basePrice
            allowSale
            orderQty
            description
            listImage
            saleQty
            remaining
            rating
            hasSale
            saleValue
            saleType
            youtubeLink
            sharedProducts {
              id
              name
              listImage
              basePrice
            }
            productMaster {
              sharedProducts {
                id
                name
                listImage
                basePrice 
                cost
                product{
                  id
                  name
                  productMasterId
                  basePrice
                  allowSale
                  orderQty
                  description
                  listImage
                  saleQty
                  rating
                  hasSale
                  saleValue
                  saleType
                  youtubeLink
                }
              }
              name
              id
              faq {
                question
                answer
              }
              categories {
                name
              }
              attributes {
                attribute {
                  id
                  name
                }
                values {
                  value
                  productIds
                }
                attributeId
              }
                
            }  
            blockItem { image }  
    ''').then((value) {
      if (value == null) return null;

      return value;
    }).catchError((_) => null);
  }
}

class ProductStackRepository extends ProductRepository
    with InitListProtocol, LoadMoreProtocol {
  num _limit = 10;
  String _query = '''
            id
            blockItem {
              title
              image
              product {
                basePrice 
                hasSale 
                rating
                saleValue 
                saleQty
                saleType 
                countdownDate
                remaining
              }
              action {
                type
                screenId
                categoryId
                productId
                link
              }
            }
    ''';
  @override
  Future<ResultInitList> init({dynamic parameter}) {
    return getAll(
        query: _query,
        queryInput: GraphQLQueryInput(
          limit: _limit,
          order: {"_id": -1},
        )).then((value) {
      if (appCheckListEmpty(value?.data) || value?.pagination?.total == null)
        return null;

      return ResultInitListSuccess<Product>(
          value.pagination.total, value?.data);
    }).catchError((_) => null);
  }

  @override
  Future<ResultLoadMoreSuccess> loadMore({int page, dynamic parameter}) {
    return getAll(
        query: _query,
        queryInput: GraphQLQueryInput(
          limit: _limit,
          page: page,
          order: {"_id": -1},
        )).then((value) {
      if (!appCheckListEmpty(value?.data))
        return ResultLoadMoreSuccess<Product>(value.data);

      return null;
    });
  }
}

class ProductGridPageRepository extends ProductRepository
    with InitListProtocol, LoadMoreProtocol {
  final _query = '''
         id
            blockItem {
              title
              image
              product {
                countdownDate
                basePrice 
                hasSale 
                saleValue 
                saleType 
                saleQty
                rating
                remaining
              }
              action {
                type
                screenId
                categoryId
                productId
                link
              }
            }''';

  @override
  Future<ResultInitList> init({dynamic parameter}) {
    return getAll(
        query: _query,
        queryInput: GraphQLQueryInput(
            limit: 6,
            page: 1,
            order: {"updatedAt": -1},
            filter: {"categoryIds": parameter})).then((value) {
      if (appCheckListEmpty(value?.data) || value?.pagination?.total == null)
        return null;

      return ResultInitListSuccess<Product>(
          value.pagination.total, value?.data);
    }).catchError((_) => null);
  }

  @override
  Future<ResultLoadMoreSuccess> loadMore({int page, dynamic parameter}) {
    return getAll(
        query: _query,
        queryInput: GraphQLQueryInput(
            limit: 6,
            page: page,
            filter: {"categoryIds": parameter})).then((value) {
      if (!appCheckListEmpty(value?.data))
        return ResultLoadMoreSuccess<Product>(value.data);

      return null;
    });
  }
}

class ProductGridPageRepositoryGetAll extends ProductRepository
    with InitListProtocol, LoadMoreProtocol {
  final _query = '''
         id
            blockItem {
              title
              image
              product {
                basePrice 
                hasSale 
                saleValue 
                saleType
                rating 
                saleQty
                countdownDate
                remaining
              }
              action {
                type
                screenId
                categoryId
                productId
                link
              }
            }''';

  @override
  Future<ResultInitList> init({dynamic parameter}) {
    return getAll(
      query: _query,
      queryInput: parameter == null || parameter == ""
          ? GraphQLQueryInput(
              limit: 9,
              page: 1,
              order: {"updatedAt": -1},
            )
          : GraphQLQueryInput(
              limit: 9,
              page: 1,
              order: {"updatedAt": -1},
              filter: {"categoryIds": parameter}),
    ).then((value) {
      if (appCheckListEmpty(value?.data) || value?.pagination?.total == null)
        return null;

      return ResultInitListSuccess<Product>(
          value.pagination.total, value?.data);
    }).catchError((_) => null);
  }

  @override
  Future<ResultLoadMoreSuccess> loadMore({int page, dynamic parameter}) {
    return getAll(
            query: _query,
            queryInput: parameter == null || parameter == ""
                ? GraphQLQueryInput(
                    limit: 9,
                    page: page,
                    order: {"updatedAt": -1},
                  )
                : GraphQLQueryInput(
                    limit: 9,
                    page: page,
                    filter: {"categoryIds": parameter},
                    order: {"updatedAt": -1},
                  ))
        .then((value) {
      if (!appCheckListEmpty(value?.data))
        return ResultLoadMoreSuccess<Product>(value.data);

      return null;
    });
  }
}

class ProductGridPageRepositorySearch extends ProductRepository
    with InitListProtocol, LoadMoreProtocol {
  final _query = '''
         id
            blockItem {
              title
              image
              product {
                basePrice 
                hasSale 
                saleValue
                rating
                saleType 
                countdownDate
                saleQty
                remaining
              }
              action {
                type
                screenId
                categoryId
                productId
                link
              }
            }''';

  @override
  Future<ResultInitList> init({dynamic parameter}) {
    return getAll(
      query: _query,
      queryInput: parameter == null || parameter == ""
          ? GraphQLQueryInput(
              limit: 10,
              page: 1,
              order: {"updatedAt": -1},
            )
          : GraphQLQueryInput(
              limit: 10,
              page: 1,
              order: {"updatedAt": -1},
              search: parameter,
            ),
    ).then((value) {
      if (appCheckListEmpty(value?.data) || value?.pagination?.total == null)
        return null;

      return ResultInitListSuccess<Product>(
          value.pagination.total, value?.data);
    }).catchError((_) => null);
  }

  @override
  Future<ResultLoadMoreSuccess> loadMore({int page, dynamic parameter}) {
    return getAll(
            query: _query,
            queryInput: parameter == null || parameter == ""
                ? GraphQLQueryInput(
                    limit: 10,
                    page: page,
                  )
                : GraphQLQueryInput(
                    limit: 10,
                    page: page,
                    search: parameter,
                  ))
        .then((value) {
      if (!appCheckListEmpty(value?.data)) {
        return ResultLoadMoreSuccess<Product>(value.data);
      }

      return null;
    });
  }
}
