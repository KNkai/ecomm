import 'package:genie/src/models/delivery_method.model.dart';
import 'package:genie/src/pages/payment/payment_controller.dart';
import 'package:genie/src/services/app_client.dart';

class DeliveryMethodRepository {
  Future getAllDeliveryMethod() {
    return AppClient.instance.execute('''
    query {
      getAllDeliveryMethod {
        value label
      }
    }
    ''').then((value) {
      if (value?.data == null || value.data['getAllDeliveryMethod'] == null)
        return null;

      return (value.data['getAllDeliveryMethod'] as List)
          .map((e) => ModelShipMethod(method: DeliveryMethod.fromJson(e)))
          .toList();
    }).catchError((_) {
      return null;
    });
  }
}
