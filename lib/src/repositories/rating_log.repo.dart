import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/create_rating_log_input.dart';
import 'package:genie/src/models/rating_log.model.dart';
import 'package:genie/src/services/graphql.dart';

class RatingLogRepository extends GraphQL<RatingLog>
    with LoadMoreProtocol, InitListProtocol {
  @override
  Future<ResultInitList> init({dynamic parameter}) {
    return getAll(query: '''
id
customer {
  id
  name
  avatar
}
message
rating
replies {
  message
  user {
    name
    role
    avatar
  }
}
updatedAt
    ''', queryInput: GraphQLQueryInput(filter: {'productMasterId': parameter}))
        .then((value) {
      if (value?.data == null ||
          value.data.isEmpty ||
          value?.pagination?.total == null) return null;

      return ResultInitListSuccess<RatingLog>(
          value.pagination.total, value.data);
    }).catchError((_) => null);
  }

  @override
  Future<ResultLoadMoreSuccess> loadMore({int page, dynamic parameter}) {
    return getAll(query: '''
id
customer {
  id
  name
  avatar
}
message
rating
replies {
  message
  user {
    name
    role
    avatar
  }
}
updatedAt
    ''', queryInput: GraphQLQueryInput(filter: {'productMasterId': parameter}))
        .then((value) {
      if (value?.data == null ||
          value.data.isEmpty ||
          value?.pagination?.total == null) return null;

      return ResultLoadMoreSuccess<RatingLog>(value.data);
    }).catchError((_) => null);
  }

  void writeRating(
      String msg, int rating, String productId, String customerId) {
    create(
        query: 'id',
        variables: CreateRatingLogInput(
                message: msg,
                rating: rating.toDouble(),
                productId: productId,
                customerId: customerId)
            .toJson());
  }

  @override
  RatingLog fromJson(Map<String, dynamic> json) {
    return RatingLog.fromJson(json);
  }

  @override
  String get modelName => 'RatingLog';
}
