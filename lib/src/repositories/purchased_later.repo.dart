import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/services/graphql.dart';
import 'package:genie/src/utils/util.dart';

class PurchasedLaterRepository extends GraphQL<PurchasedLaterProduct> {
  addPurchasedLaterProduct(String idProduct, String nameProduct) async {
    String fragment = '''
        mutation{
  createPurchasedLaterProduct(data:{productId:"${idProduct}",productName:"${nameProduct}"}){
    product{
      id
      name
    }
  }
}
  ''';

    return AppClient.instance
        .execute(fragment)
        .then((value) {})
        .catchError((onError) {});
  }

  deleteOnePurchasedLaterProd(String id) {
    deleteOne(id: id);
  }

  @override
  PurchasedLaterProduct fromJson(Map<String, dynamic> json) =>
      PurchasedLaterProduct.fromJson(json);

  @override
  String get modelName => 'PurchasedLaterProduct';
}

class PurchasedLaterStackRepository extends PurchasedLaterRepository
    with InitListProtocol, LoadMoreProtocol {
  num _limit = 10;
  String _query = '''
    id
    product{
      id
      blockItem {
        title
        image
        product {
          basePrice 
          hasSale 
          saleValue 
          saleType 
          saleQty
        }
        action {
          type
          screenId
          categoryId
          productId
          link
        }
      }
    }
  ''';

  @override
  Future<ResultInitList> init({dynamic parameter}) {
    return getAll(
        query: _query,
        queryInput: GraphQLQueryInput(
          limit: 6,
          page: 1,
          order: {"updatedAt": -1},
        )).then((value) {
      if (appCheckListEmpty(value?.data) || value?.pagination?.total == null) {
        return null;
      }

      return ResultInitListSuccess<PurchasedLaterProduct>(
          value.pagination.total, value?.data);
    }).catchError((_) => print("ERRPR ${_.toString()}"));
  }

  @override
  Future<ResultLoadMoreSuccess> loadMore({int page, dynamic parameter}) {
    return getAll(
        query: _query,
        queryInput: GraphQLQueryInput(
          limit: _limit,
          page: page,
          order: {"_id": -1},
        )).then((value) {
      if (!appCheckListEmpty(value?.data))
        return ResultLoadMoreSuccess<PurchasedLaterProduct>(value.data);

      return null;
    });
  }
}
