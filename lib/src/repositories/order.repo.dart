import 'dart:async';

import '../components/init_loadmore_search/init_loadmore_search_controller.dart';
import '../models/create_order_input.model.dart';
import '../models/order.model.dart';
import '../models/order_status.model.dart';
import '../services/app_client.dart';
import '../services/graphql.dart';
import '../utils/util.dart';

class OrderRepository extends GraphQL<Order> {
  String order_query = """
            id
            subtotal
            customerName
            customerPhone
            address
            province
            provinceId
            status
            district
            districtId
            ward
            wardId
            shipMethod
            shipMethodText
            rateAt
            rate
            rateComment
            paymentInfo{
              method
              appScheme
              merchantcode
              amount
              orderId
              orderLabel
              merchantnamelabel
              fee
              description
              username
              partner
              extra
              isTestMode
              status
            }
            code
            paymentMethod
            amount
            rewardPoint
            cumulativePoint
            shipfee
            discountLogs {
                type
                value
            }
            items {
                quantity
                hasSale
                saleValue
                basePrice
                saleType
                isOffer
                product {
                  id
                  allowSale
                  listImage
                  name
            }
          }
  """;
//   Future generateDraftOrder(CreateOrderInput input) {
//     return AppClient.instance.execute('''
// mutation G(\$d: CreateOrderInput!) {
//     generateDraftOrder(data: \$d) {
//         invalid
//         invalidReason
//         invalidEvoucher{id}
//         invalidCampaign{id}
//         order {
//             subtotal
//             customerName
//             customerPhone
//             address
//             province
//             provinceId
//             status
//             district
//             districtId
//             ward
//             wardId
//             shipMethod
//             shipMethodText
//             code
//             paymentMethod
//             amount
//             rewardPoint
//             cumulativePoint
//             shipfee
//             discountLogs {
//                 type
//                 value
//             }
//             items {
//                 quantity
//                 hasSale
//                 saleValue
//                 basePrice
//                 saleType
//                 isOffer
//                 product {
//                   id
//                   listImage
//                   name
//                   allowSale
//             }
//         }
//       }
//     }
// }
//     ''', variables: {'d': input}).then((response) {
//       if (response?.data == null ||
//           response.data['generateDraftOrder'] == null ||
//           response.data['generateDraftOrder']['order'] == null) return null;

//       final order = response.data['generateDraftOrder'];

//       if (order['invalid'] != null && order['invalid'] == true) {
//         List<String> msg = [
//           order['invalidReason'] as String ?? 'Có lỗi xảy ra',
//           (order['invalidEvoucher'] != null || order['invalidCampaign'] != null
//               ? "voucher"
//               : ""),
//         ];
//         return msg;
//         // return order['invalidReason'] as String ?? 'Có lỗi xảy ra';
//       }

//       return Order.fromJson(response.data['generateDraftOrder']['order']);
//     }).catchError((e) {
//       return print(e.toString());
//     });
//   }

  Future generateDraftOrder(CreateOrderInput input) {
    return AppClient.instance.execute('''
mutation G(\$d: CreateOrderInput!) {
    generateDraftOrder(data: \$d) {
        invalid
        invalidReason
        invalidEvoucher{id}
        invalidCampaign{id}
        order {
            $order_query
      }
    }
}
    ''', variables: {'d': input}).then((response) {
      if (response?.data == null ||
          response.data['generateDraftOrder'] == null ||
          response.data['generateDraftOrder']['order'] == null) return null;

      final order = response.data['generateDraftOrder'];

      if (order['invalid'] != null && order['invalid'] == true) {
        List<String> msg = [
          order['invalidReason'] as String ?? 'Có lỗi xảy ra',
          (order['invalidEvoucher'] != null || order['invalidCampaign'] != null
              ? "voucher"
              : ""),
        ];
        return msg;
        // return order['invalidReason'] as String ?? 'Có lỗi xảy ra';
      }

      return Order.fromJson(response.data['generateDraftOrder']['order']);
    }).catchError((e) {
      return print(e.toString());
    });
  }

  Future cancelOrder(String id) {
    final _query = '''
      mutation{
        cancelOrder(orderId:"$id"){
          id
        }
      }
    ''';
    return AppClient.instance.execute(_query);
  }

  Future createOrder(CreateOrderInput input) {
    return create(query: '''
id
paymentInfo{
              method
              appScheme
              merchantcode
              amount
              orderId
              orderLabel
              merchantnamelabel
              fee
              description
              username
              partner
              extra
              isTestMode
              status
            }
    ''', variables: input.toJson()).then((response) {
      if (response == null) return null;

      return response;
    }).catchError((e) {
      return null;
    });
  }

  Future getOrderDetail(String id) {
    return getOne(query: '''
    id
    subtotal
    customerName
    customerPhone
    address
    province
    provinceId
    status
    district
    districtId
    ward
    wardId
    shipMethod
    shipMethodText
    paymentStatus
    rewardPoint
    cumulativePoint
    code
    rate
    rateComment
    rateAt
    paymentMethod
    paymentInfo{
      method
      appScheme
      merchantcode
      amount
      orderId
      orderLabel
      merchantnamelabel
      fee
      description
      username
      partner
      extra
      isTestMode
      status
    }
    amount
    shipfee
      discountLogs {
                type
                value
            }
    items {
      quantity
        hasSale
        saleValue
        basePrice
        saleType
        isOffer
      product {
        id
        allowSale
        listImage
        name
      }
    }
    ''', id: id).then((response) {
      if (response == null) return null;

      return response;
    }).catchError((_) {
      return null;
    });
  }

  Future rateOrder({String id, int rate, String rateComt}) async {
    final _query = '''
      mutation{
        rateOrder(orderId:"$id", rate:$rate, rateComment:"$rateComt"){
          rate
          rateComment
        }
      }
''';
    print(_query);
    return AppClient.instance.execute(_query).then((value) => value);
  }

  Future<Order> payMomo(String id, String appToken, String phone) {
    return AppClient.instance.execute('''
        mutation{
          payMomo(orderId:"""$id""", appToken:"""$appToken""", phone: """$phone"""){
            $order_query
          }
        }
      ''').then((value) {
      return value.data == null ? null : Order.fromJson(value.data['payMomo']);
    }).catchError((e) {
      print(e);
      return null;
    });
  }

  @override
  Order fromJson(Map<String, dynamic> json) {
    return Order.fromJson(json);
  }

  @override
  String get modelName => 'Order';
}

class OrderHistoryRepository extends OrderRepository
    with InitListProtocol, LoadMoreProtocol {
  @override
  Future<ResultInitList> init({parameter}) {
    print(parameter);
    var c = Completer<ResultInitList>();
    if (parameter is OrderStatus) {
      return getAll(
          query: '''
            id
            code
            itemCount
            amount
    ''',
          queryInput: GraphQLQueryInput(
              order: {"_id": -1},
              filter: {'status': parameter.value})).then((response) {
        print(response.data);
        if (response?.pagination?.total == null) return ResultInitListFailed();

        return ResultInitListSuccess<Order>(
            response.pagination.total, response?.data);
      }).catchError((_) {
        ResultInitListFailed();
      });
    } else {
      c.complete(ResultInitListFailed());
    }
    return c.future;
  }

  @override
  Future<ResultLoadMoreSuccess> loadMore({int page, parameter}) {
    var c = Completer<ResultLoadMoreSuccess>();
    if (parameter is OrderStatus) {
      return getAll(
          query: '''
          id
            code
            itemCount
            amount
    ''',
          queryInput: GraphQLQueryInput(
              page: page,
              order: {"_id": -1},
              filter: {'status': parameter.value})).then((value) {
        if (!appCheckListEmpty(value?.data))
          return ResultLoadMoreSuccess<Order>(value.data);

        return null;
      });
    } else
      c.complete(null);
    return c.future;
  }
}
