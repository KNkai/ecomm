import 'package:genie/src/models/evoucher.model.dart';

class GroupEvoucher {
  final String title;
  final List<Evoucher> listEvoucher;

  GroupEvoucher({this.title, this.listEvoucher});

  factory GroupEvoucher.fromJson(Map<String, dynamic> json) {
    return GroupEvoucher(
      title: json['title'] == null ? null : json['title'],
      listEvoucher: json['evouchers'] == null
          ? null
          : List<Evoucher>.from(
              json['evouchers'].map((e) => Evoucher.fromJson(e))).toList(),
    );
  }
}
