class KeyWordClass {
  String id;
  String keyWord;
  int count;
  KeyWordClass({this.keyWord, this.count, this.id});

  factory KeyWordClass.fromJson(Map<String, dynamic> json) => KeyWordClass(
        id: json["id"],
        keyWord: json["keyWord"] == null ? null : json["keyWord"],
        count: json["count"] == null ? null : json["count"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "keyWord": keyWord,
        "count": count,
      };
}
