import 'package:genie/src/models/payment_method.model.dart';
import 'package:genie/src/pages/cart/cart_controller.dart';

class CreateOrderInput {
  CreateOrderInput({
    this.customerName,
    this.customerPhone,
    this.shipMethod,
    this.address,
    this.provinceId,
    this.wardId,
    this.note,
    this.districtId,
    this.evoucherIds,
    this.paymentMethod,
    this.items,
  });

  bool isAnonymous = false;
  String customerName;
  String customerPhone;
  String shipMethod;
  String address;
  String provinceId;
  String wardId;
  String districtId;
  List<String> evoucherIds;
  PaymentMethodModel paymentMethod;
  String saleChannel = 'APP';
  List<ModelCart> items;
  String note;

  Map<String, dynamic> toJson() => {
        "isAnonymous": isAnonymous ?? false,
        "customerName": customerName,
        "customerPhone": customerPhone,
        "shipMethod": shipMethod,
        "address": address,
        "provinceId": provinceId,
        "wardId": wardId,
        "evoucherIds": evoucherIds,
        "districtId": districtId,
        "paymentMethod": paymentMethod.value,
        'note': note,
        "saleChannel": saleChannel,
        "items": items == null
            ? null
            : items
                .map((x) => {
                      "productId": x.productId,
                      "quantity": x.quantity,
                      "note": x.note
                    })
                .toList(),
      };
}
