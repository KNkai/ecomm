import 'package:genie/src/utils/util.dart';

class PaymentInfo {
  PaymentInfo({
    this.method,
    this.appScheme,
    this.merchantcode,
    this.amount,
    this.orderId,
    this.orderLabel,
    this.merchantnamelabel,
    this.fee,
    this.description,
    this.username,
    this.partner,
    this.extra,
    this.isTestMode,
    this.status,
  });

  String method;
  String appScheme;
  String merchantcode;
  double amount;
  String orderId;
  String orderLabel;
  String merchantnamelabel;
  double fee;
  String description;
  String username;
  String partner;
  String extra;
  bool isTestMode;
  String status;

  factory PaymentInfo.fromJson(Map<String, dynamic> json) => PaymentInfo(
        method: json["method"] == null ? null : json["method"],
        appScheme: json["appScheme"] == null ? null : json["appScheme"],
        merchantcode:
            json["merchantcode"] == null ? null : json["merchantcode"],
        amount: json["amount"] == null
            ? null
            : appConvertDoubleJson(json["amount"]),
        orderId: json["orderId"] == null ? null : json["orderId"],
        orderLabel: json["orderLabel"] == null ? null : json["orderLabel"],
        merchantnamelabel: json["merchantnamelabel"] == null
            ? null
            : json["merchantnamelabel"],
        fee: json["fee"] == null ? null : appConvertDoubleJson(json["fee"]),
        description: json["description"] == null ? null : json["description"],
        username: json["username"] == null ? null : json["username"],
        partner: json["partner"] == null ? null : json["partner"],
        extra: json["extra"] == null ? null : json["extra"],
        isTestMode: json["isTestMode"] == null ? null : json["isTestMode"],
        status: json["status"] == null ? null : json["status"],
      );
}
