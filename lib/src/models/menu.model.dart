import 'package:genie/src/models/block.model.dart';

class Menu {
  Menu({
    this.id,
    this.name,
    this.activated,
    this.sections,
  });

  String id;
  String name;
  bool activated;
  List<Section> sections;

  factory Menu.fromJson(Map<String, dynamic> json) => Menu(
        id: json["id"],
        name: json["name"],
        activated: json["activated"],
        sections: List<Section>.from(
            json["sections"].map((x) => Section.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "activated": activated,
        "sections": List<dynamic>.from(sections.map((x) => x.toJson())),
      };
}

class Section {
  Section({
    this.title,
    this.subMenus,
  });

  String title;
  List<SubMenu> subMenus;

  factory Section.fromJson(Map<String, dynamic> json) => Section(
        title: json["title"],
        subMenus: List<SubMenu>.from(
            json["subMenus"].map((x) => SubMenu.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "subMenus": List<dynamic>.from(subMenus.map((x) => x.toJson())),
      };
}

class SubMenu {
  SubMenu({
    this.title,
    this.icon,
    this.action,
  });

  String title;
  String icon;
  BlockItemAction action;

  factory SubMenu.fromJson(Map<String, dynamic> json) => SubMenu(
        title: json["title"],
        icon: json["icon"],
        action: BlockItemAction.fromJson(json["action"]),
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "icon": icon,
        "action": action.toJson(),
      };
}
