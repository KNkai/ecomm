import 'dart:convert';

import 'package:genie/src/models/block.model.dart';

class Screen {
  Screen({
    this.id,
    this.createdAt,
    this.updatedAt,
    this.code,
    this.name,
    this.popup,
    this.showSuggestProduct,
    this.type,
    this.isHome,
    this.isPromotion,
  });
  String id;
  DateTime createdAt;
  DateTime updatedAt;
  String code;
  String name;
  ScreenPopup popup;
  bool showSuggestProduct;
  String type;
  bool isHome;
  bool isPromotion;

  factory Screen.fromRawJson(String str) => Screen.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Screen.fromJson(Map<String, dynamic> json) => Screen(
        id: json["id"] == null ? null : json["id"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        code: json["code"] == null ? null : json["code"],
        name: json["name"] == null ? null : json["name"],
        popup:
            json["popup"] == null ? null : ScreenPopup.fromJson(json["popup"]),
        showSuggestProduct: json["showSuggestProduct"] == null
            ? null
            : json["showSuggestProduct"],
        isHome: json["isHome"] == null ? null : json["isHome"],
        isPromotion: json["isPromotion"] == null ? null : json["isPromotion"],
        type: json["type"] == null ? null : json["type"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "code": code == null ? null : code,
        "name": name == null ? null : name,
        "popup": popup == null ? null : popup.toJson(),
        "showSuggestProduct":
            showSuggestProduct == null ? null : showSuggestProduct,
        "type": type == null ? null : type,
      };
}

class ScreenPopup {
  ScreenPopup({
    this.enabled,
    this.image,
    this.action,
  });

  bool enabled;
  String image;
  BlockAction action;

  factory ScreenPopup.fromRawJson(String str) =>
      ScreenPopup.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ScreenPopup.fromJson(Map<String, dynamic> json) => ScreenPopup(
      enabled: json["enabled"] == null ? null : json["enabled"],
      image: json["image"] == null ? null : json["image"],
      action:
          json["action"] == null ? null : BlockAction.fromJson(json["action"]));

  Map<String, dynamic> toJson() => {
        "enabled": enabled == null ? null : enabled,
        "image": image == null ? null : image,
        "action": action == null ? null : action.toJson()
      };
}
