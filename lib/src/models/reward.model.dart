import 'package:flutter/material.dart';
import 'package:genie/src/utils/util.dart';

import 'campaign.model.dart';

class Reward {
  Reward({
    @required this.id,
    @required this.name,
    @required this.point,
    @required this.image,
    @required this.content,
    @required this.issueNumber,
    @required this.qtyExchanged,
    @required this.type,
    @required this.isActivated,
    @required this.startAt,
    @required this.endAt,
    @required this.condition,
    @required this.campaign,
  });

  String id;
  String name;
  int point;
  String image;
  String content;
  int issueNumber;
  int qtyExchanged;
  String type;
  bool isActivated;
  DateTime startAt;
  DateTime endAt;
  dynamic condition;
  Campaign campaign;

  factory Reward.fromJson(Map<String, dynamic> json) => Reward(
        id: json["id"],
        name: json["name"] == null ? null : json["name"],
        point: json["point"] == null ? null : appConvertIntJson(json['point']),
        image: json["image"] == null ? null : json["image"],
        content: json["content"] == null ? null : json["content"],
        issueNumber: json["issueNumber"] == null ? null : json["issueNumber"],
        qtyExchanged:
            json["qtyExchanged"] == null ? null : json["qtyExchanged"],
        type: json["type"] == null ? null : json["type"],
        isActivated: json["isActivated"] == null ? null : json["isActivated"],
        startAt:
            json["startAt"] == null ? null : DateTime.parse(json["startAt"]),
        endAt: json["endAt"] == null ? null : DateTime.parse(json["endAt"]),
        condition: json["condition"] == null ? null : json["condition"],
        campaign: json["campaign"] == null
            ? null
            : Campaign.fromJson(json["campaign"]),
      );
}
