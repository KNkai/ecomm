import 'dart:convert';

class Category {
  Category({
    this.id,
    this.priority,
    this.showOnShop,
    this.name,
  });
  final String id;
  final String name;
  final int priority;
  final bool showOnShop;

  factory Category.fromRawJson(String str) =>
      Category.fromJson(json.decode(str));

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        priority: json["priority"] == null ? null : json["priority"],
        showOnShop: json["showOnShop"] == null ? null : json["showOnShop"],
      );
}
