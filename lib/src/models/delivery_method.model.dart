import 'package:flutter/cupertino.dart';

class DeliveryMethod {
  final String value;
  final String label;

  DeliveryMethod({@required this.value, @required this.label});

  factory DeliveryMethod.fromJson(Map<String, dynamic> json) {
    return DeliveryMethod(value: json["value"], label: json['label']);
  }
}
