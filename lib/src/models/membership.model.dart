class MemberShipClass {
  MemberShipClass({
    this.id,
    this.name,
    this.code,
    this.color,
    this.requiredPoint,
    this.requiredOrder,
    this.requiredSales,
    this.privileges,
  });

  String id;
  String name;
  String code;
  String color;
  int requiredPoint;
  int requiredOrder;
  int requiredSales;
  String privileges;

  factory MemberShipClass.fromJson(Map<String, dynamic> json) =>
      MemberShipClass(
        id: json["id"],
        name: json["name"] == null ? null : json["name"],
        code: json["code"] == null ? null : json["code"],
        color: json["color"] == null ? null : json["color"],
        requiredPoint:
            json["requiredPoint"] == null ? null : json["requiredPoint"],
        requiredOrder:
            json["requiredOrder"] == null ? null : json["requiredOrder"],
        requiredSales:
            json["requiredSales"] == null ? null : json["requiredSales"],
        privileges: json["privileges"] == null ? null : json["privileges"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "code": code,
        "color": color,
        "requiredPoint": requiredPoint,
        "requiredOrder": requiredOrder,
        "requiredSales": requiredSales,
        "privileges": privileges,
      };
}
