import 'package:flutter/material.dart';

class CreateRatingLogInput {
  CreateRatingLogInput({
    @required this.message,
    @required this.productId,
    @required this.rating,
    @required this.customerId,
  });

  final String message;
  final String productId;
  final String customerId;
  final double rating;

  Map<String, dynamic> toJson() => {
        "message": message,
        "rating": rating,
        "productId": productId,
        "customerId": customerId
      };
}
