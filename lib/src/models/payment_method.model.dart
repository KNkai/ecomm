enum PaymentMethod { cod, bankTransfer }

const _kPaymentMethodCodValue = 'COD';
const _kPaymentMethodBankTransferValue = 'BANK_TRANSFER';

PaymentMethod convertPaymentMethod(String value) {
  if (value == null) return null;
  switch (value) {
    case _kPaymentMethodCodValue:
      return PaymentMethod.cod;
    case _kPaymentMethodBankTransferValue:
      return PaymentMethod.bankTransfer;
  }
  return null;
}

extension PaymentMethodExtendsion on PaymentMethod {
  String get value {
    switch (this) {
      case PaymentMethod.cod:
        return _kPaymentMethodCodValue;
      default:
        return _kPaymentMethodBankTransferValue;
    }
  }

  String get title {
    switch (this) {
      case PaymentMethod.cod:
        return 'COD';
      default:
        return 'Chuyển khoản';
    }
  }
}

class PaymentMethodModel {
  PaymentMethodModel({
    this.value,
    this.label,
  });

  final String value;
  final String label;

  factory PaymentMethodModel.fromJson(Map<String, dynamic> json) =>
      PaymentMethodModel(
        value: json["value"],
        label: json["label"],
      );

  Map<String, dynamic> toJson() => {
        "value": value,
        "label": label,
      };
}
