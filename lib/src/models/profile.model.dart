class Profile {
  Profile({
    this.id,
    this.name,
    this.value,
  });

  String id;
  String name;
  int value;

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        value: json["value"] == null ? null : json["value"],
      );
}
