class Campaign {
  Campaign({
    this.taken,
    this.code,
    this.endAt,
    this.startAt,
    this.id,
    this.image,
    this.name,
    // this.type,
    this.createdAt,
    this.detail,
    this.isPopularCampaign,
  });

  String image;
  String name;
  bool taken;
  DateTime endAt;
  DateTime startAt;
  DateTime createdAt;
  String id;
  String code;
  bool isPopularCampaign;
  // CampaignType type;
  List<DetailCampaign> detail;

  factory Campaign.fromJson(Map<String, dynamic> json) {
    return Campaign(
        id: json['id'],
        code: json['code'],
        taken: json['taken'],
        isPopularCampaign: json['popularCampaign'],
        endAt: json['endAt'] == null ? null : DateTime.parse(json['endAt']),
        createdAt: json['createdAt'] == null
            ? null
            : DateTime.parse(json['createdAt']),
        startAt:
            json['startAt'] == null ? null : DateTime.parse(json['startAt']),
        name: json["name"],
        image: json["image"],
        // type: _convertCampainType(json["type"]),
        detail: json['details'] == null
            ? null
            : List<DetailCampaign>.from(
                json["details"].map((x) => DetailCampaign.fromJson(x))));
  }
}

enum CampaignType { discountBill, discountItem, offerItem, shipFee, bonusPoint }

const _kDiscountBill = 'DISCOUNT_BILL';
const _kDiscountItem = 'DISCOUNT_ITEM';
const _kOfferItem = 'OFFER_ITEM';
const _kShipFee = 'SHIP_FEE';
const _kBonusPont = 'BONUS_POINT';

// CampaignType _convertCampainType(String value) {
//   if (value == null) return null;
//   switch (value) {
//     case _kDiscountBill:
//       return CampaignType.discountItem;
//     case _kDiscountItem:
//       return CampaignType.discountBill;
//     case _kOfferItem:
//       return CampaignType.offerItem;
//     case _kShipFee:
//       return CampaignType.shipFee;
//     case _kBonusPont:
//       return CampaignType.bonusPoint;
//   }
//   return null;
// }

extension CampainTypeExtension on CampaignType {
  String get value {
    switch (this) {
      case CampaignType.discountBill:
        return _kDiscountItem;
      case CampaignType.discountItem:
        return _kDiscountBill;
      case CampaignType.offerItem:
        return _kOfferItem;
      case CampaignType.shipFee:
        return _kShipFee;
      case CampaignType.bonusPoint:
        return _kBonusPont;
    }
    return '';
  }

  String get title {
    switch (this) {
      case CampaignType.offerItem:
        return 'Sản phẩm ưu đãi';
      case CampaignType.discountItem:
        return 'Sản phẩm giảm giá';
      case CampaignType.discountBill:
        return 'Giảm giá';
      case CampaignType.bonusPoint:
        return 'Cộng điểm';
      case CampaignType.shipFee:
        return 'Phí ship';
    }
    return '';
  }
}

class DetailCampaign {
  String title;
  String content;

  DetailCampaign({this.title, this.content});

  factory DetailCampaign.fromJson(Map<String, dynamic> json) {
    return DetailCampaign(
      title: json["title"] == null ? null : json["title"],
      content: json["content"] == null ? null : json["content"],
    );
  }
}
