class ValueType {
  ValueType({
    this.value,
    this.productIds,
  });

  final String value;
  final List<String> productIds;

  factory ValueType.fromJson(Map<String, dynamic> json) => ValueType(
        value: json["value"] == null ? null : json["value"],
        productIds: json["productIds"] == null
            ? null
            : List<String>.from(json["productIds"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "value": value == null ? null : value,
        "productIds": productIds == null
            ? null
            : List<dynamic>.from(productIds.map((x) => x)),
      };
}
