enum OrderStatus { pending, approve, canceled, delivering, returned }

const _kCanceled = 'CANCELED';
const _kApproved = 'APPROVED';
const _kPending = 'PENDING';
const _kDelivering = 'DELIVERING';
const _kReturned = 'RETURNED';

OrderStatus convertOrderStatus(String value) {
  if (value == null) return null;
  switch (value) {
    case _kPending:
      return OrderStatus.pending;
    case _kApproved:
      return OrderStatus.approve;
    case _kCanceled:
      return OrderStatus.canceled;
    case _kDelivering:
      return OrderStatus.delivering;
    case _kReturned:
      return OrderStatus.returned;
  }
  return null;
}

extension OrderStatusExtension on OrderStatus {
  String get value {
    switch (this) {
      case OrderStatus.canceled:
        return _kCanceled;
      case OrderStatus.approve:
        return _kApproved;
      case OrderStatus.pending:
        return _kPending;
      case OrderStatus.delivering:
        return _kDelivering;
      case OrderStatus.returned:
        return _kReturned;
    }
    return '';
  }

  String get title {
    switch (this) {
      case OrderStatus.canceled:
        return 'Đã huỷ';
      case OrderStatus.approve:
        return 'Đã giao';
      case OrderStatus.pending:
        return 'Chờ xử lý';
      case OrderStatus.delivering:
        return 'Đang giao';
      case OrderStatus.returned:
        return 'Đã trả';
    }
    return '';
  }
}
