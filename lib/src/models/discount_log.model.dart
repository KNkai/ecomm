import 'package:genie/src/utils/util.dart';

class DiscountLog {
  DiscountLog({this.id, this.type, this.value});

  final String id;
  final DiscountLogType type;
  final double value;

  factory DiscountLog.fromJson(Map<String, dynamic> json) {
    return DiscountLog(
        id: json["id"],
        type:
            json["type"] == null ? null : _convertDiscountLogType(json['type']),
        value: appConvertDoubleJson(json['value']));
  }
}

enum DiscountLogType { shipFee, subTotal, offerItem, bonusPoint }

const _kSubTotal = 'SUBTOTAL';
const _kOfferItem = 'OFFER_ITEM';
const _kShipFee = 'SHIP_FEE';
const _kBonusPont = 'BONUS_POINT';

DiscountLogType _convertDiscountLogType(String value) {
  if (value == null) return null;
  switch (value) {
    case _kSubTotal:
      return DiscountLogType.subTotal;
    case _kOfferItem:
      return DiscountLogType.offerItem;
    case _kShipFee:
      return DiscountLogType.shipFee;
    case _kBonusPont:
      return DiscountLogType.bonusPoint;
  }
  return null;
}

extension DiscountLogTypeExtension on DiscountLogType {
  String get value {
    switch (this) {
      case DiscountLogType.subTotal:
        return _kSubTotal;
      case DiscountLogType.offerItem:
        return _kOfferItem;
      case DiscountLogType.shipFee:
        return _kShipFee;
      case DiscountLogType.bonusPoint:
        return _kBonusPont;
    }
    return '';
  }

  String get title {
    switch (this) {
      case DiscountLogType.offerItem:
        return 'Sản phẩm tặng';
      case DiscountLogType.subTotal:
        return 'Giảm giá đơn hàng';
      case DiscountLogType.shipFee:
        return 'Giảm phí ship';
      case DiscountLogType.bonusPoint:
        return 'Cộng điểm';
    }
    return '';
  }
}
