import 'dart:convert';

import 'package:genie/src/models/block.model.dart';
import 'package:genie/src/models/campaign.model.dart';
import 'package:genie/src/models/category.model.dart';
import 'package:genie/src/models/faq_type.model.dart';
import 'package:genie/src/models/value_type.model.dart';
import 'package:genie/src/utils/util.dart';

import 'attribute.model.dart';

class ProductMaster {
  ProductMaster({
    this.barCode,
    this.basePrice,
    this.cost,
    this.code,
    this.description,
    this.id,
    this.listImage,
    this.maxPrice,
    this.minPrice,
    this.name,
    this.note,
    this.weight,
    this.categories,
    this.faq,
    this.sharedProducts,
    this.attributes,
    this.product,
    this.remaining,
  });

  String barCode;
  double basePrice;
  double cost;
  String code;
  String description;
  String id;
  List<String> listImage;
  double maxPrice;
  double minPrice;
  String name;
  String note;
  double weight;
  List<ProductMaster> sharedProducts;
  List<ProductAttribute> attributes;
  List<FaqType> faq;
  List<Category> categories;
  Product product;
  int remaining;

  factory ProductMaster.fromRawJson(String str) =>
      ProductMaster.fromJson(json.decode(str));

  factory ProductMaster.fromJson(Map<String, dynamic> json) => ProductMaster(
        barCode: json["barCode"],
        basePrice:
            json["basePrice"] == null ? null : json["basePrice"].toDouble(),
        cost: json["cost"] == null ? null : json["cost"].toDouble(),
        code: json["code"],
        description: json["description"],
        id: json["id"],
        listImage: json["listImage"] == null
            ? null
            : List<String>.from(json["listImage"].map((x) => x)),
        maxPrice: json["maxPrice"] == null ? null : json["maxPrice"].toDouble(),
        minPrice: json["minPrice"] == null ? null : json["minPrice"].toDouble(),
        name: json["name"],
        note: json["note"],
        faq: json['faq'] == null
            ? null
            : List<FaqType>.from(json['faq'].map((x) => FaqType.fromJson(x))),
        weight: json["weight"] == null ? null : json["weight"].toDouble(),
        categories: json['categories'] == null
            ? null
            : List<Category>.from(
                json["categories"].map((x) => Category.fromJson(x))),
        sharedProducts: json['sharedProducts'] == null
            ? null
            : List<ProductMaster>.from(
                json['sharedProducts'].map((e) => ProductMaster.fromJson(e))),
        attributes: json['attributes'] == null
            ? null
            : List<ProductAttribute>.from(
                json["attributes"].map((x) => ProductAttribute.fromJson(x))),
        product:
            json['product'] == null ? null : Product.fromJson(json['product']),
      );
}

class ProductAttribute {
  ProductAttribute({
    this.attribute,
    this.values,
    this.attributeId,
  });

  final Attribute attribute;
  final List<ValueType> values;
  final String attributeId;

  factory ProductAttribute.fromJson(Map<String, dynamic> json) =>
      ProductAttribute(
        attribute: json["attribute"] == null
            ? null
            : Attribute.fromJson(json["attribute"]),
        values: json["values"] == null
            ? null
            : List<ValueType>.from(
                json["values"].map((x) => ValueType.fromJson(x))),
        attributeId: json["attributeId"] == null ? null : json["attributeId"],
      );

  Map<String, dynamic> toJson() => {
        "attribute": attribute == null ? null : attribute.toJson(),
        "values": values == null
            ? null
            : List<dynamic>.from(values.map((x) => x.toJson())),
        "attributeId": attributeId == null ? null : attributeId,
      };
}

class Product {
  Product(
      {this.attributes,
      this.barCode,
      this.basePrice,
      this.code,
      this.description,
      this.id,
      this.listImage,
      this.name,
      this.note,
      this.productMasterId,
      this.rating,
      this.saleQty,
      this.weight,
      this.saleType,
      this.saleValue,
      this.hasSale,
      this.thumbnail,
      this.blockItem,
      this.sharedProducts,
      this.campaigns,
      this.youtubeLink,
      this.countdownDate,
      this.remaining,
      this.allowSale,
      this.productMaster});

  List<ProductAttribute> attributes;
  String barCode;
  double basePrice;
  String code;
  String description;
  String id;
  List<String> listImage;
  String name;
  String note;
  String productMasterId;
  double rating;
  String youtubeLink;
  int saleQty;
  double weight;
  String saleType;
  num saleValue;
  bool hasSale;
  String thumbnail;
  BlockItem blockItem;
  ProductMaster productMaster;
  List<Campaign> campaigns;
  List<ProductMaster> sharedProducts;
  DateTime countdownDate;
  bool allowSale;
  int remaining;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
      attributes: json["attributes"] == null
          ? null
          : List<ProductAttribute>.from(
              json["attributes"].map((x) => ProductAttribute.fromJson(x))),
      barCode: json["barCode"],
      basePrice:
          json["basePrice"] == null ? null : json["basePrice"].toDouble(),
      code: json["code"],
      remaining: json["remaining"] == null ? null : json['remaining'],
      description: json["description"],
      id: json["id"],
      allowSale: json["allowSale"],
      listImage: json["listImage"] == null
          ? null
          : List<String>.from(json["listImage"].map((x) => x)),
      name: json["name"],
      note: json["note"],
      campaigns: json['campaigns'] == null
          ? null
          : List<Campaign>.from(
              json["campaigns"].map((x) => Campaign.fromJson(x))),
      sharedProducts: json['sharedProducts'] == null
          ? null
          : List<ProductMaster>.from(
              json["sharedProducts"].map((x) => ProductMaster.fromJson(x))),
      productMasterId: json["productMasterId"],
      rating: json["rating"] == null ? null : json["rating"].toDouble(),
      saleQty:
          json["saleQty"] == null ? null : appConvertIntJson(json["saleQty"]),
      weight: json["weight"] == null ? null : json["weight"].toDouble(),
      saleType: json["saleType"],
      saleValue:
          json["saleValue"] == null ? null : json["saleValue"].toDouble(),
      hasSale: json["hasSale"],
      thumbnail: json["thumbnail"],
      youtubeLink: json['youtubeLink'],
      blockItem: json["blockItem"] == null
          ? null
          : BlockItem.fromJson(json["blockItem"]),
      countdownDate: json['countdownDate'] == null
          ? null
          : DateTime.parse(json['countdownDate']),
      productMaster: json['productMaster'] == null
          ? null
          : ProductMaster.fromJson(json['productMaster']));

  double get getSalePrice {
    try {
      if (hasSale) {
        if (saleType == "CASH")
          return basePrice - saleValue;
        else
          return basePrice - (basePrice * saleValue / 100);
      } else {
        return basePrice;
      }
    } catch (_) {
      return 0;
    }
  }

  String get getSaleContent {
    return saleType == "PERCENT" ? '${saleValue.toStringAsFixed(0)}%' : 'Giá';
  }

  Map<String, dynamic> toJson() => {
        "attributes": attributes == null
            ? null
            : List<dynamic>.from(attributes.map((x) => x.toJson())),
        "barCode": barCode == null ? null : barCode,
        "basePrice": basePrice == null ? null : basePrice,
        "code": code == null ? null : code,
        "description": description == null ? null : description,
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "note": note == null ? null : note,
        'youtubeLink': youtubeLink,
        "productMasterId": productMasterId == null ? null : productMasterId,
        "rating": rating == null ? null : rating,
        "saleQty": saleQty == null ? null : saleQty,
        "weight": weight == null ? null : weight,
        "saleType": saleType == null ? null : saleType,
        "saleValue": saleValue == null ? null : saleValue,
        "hasSale": hasSale == null ? null : hasSale,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "blockItem": blockItem == null ? null : blockItem.toJson(),
      };
}

class FavoriteProduct {
  final String id;
  final String productId;
  final Product product;

  FavoriteProduct({this.productId, this.id, this.product});

  factory FavoriteProduct.fromJson(Map<String, dynamic> json) =>
      FavoriteProduct(
        id: json["id"] == null ? null : json["id"],
        productId: json["productId"] == null ? null : json["productId"],
        product:
            json["product"] == null ? null : Product.fromJson(json["product"]),
      );
}

class PurchasedLaterProduct {
  final String id;
  final Product product;

  PurchasedLaterProduct({this.id, this.product});

  factory PurchasedLaterProduct.fromJson(Map<String, dynamic> json) =>
      PurchasedLaterProduct(
        id: json["id"] == null ? null : json["id"],
        product:
            json["product"] == null ? null : Product.fromJson(json["product"]),
      );
}
