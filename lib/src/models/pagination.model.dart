class Pagination {
  Pagination({
    this.limit,
    this.offset,
    this.page,
    this.total,
  });

  int limit;
  int offset;
  int page;
  final int total;

  factory Pagination.fromJson(Map<String, dynamic> json) => Pagination(
        limit: json["limit"],
        offset: json["offset"],
        page: json["page"],
        total: json["total"],
      );
}
