import 'package:flutter/material.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/utils/util.dart';

class OrderItem {
  String productName;
  int amount;
  bool hasSale;
  double saleValue;
  double basePrice;
  String saleType;
  int quantity;
  Product product;
  bool isOffer;

  double get salePrice {
    try {
      if (hasSale) {
        if (saleType == "CASH")
          return basePrice - saleValue;
        else
          return basePrice - (basePrice * saleValue / 100);
      } else {
        return basePrice;
      }
    } catch (_) {
      return 0;
    }
  }

  OrderItem(
      {@required this.productName,
      @required this.amount,
      @required this.hasSale,
      @required this.product,
      @required this.quantity,
      @required this.isOffer,
      @required this.saleValue,
      @required this.basePrice,
      @required this.saleType});

  factory OrderItem.fromJson(Map<String, dynamic> json) {
    return OrderItem(
      product:
          json['product'] == null ? null : Product.fromJson(json['product']),
      quantity: appConvertIntJson(json['quantity']),
      productName: json["productName"],
      isOffer: json['isOffer'] ?? false,
      amount: appConvertIntJson(json["amount"]),
      hasSale: json["hasSale"] ?? false,
      saleValue: appConvertDoubleJson(json["saleValue"]),
      basePrice: appConvertDoubleJson(json["basePrice"]),
      saleType: json["saleType"],
    );
  }
}
