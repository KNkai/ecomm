import 'package:genie/src/models/product.model.dart';

class OfferItems {
  OfferItems({
    this.id,
    this.qty,
    this.note,
    this.product,
  });

  String id;
  int qty;
  String note;
  Product product;

  factory OfferItems.fromJson(Map<String, dynamic> json) => OfferItems(
        id: json["id"],
        qty: json["qty"],
        note: json["note"],
        product: Product.fromJson(json["product"]),
      );
}
