class Province {
  final String name;
  final String id;

  Province({this.name, this.id});

  factory Province.fromJson(Map<String, dynamic> json) => Province(
        id: json["id"],
        name: json["province"],
      );
}

class Ward {
  final String name;
  final String id;

  Ward({this.name, this.id});

  factory Ward.fromJson(Map<String, dynamic> json) => Ward(
        name: json["ward"],
        id: json["id"],
      );
}

class District {
  final String name;
  final String id;

  District({this.name, this.id});

  factory District.fromJson(Map<String, dynamic> json) => District(
        name: json["district"],
        id: json["id"],
      );
}
