import 'package:genie/src/models/customer.model.dart';
import 'package:genie/src/models/rating_replay.model.dart';

class RatingLog {
  RatingLog(
      {this.replies,
      this.id,
      this.customer,
      this.message,
      this.rating,
      this.updatedAt});

  final String id;
  final Customer customer;
  final String message;
  final double rating;
  final DateTime updatedAt;
  final List<RatingReplay> replies;
  factory RatingLog.fromJson(Map<String, dynamic> json) {
    return RatingLog(
        id: json["id"],
        customer: json["customer"] == null
            ? null
            : Customer.fromJson(json['customer']),
        message: json["message"] == null ? null : json["message"],
        rating: json["rating"] == null ? null : json["rating"].toDouble(),
        replies: json["replies"] == null
            ? null
            : List<RatingReplay>.from(
                json["replies"].map((x) => RatingReplay.fromJson(x))),
        updatedAt: json['updatedAt'] == null
            ? null
            : DateTime.parse(json['updatedAt']));
  }
}
