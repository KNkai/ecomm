import 'campaign.model.dart';

class Evoucher {
  Evoucher({
    this.status,
    this.effectiveAt,
    this.id,
    this.campaign,
    this.staus,
    this.groupTitle,
    this.expiredAt,
    this.almostExpired,
  });

  EvoucherStatus status;
  Campaign campaign;
  DateTime effectiveAt;
  String id;
  EvoucherStatus staus;
  DateTime expiredAt;
  String groupTitle;
  bool almostExpired;

  factory Evoucher.fromJson(Map<String, dynamic> json) {
    return Evoucher(
      id: json['id'],
      campaign:
          json["campaign"] == null ? null : Campaign.fromJson(json["campaign"]),
      status: _convertEvoucherStatus(json['status']),
      expiredAt:
          json["expiredAt"] == null ? null : DateTime.parse(json["expiredAt"]),
      effectiveAt: json["effectiveAt"] == null
          ? null
          : DateTime.parse(json["effectiveAt"]),
      almostExpired:
          json['almostExpired'] == null ? null : json['almostExpired'],
    );
  }
}

enum EvoucherStatus { notActivated, activated, expired }

const _kActivated = 'ACTIVATED';
const _kNotActivated = 'NOT_ACTIVATED';
const _kExpired = 'EXPIRED';

EvoucherStatus _convertEvoucherStatus(String value) {
  if (value == null) return null;
  switch (value) {
    case _kActivated:
      return EvoucherStatus.activated;
    case _kNotActivated:
      return EvoucherStatus.notActivated;
    case _kExpired:
      return EvoucherStatus.expired;
  }
  return null;
}

extension EvoucherStatusExtension on EvoucherStatus {
  String get value {
    switch (this) {
      case EvoucherStatus.notActivated:
        return _kNotActivated;
      case EvoucherStatus.activated:
        return _kActivated;
      case EvoucherStatus.expired:
        return _kExpired;
    }
    return '';
  }

  String get title {
    switch (this) {
      case EvoucherStatus.expired:
        return 'Đã hết hạn';
      case EvoucherStatus.activated:
        return 'Hiệu lực';
      case EvoucherStatus.notActivated:
        return 'Không có hiệu lực';
    }
    return '';
  }
}
