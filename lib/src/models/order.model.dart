import 'package:flutter/cupertino.dart';
import 'package:genie/src/models/discount_log.model.dart';
import 'package:genie/src/models/order_item.model.dart';
import 'package:genie/src/models/order_status.model.dart';
import 'package:genie/src/models/payment_info.model.dart';
import 'package:genie/src/models/payment_method.model.dart';
import 'package:genie/src/utils/util.dart';

class Order {
  final double rewardPoint;
  final double cumulativePoint;
  final double shipfee;
  final String code;
  final int itemCount;
  final double amount;
  final String id;
  final String shipMethod;
  final PaymentMethod paymentMethod;
  final PaymentStatus paymentStatus;
  final List<OrderItem> items;
  final List<OrderItem> offerItems;
  final List<OrderItem> rawItems;
  final OrderStatus status;
  final String customerName;
  final String customerPhone;
  final String address;
  final String province;
  final String district;
  final String ward;
  final String provinceId;
  final String districtId;
  final String wardId;
  final double subtotal;
  final String shipMethodText;
  final PaymentInfo paymentInfo;
  final int rate;
  final String rateComment;
  final DateTime rateAt;

  double get total {
    double total = 0;

    items.forEach((element) {
      total += element.salePrice * element.quantity;
    });
    return total;
  }

  final List<DiscountLog> discountLogs;

  Order(
      {this.rateAt,
      this.rate,
      this.rateComment,
      @required this.provinceId,
      this.paymentInfo,
      @required this.districtId,
      @required this.wardId,
      @required this.shipMethodText,
      @required this.rewardPoint,
      @required this.status,
      @required this.cumulativePoint,
      @required this.shipfee,
      @required this.code,
      @required this.shipMethod,
      @required this.customerPhone,
      @required this.rawItems,
      @required this.customerName,
      @required this.discountLogs,
      @required this.address,
      @required this.offerItems,
      @required this.subtotal,
      @required this.province,
      @required this.district,
      @required this.ward,
      @required this.paymentStatus,
      @required this.paymentMethod,
      @required this.id,
      @required this.itemCount,
      @required this.amount,
      @required this.items});

  factory Order.fromJson(Map<String, dynamic> json) {
    final raws = json['items'];
    List<OrderItem> items;
    List<OrderItem> offerItems;
    List<OrderItem> rawItems;
    if (raws != null && raws is List && raws.isNotEmpty) {
      offerItems = [];
      items = [];
      rawItems = [];
      raws.forEach((rawI) {
        final orderItem = OrderItem.fromJson(rawI);
        rawItems.add(orderItem);
        (orderItem.isOffer) ? offerItems.add(orderItem) : items.add(orderItem);
      });
    }
    return Order(
        subtotal: appConvertDoubleJson(json['subtotal']),
        customerPhone: json['customerPhone'],
        rate: json['rate'],
        rateComment: json['rateComment'],
        rateAt: json['rateAt'] == null ? null : DateTime.parse(json['rateAt']),
        customerName: json['customerName'],
        address: json['address'],
        ward: json['ward'],
        rawItems: rawItems,
        district: json['district'],
        province: json['province'],
        wardId: json['wardId'],
        districtId: json['districtId'],
        provinceId: json['provinceId'],
        shipMethodText: json['shipMethodText'],
        status: convertOrderStatus(json['status']),
        paymentStatus: _convertPaymentStatus(json['paymentStatus']),
        paymentMethod: convertPaymentMethod(json['paymentMethod']),
        discountLogs: json['discountLogs'] == null
            ? null
            : List<DiscountLog>.from(
                json['discountLogs'].map((x) => DiscountLog.fromJson(x))),
        shipMethod: json['shipMethod'] == null ? null : json['shipMethod'],
        items: items,
        offerItems: offerItems,
        id: json['id'],
        code: json['code'],
        itemCount: json['itemCount'],
        paymentInfo: json['paymentInfo'] == null
            ? null
            : PaymentInfo.fromJson(json['paymentInfo']),
        amount: appConvertDoubleJson(json['amount']),
        shipfee: appConvertDoubleJson(json['shipfee']),
        rewardPoint: appConvertDoubleJson(json['rewardPoint']),
        cumulativePoint: appConvertDoubleJson(json['cumulativePoint']));
  }
}

enum PaymentStatus { pending, paid, canceled }

PaymentStatus _convertPaymentStatus(String value) {
  if (value == null) return null;
  switch (value) {
    case 'CANCELED':
      return PaymentStatus.canceled;
    case 'PAID':
      return PaymentStatus.paid;
    case 'PENDING':
      return PaymentStatus.pending;
  }
  return null;
}

extension PaymentStatusExtension on PaymentStatus {
  String get value {
    switch (this) {
      case PaymentStatus.canceled:
        return 'CANCELED';
      case PaymentStatus.paid:
        return 'PAID';
      case PaymentStatus.pending:
        return 'PENDING';
    }
    return '';
  }

  String get title {
    switch (this) {
      case PaymentStatus.paid:
        return 'Đã thanh toán';
      case PaymentStatus.pending:
        return 'Đang chờ';
      case PaymentStatus.canceled:
        return 'Đã huỷ';
    }
    return '';
  }
}

enum ShipMethod { viettelPost, grab }

extension ShipMethodExtendsion on ShipMethod {
  String get value {
    switch (this) {
      case ShipMethod.grab:
        return 'GRAB';
      default:
        return 'VIETTEL_POST';
    }
  }
}
