import 'package:genie/src/utils/util.dart';

class UserModel {
  UserModel({this.unseenNotify, this.avatar, this.name, this.role});

  int unseenNotify;
  String avatar;
  String name;
  String role;

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
        unseenNotify: appConvertIntJson(json["unseenNotify"]),
        avatar: json['avatar'],
        name: json['name']);
  }
}
