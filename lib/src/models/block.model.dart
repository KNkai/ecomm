import 'dart:convert';

import 'package:genie/src/models/campaign.model.dart';
import 'package:genie/src/models/category.model.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/models/screen.model.dart';

class Block {
  Block(
      {this.action,
      this.backgroundColor,
      this.code,
      this.id,
      this.items,
      this.name,
      this.priority,
      this.screenId,
      this.subtitle,
      this.textColor,
      this.title,
      this.type,
      this.gridCol});

  BlockAction action;
  String backgroundColor;
  String code;
  String id;
  List<BlockItem> items;
  String name;
  double priority;
  String screenId;
  String subtitle;
  String textColor;
  String title;
  String type;
  num gridCol;

  factory Block.fromRawJson(String str) => Block.fromJson(json.decode(str));

  factory Block.fromJson(Map<String, dynamic> json) => Block(
        action: json["action"] == null
            ? null
            : BlockAction.fromJson(json["action"]),
        backgroundColor:
            json["backgroundColor"] == null ? null : json["backgroundColor"],
        code: json["code"] == null ? null : json["code"],
        id: json["id"] == null ? null : json["id"],
        items: json["items"] == null
            ? null
            : List<BlockItem>.from(
                json["items"].map((x) => BlockItem.fromJson(x))),
        name: json["name"] == null ? null : json["name"],
        priority: json["priority"] == null ? null : json["priority"].toDouble(),
        screenId: json["screenId"] == null ? null : json["screenId"],
        subtitle: json["subtitle"] == null ? null : json["subtitle"],
        textColor: json["textColor"] == null ? null : json["textColor"],
        title: json["title"] == null ? null : json["title"],
        type: json["type"] == null ? null : json["type"],
        gridCol: json["gridCol"] == null ? null : json["gridCol"].toInt(),
      );
  Map<String, dynamic> toJson() => {
        "action": action == null ? null : action.toJson(),
        "backgroundColor": backgroundColor == null ? null : backgroundColor,
        "code": code == null ? null : code,
        "id": id == null ? null : id,
        "items": items == null
            ? null
            : List<dynamic>.from(items.map((x) => x.toJson())),
        "name": name == null ? null : name,
        "priority": priority == null ? null : priority,
        "screenId": screenId == null ? null : screenId,
        "subtitle": subtitle == null ? null : subtitle,
        "textColor": textColor == null ? null : textColor,
        "title": title == null ? null : title,
        "type": type == null ? null : type,
        "gridCol": gridCol == null ? null : gridCol,
      };
}

class BlockAction {
  BlockAction({
    this.categoryId,
    this.link,
    this.productId,
    this.screenId,
    this.type,
    this.label,
    this.screen,
    this.product,
    this.campaign,
    this.category,
  });

  String categoryId;
  String link;
  String productId;
  String screenId;
  String type;
  String label;
  Screen screen;
  Product product;
  Campaign campaign;
  Category category;

  factory BlockAction.fromRawJson(String str) =>
      BlockAction.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory BlockAction.fromJson(Map<String, dynamic> json) => BlockAction(
        categoryId: json["categoryId"] == null ? null : json["categoryId"],
        link: json["link"] == null ? null : json["link"],
        productId: json["productId"] == null ? null : json["productId"],
        screenId: json["screenId"] == null ? null : json["screenId"],
        type: json["type"] == null ? null : json["type"],
        label: json["label"] == null ? null : json["label"],
        screen: json["screen"] == null ? null : Screen.fromJson(json["screen"]),
        product:
            json["product"] == null ? null : Product.fromJson(json["product"]),
        campaign: json["campaign"] == null
            ? null
            : Campaign.fromJson(json["campaign"]),
        category: json["category"] == null
            ? null
            : Category.fromJson(json["category"]),
      );

  Map<String, dynamic> toJson() => {
        "categoryId": categoryId == null ? null : categoryId,
        "link": link == null ? null : link,
        "productId": productId == null ? null : productId,
        "screenId": screenId == null ? null : screenId,
        "type": type == null ? null : type,
        "label": label == null ? null : label,
      };
}

class BlockItem {
  BlockItem({
    this.action,
    this.blockId,
    this.hidden,
    this.id,
    this.image,
    this.subtitle,
    this.title,
    this.product,
    this.backgroundColor,
    this.textColor,
    this.campaign,
  });

  BlockItemAction action;
  String blockId;
  bool hidden;
  String id;
  String image;
  String subtitle;
  String title;
  Product product;
  String backgroundColor;
  String textColor;
  Campaign campaign;

  factory BlockItem.fromRawJson(String str) =>
      BlockItem.fromJson(json.decode(str));

  factory BlockItem.fromJson(Map<String, dynamic> json) => BlockItem(
        action: json["action"] == null
            ? null
            : BlockItemAction.fromJson(json["action"]),
        blockId: json["blockId"] == null ? null : json["blockId"],
        hidden: json["hidden"] == null ? null : json["hidden"],
        id: json["id"] == null ? null : json["id"],
        image: json["image"] == null ? null : json["image"],
        subtitle: json["subtitle"] == null ? null : json["subtitle"],
        title: json["title"] == null ? null : json["title"],
        product:
            json["product"] == null ? null : Product.fromJson(json["product"]),
        backgroundColor:
            json["backgroundColor"] == null ? null : json["backgroundColor"],
        textColor: json["textColor"] == null ? null : json["textColor"],
        campaign: json["campaign"] == null
            ? null
            : Campaign.fromJson(json["campaign"]),
      );
  Map<String, dynamic> toJson() => {
        "action": action == null ? null : action.toJson(),
        "blockId": blockId == null ? null : blockId,
        "hidden": hidden == null ? null : hidden,
        "id": id == null ? null : id,
        "image": image == null ? null : image,
        "subtitle": subtitle == null ? null : subtitle,
        "title": title == null ? null : title,
        "product": product == null ? null : product.toJson(),
        "backgroundColor": backgroundColor == null ? null : backgroundColor,
        "textColor": textColor == null ? null : textColor,
      };
}

class BlockItemAction {
  BlockItemAction({
    this.categoryId,
    this.link,
    this.productId,
    this.screenId,
    this.type,
  });

  String categoryId;
  String link;
  String productId;
  String screenId;
  String type;

  factory BlockItemAction.fromRawJson(String str) =>
      BlockItemAction.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory BlockItemAction.fromJson(Map<String, dynamic> json) =>
      BlockItemAction(
        categoryId: json["categoryId"] == null ? null : json["categoryId"],
        link: json["link"] == null ? null : json["link"],
        productId: json["productId"] == null ? null : json["productId"],
        screenId: json["screenId"] == null ? null : json["screenId"],
        type: json["type"] == null ? null : json["type"],
      );

  Map<String, dynamic> toJson() => {
        "categoryId": categoryId == null ? null : categoryId,
        "link": link == null ? null : link,
        "productId": productId == null ? null : productId,
        "screenId": screenId == null ? null : screenId,
        "type": type == null ? null : type,
      };
}
