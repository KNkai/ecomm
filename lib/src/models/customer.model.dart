class Customer {
  Customer(
      {this.provinceId,
      this.wardId,
      this.districtId,
      this.id,
      this.name,
      this.avatar,
      this.rewardPoint,
      this.cumulativePoint,
      this.rank,
      this.email,
      this.birthday,
      this.phone,
      this.unseenNotify,
      this.gender,
      this.address,
      this.province,
      this.ward,
      this.district,
      this.recentKeywords});

  String id;
  String name;
  dynamic avatar;
  int rewardPoint;
  int cumulativePoint;
  String rank;
  dynamic email;
  DateTime birthday;
  String phone;
  int unseenNotify;
  String gender;
  String address, province, ward, district;
  String provinceId, wardId, districtId;
  List<String> recentKeywords;

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
      id: json["id"] == null ? null : json["id"],
      name: json["name"] == null ? null : json["name"],
      gender: json["gender"] == null ? null : json["gender"],
      avatar: json["avatar"] == null ? null : json["avatar"],
      unseenNotify: json["unseenNotify"] == null ? null : json["unseenNotify"],
      rewardPoint: json["rewardPoint"] == null ? null : json["rewardPoint"],
      cumulativePoint:
          json["cumulativePoint"] == null ? null : json["cumulativePoint"],
      rank: json["rank"] == null ? null : json["rank"],
      email: json["email"] == null ? null : json["email"],
      birthday:
          json["birthday"] == null ? null : DateTime.parse(json["birthday"]),
      phone: json["phone"] == null ? null : json["phone"],
      address: json["address"] == null ? null : json["address"],
      province: json["province"] == null ? null : json["province"],
      ward: json["ward"] == null ? null : json["ward"],
      district: json["district"] == null ? null : json["district"],
      provinceId: json["provinceId"] == null ? null : json["provinceId"],
      wardId: json["wardId"] == null ? null : json["wardId"],
      districtId: json["districtId"] == null ? null : json["districtId"],
      recentKeywords: json["recentKeyWords"] == null
          ? null
          : List<String>.from(json["recentKeyWords"].map((e) => e)).toList());
}
