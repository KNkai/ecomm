import 'package:genie/src/models/user.model.dart';

class RatingReplay {
  RatingReplay({this.user, this.message});

  final UserModel user;
  final String message;
  factory RatingReplay.fromJson(Map<String, dynamic> json) => RatingReplay(
      user: json["user"] == null ? null : UserModel.fromJson(json['user']),
      message: json["message"] == null ? null : json["message"]);
}
