import 'package:genie/src/models/campaign.model.dart';
import 'package:genie/src/models/category.model.dart';
import 'package:genie/src/models/screen.model.dart';

class NotifcationModel {
  NotifcationModel({
    this.campaign,
    this.screen,
    this.productId,
    this.category,
    this.orderId,
    this.title,
    this.body,
    this.id,
    this.clickAction,
    this.data,
    this.seen,
    this.image,
    this.createdAt,
    this.type,
    this.link,
    this.campaignId,
    this.categoryId,
  });

  final String orderId;
  final String id;
  final String title;
  final String body;
  final String clickAction;
  final String data;
  bool seen;
  final String image;
  final DateTime createdAt;
  final String type;
  final String link;
  final Screen screen;
  final String productId;
  final Category category;
  final Campaign campaign;
  final String campaignId;
  final String categoryId;

  factory NotifcationModel.fromJson(Map<String, dynamic> json) =>
      NotifcationModel(
        orderId: json["orderId"],
        id: json["id"],
        title: json["title"],
        body: json["body"],
        clickAction: json["clickAction"],
        data: json["data"],
        seen: json["seen"] ?? false,
        image: json["image"],
        type: json['type'],
        link: json['link'] == null ? null : json['link'],
        productId: json['productId'] == null ? null : json['productId'],
        categoryId: json['categoryId'] == null ? null : json['categoryId'],
        campaignId: json['campaignId'] == null ? null : json['campaignId'],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]).toLocal(),
        screen: json['screen'] == null ? null : Screen.fromJson(json['screen']),
        category: json['category'] == null
            ? null
            : Category.fromJson(json['category']),
        campaign: json['campaign'] == null
            ? null
            : Campaign.fromJson(json['campaign']),
      );
}
