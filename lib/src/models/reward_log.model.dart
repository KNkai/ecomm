import 'package:flutter/cupertino.dart';
import 'package:genie/src/models/reward.model.dart';

class RewardLog {
  final String id;
  final Reward reward;

  RewardLog({@required this.id, @required this.reward});

  factory RewardLog.fromJson(Map<String, dynamic> json) {
    return RewardLog(
        id: json['id'],
        reward:
            json['reward'] == null ? null : Reward.fromJson(json['reward']));
  }
}
