class FaqType {
  FaqType({
    this.question,
    this.answer,
  });

  final String question;
  final String answer;

  factory FaqType.fromJson(Map<String, dynamic> json) => FaqType(
        question: json["question"],
        answer: json["answer"],
      );
}
