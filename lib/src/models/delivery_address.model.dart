class DeliveryAddress {
  DeliveryAddress(
      {this.id,
      this.name,
      this.address,
      this.province,
      this.district,
      this.ward,
      this.phone,
      this.isDefault,
      this.provinceId,
      this.districtId,
      this.wardId});

  final String id;
  final String name;
  final String address;
  final String province;
  final String district;
  final String ward;
  final String provinceId;
  final String districtId;
  final String wardId;
  final String phone;
  final bool isDefault;
  factory DeliveryAddress.fromJson(Map<String, dynamic> json) {
    return DeliveryAddress(
      id: json['id'],
      name: json["name"],
      address: json['address'],
      province: json["province"],
      district: json["district"],
      ward: json["ward"],
      provinceId: json["provinceId"],
      districtId: json["districtId"],
      wardId: json["wardId"],
      phone: json["phone"],
      isDefault: json["isDefault"] == null ? null : json["isDefault"] as bool,
    );
  }
}
