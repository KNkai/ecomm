import 'product.model.dart';

class DiscountItems {
  DiscountItems({
    this.id,
    this.discountUnit,
    this.discountValue,
    this.maxDiscount,
    this.product,
  });

  String id;
  String discountUnit;
  double discountValue;
  double maxDiscount;
  Product product;

  factory DiscountItems.fromJson(Map<String, dynamic> json) => DiscountItems(
        id: json["id"],
        discountUnit: json["discountUnit"],
        discountValue: json["discountValue"].toDouble(),
        maxDiscount: json["maxDiscount"].toDouble(),
        product: Product.fromJson(json["product"]),
      );
}
