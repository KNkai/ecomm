class AppKey {
  // Share Preference
  static const String cartData = '1';
  // Secure storage. Keychain & KeyStore
  static const String xToken = '2';
  static const String popupData = '3';
  static const String customerId = '4';
}
