import 'package:flutter/material.dart';

class AppColor {
  AppColor._();
  static const primary = Color(0xFF64A8A7);
  static Color separate = Colors.grey.withOpacity(0.3);
}
