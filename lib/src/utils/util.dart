import 'dart:async';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/components/app_webview.dart';
import 'package:genie/src/models/block.model.dart';
import 'package:genie/src/models/screen.model.dart';
import 'package:genie/src/pages/category/category_page.dart';
import 'package:genie/src/pages/default_screen/default_screen_page.dart';
import 'package:genie/src/pages/detail_product/detail_product_page.dart';
import 'package:genie/src/pages/order_detail/order_detail_page.dart';
import 'package:genie/src/utils/size_config.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';

import 'app_color.dart';

String appFullAddress(
    {String address, String ward, String district, String province}) {
  final _adress = address == null ? '' : '$address,';
  final _ward = ward == null || ward.isEmpty ? '' : ' $ward,';
  final _district = district == null || district.isEmpty ? '' : ' $district,';
  final _province = province == null || province.isEmpty ? '' : ' $province';
  return '$_adress$_ward$_district$_province';
}

bool checkPhoneNumberFormat(String phone) {
  if (phone == null || phone.isEmpty) return false;
  return RegExp(r"^[0-9]{10}$").hasMatch(phone);
}

String appCurrency(num money) {
  if (money == null) return 'đ';
  if (money is double) money = money.toInt();
  if (money == 0) return '0 đ';
  if (money <= 999) return '$money đ';
  return NumberFormat('##,000 đ').format(money);
}

String appConvertDateTime(DateTime time, {String format = "dd/MM/yyyy"}) {
  return time == null ? '' : DateFormat(format).format(time.toLocal());
}

bool appCheckEmptyString(String value) {
  if (value == null) return true;

  return value.isEmpty;
}

String appConvertStringDouble(double value) {
  if (value == null) return '';
  return value.toString();
}

bool appCheckListEmpty(List list) {
  if (list == null) return true;

  return list.isEmpty;
}

double appConvertDoubleJson(dynamic value) {
  if (value == null) return null;
  if (value is int) return value.toDouble();
  return value;
}

int appConvertIntJson(dynamic value) {
  if (value == null) return null;
  if (value is double) return value.toInt();
  return value;
}

Future<String> getUniqueDeviceId() {
  final infoPlugin = DeviceInfoPlugin();
  if (Platform.isIOS) {
    return infoPlugin.iosInfo.then((info) {
      return info.identifierForVendor;
    });
  } else if (Platform.isAndroid) {
    return infoPlugin.androidInfo.then((info) {
      return info.androidId;
    });
  }

  return null;
}

void onCustomPersionRequest(
    {@required Permission permission,
    Function onGranted,
    Function onAlreadyDenied,
    Function onJustDeny,
    Function onAndroidPermanentDenied}) {
  permission.status.then(
    (value) {
      if (value.isUndetermined) {
        Permission.camera.request().then((value) {
          if (value.isDenied && onJustDeny != null) {
            onJustDeny();
          } else if (value.isGranted && onGranted != null) {
            onGranted();
          } else if (value.isPermanentlyDenied &&
              onAndroidPermanentDenied != null) {
            onAndroidPermanentDenied();
          }
        });
      } else if (value.isDenied && onAlreadyDenied != null) {
        onAlreadyDenied();
      } else if (value.isGranted && onGranted != null) {
        onGranted();
      }
    },
  );
}

void appShowModalBottomSheet(
    {@required BuildContext context,
    @required Widget child,
    VoidCallback onBtnTap,
    bool isScrollControlled = true,
    VoidCallback onExit,
    @required String title}) {
  showModalBottomSheet(
    isScrollControlled: isScrollControlled,
    context: context,
    builder: (context) {
      return Padding(
        padding: EdgeInsets.only(
            left: 26,
            right: 26,
            bottom: MediaQuery.of(context).viewInsets.bottom + 20,
            top: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Expanded(
                  child: Text(
                    title,
                    style: const TextStyle(fontSize: 18),
                  ),
                ),
                GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: const Icon(Icons.close))
              ],
            ),
            const Divider(
              thickness: 0.8,
            ),
            child,
            if (onBtnTap != null)
              Column(children: [
                const SizedBox(
                  height: 10,
                ),
                Center(
                    child: AppBtn(
                  title: 'Xác nhận',
                  onTap: () {
                    onBtnTap();
                    Navigator.of(context).pop();
                  },
                )),
              ])
          ],
        ),
      );
    },
  ).then((_) {
    if (onExit != null) onExit();
  });
}

class WarningDialog {
  static bool _isShow = false;

  static void show(BuildContext context, String content, String btnText,
      {String title, Function onCloseDialog}) {
    if (!_isShow) {
      _isShow = true;
      Widget alert;
      Widget _titleWidget({TextAlign textAlign = TextAlign.start}) =>
          title != null
              ? Text(
                  title,
                  textAlign: textAlign,
                )
              : null;

      if (content != null && content.isNotEmpty) {
        if (Platform.isAndroid) {
          // If it has plenty of buttons, it will stack them on vertical way.
          // If title isn't supplied, height of this alert will smaller than the one has title.
          alert = AlertDialog(
            title: _titleWidget(),
            content: Text(
              content,
              textAlign: TextAlign.start,
            ),
            actions: [
              FlatButton(
                child: Text(btnText),
                onPressed: () {
                  _isShow = false;
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        } else {
          // Almost similiar with Cupertino style.
          // If title isn't supplied, height of this alert will smaller than the one has title.
          alert = CupertinoAlertDialog(
            title: Padding(
              padding: const EdgeInsets.only(
                bottom: 10,
              ),
              child: _titleWidget(textAlign: TextAlign.center),
            ),
            content: Text(
              content,
              textAlign: TextAlign.start,
            ),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () {
                  _isShow = false;
                  Navigator.of(context).pop();
                },
                child: Text(
                  btnText,
                ),
              )
            ],
          );
        }

        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert;
          },
        ).then((value) {
          _isShow = false;
          if (onCloseDialog != null) {
            onCloseDialog();
          }
        });
      }
    }
  }
}

openMessenger(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    print("cannot open the messenger");
  }
}

makePhoneCall(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

List<List<T>> chunk<T>(List<T> array, int size) {
  List<List<T>> chunks = [];
  for (var i = 0; i < array.length; i += size) {
    chunks.add(
        array.sublist(i, i + size > array.length ? array.length : i + size));
  }
  return chunks;
}

showDialogIfFirstLoaded({
  BuildContext context,
  bool isShow,
  ScreenPopup screenPopup,
  BlockItemAction action,
}) async {
  isShow
      ? showDialog(
          context: context,
          builder: (BuildContext context) {
            return Container(
              color: Colors.transparent,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: [
                  Positioned.fill(
                    top: -100,
                    child: Align(
                      alignment: Alignment.center,
                      child: Image.network(
                        screenPopup.image,
                        height: MediaQuery.of(context).size.height / 2,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Positioned(
                    top: MediaQuery.of(context).size.height / 6,
                    right: MediaQuery.of(context).size.width / 8,
                    child: MaterialButton(
                      child: const Icon(
                        Icons.close_rounded,
                        color: Colors.black,
                      ),
                      shape: const CircleBorder(),
                      color: Colors.white,
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Positioned.fill(
                    top: (MediaQuery.of(context).size.height / 2) - 100,
                    child: Align(
                      alignment: Alignment.center,
                      child: AppBtn(
                        onTap: () async {
                          if (action != null) {
                            Navigator.of(context).pop();
                            switch (action.type) {
                              case "GO_TO_SCREEN":
                                DefaultScreenPage.push(
                                    context: context,
                                    screenId: action.screenId,
                                    title: screenPopup.action.screen.name);
                                return;
                              case "GO_TO_CATEGORY":
                                return CategoryPage.push(context,
                                    categoryId: action.categoryId,
                                    title: screenPopup.action.category.name);
                              case "GO_TO_PRODUCT":
                                return DetailProductPage.push(
                                    context: context,
                                    productId: screenPopup.action.productId);
                              default:
                                return AppWebView.push(
                                    context: context,
                                    title: "title",
                                    url: action.link);
                                return;
                            }
                          }
                        },
                        title: screenPopup.action.label == null
                            ? ""
                            : screenPopup.action.label,
                        color: AppColor.primary,
                        textColor: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        )
      : Container();
}

Map<String, bool> PopupFirstLoad = {};

setPopup(BuildContext context, Screen screen, {bool force = false}) {
  if (force || PopupFirstLoad[screen?.id] == null) {
    Future.delayed(const Duration(milliseconds: 300), () {
      BlockItemAction _tempBlocItemAction = BlockItemAction(
        type: screen?.popup?.action?.type ?? "",
        screenId: screen?.popup?.action?.screenId ?? "",
        categoryId: screen?.popup?.action?.categoryId ?? "",
        link: screen?.popup?.action?.link ?? "",
        productId: screen?.popup?.action?.productId ?? "",
      );
      showDialogIfFirstLoaded(
        context: context,
        isShow: screen?.popup?.enabled ?? false,
        screenPopup: screen?.popup,
        action: _tempBlocItemAction,
      );
    });
    PopupFirstLoad[screen?.id] = true;
  }
}

int convertColor(String color) {
  var _color = "0xff";
  _color += color.split('#')[1];
  return int.parse(_color);
}

showLoaderDialog(BuildContext context, String id, String title,
    {bool isPushNew = true, bool isSuccess = true}) {
  AlertDialog alert = AlertDialog(
    content: SizedBox(
      width: SizeConfig.widthDevice / 3,
      height: SizeConfig.widthDevice / 4,
      child: new Column(
        children: [
          Container(margin: EdgeInsets.all(10), child: Text(title)),
          CircularProgressIndicator(),
        ],
      ),
    ),
  );
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      Future.delayed(Duration(seconds: isSuccess ? 2 : 1)).then(
        (value) {
          if (isPushNew) {
            OrderDetailPage.pushNew(context: context, id: id);
          } else {
            Navigator.of(context).pop();
          }
        },
      );
      return alert;
    },
  );
}

extension GlobalKeyExtension on GlobalKey {
  Offset get globalPaintBounds {
    final renderObject = currentContext?.findRenderObject();
    var translation = renderObject?.getTransformTo(null)?.getTranslation();
    if (translation != null && renderObject.paintBounds != null) {
      return Offset(translation.x, translation.y);
    } else {
      return null;
    }
  }
}
