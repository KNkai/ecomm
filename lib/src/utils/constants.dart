import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

Center kLoadingSpinner = const Center(
  child: SpinKitCircle(
    color: Color(0xff008080),
    size: 50.0,
  ),
);
