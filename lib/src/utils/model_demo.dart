import 'dart:math';

final List<ItemOrderModel> ItemOrderList = [
  ItemOrderModel(
    id: "ABCD",
    amount: 9000000,
    endFinish: DateTime.now().subtract(const Duration(days: 10)),
    startFinish: DateTime.now(),
    totalProduct: 5,
    status: 1,
  ),
  ItemOrderModel(
    id: "EFGH",
    amount: 6546654,
    endFinish: DateTime.now().subtract(const Duration(days: 10)),
    startFinish: DateTime.now(),
    totalProduct: 23,
    status: 1,
  ),
  ItemOrderModel(
    id: "ADQWD",
    amount: 4265443,
    endFinish: DateTime.now().subtract(const Duration(days: 20)),
    startFinish: DateTime.now(),
    totalProduct: 4,
    status: 2,
  ),
  ItemOrderModel(
    id: "QWDQFG",
    amount: 5453453,
    endFinish: DateTime.now().subtract(const Duration(days: 15)),
    startFinish: DateTime.now(),
    totalProduct: 15,
    status: 3,
  ),
  ItemOrderModel(
    id: "FHFDDFG",
    amount: 45345354,
    endFinish: DateTime.now().subtract(const Duration(days: 5)),
    startFinish: DateTime.now(),
    totalProduct: 25,
    status: 4,
  ),
];

class ItemOrderModel {
  final String id;
  final DateTime startFinish;
  final DateTime endFinish;
  final int totalProduct;
  final double amount;
  final int status;

  ItemOrderModel(
      {this.status,
      this.id,
      this.startFinish,
      this.endFinish,
      this.totalProduct,
      this.amount});
}

class ProductOrder {
  final String idOrder;
  final String urlImage;
  final String title;
  final String capacity;
  final int total;
  final double baseCost;
  final double promoCost;

  ProductOrder({
    this.idOrder,
    this.urlImage,
    this.title,
    this.capacity,
    this.total,
    this.baseCost,
    this.promoCost,
  });
}

final List<ProductOrder> ListProductOrder = [
  ProductOrder(
    urlImage: avatarNetWork,
    baseCost: randomCost().toDouble(),
    capacity: "300ml",
    promoCost: randomCost().toDouble(),
    title: "Whitening Functional Food Max White Plus Max",
    total: 3,
    idOrder: "ABCD",
  ),
  ProductOrder(
    urlImage: avatarNetWork,
    baseCost: randomCost().toDouble(),
    capacity: "300ml",
    promoCost: randomCost().toDouble(),
    title: "Whitening Functional Food Max White Plus Max",
    total: 3,
    idOrder: "ABCD",
  ),
  ProductOrder(
    urlImage: avatarNetWork,
    baseCost: randomCost().toDouble(),
    capacity: "300ml",
    promoCost: randomCost().toDouble(),
    title: "Whitening Functional Food Max White Plus Max",
    total: 3,
    idOrder: "EFGH",
  ),
  ProductOrder(
    urlImage: avatarNetWork,
    baseCost: randomCost().toDouble(),
    capacity: "300ml",
    promoCost: randomCost().toDouble(),
    title: "Whitening Functional Food Max White Plus Max",
    total: 3,
    idOrder: "EFGH",
  ),
  ProductOrder(
    urlImage: avatarNetWork,
    baseCost: randomCost().toDouble(),
    capacity: "300ml",
    promoCost: randomCost().toDouble(),
    title: "Whitening Functional Food Max White Plus Max",
    total: 3,
    idOrder: "ADQWD",
  ),
  ProductOrder(
    urlImage: avatarNetWork,
    baseCost: randomCost().toDouble(),
    capacity: "300ml",
    promoCost: randomCost().toDouble(),
    title: "Whitening Functional Food Max White Plus Max",
    total: 3,
    idOrder: "ABCD",
  ),
];

int randomCost() {
  Random random = new Random();
  int cost = 100000 + random.nextInt(10000000);
  return cost;
}

class InfoPaymentModel {
  final String bankName;
  final String name;
  final String numBank;

  InfoPaymentModel({this.bankName, this.name, this.numBank});
}

final List<InfoPaymentModel> ListInfoPayment = [
  InfoPaymentModel(
    bankName: "VIETCOMBANK",
    name: "Đỗ Lê Kim Huệ",
    numBank: "10422 8496 26019 ",
  ),
  InfoPaymentModel(
    bankName: "TECHCOMBANK",
    name: "Đỗ Lê Kim Huệ",
    numBank: "004 100 017 0824 ",
  ),
  InfoPaymentModel(
    bankName: "BIDV",
    name: "Đỗ Lê Kim Huệ",
    numBank: "5611 0000 590 519 ",
  ),
];

// const CHATLINK = "https://m.me/VyParisSpaClinic1?ref=app";
// const HOTLINE = "0917083388";
const String avatarNetWork =
    "https://i.pinimg.com/originals/e3/d8/ef/e3d8efd1d7e6efc623af783ef91c3666.jpg";

class ButtonFilterStoreModel {
  final String title;

  ButtonFilterStoreModel({this.title});
}

List<ButtonFilterStoreModel> LISTBUTTONFILTERSTOREMODEL = [
  ButtonFilterStoreModel(
    title: "Tất cả sản phẩm",
  ),
  ButtonFilterStoreModel(
    title: "Dưỡng toàn thân",
  ),
  ButtonFilterStoreModel(
    title: "Dưỡng tóc",
  ),
];
