import 'dart:ui';

class SizeConfig {
  static double _heightDevice;
  static double _widthDevice;

  static double get heightDevice => _heightDevice;
  static double get widthDevice => _widthDevice;

  static init() {
    _heightDevice = window.physicalSize.height / window.devicePixelRatio;
    _widthDevice = window.physicalSize.width / window.devicePixelRatio;
  }
}
