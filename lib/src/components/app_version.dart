import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

class AppVersion extends StatelessWidget {
  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    return info.version;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 40),
      child: FutureBuilder(
        future: _initPackageInfo(),
        builder: (context, snapshot) {
          return Center(
            child: Text(
              snapshot.data == null
                  ? "Loading..."
                  : "Phiên Bản ${snapshot.data}",
              style:
                  TextStyle(fontSize: 14, color: Colors.grey.withOpacity(0.7)),
            ),
          );
        },
      ),
    );
  }
}
