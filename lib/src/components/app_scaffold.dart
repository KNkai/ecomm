import 'package:flutter/material.dart';
import 'package:genie/src/components/custom_app_bar.dart';

import 'app_floating_contact.dart';

class AppScaffold extends StatelessWidget {
  const AppScaffold({
    @required this.child,
    this.title,
    this.showAppbar = true,
    this.showBackBtn = true,
    this.showCartBtn = true,
    this.onCustomonBackTap,
    this.showSearchBtn = false,
    this.showButtonBottom = false,
    this.bottomWidget,
    this.isPaddingContact = false,
    this.showContact = false,
  });

  final Widget child;
  final String title;
  final bool showAppbar;
  final bool showBackBtn;
  final bool showCartBtn;
  final bool showSearchBtn;
  final VoidCallback onCustomonBackTap;
  final bool showButtonBottom;
  final Widget bottomWidget;
  final bool isPaddingContact;
  final bool showContact;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: Colors.white,

        appBar: showAppbar
            ? CustomAppBar(
                title: title,
                showBackBtn: showBackBtn,
                showCartBtn: showCartBtn,
                onCustomBackTap: onCustomonBackTap,
                showSearchBtn: showSearchBtn,
              )
            : null,
        body: SafeArea(
          child: child,
          bottom: false,
        ),
        floatingActionButton: showContact
            ? isPaddingContact
                ? Padding(
                    padding: EdgeInsets.only(bottom: 100),
                    child: AppFloatingContact(),
                  )
                : AppFloatingContact()
            : const SizedBox.shrink(),
        // floatingActionButton: showButtonBottom ? bottomWidget : null,
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
        // bottomNavigationBar: NavBarApp(),
      ),
    );
  }
}
