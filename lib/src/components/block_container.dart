import 'package:flutter/material.dart';
import 'package:genie/src/utils/hex_color.dart';

class BlockContainer extends StatelessWidget {
  final String title;
  final Widget child;
  final Color titleColor;
  final String subTitle;
  final double paddingBottom;
  final String hexColor;
  const BlockContainer(
      {@required this.title,
      @required this.child,
      this.titleColor,
      this.hexColor,
      this.paddingBottom,
      this.subTitle});

  @override
  Widget build(BuildContext context) {
    final backGroundColor =
        HexColor.parseTry(hexColor, errorColor: Colors.white);
    return ColoredBox(
      color: backGroundColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (backGroundColor != Colors.white &&
              this.title != null &&
              this.title.isNotEmpty)
            const SizedBox(
              height: 20,
            ),
          if (this.title != null && !this.title.isEmpty) ...[
            Padding(
              padding: const EdgeInsets.only(left: 26),
              child: Text(
                this.title,
                style: TextStyle(fontSize: 21, color: titleColor),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            if (paddingBottom != null)
              const SizedBox(
                height: 32,
              ),
          ],
          child,
          if (backGroundColor != Colors.white)
            const SizedBox(
              height: 32,
            ),
        ],
      ),
    );
  }
}
