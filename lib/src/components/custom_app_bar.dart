import 'package:flutter/material.dart';
import 'package:genie/src/components/app_cart_icon.dart';
import 'package:genie/src/components/app_search_icon.dart';
import 'package:genie/src/utils/app_color.dart';

class CustomAppBar extends PreferredSize {
  final bool showCartBtn;
  final bool showBackBtn;
  final VoidCallback onCustomBackTap;
  final String title;
  final PreferredSizeWidget bottom;
  final bool showSearchBtn;

  const CustomAppBar(
      {this.showSearchBtn = false,
      this.showBackBtn = true,
      this.showCartBtn = true,
      this.bottom,
      this.onCustomBackTap,
      this.title});

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight +
      (bottom is TabBar ? (bottom as TabBar).preferredSize.height : 0));

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: false,
      brightness: Brightness.light,
      bottom: this.bottom,
      titleSpacing: -10, // WTF
      actions: (this.showCartBtn)
          ? [
              showSearchBtn
                  ? Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: AppSearchIcon(),
                    )
                  : Container(),
              const Padding(
                padding: EdgeInsets.only(right: 10),
                child: AppCartIcon(),
              ),
            ]
          : null,
      leading: showBackBtn
          ? GestureDetector(
              onTap: () {
                if (onCustomBackTap != null)
                  onCustomBackTap();
                else
                  Navigator.of(context).pop();
              },
              behavior: HitTestBehavior.opaque,
              child: const Icon(
                Icons.arrow_back_ios,
                color: AppColor.primary,
                size: 19,
              ),
            )
          : const SizedBox(
              width: 19,
            ),
      backgroundColor: Colors.white,
      title: title != null
          ? Text(
              title,
              style: const TextStyle(
                  color: Colors.black, fontWeight: FontWeight.normal),
            )
          : null,
    );
  }
}
