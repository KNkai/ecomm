import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';

import 'app_checkbox.dart';

class BigTicket extends StatelessWidget {
  final String urlImage;
  final String smallTitle;
  final String title;
  final double height;
  final double width;
  final double sizeLeadingIcon;
  final bool select;
  final VoidCallback onSave;
  final bool isSave;
  final BigTicketMode mode;
  BigTicket(
      {@required this.urlImage,
      @required this.smallTitle,
      @required this.title,
      @required this.mode,
      this.select,
      this.onSave,
      this.isSave = false,
      this.height = 75,
      this.sizeLeadingIcon = 39,
      this.width = double.infinity});

  final paddingLeftIcon = 8.0;
  final paddingRightIcon = 0.0;
  final paddingTopIcon = 8.0;
  final paddingBottomIcon = 8.0;

  @override
  Widget build(BuildContext context) {
    // print('urlImage h${urlImage}h');
    return Stack(
      children: [
        Positioned.fill(
          left: 1,
          right: 1,
          child: DecoratedBox(
              decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: Colors.grey[200],
                spreadRadius: 1,
                blurRadius: 5,
                offset: Offset(0, 5))
          ])),
        ),
        Positioned.fill(
          child: LayoutBuilder(builder: (_, constraint) {
            return CustomPaint(
              size: Size(constraint.biggest.width, constraint.biggest.height),
              painter: _BigTicketPainter(
                  sizeLeadingIcon: Size(
                      sizeLeadingIcon + paddingLeftIcon + paddingRightIcon,
                      sizeLeadingIcon + paddingTopIcon + paddingBottomIcon)),
            );
          }),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 11),
          child: Row(
            children: [
              SizedBox(width: paddingLeftIcon),
              SizedBox(
                  width: sizeLeadingIcon,
                  height: sizeLeadingIcon,
                  child: CircleAvatar(
                    backgroundImage: urlImage != null && urlImage != ''
                        ? NetworkImage(
                            urlImage,
                          )
                        : AssetImage(
                            AppAsset.evoucherDefault,
                          ),
                  )),
              SizedBox(width: 25 + paddingRightIcon),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(title ?? '',
                        style: const TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w400)),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(smallTitle ?? '',
                        style:
                            TextStyle(color: Color(0xff9b9b9b), fontSize: 13))
                  ],
                ),
              ),
              (mode == BigTicketMode.select)
                  ? Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 7),
                      child: AppSingleCheckBox(
                          select: select ?? false, disable: false),
                    )
                  : mode == BigTicketMode.save
                      ? Padding(
                          padding: const EdgeInsets.only(right: 16),
                          child: _SavedButton(
                              isSave: isSave,
                              onTap: () {
                                onSave();
                              }),
                        )
                      : SizedBox.shrink()
            ],
          ),
        ),
      ],
    );
  }
}

class _BigTicketPainter extends CustomPainter {
  _BigTicketPainter({@required this.sizeLeadingIcon});

  final Size sizeLeadingIcon;

  final _paintTicketPath = Paint()
    ..color = Colors.white
    ..style = PaintingStyle.fill;

  final _paintDash = Paint()
    ..color = Colors.grey[300]
    ..strokeWidth = 1.1
    ..style = PaintingStyle.stroke;

  void paint(Canvas canvas, Size size) {
    final width = size.width;
    final height = size.height;
    final maxRadius = width <= height ? width : height;
    const inputRadius = 20.0;
    final radius = inputRadius >= maxRadius ? maxRadius : inputRadius;
    const radiusSmallCircle = 20.0;
    const sizeSmallCircle = radiusSmallCircle / 2;
    final xE = width - radius; // real xE position
    final bcWidth = sizeLeadingIcon.width; // size icon
    final xDash = sizeLeadingIcon.width + radiusSmallCircle / 4;
    const yDashStart = radiusSmallCircle / 4 + 2;
    const stepHeight = 5;
    const gapDash = 2;
    final yDashEnd = height - (radiusSmallCircle / 4) - stepHeight;
    const yFirstStep = yDashStart + stepHeight;
    final List<Offset> steps = [];

    final ticketPath = (Path()
          // Replay (1)
          ..moveTo(0, height / 2)
          ..lineTo(0, radius / 2)
          ..addArc(Rect.fromLTWH(0, 0, radius, radius), pi, pi / 2)
          ..lineTo(bcWidth, 0)
          ..arcTo(
              Rect.fromLTWH(bcWidth, -sizeSmallCircle / 2, sizeSmallCircle - 1,
                  sizeSmallCircle),
              pi,
              -pi,
              false)
          ..lineTo(width - radius / 2, 0)
          ..arcTo(Rect.fromLTRB(xE, 0, width, radius), -pi / 2, pi / 2, false)
          ..lineTo(width, height / 2)
          ..lineTo(0, height / 2))
        .transform(Matrix4.rotationX(pi).storage)
        .transform(Matrix4.translationValues(0, height, 0).storage)
          // Replay (2)
          ..moveTo(0, height / 2)
          ..lineTo(0, radius / 2)
          ..addArc(Rect.fromLTWH(0, 0, radius, radius), pi, pi / 2)
          ..lineTo(bcWidth, 0)
          ..arcTo(
              Rect.fromLTWH(bcWidth, -sizeSmallCircle / 2, sizeSmallCircle - 1,
                  sizeSmallCircle),
              pi,
              -pi,
              false)
          ..lineTo(width - radius / 2, 0)
          ..arcTo(Rect.fromLTRB(xE, 0, width, radius), -pi / 2, pi / 2, false)
          ..lineTo(width, height / 2)
          ..lineTo(0, height / 2);

    // Cause drop frames, uses BoxDecoration instead.
    // canvas.drawShadow(
    //     ticketPath.transform(Matrix4.translationValues(0, -2, 0).storage),
    //     Colors.grey[100],
    //     3,
    //     false);
    canvas.drawPath(ticketPath, _paintTicketPath);

    steps.add(Offset(xDash, yDashStart));
    steps.add(Offset(xDash, yFirstStep));
    for (double c = yFirstStep + gapDash;
        c < yDashEnd;
        c += gapDash + stepHeight) {
      steps.add(Offset(xDash, c));
      steps.add(Offset(xDash, c + stepHeight));
    }

    canvas.drawPoints(PointMode.lines, steps, _paintDash);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}

class _SavedButton extends StatefulWidget {
  final VoidCallback onTap;
  final bool isSave;
  _SavedButton({@required this.onTap, this.isSave});
  @override
  __SavedButtonState createState() => __SavedButtonState();
}

class __SavedButtonState extends State<_SavedButton> {
  bool _isSave;
  @override
  void initState() {
    _isSave = widget.isSave ?? false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        if (!_isSave) {
          widget.onTap();
          setState(() {
            _isSave = true;
          });
        }
      },
      child: Text(
        _isSave ? 'Đã lưu' : "Lưu mã",
        style: TextStyle(
            color: _isSave ? Colors.grey[300] : AppColor.primary, fontSize: 15),
      ),
    );
  }
}

enum BigTicketMode { select, none, save }
