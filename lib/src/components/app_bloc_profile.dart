import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/controllers/auth_controller.dart';
import 'package:genie/src/repositories/profile.repo.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:provider/provider.dart';

class AppBlocProfile extends StatefulWidget {
  const AppBlocProfile();

  @override
  _AppBlocProfileState createState() => _AppBlocProfileState();
}

class _AppBlocProfileState extends State<AppBlocProfile> {
  final _profileRepo = ProfileRepository();
  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<AuthController>(context);
    var check;
    Future.delayed(Duration(seconds: 1)).then((value) {
      check = AppClient.instance.checkExistToken();
      setState(() {});
    });

    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          child: auth.customerNotifer.value != null
              ? _Logged(
                  profileRepo: _profileRepo,
                  auth: auth,
                )
              : _NotLogin(),
        ),
      ],
    );
  }
}

class _Logged extends StatefulWidget {
  final ProfileRepository profileRepo;
  final AuthController auth;

  const _Logged({Key key, @required this.auth, @required this.profileRepo})
      : super(key: key);

  @override
  __LoggedState createState() => __LoggedState();
}

class __LoggedState extends State<_Logged> {
  Future _loadGreeting;
  @override
  void initState() {
    _loadGreeting = widget.profileRepo.getOneSettingByKey('GREETING_MSG');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      width: double.infinity,
      child: Row(
        children: [
          widget.auth.customerNotifer.value.avatar != null
              ? CircleAvatar(
                  backgroundImage:
                      NetworkImage(widget.auth.customerNotifer.value.avatar),
                  radius: 30,
                )
              : const CircleAvatar(
                  backgroundImage: AssetImage(AppAsset.errorPlaceHolder),
                  maxRadius: 45,
                  backgroundColor: Colors.grey),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'Xin chào ${widget.auth.customerNotifer.value.name.split(' ').last},',
                    style: TextStyle(
                      color: AppColor.primary,
                      fontSize: 24,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: FutureBuilder<dynamic>(
                      future: _loadGreeting,
                      builder: (context, snapshot) {
                        if (snapshot.data != null)
                          return Text(
                            snapshot?.data['value']?.toString(),
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                            ),
                          );
                        return Text('');
                      }),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _NotLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      width: double.infinity,
      child: Row(
        children: [
          CircleAvatar(
            backgroundImage: AssetImage(AppAsset.avatar),
            radius: 30,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'Chào mừng đến với Genie!',
                    style: TextStyle(
                      color: AppColor.primary,
                      fontSize: 20,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'Đăng nhập để sử dụng ứng dụng.',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
