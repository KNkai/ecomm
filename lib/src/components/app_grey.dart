import 'package:flutter/material.dart';
import 'package:genie/src/utils/app_color.dart';

class AppGray extends StatelessWidget {
  final String left;
  final String right;
  final TextStyle leftStyle;
  final TextStyle rightStyle;
  final Color barColor;
  final VoidCallback onRightTap;
  const AppGray({
    this.left,
    this.right,
    this.onRightTap,
    this.leftStyle,
    this.rightStyle = const TextStyle(color: AppColor.primary, fontSize: 12),
    this.barColor,
  });
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: ColoredBox(
        color: barColor == null ? const Color(0xff9B9B9B).withOpacity(0.1) : barColor,
        child: Padding(
          child: Row(
            children: [
              Expanded(
                child: Text(
                  left ?? '',
                  style: leftStyle,
                ),
              ),
              if (right != null)
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    if (onRightTap != null) onRightTap();
                  },
                  child: Text(
                    right,
                    style: rightStyle,
                  ),
                )
            ],
          ),
          padding: const EdgeInsets.symmetric(horizontal: 26, vertical: 15),
        ),
      ),
    );
  }
}
