import 'package:flutter/material.dart';

class ErrorText extends StatelessWidget {
  const ErrorText({Key key, this.onClick}) : super(key: key);
  final GestureTapCallback onClick;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 40),
      child: Center(
        child: Column(children: [
          const Text('Có lỗi xảy ra xin thử lại'),
          const SizedBox(
            height: 10,
          ),
          Align(
            alignment: Alignment.center,
            child: GestureDetector(
              onTap: onClick,
              behavior: HitTestBehavior.opaque,
              child: const Icon(Icons.refresh),
            ),
          )
        ]),
      ),
    );
  }
}
