import 'package:flutter/material.dart';

class RefreshScrollView extends StatelessWidget {
  final Future<void> Function() onRefresh;
  final Future<void> Function() onLoadMore;
  final Widget child;

  RefreshScrollView({
    Key key,
    this.onRefresh,
    @required this.child,
    this.onLoadMore,
  });

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollNotification>(
        child: RefreshIndicator(
            onRefresh: () async {
              if (onRefresh != null) await onRefresh();
            },
            child: child),
        onNotification: (notification) {
          if (notification.metrics.pixels ==
                  notification.metrics.maxScrollExtent &&
              onLoadMore != null) {
            onLoadMore();
          }
          return true;
        });
  }
}
