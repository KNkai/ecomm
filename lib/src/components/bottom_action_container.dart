import 'package:flutter/material.dart';

class BottomActionContainer extends StatelessWidget {
  final Widget child;
  final Widget bottomChild;
  const BottomActionContainer({@required this.child, @required this.bottomChild});
  @override
  Widget build(Object context) {
    final safeAreaPadding = MediaQuery.of(context).padding.bottom;

    return Column(
      children: [
        Expanded(
          child: child,
        ),
        SizedBox(
          width: double.infinity,
          child: DecoratedBox(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey[200],
                    spreadRadius: 3.5,
                    blurRadius: 20,
                    offset: const Offset(10, 0), // changes position of shadow
                  ),
                ],
                color: Colors.white,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10))),
            child: Padding(
              padding: EdgeInsets.only(
                  top: 24, bottom: safeAreaPadding == 0 ? 24 : safeAreaPadding),
              child: Center(child: this.bottomChild),
            ),
          ),
        )
      ],
    );
  }
}
