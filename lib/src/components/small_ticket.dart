import 'dart:math';

import 'package:flutter/material.dart';
import 'package:genie/src/utils/app_color.dart';

class SmallTicket extends StatefulWidget {
  final String title;
  final Color color;
  const SmallTicket({@required this.title, this.color = AppColor.primary});

  @override
  _SmallTicketState createState() => _SmallTicketState();
}

class _SmallTicketState extends State<SmallTicket> {
  @override
  Widget build(BuildContext context) {
    if (widget.title == null || widget.title.isEmpty)
      return const SizedBox.shrink();
    return Stack(
      children: [
        Positioned.fill(
          child: LayoutBuilder(builder: (context, constraints) {
            return CustomPaint(
              size: Size(constraints.biggest.width, constraints.biggest.height),
              painter:
                  _TicketPainter(colors: widget.color, title: widget.title),
            );
          }),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 6, horizontal: 10),
          child: Text(
            widget.title,
            style: TextStyle(color: Colors.white, fontSize: 14),
          ),
        )
      ],
    );
  }
}

class _TicketPainter extends CustomPainter {
  final Color colors;
  final String title;
  _TicketPainter({this.colors, @required this.title});

  var _paint = Paint();

  @override
  void paint(Canvas canvas, Size size) {
    final width = size.width; // 20 padding
    final height = size.height; // 20 padding
    final maxRadius = width <= height ? width : height;
    const inputRadius = 20.0;
    final radius = inputRadius >= maxRadius ? maxRadius : inputRadius;
    const radiusSmallCircle = 14.0;
    const sizeSmallCircle = radiusSmallCircle / 2;
    final xE = width - radius; // real xE position
    final bcWidth = .26 * width;
    final bdWidth = bcWidth + sizeSmallCircle;

    // _paintHalf(
    //     canvas, height, radius, bcWidth, sizeSmallCircle, bdWidth, width, xE);
    // canvas.transform(Matrix4.rotationX(pi).storage);
    // canvas.transform(Matrix4.translationValues(0, -height, 0).storage);
    // // low half
    // _paintHalf(
    //     canvas, height, radius, bcWidth, sizeSmallCircle, bdWidth, width, xE);
    //
    var paint = Paint()..color = colors;
    var _halfPath = Path()
      ..moveTo(0, height / 2)
      ..lineTo(0, radius / 2)
      ..arcTo(Rect.fromLTWH(0, 0, radius, radius), pi, pi / 2, false)
      ..lineTo(radius / 2, 0)
      ..lineTo(bcWidth, 0)
      ..arcTo(
          Rect.fromLTWH(
              bcWidth - 1,
              -sizeSmallCircle / 2 - 0.5,
              sizeSmallCircle - 1, // -1 to avoid gap
              sizeSmallCircle),
          pi,
          -pi,
          false)
      ..lineTo(bdWidth - 2, 0)
      ..lineTo(width - radius / 2, 0)
      ..arcTo(Rect.fromLTRB(xE, 0, width, radius), -pi / 2, pi / 2, false)
      ..lineTo(width, height / 2)
      ..close();

    Matrix4 matrix4 = Matrix4.rotationX(135);

    var _fullPath = Path()
      ..addPath(_halfPath, Offset(0, 0))
      ..addPath(_halfPath.transform(matrix4.storage), Offset(0, height));

    canvas.drawPath(_fullPath, paint);
  }

  void _paintHalf(Canvas canvas, double height, double radius, double bcWidth,
      double sizeSmallCircle, double bdWidth, double width, double xE) {
    _paint = Paint()
      ..color = colors ?? AppColor.primary
      ..style = PaintingStyle.stroke
      ..strokeJoin = StrokeJoin.round
      ..strokeWidth = 1.2;
    // OA
    canvas.drawLine(Offset(0, height / 2), Offset(0, radius / 2), _paint);
    // AB
    canvas.drawArc(
        Rect.fromLTWH(0, 0, radius, radius), pi, pi / 2, false, _paint);
    // BC
    canvas.drawLine(Offset(radius / 2, 0), Offset(bcWidth, 0), _paint);
    // CD
    canvas.drawArc(
        Rect.fromLTWH(
            bcWidth - 1,
            -sizeSmallCircle / 2 - 0.5,
            sizeSmallCircle - 1, // -1 to avoid gap
            sizeSmallCircle),
        pi,
        -pi,
        false,
        _paint);
    // DE
    canvas.drawLine(Offset(bdWidth - 2, 0), Offset(width - radius / 2, 0),
        _paint); // adjust
    // EF
    canvas.drawArc(
        Rect.fromLTRB(xE, 0, width, radius), -pi / 2, pi / 2, false, _paint);
    // FG
    canvas.drawLine(Offset(width, radius / 2 - 1), Offset(width, height / 2),
        _paint); // -1 remove gap
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
