import 'package:flutter/material.dart';
import 'package:genie/src/pages/cart/cart_page.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';

import '../helper/cart_data.dart';

class AppCartIcon extends StatelessWidget {
  final sizeIcon = 30.0;

  const AppCartIcon();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        CartPage.push(context);
      },
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 10, top: 10),
            child:
                Image.asset(AppAsset.cart, width: sizeIcon, height: sizeIcon),
          ),
          Positioned(
              right: 0,
              top: 0,
              child: ValueListenableBuilder(
                  valueListenable: CartData.getCtl(context).stateNumberCart,
                  builder: (_, quantity, __) {
                    if (quantity == null || quantity == 0)
                      return const SizedBox.shrink();
                    return DecoratedBox(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: Text(quantity.toString(),
                              style:
                                  const TextStyle(color: Colors.white, fontSize: 10)),
                        ),
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(40)),
                            color: AppColor.primary));
                  }))
        ],
      ),
    );
  }
}
