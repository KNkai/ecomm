import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:genie/src/pages/detail_product/components/star_rating.dart';

import '../../../models/block.model.dart';
import '../../../utils/app_asset.dart';
import '../../../utils/app_color.dart';
import '../../../utils/util.dart';
import '../../ribbon.dart';
import 'block_item_action_handler.dart';

class ItemProductGrid extends StatelessWidget {
  final BlockItem item;
  final bool fullSize;
  final bool isFavourite;

  const ItemProductGrid(
      {Key key,
      @required this.item,
      this.fullSize = true,
      this.isFavourite = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlockItemActionHandler(
      action: item.action,
      title: item.title,
      child: DecoratedBox(
        decoration: BoxDecoration(
            border: Border.all(width: 0.8, color: Colors.grey[200])),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 100,
              child: Stack(
                children: [
                  Ribbon(
                      title: 'Giảm',
                      content: item.product?.getSaleContent ?? '',
                      showRibbon: item.product?.hasSale ?? false,
                      child: Padding(
                        padding: const EdgeInsets.all(1),
                        child: FadeInImage.assetNetwork(
                          placeholder: AppAsset.errorPlaceHolder,
                          image: item.image,
                          fit: BoxFit.fitWidth,
                        ),
                      )),
                  if (isFavourite)
                    Positioned(
                      top: 10,
                      right: 10,
                      child: GestureDetector(
                        child: SizedBox(
                          width: 25,
                          child: Image.asset(AppAsset.trash),
                        ),
                        onTap: () {
                          print(item.product.id);
                        },
                      ),
                    ),
                ],
              ),
            ),
            const SizedBox(height: 5),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Text(
                    item.title + "\n\n",
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 13),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: StarRating(
                    value: item?.product?.rating ?? 0,
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                (item.product.countdownDate != null &&
                        item.product.countdownDate.isAfter(DateTime.now()))
                    ? Container(
                        height: 15,
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Text(
                            'Còn lại ${item.product?.countdownDate?.toLocal()?.difference(DateTime.now())?.inDays ?? ""} ngày',
                            textAlign: TextAlign.end,
                            style: const TextStyle(
                                fontSize: 10, color: Colors.red)),
                      )
                    : const SizedBox(height: 15),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Divider(
                thickness: 0.8,
                color: Colors.grey[200],
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(left: 8, right: 8, top: 6, bottom: 6),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (fullSize || item.product.hasSale) ...[
                        const SizedBox(height: 7),
                        Text(
                          item.product.hasSale
                              ? appCurrency(item.product.basePrice)
                              : '',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                              color: Colors.grey,
                              fontSize: 10,
                              decoration: TextDecoration.lineThrough),
                        ),
                      ],
                      Text(
                        appCurrency(item.product.getSalePrice),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                            color: AppColor.primary, fontSize: 13),
                      ),
                      Text(
                          'Đã bán ${item.product?.saleQty == "" || item.product?.saleQty == null ? "0" : item.product?.saleQty.toString()} ${item.product.remaining == null ? '' : " / Còn " + item.product.remaining.toString()}',
                          textAlign: TextAlign.start,
                          style: const TextStyle(fontSize: 10))
                    ],
                  ),

                  // const SizedBox(
                  //   height: 7,
                  // ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
