import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/list_block/components/block_item_action_handler.dart';
import 'package:genie/src/models/block.model.dart';
import 'package:genie/src/pages/detail_product/components/detail_discount_component.dart';
import 'package:genie/src/utils/size_config.dart';

class BlockBannerCharacter extends StatefulWidget {
  final Block block;

  const BlockBannerCharacter({Key key, this.block}) : super(key: key);

  @override
  _BlockBannerCharacterState createState() => _BlockBannerCharacterState();
}

class _BlockBannerCharacterState extends State<BlockBannerCharacter>
    with TickerProviderStateMixin {
  List<BlockItem> listBlockItem = [];

  AnimationController _animationController;
  Animation<double> animation;
  bool isForward = false;
  BlockItemActionHandler currentActionHandler;
  List<BlockItemActionHandler> _listBlockItemActionHandler = [];

  int count = 0;
  @override
  void initState() {
    if (widget.block.items != null) {
      widget.block.items.forEach((e) {
        if (!e.hidden)
          _listBlockItemActionHandler.add(
            BlockItemActionHandler(
              action: e.action,
              child: Text(
                e.title,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: HexColor(e.textColor),
                ),
              ),
            ),
          );
      });
      if (_listBlockItemActionHandler.length != 0) {
        currentActionHandler = _listBlockItemActionHandler[0];
        _animationController = AnimationController(
          vsync: this,
          duration: Duration(milliseconds: 3000),
        );
        animation = Tween(begin: 0.0, end: 500.0).animate(_animationController);
        runListString(_listBlockItemActionHandler);
      }
    }
    super.initState();
  }

  runListString(List<BlockItemActionHandler> listBlocAction) async {
    print(listBlocAction.length);
    int count = listBlocAction.length;
    while (true) {
      if (count != 0) {
        --count;

        await Future.delayed(Duration(seconds: 3));
        _animationController.forward();
        await Future.delayed(Duration(seconds: 4));
        _animationController.reverse();
        await Future.delayed(Duration(seconds: 4));
        currentActionHandler = listBlocAction[count];
      } else {
        count = listBlocAction.length;
      }
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return _listBlockItemActionHandler.length == 0 ||
            _listBlockItemActionHandler == null
        ? const SizedBox()
        : AnimatedBuilder(
            animation: _animationController,
            builder: (context, snapshot) {
              return Container(
                padding: EdgeInsets.only(left: 10),
                width: double.infinity,
                child: Transform.translate(
                  offset: Offset(
                      Tween(begin: 0.0, end: SizeConfig.widthDevice)
                          .animate(_animationController)
                          .value,
                      0.0),
                  child: currentActionHandler,
                ),
              );
            },
          );
  }
}
