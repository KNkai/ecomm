import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/widgets.dart';
import 'package:genie/src/utils/app_asset.dart';

import '../../../models/block.model.dart';
import '../../../utils/size_config.dart';
import 'block_item_action_handler.dart';

class ImageScroll extends StatefulWidget {
  final Block block;

  const ImageScroll({Key key, @required this.block}) : super(key: key);

  @override
  _ImageScrollState createState() => _ImageScrollState();
}

class _ImageScrollState extends State<ImageScroll> {
  final _controller = CarouselController();

  @override
  void dispose() {
    _controller.stopAutoPlay();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      carouselController: _controller,
      options: CarouselOptions(
          reverse: false,
          autoPlay: true,
          autoPlayInterval: const Duration(seconds: 5),
          aspectRatio: (207 / 80),
          autoPlayAnimationDuration: const Duration(seconds: 3),
          viewportFraction: 1),
      items: widget.block.items.map((i) {
        return Builder(
          builder: (BuildContext context) {
            return BlockItemActionHandler(
                title: i.title,
                action: i.action,
                child: FadeInImage.assetNetwork(
                  placeholder: AppAsset.banner,
                  image: i.image,
                  fit: BoxFit.fill,
                  width: SizeConfig.widthDevice,
                ));
          },
        );
      }).toList(),
    );
  }
}
