import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../models/block.model.dart';
import 'block_item_action_handler.dart';

class VerticalIconTitleScroll extends StatelessWidget {
  final _scrollPaddingTop = 0.0;
  final _widthWidget = 110.0;

  final Block block;

  const VerticalIconTitleScroll({Key key, @required this.block})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    const _heightWidget = 170;
    return SizedBox(
      height: _heightWidget + _scrollPaddingTop,
      child: ListView.separated(
          padding: const EdgeInsets.only(left: 26, right: 26),
          separatorBuilder: (_, __) {
            return const SizedBox(width: 15);
          },
          scrollDirection: Axis.horizontal,
          itemCount: block.items.length,
          itemBuilder: (_, index) => BlockItemActionHandler(
                action: block.items[index].action,
                title: block.items[index].title,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey[200],
                          spreadRadius: 5,
                          blurRadius: 10,
                          offset: const Offset(0, 10),
                          // changes position of shadow
                        ),
                      ],
                      border: Border.all(color: Colors.grey[350], width: .5)),
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: SizedBox(
                      width: _widthWidget,
                      child: Padding(
                        padding: const EdgeInsets.all(8),
                        child: Column(
                          children: [
                            Image.network(
                              block.items[index].image,
                              fit: BoxFit.contain,
                              width: 76,
                              height: 88,
                            ),
                            const SizedBox(height: 11),
                            Expanded(
                              child: Text(
                                block.items[index].title ?? "",
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.center,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              )),
    );
  }
}
