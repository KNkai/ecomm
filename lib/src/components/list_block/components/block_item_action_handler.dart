import 'package:flutter/material.dart';
import 'package:genie/src/components/app_webview.dart';
import 'package:genie/src/models/block.model.dart';
import 'package:genie/src/pages/category/category_page.dart';
import 'package:genie/src/pages/default_screen/default_screen_page.dart';
import 'package:genie/src/pages/detail_product/detail_product_page.dart';

class BlockItemActionHandler extends StatelessWidget {
  const BlockItemActionHandler({
    Key key,
    @required this.child,
    @required this.action,
    this.title,
  }) : super(key: key);

  final Widget child;
  final String title;
  final BlockItemAction action;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (action != null) {
          switch (action.type) {
            case "GO_TO_SCREEN":
              DefaultScreenPage.push(
                  context: context, screenId: action.screenId, title: title);
              return;
            case "GO_TO_CATEGORY":
              return CategoryPage.push(context,
                  categoryId: action.categoryId, title: title);
            case "GO_TO_PRODUCT":
              return DetailProductPage.push(
                  context: context, productId: action.productId);
            default:
              return AppWebView.push(
                  context: context, title: title, url: action.link);
              return;
          }
        }
      },
      child: child,
    );
  }
}
