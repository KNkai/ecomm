import 'package:flutter/material.dart';
import 'package:genie/src/components/list_block/components/block_item_action_handler.dart';
import 'package:genie/src/models/block.model.dart';
import 'package:genie/src/utils/size_config.dart';

class ImageStack extends StatelessWidget {
  final Block block;
  const ImageStack({@required this.block});
  @override
  Widget build(BuildContext context) {
    if (block?.items == null && block.items.isEmpty) return const SizedBox.shrink();
    return Column(
      children: [
        for (int c = 0; c < block.items.length; c++)
          BlockItemActionHandler(
            child: Image.network(
              block.items[c].image,
              width: double.infinity,
              height: SizeConfig.widthDevice / (207 / 80),
              fit: BoxFit.fill,
            ),
            action: block.items[c].action,
            title: block.items[c].title,
          ),
      ],
    );
  }
}
