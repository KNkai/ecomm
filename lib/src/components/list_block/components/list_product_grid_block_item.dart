import 'package:flutter/material.dart';
import 'package:genie/src/models/block.model.dart';
import 'package:genie/src/utils/util.dart';

import 'item_product_grid.dart';

class ListProductGridBlockItem extends StatelessWidget {
  const ListProductGridBlockItem({
    Key key,
    @required this.items,
    this.isFavourite = false,
    // this.favoriteController,
  }) : super(key: key);

  final List<BlockItem> items;
  final bool isFavourite;
  // final FavoriteController favoriteController;

  @override
  Widget build(BuildContext context) {
    var chunks = chunk<BlockItem>(items, 3);
    return Column(
        children: List.generate(
      chunks.length,
      (index) {
        bool fullSize =
            chunks[index].indexWhere((element) => element.product.hasSale) !=
                -1;
        return Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 1,
                  child: ItemProductGrid(
                    item: chunks[index][0],
                    fullSize: fullSize,
                    isFavourite: isFavourite,
                    // favoriteController: favoriteController,
                  ),
                ),
                if (chunks[index].length == 3) ...[
                  const SizedBox(width: 8),
                  Expanded(
                    child: ItemProductGrid(
                      item: chunks[index][1],
                      fullSize: fullSize,
                      isFavourite: isFavourite,
                    ),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: ItemProductGrid(
                      item: chunks[index][2],
                      fullSize: fullSize,
                      isFavourite: isFavourite,
                    ),
                  ),
                ],
                if (chunks[index].length == 2) ...[
                  const SizedBox(width: 8),
                  Expanded(
                    child: ItemProductGrid(
                      item: chunks[index][1],
                      fullSize: fullSize,
                      isFavourite: isFavourite,
                    ),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: SizedBox(),
                  ),
              ],
                if (chunks[index].length == 1) ...[
                  const SizedBox(width: 8),
                  Expanded(
                    flex: 1,
                    child: SizedBox(),
                  ),const SizedBox(width: 8),
                  Expanded(
                    flex: 1,
                    child: SizedBox(),
                  ),
                ],
              ],
            ),
            const SizedBox(height: 8),
          ],
        );
      },
    ));
  }
}
