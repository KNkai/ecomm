import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../models/block.model.dart';
import '../../../utils/app_color.dart';
import '../../../utils/hex_color.dart';
import '../../../utils/size_config.dart';
import 'block_item_action_handler.dart';

class HorizontalIconTitleScroll extends StatelessWidget {
  final _widthItem = SizeConfig.widthDevice * 0.65628;
  final _heightItem = 103.0;
  final _heightContent = 86.0;

  final Block block;

  HorizontalIconTitleScroll({Key key, @required this.block}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: _heightItem,
        child: ListView.separated(
            padding: const EdgeInsets.symmetric(horizontal: 26),
            separatorBuilder: (_, __) => const SizedBox(width: 15),
            scrollDirection: Axis.horizontal,
            itemCount: block.items.length,
            itemBuilder: (_, index) {
              final item = block.items[index];
              return BlockItemActionHandler(
                title: item.title,
                action: item.action,
                child: SizedBox(
                  width: _widthItem,
                  child: Stack(
                    children: [
                      Positioned(
                          left: 0,
                          top: 0,
                          bottom: 0,
                          width: _widthItem,
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child: SizedBox(
                              height: _heightContent,
                              width: double.infinity,
                              child: DecoratedBox(
                                decoration: BoxDecoration(
                                    color: HexColor.parseTry(
                                        item.backgroundColor,
                                        errorColor: AppColor.primary),
                                    borderRadius:
                                        BorderRadius.circular(_heightItem)),
                              ),
                            ),
                          )),
                      Positioned(
                        child: Row(
                          children: [
                            const SizedBox(
                              width: 7,
                            ),
                            Image.network(
                              item.image,
                              fit: BoxFit.fitHeight,
                            ),
                            const SizedBox(
                              width: 7,
                            ),
                            Expanded(
                              child: Padding(
                                padding:
                                    EdgeInsets.only(right: _heightItem / 3),
                                child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: SizedBox(
                                    width: double.infinity,
                                    height: _heightContent,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Expanded(
                                          child: Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              item.title ?? "",
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: HexColor.parseTry(
                                                      item.textColor,
                                                      errorColor:
                                                          Colors.white)),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(item.subtitle ?? "",
                                                style: TextStyle(
                                                    fontSize: 13,
                                                    color: Colors.grey[300])),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                      // Positioned(
                      //     left: 0,
                      //     right: 0,
                      //     bottom: 0,
                      //     height: _heightContent,
                      //     child: DecoratedBox(
                      //       decoration: BoxDecoration(
                      //           color: HexColor.parseTry(item.backgroundColor,
                      //               errorColor: AppColor.primary),
                      //           borderRadius:
                      //               BorderRadius.circular(_widthItem)),
                      //     )),
                      // Positioned(
                      //     bottom: 0,
                      //     left: 15, // offset padding (*)
                      //     top: 0,
                      //     child: Image.network(item.image)),
                      // Positioned(
                      //     bottom: 0,
                      //     left: _widthImg + 15, // offset padding (*)
                      //     width: 0.64581 * _widthItem,
                      //     height: _heightContent,
                      //     child: Padding(
                      //       padding: EdgeInsets.all(10),
                      //       child: Row(
                      //                                       children: [Column(
                      //           crossAxisAlignment: CrossAxisAlignment.start,
                      //           children: [
                      //             Expanded(
                      //               flex: 2,
                      //               child: Text(
                      //                 item.title ?? "",
                      //                 maxLines: 2,
                      //                 overflow: TextOverflow.ellipsis,
                      //                 style: TextStyle(
                      //                     color: HexColor.parseTry(
                      //                         item.textColor,
                      //                         errorColor: Colors.white)),
                      //               ),
                      //             ),
                      //             const SizedBox(
                      //               height: 7,
                      //             ),
                      //             Expanded(
                      //               flex: 1,
                      //               child: Text(item.subtitle ?? "",
                      //                   style:
                      //                       TextStyle(color: Colors.grey[300])),
                      //             )
                      //           ],
                      //         )],
                      //       ),
                    ],
                  ),
                ),
              );
            }));
  }
}
