import 'package:flutter/material.dart';
import 'package:genie/src/components/big_ticket.dart';
import 'package:genie/src/components/block_container.dart';
import 'package:genie/src/components/list_block/controller/list_block_controller.dart';
import 'package:genie/src/controllers/auth_controller.dart';
import 'package:genie/src/helper/auth.dart';
import 'package:genie/src/models/block.model.dart';
import 'package:genie/src/pages/voucher_detail/voucher_detail.dart';
import 'package:provider/provider.dart';

class PromotionBlock extends StatelessWidget {
  final Block block;
  final ListBlockController controller;

  const PromotionBlock({@required this.block, @required this.controller});

  @override
  Widget build(BuildContext context) {
    return BlockContainer(
      title: block?.title,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Column(
          children: List.generate(block?.items?.length, (index) {
            return GestureDetector(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: OneCampaignInBlock(
                  item: block.items[index],
                  controller: controller,
                ),
              ),
              onTap: () {
                VoucherDetail.push(
                  context: context,
                  campaign: block?.items[index].campaign,
                  title: block?.items[index]?.title,
                  subTitle: block?.items[index]?.subtitle,
                  imageCampaign: block?.items[index]?.image,
                );
              },
            );
          }),
        ),
      ),
    );
  }
}

class OneCampaignInBlock extends StatelessWidget {
  const OneCampaignInBlock({
    Key key,
    @required this.item,
    this.controller,
  }) : super(key: key);

  final BlockItem item;
  final ListBlockController controller;

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: BigTicket(
        title: item.title,
        smallTitle: item.subtitle,
        urlImage: item.image,
        mode: BigTicketMode.save,
        isSave: item.campaign?.taken,
        onSave: () {
          Auth.checkExistUser(
            context: context,
            onAlreadyLogin: () {
              if (item.id != null) controller.getVoucher(item.id);
            },
            onLoginSuccess: () {
              // AppNavBar.push(context);
              Navigator.of(context).pop();
              var auth = Provider.of<AuthController>(context, listen: false);
              auth.getCustomer();
            },
          );
        },
      ),
    );
  }
}
