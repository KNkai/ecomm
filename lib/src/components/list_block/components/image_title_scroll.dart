import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../models/block.model.dart';
import '../../../utils/app_color.dart';
import '../../../utils/hex_color.dart';
import '../../../utils/size_config.dart';
import '../../block_container.dart';
import 'block_item_action_handler.dart';

class ImageTitleScroll extends StatelessWidget {
  final _widthImage = SizeConfig.widthDevice * 0.8768115942;

  final Block block;

  ImageTitleScroll({Key key, @required this.block});

  @override
  Widget build(BuildContext context) {
    final _heightImage = _widthImage / (726 / 320);
    final _heightItem = _heightImage + 50;

    return BlockContainer(
        title: block.title,
        hexColor: block.backgroundColor,
        titleColor: HexColor.parseTry(this.block.textColor),
        child: SizedBox(
          height: _heightItem,
          child: ListView.separated(
              padding: const EdgeInsets.only(
                left: 26,
                right: 26,
              ),
              separatorBuilder: (_, __) => const SizedBox(width: 15),
              scrollDirection: Axis.horizontal,
              itemCount: block.items.length,
              itemBuilder: (_, index) {
                var item = block.items[index];
                var padding2 = Padding(
                  padding: const EdgeInsets.only(top: 11, bottom: 0, left: 6),
                  child: Text(
                    item.title,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(color: Colors.white, fontSize: 18),
                  ),
                );
                return SizedBox(
                  width: _widthImage,
                  child: BlockItemActionHandler(
                    action: item.action,
                    title: item.title,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.network(
                          item.image,
                          width: _widthImage,
                          height: _heightImage,
                          fit: BoxFit.fill,
                        ),
                        Expanded(
                          child: ColoredBox(
                            color: AppColor.primary,
                            child: ConstrainedBox(
                              constraints: const BoxConstraints.expand(),
                              child: padding2,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }),
        ));
  }
}
