import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:genie/src/utils/util.dart';

import '../../../models/block.model.dart';
import '../../../utils/app_asset.dart';
import '../../../utils/app_color.dart';
import '../../../utils/size_config.dart';
import '../../block_container.dart';
import '../../ribbon.dart';
import 'block_item_action_handler.dart';

class ProductScroll extends StatelessWidget {
  final _widthItem = SizeConfig.widthDevice * 0.80458;

  final Block block;

  ProductScroll({Key key, @required this.block}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    const _heightItem = 135.0;
    // const _heightItem = 124.0;
    return BlockContainer(
      title: block.title ?? "",
      child: SizedBox(
        height: _heightItem,
        child: ListView(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          children: List.generate(block.items.length, (index) {
            final item = block.items[index];
            return BlockItemActionHandler(
              action: item.action,
              title: item.title,
              child: Container(
                margin: EdgeInsets.only(right: 20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[200],
                        spreadRadius: 5,
                        blurRadius: 10,
                        offset:
                            const Offset(0, 10), // / changes position of shadow
                      )
                    ],
                    border: Border.all(color: Colors.grey[300], width: .8)),
                width: _widthItem,
                // height: _heightItem,
                child: Padding(
                  padding: const EdgeInsets.all(
                      1), // to see the border's color line when image has white background.
                  child: Row(
                    children: [
                      SizedBox(
                        width: _heightItem,
                        // height: _heightItem,
                        child: Ribbon(
                            showRibbon: item.product?.hasSale ?? false,
                            title: 'Giảm',
                            content: item?.product?.getSaleContent ?? '',
                            child: FadeInImage.assetNetwork(
                              placeholder: AppAsset.errorPlaceHolder,
                              image: item.image,
                              fit: BoxFit.cover,
                            )),
                      ),
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 10, top: 8, bottom: 5),
                          child: _SelectSuitableItem(item: item),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }
}

class _SelectSuitableItem extends StatelessWidget {
  const _SelectSuitableItem({@required this.item});
  final BlockItem item;
  @override
  Widget build(BuildContext context) {
    final hasSale = item?.product?.hasSale ?? false;

    if (!hasSale && !appCheckEmptyString(item?.subtitle))
      return _NoBottomItem(item: item);

    if (!hasSale && appCheckEmptyString(item?.subtitle))
      return _MiddleItem(item: item);

    if (hasSale && appCheckEmptyString(item?.subtitle))
      return _NoHeaderItem(item: item);

    return _FullItem(item: item);
  }
}

class _FullItem extends StatelessWidget {
  const _FullItem({@required this.item});
  final BlockItem item;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          flex: 1,
          child: _HeaderSubTitle(
            item: item,
          ),
        ),
        Expanded(
          flex: 3,
          child: _MiddleItem(
            item: item,
          ),
        ),
        Expanded(
          flex: 1,
          child: _BottomItem(
            item: item,
          ),
        )
      ],
    );
  }
}

class _NoHeaderItem extends StatelessWidget {
  const _NoHeaderItem({@required this.item});
  final BlockItem item;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _MiddleItem(
          item: item,
        ),
        const SizedBox(
          height: 10,
        ),
        _BottomItem(
          item: item,
        )
      ],
    );
  }
}

class _NoBottomItem extends StatelessWidget {
  const _NoBottomItem({@required this.item});
  final BlockItem item;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _HeaderSubTitle(
          item: item,
        ),
        const SizedBox(
          height: 5,
        ),
        _MiddleItem(
          item: item,
        ),
      ],
    );
  }
}

///
/// Sub widgets
///
class _HeaderSubTitle extends StatelessWidget {
  const _HeaderSubTitle({@required this.item});
  final BlockItem item;
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Text(
        item.subtitle,
        maxLines: 1,
        style: const TextStyle(fontSize: 14, color: Colors.grey),
      ),
    );
  }
}

class _MiddleItem extends StatelessWidget {
  const _MiddleItem({@required this.item, this.paddingTitleBottom = 5});
  final BlockItem item;
  final double paddingTitleBottom;
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(item.title ?? "", maxLines: 2),
          SizedBox(
            height: paddingTitleBottom,
          ),
          Text(
            appCurrency(item.product?.getSalePrice),
            style: const TextStyle(color: AppColor.primary),
          ),
        ],
      ),
    );
  }
}

class _BottomItem extends StatelessWidget {
  const _BottomItem({@required this.item});
  final BlockItem item;
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Text(
        appCurrency(item?.product?.basePrice),
        style: const TextStyle(
            color: Colors.grey,
            fontSize: 14,
            decoration: TextDecoration.lineThrough),
      ),
    );
  }
}
