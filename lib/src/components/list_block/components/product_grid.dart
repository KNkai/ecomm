import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../models/block.model.dart';
import '../../../utils/hex_color.dart';
import '../../../utils/size_config.dart';
import '../../block_container.dart';
import 'list_product_grid_block_item.dart';

class ProductGrid extends StatelessWidget {
  final Block block;

  const ProductGrid({Key key, @required this.block});

  @override
  Widget build(BuildContext context) {
    return BlockContainer(
        title: block.title ?? "",
        titleColor: HexColor.parseTry(block.textColor),
        child: SizedBox(
          width: SizeConfig.widthDevice,
          child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 26),
              child: ListProductGridBlockItem(items: block.items)),
        ));
  }
}
