import 'dart:async';
import 'dart:convert';

import 'package:genie/src/models/block.model.dart';
import 'package:genie/src/repositories/screen.repo.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../repositories/block.repo.dart';
import 'list_block_controller.dart';

class PromotionBlockController extends ListBlockController {
  PromotionBlockController() {
    loadScreen();
  }
  final stateListBlock = StreamController<StateListBlock>();
  final _repo = BlockRepository();
  final _screenRepo = ScreenRepository();

  bool _lockLoad = false;

  Future eventLoad() async {
    await getFromCache();
    if (!_lockLoad) {
      _lockLoad = true;
      return _repo.getAllPromotionBlock().then((value) {
        try {
          if (value == null)
            stateListBlock.add(StateListBlockEmpty());
          else {
            stateListBlock.add(StateListBlockLoaded(blocks: value));
            setToCache(value);
          }
        } finally {
          _lockLoad = false;
        }
      });
    }
  }

  Future<List<Block>> getFromCache() async {
    SharedPreferences spref = await SharedPreferences.getInstance();
    var json = spref.get('promotion-screen-block');
    if (json == null) return null;
    List<Block> blocks =
        jsonDecode(json).map<Block>((i) => Block.fromJson(i)).toList();
    stateListBlock.add(StateListBlockLoaded(blocks: blocks));
    return blocks;
  }

  Future setToCache(List<Block> blocks) async {
    SharedPreferences spref = await SharedPreferences.getInstance();
    spref.setString('promotion-screen-block',
        jsonEncode(blocks.map((e) => e.toJson()).toList()));
  }

  Future loadScreen() async {
    _screenRepo.getPromotionScreen().then((res) {
      stateScreen.add(res);
      screen = res;
    });
  }
}
