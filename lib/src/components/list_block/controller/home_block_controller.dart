import 'dart:async';
import 'dart:convert';
import 'package:genie/src/repositories/screen.repo.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../models/block.model.dart';
import '../../../repositories/block.repo.dart';
import 'list_block_controller.dart';

class HomeBlockController extends ListBlockController {
  HomeBlockController() {
    loadScreen();
  }
  final stateListBlock = StreamController<StateListBlock>();
  final _repo = BlockRepository();
  final _screenRepo = ScreenRepository();

  bool _lockLoad = false;

  Future eventLoad() async {
    await getFromCache();
    if (!_lockLoad) {
      _lockLoad = true;
      return _repo.getAllHomeBlock().then((value) {
        try {
          if (value == null)
            stateListBlock.add(StateListBlockEmpty());
          else {
            stateListBlock.add(StateListBlockLoaded(blocks: value));
            setToCache(value);
          }
        } finally {
          _lockLoad = false;
        }
      });
    }
  }

  Future<List<Block>> getFromCache() async {
    SharedPreferences spref = await SharedPreferences.getInstance();
    var json = spref.get('home-screen-block');
    if (json == null) return null;
    List<Block> blocks =
        jsonDecode(json).map<Block>((i) => Block.fromJson(i)).toList();
    stateListBlock.add(StateListBlockLoaded(blocks: blocks));
    return blocks;
  }

  Future setToCache(List<Block> blocks) async {
    SharedPreferences spref = await SharedPreferences.getInstance();
    spref.setString('home-screen-block',
        jsonEncode(blocks.map((e) => e.toJson()).toList()));
  }

  Future loadScreen() async {
    _screenRepo.getHomeScreen().then((res) {
      stateScreen.add(res);
      screen = res;
    });
  }

  Future<bool> isShowSuggest() async {
    return await _screenRepo.getHomeScreen().then((res) {
      return res?.showSuggestProduct;
    });
  }
}
