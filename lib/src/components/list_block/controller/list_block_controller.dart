import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:genie/src/models/block.model.dart';
import 'package:genie/src/models/screen.model.dart';
import 'package:genie/src/repositories/block.repo.dart';
import 'package:genie/src/repositories/screen.repo.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ListBlockController {
  ListBlockController({this.screenId}) {
    loadScreen();
  }

  final String screenId;
  final stateListBlock = StreamController<StateListBlock>.broadcast();
  final _repo = BlockRepository();
  final _screenRepo = ScreenRepository();

  bool _lockLoad = false;
  final stateScreen = StreamController<Screen>.broadcast();
  Screen screen;

  Future eventLoad() {
    if (!_lockLoad) {
      _lockLoad = true;
      return _repo.getAllBlockDashboard(screenId).then((value) {
        try {
          if (value == null)
            stateListBlock.add(StateListBlockEmpty());
          else
            stateListBlock.add(StateListBlockLoaded(blocks: value));
        } finally {
          _lockLoad = false;
        }
      });
    }
    return null;
  }

  Future<List<Block>> getFromCache() async {
    SharedPreferences spref = await SharedPreferences.getInstance();
    var json = spref.get('screen-$screenId');
    if (json == null) return null;
    List<Block> blocks =
        jsonDecode(json).map<Block>((i) => Block.fromJson(i)).toList();
    stateListBlock.add(StateListBlockLoaded(blocks: blocks));
    return blocks;
  }

  Future setToCache(List<Block> blocks) async {
    SharedPreferences spref = await SharedPreferences.getInstance();
    spref.setString(
        'screen-$screenId', jsonEncode(blocks.map((e) => e.toJson()).toList()));
  }

  Future loadScreen() async {
    _screenRepo
        .getOne(query: _screenRepo.screenFragment, id: screenId)
        .then((res) {
      stateScreen.add(res);
      screen = res;
    });
  }

  Future getVoucher(String idCampaign) async {
    _screenRepo.getCampaign(idCampaign);
    ;
  }
}

abstract class StateListBlock {}

class StateListBlockLoaded extends StateListBlock {
  StateListBlockLoaded({@required this.blocks});
  final List<Block> blocks;
}

class StateListBlockEmpty extends StateListBlock {}
