import 'package:flutter/material.dart';
import 'package:genie/src/components/list_block/components/block_banner_character.dart';
import 'package:genie/src/components/list_block/components/horizontal_icon_title_scroll.dart';
import 'package:genie/src/components/list_block/components/image_stack.dart';
import 'package:genie/src/components/list_block/components/promotion_block.dart';
import 'package:genie/src/controllers/auth_controller.dart';
import 'package:genie/src/models/customer.model.dart';
import 'package:genie/src/utils/constants.dart';
import 'package:provider/provider.dart';

import '../../models/block.model.dart';
import 'components/image_scroll.dart';
import 'components/image_title_scroll.dart';
import 'components/product_grid.dart';
import 'components/product_scroll.dart';
import 'components/vertical_icon_title_scroll.dart';
import 'controller/list_block_controller.dart';

class ChildColumnListBlock extends StatelessWidget {
  final ListBlockController controller;
  final ListBlockMode mode;
  const ChildColumnListBlock({@required this.controller, @required this.mode});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<StateListBlock>(
      stream: controller.stateListBlock.stream,
      builder: (context, ss) {
        if (ss?.data == null) {
          return FutureBuilder(
              future: Future.delayed(const Duration(milliseconds: 100)),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  final childX = SizedBox(
                    height: 100,
                    child: Center(
                      child: kLoadingSpinner,
                    ),
                  );
                  return mode == ListBlockMode.company
                      ? SliverToBoxAdapter(
                          child: childX,
                        )
                      : childX;
                }
                return mode == ListBlockMode.company
                    ? SliverToBoxAdapter(child: SizedBox.shrink())
                    : SizedBox.shrink();
              }); // only first time
        }

        if (ss.data is StateListBlockEmpty) {
          final child = const SizedBox(
            height: 100,
            child: Center(
              child: Text('Dữ liệu bị trống'),
            ),
          );
          return mode == ListBlockMode.company
              ? SliverToBoxAdapter(child: child)
              : child;
        }

        final StateListBlockLoaded blocksLoaded = ss.data;
        if (mode == ListBlockMode.alone) {
          return ListView.builder(
              itemCount: blocksLoaded.blocks.length,
              itemBuilder: (_, index) {
                return _ParseBlock(blocksLoaded.blocks[index], controller);
              });
        } else {
          return SliverList(
            delegate: SliverChildBuilderDelegate(
              (_, index) {
                return _ParseBlock(blocksLoaded.blocks[index], controller);
              },
              childCount: blocksLoaded.blocks.length,
            ),
          );
        }
      },
    );
  }
}

class _ParseBlock extends StatelessWidget {
  final Block block;
  final ListBlockController controller;

  _ParseBlock(this.block, this.controller);
  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<AuthController>(context, listen: false);
    switch (block.type) {
      case "LIST_BANNER":
        return Padding(
          key: ObjectKey(block.id),
          padding: const EdgeInsets.symmetric(vertical: 11),
          child: ImageScroll(block: block),
        );
      case "LIST_ICON":
        return Padding(
            key: ObjectKey(block.id),
            padding: const EdgeInsets.symmetric(vertical: 11),
            child: VerticalIconTitleScroll(block: block));
      case "LIST_PRODUCT":
        return Padding(
          key: ObjectKey(block.id),
          padding: const EdgeInsets.symmetric(vertical: 1),
          child: ProductScroll(block: block),
        );
      case "LIST_SIMPLE_CARD":
        return Padding(
          key: ObjectKey(block.id),
          padding: const EdgeInsets.symmetric(vertical: 11),
          child: ImageTitleScroll(block: block),
        );
      case "LIST_CAMPAIGN":
        return Padding(
          key: ObjectKey(block.id),
          padding: const EdgeInsets.symmetric(vertical: 11),
          child: PromotionBlock(
            block: block,
            controller: controller,
          ),
        );
      case "LIST_PRODUCT_GRID_CARD":
        return Padding(
          key: ObjectKey(block.id),
          padding: const EdgeInsets.symmetric(vertical: 11),
          child: ProductGrid(block: block),
        );
      case "LIST_HORIZONTAL_ICON_TITLE":
        return Padding(
            key: ObjectKey(block.id),
            padding: const EdgeInsets.symmetric(vertical: 11),
            child: HorizontalIconTitleScroll(block: block));
      case "LIST_VERTIAL_BANNER":
        return Padding(
          key: ObjectKey(block.id),
          padding: const EdgeInsets.symmetric(vertical: 11),
          child: ImageStack(
            block: block,
          ),
        );
      case "CHARACTER_BANNER":
        return ValueListenableBuilder<Customer>(
            valueListenable: auth.customerNotifer,
            builder: (_, data, __) {
              if (data != null) {
                return Padding(
                  key: ObjectKey(block.id),
                  padding: const EdgeInsets.symmetric(vertical: 11),
                  child: BlockBannerCharacter(
                    block: block,
                  ),
                );
              } else
                return const SizedBox();
            });
      case "LIST_IMAGE":
      default:
        return Container(
          key: ObjectKey(block.id),
        );
    }
  }
}

enum ListBlockMode { alone, company }
