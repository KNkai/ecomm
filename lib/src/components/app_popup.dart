import 'package:flutter/material.dart';
import 'package:genie/src/models/block.model.dart';
import 'package:genie/src/models/screen.model.dart';

class AppPopup extends StatelessWidget {
  final BuildContext context;
  final bool isShow;
  final ScreenPopup screenPopup;
  final BlockItemAction action;

  const AppPopup({
    Key key,
    this.context,
    this.isShow,
    this.screenPopup,
    this.action,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        // child: showDialogIfFirstLoaded(context, isShow, screenPopup, action),
        );
  }
}
