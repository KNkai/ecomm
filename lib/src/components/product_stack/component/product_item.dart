import 'package:flutter/material.dart';
import 'package:genie/src/components/list_block/components/block_item_action_handler.dart';
import 'package:genie/src/pages/detail_product/components/star_rating.dart';
import 'package:genie/src/pages/favorites/favorite_controller.dart';
import 'package:genie/src/pages/purchased_later/purchased_later_controller.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';

import '../../../models/block.model.dart';
import '../../ribbon.dart';

class ProductItem extends StatelessWidget {
  final BlockItem item;
  final bool isFavourite;
  final PurchasedLaterController purchasedLaterController;
  final FavoriteController favouriteController;
  final GestureTapCallback onDelete;

  const ProductItem({
    Key key,
    @required this.item,
    this.purchasedLaterController = null,
    this.isFavourite = false,
    this.favouriteController = null,
    this.onDelete,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlockItemActionHandler(
      action: item.action,
      title: item.title,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 26),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 78,
              width: 78,
              child: Ribbon(
                title: 'Giảm',
                content: item?.product?.getSaleContent ?? '',
                showRibbon: item.product.hasSale ?? false,
                child: FadeInImage.assetNetwork(
                  image: item.image,
                  placeholder: AppAsset.errorPlaceHolder,
                ),
              ),
            ), // square,
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Stack(
                      children: [
                        Padding(
                          padding: isFavourite
                              ? const EdgeInsets.only(right: 30)
                              : const EdgeInsets.only(right: 0),
                          child: Text(
                            item?.title ?? '',
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(fontSize: 18),
                          ),
                        ),
                        if (isFavourite)
                          Positioned(
                            top: 0,
                            right: 0,
                            child: GestureDetector(
                              child: Image.asset(
                                AppAsset.trash,
                                width: 20,
                              ),
                              onTap: () {
                                if (onDelete != null) onDelete();
                              },
                            ),
                          ),
                      ],
                    ),
                    const SizedBox(height: 9),
                    Row(
                      children: [
                        Text(
                          appCurrency(item?.product?.getSalePrice),
                          style: const TextStyle(
                              color: AppColor.primary, fontSize: 14),
                        ),
                        const SizedBox(width: 10),
                        if (item.product.hasSale ?? false)
                          Text(
                            appCurrency(item?.product?.basePrice),
                            style: const TextStyle(
                              color: Colors.grey,
                              fontSize: 14,
                              decoration: TextDecoration.lineThrough,
                            ),
                          ),
                      ],
                    ),
                    const SizedBox(height: 5),
                    Row(
                      children: [
                        StarRating(
                          value: item?.product?.rating ?? 0,
                          size: 16,
                        ),
                        Expanded(
                          child: SizedBox(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                RichText(
                                  text: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: "Đã bán ",
                                        style: TextStyle(
                                          color: Colors.black,
                                        ),
                                      ),
                                      TextSpan(
                                        text: item?.product?.saleQty == null
                                            ? "0"
                                            : item?.product?.saleQty.toString(),
                                        style: const TextStyle(
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
