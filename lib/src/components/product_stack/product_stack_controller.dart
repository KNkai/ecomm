import 'package:flutter/material.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/repositories/product.repo.dart';

class ProductStackController extends InitLoadMoreSearchController<Product> {
  ProductStackController._(
      {@required InitListProtocol initProtocol,
      @required LoadMoreProtocol loadMoreProtocol})
      : super(
            initProtocol: initProtocol,
            loadMoreProtocol: loadMoreProtocol,
            hashLoadingInit: false);

  factory ProductStackController() {
    final productStackProtocol = ProductStackRepository();
    return ProductStackController._(
        initProtocol: productStackProtocol,
        loadMoreProtocol: productStackProtocol);
  }
}
