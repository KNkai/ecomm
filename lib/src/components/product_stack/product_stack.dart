import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/product.model.dart';

import 'component/product_item.dart';
import 'product_stack_controller.dart';

class ProductStack extends StatelessWidget {
  final ProductStackController controller;

  const ProductStack({@required this.controller});
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<ListState>(
      stream: controller.stateList.stream,
      builder: (context, ss) {
        if (ss?.data == null || (ss.data is ListEmptyState))
          // return const SizedBox.shrink();
          return SliverToBoxAdapter(
            child: const SizedBox.shrink(),
          );

        final ListLoadState<Product> stateProduct = ss.data;
        // return ListView.separated(
        //     itemBuilder: (_, index) =>
        //         ProductItem(item: stateProduct.data[index].blockItem),
        //     separatorBuilder: (_, __) =>
        //         Divider(color: Colors.grey[100], thickness: 3),
        //     itemCount: stateProduct.data.length);
        return SliverList(
            delegate: SliverChildBuilderDelegate((_, i) {
          final index = i ~/ 2;
          if (i.isEven) return Divider(color: Colors.grey[100], thickness: 3);
          return ProductItem(item: stateProduct.data[index].blockItem);
        }, childCount: max(0, stateProduct.data.length * 2 - 1)));
      },
    );
  }
}
