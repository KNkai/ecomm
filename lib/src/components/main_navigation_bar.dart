import 'dart:async';

import 'package:flutter/material.dart';
import 'package:genie/src/components/app_cart_icon.dart';
import 'package:genie/src/pages/review_screen/review_screen_page.dart';
import 'package:genie/src/pages/search/components/search_page_keyword.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';

class MainNavigationBar extends StatelessWidget {
  const MainNavigationBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
        child: Stack(children: [
          LogoTopBarDashBoard(),
          Positioned.fill(
            left: 10,
            child: Row(
              children: [
                Expanded(
                  child: GestureDetector(
                    child: Align(
                      child: Image.asset(
                        AppAsset.drawer,
                        width: 20,
                      ),
                      alignment: Alignment.centerLeft,
                    ),
                    onTap: () {
                      Scaffold.of(context).openDrawer();
                    },
                  ),
                ),
              ],
            ),
          ),
          Positioned.fill(
            child: Row(
              children: [
                Expanded(
                  flex: 10,
                  child: Align(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: IconButton(
                        onPressed: () {
                          SearchPageKeyWord.push(context);
                        },
                        icon: const Icon(
                          Icons.search,
                          color: AppColor.primary,
                          size: 30,
                        ),
                      ),
                    ),
                    alignment: Alignment.centerRight,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                const Expanded(
                    flex: 1,
                    child: Align(
                      child: AppCartIcon(),
                      alignment: Alignment.centerRight,
                    )),
              ],
            ),
          )
        ]));
  }
}

class LogoTopBarDashBoard extends StatefulWidget {
  const LogoTopBarDashBoard({
    Key key,
  }) : super(key: key);

  @override
  _LogoTopBarDashBoardState createState() => _LogoTopBarDashBoardState();
}

class _LogoTopBarDashBoardState extends State<LogoTopBarDashBoard> {
  Timer _timer;
  int _start = 10;
  int count = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        child: Center(
          child: FadeInImage.assetNetwork(
              placeholder: AppAsset.dashboardIc,
              image: 'https://genie.mcom.app/api/setting/redirect/APP_LOGO',
              height: 50),
        ),
        onTap: () {
          if (_start == 10) {
            count = 0;
            startTimer();
          } else {
            count++;
            if (count == 10) {
              ReviewScreenPage.go(context);
              count = 0;
              _timer.cancel();
              _start = 10;
            }
          }
          print("count: $count");
        },
      ),
    );
  }

  void startTimer() {
    if (_timer != null) {
      _timer.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        if (_start < 1) {
          timer.cancel();
        } else {
          _start = _start - 1;
        }
        print("start: $_start");
        setState(() {});
      });
    }
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}
