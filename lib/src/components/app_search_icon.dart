import 'package:flutter/material.dart';
import 'package:genie/src/pages/search/components/search_page_keyword.dart';
import 'package:genie/src/utils/app_color.dart';

class AppSearchIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        SearchPageKeyWord.push(context);
      },
      icon: const Icon(
        Icons.search,
        color: AppColor.primary,
        size: 30,
      ),
    );
  }
}
