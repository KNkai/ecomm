import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:genie/src/pages/profile/controllers/profile_controller.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';
import 'package:provider/provider.dart';

class AppFloatingContact extends StatelessWidget {
  const AppFloatingContact({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final profileController =
        Provider.of<ProfileController>(context, listen: false);
    return Padding(
      padding: const EdgeInsets.only(bottom: 10, right: 10),
      child: SpeedDial(
        icon: Icons.support_agent,
        activeIcon: Icons.close_rounded,
        animationSpeed: 100,
        backgroundColor: AppColor.primary,
        overlayOpacity: 0,
        iconTheme: IconThemeData(color: Colors.white),
        curve: Curves.bounceInOut,
        // animatedIcon: AnimatedIcons.arrow_menu,
        children: [
          SpeedDialChild(
            child: ValueListenableBuilder(
              valueListenable: profileController.hotlineNotifer,
              builder: (_, hotline, __) {
                return FloatingActionButton(
                  backgroundColor: AppColor.primary,
                  child: Icon(Icons.phone_iphone_rounded),
                  onPressed: () {
                    makePhoneCall('tel:$hotline');
                  },
                );
              },
            ),
          ),
          SpeedDialChild(
            child: ValueListenableBuilder(
                valueListenable: profileController.chatLinkNotifer,
                builder: (_, chatlink, __) {
                  return FloatingActionButton(
                    backgroundColor: AppColor.primary,
                    child: Icon(Icons.chat_rounded),
                    onPressed: () {
                      openMessenger(chatlink);
                    },
                  );
                }),
          ),
        ],
      ),
    );
  }
}
/*
ValueListenableBuilder(
                      valueListenable: _profileController.chatLinkNotifer,
                      builder: (_, chatLink, __) {
                        return ItemListView(
                          titleItem: "Chat Với Genie",
                          iconUrl: AppAsset.chat,
                          isTrailing: false,
                          afterClick: () {
                            openMessenger(chatLink);
                          },
                        );
                      }),*/
