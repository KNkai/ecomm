import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:genie/src/utils/app_asset.dart';

class Search extends StatelessWidget {
  final void Function(String) onChanged;

  const Search({@required this.onChanged});

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: Colors.grey[200],
              spreadRadius: 5,
              blurRadius: 10,
              offset: const Offset(0, 10), // changes position of shadow
            ),
          ]),
      child: Row(
        children: [
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: TextField(
              onChanged: onChanged,
              style: const TextStyle(fontSize: 20),
              cursorColor: Colors.grey,
              decoration: const InputDecoration(
                  hintText: 'Tìm kiếm',
                  hintStyle: TextStyle(fontSize: 18, color: Color(0xff9B9B9B)),
                  border: InputBorder.none),
            ),
          ),
          Image.asset(
            AppAsset.search,
            width: 20,
            height: 20,
          ),
          const SizedBox(
            width: 10,
          )
        ],
      ),
    );
  }
}
