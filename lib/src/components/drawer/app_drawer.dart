import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/drawer/controller/drawer_controller.dart';
import 'package:genie/src/models/menu.model.dart';

import 'components/drawer_header.dart';
import 'components/drawer_tile_menu.dart';

class AppDrawer extends StatefulWidget {
  const AppDrawer();

  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  final CustomDrawerController drawerController = CustomDrawerController();

  Menu menu;
  bool isStart = false;

  @override
  void initState() {
    drawerController.loadMenu();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DrawerState>(
      stream: drawerController.drawerState.stream,
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.active)
          return Container();
        if (snapshot.data is DrawerLoadedSuccess) {
          if (snapshot.data != null) {
            menu = (snapshot.data as DrawerLoadedSuccess).menu;
          }
          return Drawer(
            child: ListView(
              children: [
                const DrawerLogo(),
                DrawerTile(
                  menu: menu,
                ),
              ],
            ),
          );
        } else {
          return Drawer(
            child: ListView(
              children: [
                const DrawerLogo(),
                Visibility(
                  visible: isStart,
                  child: const SizedBox(
                    width: 120,
                    height: 120,
                    child: Center(
                      child: CupertinoActivityIndicator(
                        radius: 20,

                        // // ignore: deprecated_member_use
                        // iOSVersionStyle:
                        //     CupertinoActivityIndicatorIOSVersionStyle.iOS14,
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: !isStart,
                  child: Column(
                    children: [
                      const Text("Đang có lỗi vui lòng tải lại"),
                      IconButton(
                        onPressed: () {
                          setState(() {
                            drawerController.loadMenu();
                          });
                        },
                        icon: const Icon(Icons.refresh),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        }
      },
    );
  }
}
