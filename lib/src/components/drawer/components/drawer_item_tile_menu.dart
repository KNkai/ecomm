import 'package:flutter/material.dart';
import 'package:genie/src/models/menu.model.dart';
import 'package:genie/src/utils/app_color.dart';

import 'drawer_item_tile_submenu.dart';

class ItemTileMenu extends StatefulWidget {
  final Section section;
  const ItemTileMenu({
    Key key,
    this.section,
  }) : super(key: key);

  @override
  _TileMenuState createState() => _TileMenuState();
}

class _TileMenuState extends State<ItemTileMenu> {
  bool isUpdate = false;
  Section _section;
  @override
  void initState() {
    if (widget.section == null) {
      _section = Section(title: "", subMenus: null);
    } else {
      _section = widget.section;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<SubMenu> _listSubMenu = _section.subMenus.toList();
    List<ItemTileSubMenu> _listTileSubMenu = [];
    for (int i = 0; i < _listSubMenu.length; i++) {
      _listTileSubMenu.add(ItemTileSubMenu(
        subMenu: _listSubMenu[i],
      ));
    }

    return Column(
      children: [
        ListTile(
          title: Text(_section.title),
          trailing: IconButton(
            icon: Icon(
              !isUpdate
                  ? Icons.keyboard_arrow_right
                  : Icons.keyboard_arrow_down,
              color: AppColor.separate,
            ),
            onPressed: () {
              setState(() {
                isUpdate ? isUpdate = false : isUpdate = true;
              });
            },
          ),
        ),
        Visibility(
          visible: isUpdate,
          child: SizedBox(
            child: Column(
              children: _listTileSubMenu,
            ),
          ),
        ),
        const Divider(),
      ],
    );
  }
}
