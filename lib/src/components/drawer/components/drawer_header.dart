import 'package:flutter/material.dart';
import 'package:genie/src/utils/app_color.dart';

class DrawerLogo extends StatelessWidget {
  const DrawerLogo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DrawerHeader(
      decoration: const BoxDecoration(color: AppColor.primary),
      child: Stack(
        children: [
          Positioned(
            bottom: 0,
            left: 0,
            child: Container(
              width: 100,
              height: 100,
              decoration: const BoxDecoration(
                  image: DecorationImage(image: AssetImage("assets/logo.png"))),
            ),
          ),
        ],
      ),
    );
  }
}
