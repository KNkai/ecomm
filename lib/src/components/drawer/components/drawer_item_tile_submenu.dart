import 'package:flutter/material.dart';
import 'package:genie/src/components/list_block/components/block_item_action_handler.dart';
import 'package:genie/src/models/block.model.dart';
import 'package:genie/src/models/menu.model.dart';

class ItemTileSubMenu extends StatelessWidget {
  final SubMenu subMenu;
  const ItemTileSubMenu({
    Key key,
    this.subMenu,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SubMenu _subMenu;
    if (subMenu == null) {
      _subMenu = SubMenu();
    } else {
      _subMenu = subMenu;
    }
    BlockItem _blocItem = BlockItem();
    _blocItem.action = _subMenu.action;

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: BlockItemActionHandler(
        child: ListTile(
          title: Text(_subMenu.title),
        ),
        action: _blocItem.action,
        title: _subMenu.title,
      ),
    );
  }
}
