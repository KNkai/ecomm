import 'package:flutter/material.dart';
import 'package:genie/src/models/menu.model.dart';

import 'drawer_item_tile_menu.dart';

class DrawerTile extends StatelessWidget {
  final Menu menu;

  const DrawerTile({Key key, this.menu}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Menu _menu;
    List<Section> _listSection;
    List<ItemTileMenu> _listTileMenu = [];
    if (menu == null) {
      _menu = Menu(name: "", sections: null);
    } else {
      _menu = menu;
      _listSection = _menu.sections.toList();
      for (int i = 0; i < _listSection.length; i++) {
        _listTileMenu.add(ItemTileMenu(
          section: _listSection[i],
        ));
      }
    }

    return SizedBox(
      child: Column(
        children: _listTileMenu,
      ),
    );
  }
}
