import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:genie/src/models/menu.model.dart';
import 'package:genie/src/repositories/menu.repo.dart';

abstract class DrawerState {}

// class DrawerLoadingState extends DrawerState {}

class DrawerLoadedSuccess extends DrawerState {
  final Menu menu;

  DrawerLoadedSuccess({this.menu});
}

class DrawerLoadedFailed extends DrawerState {}

class CustomDrawerController {
  MenuRepository menuRepository = MenuRepository();
  final drawerLoading = ValueNotifier<bool>(false);
  final drawerState = StreamController<DrawerState>();
  var menu = Menu();

  loadMenu() {
    getMenu().then((value) {
      if (value == null) {
        _updateMenuState(DrawerLoadedFailed());
        return "failed";
      } else {
        _updateMenuState(DrawerLoadedSuccess(menu: value));
        return "OK";
      }
    }).catchError((onError) {
      return onError.toString();
    });
  }

  void _updateMenuState(DrawerState state) {
    drawerLoading.value = true;
    try {
      if (state is DrawerLoadedSuccess) {
        menu = state.menu;
      } else {
        menu = null;
      }
    } finally {
      drawerLoading.value = false;
    }
    drawerState.add(state);
  }

  Future getMenu() {
    return menuRepository.getAllMenu().then((value) {
      return menu = Menu.fromJson(value);
    });
  }
}
