import 'package:flutter/material.dart';

class Ribbon extends StatelessWidget {
  final String title;
  final String content;
  final Widget child;
  final bool showRibbon;
  const Ribbon(
      {@required this.title,
      @required this.child,
      @required this.content,
      this.showRibbon = false});
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(child: child),
        if (showRibbon)
          Positioned.fill(
              child: CustomPaint(
            painter: RibbonPainter(title: title, content: content),
          ))
      ],
    );
  }
}

class RibbonPainter extends CustomPainter {
  final String title;
  final String content;
  final bool isRight;

  RibbonPainter({this.isRight = false, @required this.title, this.content});

  final _paint = Paint()
    ..color = Colors.red
    ..style = PaintingStyle.fill;
  final _textPainter = TextPainter(
      textDirection: TextDirection.ltr, textAlign: TextAlign.center);
  final _scaleXRibbon = 1.2;
  final _scaleYRibboon = 1.7;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.transform(
        Matrix4.translationValues(isRight ? (size.width - 35) : 5, 0, 0)
            .storage);
    _textPainter.text = TextSpan(children: [
      TextSpan(
          text: this.title,
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 10)),
      const TextSpan(
        text: '\n',
      ),
      TextSpan(
          text: this.content,
          style: const TextStyle(
              color: Color(0xffFFE9A3),
              fontWeight: FontWeight.bold,
              fontSize: 10))
    ]);
    _textPainter.layout(
      minWidth: 0,
      maxWidth: double.infinity,
    );
    canvas.drawPath(
        (Path()
              ..lineTo(0, _textPainter.height)
              ..lineTo(_textPainter.width / 2, _textPainter.height * 0.8)
              ..lineTo(_textPainter.width, _textPainter.height)
              ..lineTo(_textPainter.width, 0))
            .transform(
                Matrix4.diagonal3Values(_scaleXRibbon, _scaleYRibboon, 0.0)
                    .storage), // padding
        _paint);
    canvas.transform(Matrix4.translationValues(
            (-(1.0 - _scaleXRibbon) / 2) * _textPainter.width,
            2,
            0) //horizontal scale up also center position of text move
        .storage);
    _textPainter.paint(canvas, const Offset(0, 0));
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
