import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/models/customer.model.dart';
import 'package:genie/src/pages/dashboard/dashboard_page.dart';
import 'package:genie/src/pages/notification/notify_page.dart';
import 'package:genie/src/pages/profile/user_page.dart';
import 'package:genie/src/pages/promotion/promotion_page.dart';
import 'package:genie/src/pages/store/store_page.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:provider/provider.dart';

import '../controllers/auth_controller.dart';
import '../helper/app_firebase_service.dart';
import '../helper/auth.dart';
import 'drawer/app_drawer.dart';

class AppNavBar extends StatefulWidget {
  static void push(BuildContext context) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => AppNavBar()), (route) => false);
  }

  @override
  _AppNavBarState createState() => _AppNavBarState();
}

class _AppNavBarState extends State<AppNavBar> {
  final _assetHome = const AssetImage(AppAsset.home);

  final _assetPercentSelect = const AssetImage(AppAsset.percentSelect);

  final _assetStoreSelect = const AssetImage(AppAsset.storeSelect);

  final _assetNotificationSelect =
      const AssetImage(AppAsset.notificationSelect);

  final _assetUserSelect = const AssetImage(AppAsset.userSelect);

  final _navigatorController = PageController();

  final _stateIndex = ValueNotifier(0);

  // var _customer = CustomerState();

  // final profileController = ProfileController();

  @override
  Widget build(BuildContext context) {
    // var _stateCustomer = ValueNotifier<CustomerState>(_customer);
    AppFirebaseService.instance.init(context);
    // _customer = Provider.of<CustomerState>(context);
    var authCtrl = Provider.of<AuthController>(context, listen: false);
    return Scaffold(
      drawer: const AppDrawer(),
      backgroundColor: Colors.white,
      body: SafeArea(
        child: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: _navigatorController,
          children: [
            const DashboardPage(),
            const PromotionPage(),
            const StorePage(),
            const NotifyPage(),
            const UserPage()
          ],
        ),
      ),
      bottomNavigationBar: ValueListenableBuilder<int>(
        valueListenable: _stateIndex,
        builder: (_, index, ___) {
          return ValueListenableBuilder<Customer>(
              valueListenable: authCtrl.customerNotifer,
              builder: (_, customer, ___) {
                return BottomNavigationBar(
                  currentIndex: index,
                  backgroundColor: Colors.white,
                  type: BottomNavigationBarType.fixed,
                  items: [
                    BottomNavigationBarItem(
                      activeIcon: const _Icon(
                        assetImage: AssetImage(AppAsset.homeSelect),
                      ),
                      icon: _Icon(assetImage: _assetHome),
                      label: 'Trang chủ',
                    ),
                    BottomNavigationBarItem(
                      activeIcon: _Icon(
                        assetImage: _assetPercentSelect,
                      ),
                      icon:
                          const _Icon(assetImage: AssetImage(AppAsset.percent)),
                      label: "Ưu đãi",
                    ),
                    BottomNavigationBarItem(
                      activeIcon: _Icon(
                        assetImage: _assetStoreSelect,
                      ),
                      icon: const _Icon(assetImage: AssetImage(AppAsset.store)),
                      label: "Cửa Hàng",
                    ),
                    BottomNavigationBarItem(
                      activeIcon: _Icon(
                          isNotiActive: true,
                          assetImage: _assetNotificationSelect,
                          isNoti: customer != null && customer.unseenNotify > 0,
                          countNoti: ((customer?.unseenNotify == null
                                          ? 0
                                          : customer?.unseenNotify) >
                                      99
                                  ? "99+"
                                  : "${customer?.unseenNotify}") ??
                              "0"),
                      icon: _Icon(
                          assetImage: AssetImage(AppAsset.notification),
                          isNoti: customer != null && customer.unseenNotify > 0,
                          countNoti: ((customer?.unseenNotify == null
                                          ? 0
                                          : customer?.unseenNotify) >
                                      99
                                  ? "99+"
                                  : "${customer?.unseenNotify}") ??
                              "0"),
                      label: "Thông báo",
                    ),
                    BottomNavigationBarItem(
                      activeIcon: _Icon(assetImage: _assetUserSelect),
                      icon: const _Icon(assetImage: AssetImage(AppAsset.user)),
                      label: "Tài khoản",
                    ),
                  ],
                  onTap: (index) {
                    if (index >= 3 && customer == null) {
                      Auth.checkExistUser(
                        context: context,
                        onAlreadyLogin: () {
                          _stateIndex.value = index;
                          _navigatorController.jumpToPage(index);
                        },
                        onLoginSuccess: () {
                          _stateIndex.value = index;
                          _navigatorController.jumpToPage(index);
                        },
                      );
                    } else {
                      _stateIndex.value = index;
                      _navigatorController.jumpToPage(index);
                    }
                  },
                );
              });
        },
      ),
      // floatingActionButton: Padding(
      //   padding: const EdgeInsets.only(bottom: 10, right: 10),
      //   child: SpeedDial(
      //     icon: Icons.call_end_rounded,
      //     activeIcon: Icons.close_rounded,
      //     animationSpeed: 100,
      //     backgroundColor: AppColor.primary,
      //     overlayOpacity: 0,
      //     iconTheme: IconThemeData(color: Colors.white),
      //     // animatedIcon: AnimatedIcons.arrow_menu,
      //     children: [
      //       SpeedDialChild(
      //           child: FloatingActionButton(
      //         onPressed: () {},
      //       )),
      //       SpeedDialChild(
      //           child: FloatingActionButton(
      //         onPressed: () {},
      //       )),
      //     ],
      //   ),
      // ),
    );
  }
}

class _Icon extends StatelessWidget {
  final bool isNoti;
  final bool isNotiActive;
  final String countNoti;
  final AssetImage assetImage;
  const _Icon(
      {@required this.assetImage,
      this.isNoti = false,
      this.countNoti,
      this.isNotiActive = false});
  @override
  Widget build(BuildContext context) {
    return isNoti
        ? Padding(
            padding: const EdgeInsets.only(bottom: 10, top: 16),
            child: Stack(
              children: [
                Positioned(
                  child: Image(
                    image: assetImage,
                    width: 25,
                    height: 27,
                  ),
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  child: Container(
                    width: 15,
                    height: 15,
                    decoration: BoxDecoration(
                      border: !isNotiActive
                          ? Border.all(color: Colors.white, width: 0.3)
                          : Border.all(color: AppColor.primary, width: 0.3),
                      color: !isNotiActive ? AppColor.primary : Colors.white,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          '$countNoti',
                          style: TextStyle(
                              fontSize: 10,
                              color: !isNotiActive
                                  ? Colors.white
                                  : AppColor.primary),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ))
        : Padding(
            padding: const EdgeInsets.only(bottom: 10, top: 16),
            child: Image(
              image: assetImage,
              width: 25,
              height: 27,
            ),
          );
  }
}
