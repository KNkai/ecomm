import 'package:flutter/material.dart';

class FinalInfoItem extends StatelessWidget {
  final String title;
  final String value;
  final bool minus;
  final Color color;
  const FinalInfoItem(
      {@required this.title,
      this.color = Colors.white,
      @required this.value,
      this.minus = false});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: Row(
        children: [
          Text(
            title,
            style: TextStyle(color: color),
          ),
          Expanded(
            child: Text(
              (minus ? '-' : '') + value,
              textAlign: TextAlign.right,
              style: TextStyle(color: color),
            ),
          )
        ],
      ),
    );
  }
}
