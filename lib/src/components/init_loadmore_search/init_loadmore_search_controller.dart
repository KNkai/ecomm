import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';

class InitLoadMoreSearchController<T> extends _SideEffectConfiguration {
  final _stateList = BehaviorSubject<ListState>();
  BehaviorSubject<ListState> get stateList => _stateList;

  final InitListProtocol initProtocol;
  final LoadMoreProtocol loadMoreProtocol;
  final SearchListProtocol searchProtocol;
  final ValueNotifier<bool> stateLoading;
  InitLoadMoreSearchController(
      {this.initProtocol,
      this.loadMoreProtocol,
      this.searchProtocol,
      bool hashLoadingInit = true})
      : stateLoading = hashLoadingInit ? ValueNotifier<bool>(false) : null;

  bool _lockLoadMore = false;
  int _page = 1;

  void evenInit({dynamic parameter}) {
    if (initProtocol != null) {
      stateLoading?.value = (true);
      _page = 1;
      initProtocol.init(parameter: parameter).then((value) {
        try {
          if (value != null && value is ResultInitListSuccess) {
            onUpdateTotal(value.total);
            _stateList.add(ListLoadState<T>(value.dataList));
            if (value.total == value.dataList.length) {
              onReachMaximumData();
            }
          } else
            _stateList.add(ListEmptyState());
        } finally {
          stateLoading?.value = (false);
        }
      });
    }
  }

  void eventLoadMore({dynamic parameter}) {
    if (_totalDataList != null &&
        _stateList?.value != null &&
        loadMoreProtocol != null) {
      final state = _stateList.value;
      if (state is ListLoadState &&
          !_lockLoadMore &&
          state.data != null &&
          state.data.length < _totalDataList) {
        _lockLoadMore = true;
        loadMoreProtocol
            .loadMore(
          page: _page + 1,
          parameter: parameter,
        )
            .then((value) {
          try {
            if (value != null && value is ResultLoadMoreSuccess<T>) {
              _page++;
              _stateList.value = (state..data.addAll(value.dataList));
            }
          } finally {
            _lockLoadMore = false;
          }
        });
      }

      if (state is ListLoadState && state.data.length >= _totalDataList) {
        onReachMaximumData();
      }
    }
  }

  void eventSearch(String text) {
    if (text != null && searchProtocol != null) {
      _page = 1;
      stateLoading?.value = (true);
      searchProtocol.search(search: text).then((value) {
        try {
          if (value != null && value is ResultSerachListSuccess) {
            _totalDataList = value.total;
            _stateList.add(ListLoadState(value.dataList));
          } else
            _stateList.add(ListEmptyState());
        } finally {
          stateLoading?.value = (false);
        }
      });
    }
  }

  void eventUpdateItem(int index, T item) {
    if (_totalDataList != null &&
        _stateList?.value != null &&
        _stateList.value is ListLoadState) {
      stateList.add((_stateList.value as ListLoadState)..data[index] = item);
    }
  }

  void dispose() {
    _stateList.close();
    stateLoading?.dispose();
  }
}

abstract class ListState {}

class ListLoadState<T> extends ListState {
  List<T> data;
  ListLoadState(this.data);
}

class ListEmptyState extends ListState {}

///
///
///
/// START: InitListProtocol
abstract class InitListProtocol {
  // output: ResultInitListSuccess => receive: ListLoadState
  // output: else => receive: ListEmptyState
  Future<ResultInitList> init({dynamic parameter});
}

abstract class ResultInitList {}

class ResultInitListSuccess<T> extends ResultInitList {
  final int total;
  final List<T> dataList;
  ResultInitListSuccess(this.total, this.dataList);
}

class ResultInitListFailed extends ResultInitList {}

/// END: InitListProtocol

///
///
///
/// START: LoadMoreProtocol
abstract class LoadMoreProtocol {
  // output: ResulLoadMoretSuccess => receive: ListLoadState
  Future<ResultLoadMoreSuccess> loadMore({int page, dynamic parameter});
}

class ResultLoadMoreSuccess<T> {
  final List<T> dataList;
  ResultLoadMoreSuccess(this.dataList);
}

/// END: LoadMoreProtocol

///
///
///
/// START: SearchListProtocol
abstract class SearchListProtocol {
  // output: ResultSearchListSuccess => receive: ListLoadState
  // output: eles => receive: ListEmptyState
  Future<ResultSearchList> search({String search});
}

abstract class ResultSearchList {}

class ResultSerachListSuccess<T> extends ResultSearchList {
  final int total;
  final List<T> dataList;
  ResultSerachListSuccess(this.total, this.dataList);
}

class ResultSearchListFailed extends ResultSearchList {}

abstract class _SideEffectConfiguration {
  int _totalDataList;

  void onUpdateTotal(int total) {
    _totalDataList = total;
  }

  void onReachMaximumData() {}
}
