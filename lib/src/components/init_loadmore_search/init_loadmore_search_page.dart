import 'package:flutter/material.dart';
import 'package:genie/src/components/app_loading.dart';
import 'package:genie/src/components/search.dart';

import 'init_loadmore_search_controller.dart';

class BaseListPage<T> extends StatefulWidget {
  final Widget Function(T each) item;
  final String title;
  final InitListProtocol getListProtocol;
  final LoadMoreProtocol loadMoreBaseListProtocol;
  final SearchListProtocol searchBaseListProtocol;
  final String bigTitle;
  final bool isSearch;
  final void Function(BuildContext context, T item) onTap;
  final bool onBack;
  final Widget Function(BuildContext, int) separatorBuilder;

  const BaseListPage(this.title, this.item,
      {this.bigTitle,
      this.isSearch = true,
      this.onTap,
      this.onBack,
      this.separatorBuilder,
      this.getListProtocol,
      this.loadMoreBaseListProtocol,
      this.searchBaseListProtocol});

  @override
  _BaseListPageState createState() => _BaseListPageState<T>();
}

class _BaseListPageState<T> extends State<BaseListPage<T>> {
  InitLoadMoreSearchController _controller;
  final _scrollController = ScrollController();

  @override
  void initState() {
    _controller = InitLoadMoreSearchController<T>(
        initProtocol: widget.getListProtocol,
        searchProtocol: widget.searchBaseListProtocol,
        loadMoreProtocol: widget.loadMoreBaseListProtocol);
    _controller.evenInit();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppLoading(
      loading: false,
      child: Column(
        children: <Widget>[
          widget.isSearch
              ? Search(
                  onChanged: (a) {
                    _controller.eventSearch(a);
                  },
                )
              : const SizedBox.shrink(),
          const SizedBox(
            height: 20,
          ),
          Expanded(
            child: StreamBuilder<ListState>(
              stream: _controller.stateList.stream,
              builder: (_, ss) {
                final state = ss?.data;
                if (state != null) {
                  if (state is ListEmptyState) {
                    return Container();
                  } else if (state is ListLoadState<T>) {
                    final itemCount = state.data.length;
                    return NotificationListener<ScrollNotification>(
                      onNotification: (ScrollNotification scrollInfo) {
                        if (scrollInfo is ScrollEndNotification &&
                            _scrollController.position.extentAfter == 0) {
                          _controller.eventLoadMore();
                        }
                        return true;
                      },
                      child: widget.separatorBuilder != null
                          ? ListView.separated(
                              shrinkWrap: true,
                              itemBuilder: (_, index) => GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  onTap: () {
                                    if (widget.onTap != null) {
                                      widget.onTap(context, state.data[index]);
                                    }
                                  },
                                  child: widget.item(state.data[index])),
                              controller: _scrollController,
                              separatorBuilder: widget.separatorBuilder,
                              itemCount: itemCount)
                          : ListView.builder(
                              shrinkWrap: true,
                              controller: _scrollController,
                              itemBuilder: (_, index) => GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  onTap: () {
                                    if (widget.onTap != null) {
                                      widget.onTap(context, state.data[index]);
                                    }
                                  },
                                  child: widget.item(state.data[index])),
                              itemCount: itemCount,
                            ),
                    );
                  }
                }

                return const SizedBox.shrink();
              },
            ),
          ),
        ],
      ),
    );
  }
}
