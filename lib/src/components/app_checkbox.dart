import 'package:flutter/material.dart';
import 'package:genie/src/utils/app_color.dart';

class AppCheckBox extends StatelessWidget {
  final bool select;
  final bool disable;
  final Widget child;
  final VoidCallback onTap;
  const AppCheckBox(
      {@required this.select,
      @required this.child,
      @required this.disable,
      @required this.onTap});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 5),
          child: Row(
            children: [
              Center(
                child: Padding(
                  padding: const EdgeInsets.only(right: 17),
                  child: AppSingleCheckBox(
                    disable: disable,
                    select: select,
                  ),
                ),
              ),
              Expanded(child: child)
            ],
          ),
        ),
      ),
    );
  }
}

class AppSingleCheckBox extends StatelessWidget {
  final bool select;
  final bool disable;

  AppSingleCheckBox({@required this.select, @required this.disable});

  @override
  Widget build(BuildContext context) {
    return select
        ? DecoratedBox(
            child: const Padding(
              padding: EdgeInsets.all(4),
              child: Icon(
                Icons.check,
                size: 11,
                color: Colors.white,
              ),
            ),
            decoration: BoxDecoration(
                color: AppColor.primary,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 7,
                      spreadRadius: 7,
                      color: AppColor.primary.withOpacity(0.13))
                ]),
          )
        : DecoratedBox(
            child: const Padding(
                padding: EdgeInsets.all(4),
                child: SizedBox(
                  width: 11,
                  height: 11,
                )),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                    width: 1,
                    color: AppColor.primary.withOpacity(disable ? 0.5 : 1))),
          );
  }
}
