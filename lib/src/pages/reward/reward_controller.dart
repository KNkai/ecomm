import 'package:genie/src/models/reward.model.dart';
import 'package:genie/src/repositories/reward.repo.dart';

import 'components/reward_item.dart';

class RewardController {
  final _rewardRepo = RewardRepository();

  Future<List<RewardItem>> loadAllReward(int rewardPoint) async {
    return await _rewardRepo.getAllReward().then((value) {
      return List<RewardItem>.from(
        value.map(
          (e) => RewardItem(
            canChange: _conditionChange(e, rewardPoint),
            reward: e,
          ),
        ),
      ).toList();
    });
  }

  bool _conditionChange(Reward reward, rewardPoint) {
    if (rewardPoint < reward.point) return false;
    if (reward.qtyExchanged == reward.issueNumber) return false;
    return true;
  }
}
