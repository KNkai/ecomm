import 'package:flutter/material.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/pages/reward/reward_controller.dart';
import 'package:genie/src/utils/util.dart';

import 'components/reward_item.dart';

class RewardPage extends StatefulWidget {
  final int rewardPoint;

  const RewardPage({Key key, @required this.rewardPoint}) : super(key: key);
  static void push({BuildContext context, int rewardPoint}) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => RewardPage(
              rewardPoint: rewardPoint,
            )));
  }

  @override
  _RewardPageState createState() => _RewardPageState();
}

class _RewardPageState extends State<RewardPage> {
  final _controller = RewardController();

  Future _loadReward;

  @override
  void initState() {
    _loadReward = _controller.loadAllReward(widget.rewardPoint);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      showAppbar: true,
      showBackBtn: true,
      showCartBtn: false,
      title: "Danh sách đổi quà",
      child: FutureBuilder<List<RewardItem>>(
          future: _loadReward,
          builder: (context, snapshot) {
            if (snapshot.data == null || snapshot.data.length == 0)
              return const Center(
                child: Text("Không có quà để đổi"),
              );
            var chunks = chunk<RewardItem>(snapshot.data, 2);
            return Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              width: double.infinity,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    children: List.generate(
                      chunks.length,
                      (index) {
                        return Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: chunks[index][0],
                                ),
                                if (chunks[index].length == 2) ...[
                                  const SizedBox(width: 8),
                                  Expanded(
                                    child: chunks[index][1],
                                  )
                                ]
                              ],
                            ),
                            const SizedBox(height: 8),
                          ],
                        );
                      },
                    ),
                  ),
                ),
              ),
            );
          }),
    );
  }
}
