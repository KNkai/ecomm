import 'package:flutter/material.dart';
import 'package:genie/src/models/reward.model.dart';
import 'package:genie/src/pages/voucher_detail/voucher_detail.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';

class RewardItem extends StatelessWidget {
  final Reward reward;
  final bool canChange;
  const RewardItem({
    Key key,
    this.reward,
    @required this.canChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        VoucherDetail.push(
          reward: reward,
          isRewardPage: true,
          context: context,
          title: reward?.name,
          subTitle: reward?.campaign?.name,
          imageCampaign: reward?.image,
          campaign: reward?.campaign,
          canChange: canChange,
        );
      },
      child: SizedBox(
        height: 319,
        width: 173,
        child: Card(
          elevation: 5,
          child: Column(
            children: [
              Expanded(
                child: reward?.image == null
                    ? SizedBox(
                        width: 80,
                        height: 80,
                        child: Image.asset(
                          AppAsset.reward_primary,
                          fit: BoxFit.fitWidth,
                        ))
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.network(
                          reward?.image,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                flex: 6,
              ),
              Expanded(
                flex: 3,
                child: Column(
                  children: [
                    Expanded(
                      flex: 2,
                      child: Container(
                        width: double.infinity,
                        padding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              reward?.name,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Container(
                              width: double.infinity,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 5, vertical: 5),
                              child: Text(
                                "${appCurrency(reward?.point).split(' ')[0]} điểm",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: AppColor.primary,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Container(
                              width: double.infinity,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 5, vertical: 5),
                              child: Text(
                                "Còn lại: ${reward.issueNumber - reward.qtyExchanged}",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 13,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  color: canChange ? AppColor.primary : Colors.grey,
                  width: double.infinity,
                  child: Center(
                    child: Text(
                      "Đổi ưu đãi",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
