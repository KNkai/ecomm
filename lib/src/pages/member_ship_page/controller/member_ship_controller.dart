import 'package:flutter/foundation.dart';
import 'package:genie/src/models/membership.model.dart';
import 'package:genie/src/repositories/membership.repo.dart';

class MemberShipController extends ChangeNotifier {
  final memberShipRepo = MemberShipClassRepository();
  ValueNotifier<List<MemberShipClass>> listMemberShip = ValueNotifier(null);
  MemberShipController() {
    getAllMemberShip();
  }

  getAllMemberShip() async {
    await memberShipRepo.getAllMemberShipClass().then((value) {
      setMemberShipClass(value);
    });
  }

  setMemberShipClass(List<MemberShipClass> _listMemberShip) {
    listMemberShip.value = _listMemberShip;
    listMemberShip.notifyListeners();
  }
  int getPosition(List<MemberShipClass> memberShipClass, var type) {
    int position;
    for (int i = 0; i < memberShipClass?.length; i++) {
      if (memberShipClass[i]?.code == type) {
        return position = i;
      }
      else position =0;
    };
    return position;
  }

  // _getPosition(List<MemberShipClass> _listMemberShip) async {
  //   await memberShipRepo.getAllMemberShipClass().then((value) {
  //     setMemberShipClass(_listMemberShip);
  //   });
  // }
}
