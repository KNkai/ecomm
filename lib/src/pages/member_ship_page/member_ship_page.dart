import 'package:flutter/material.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/models/customer.model.dart';
import 'package:genie/src/models/membership.model.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:provider/provider.dart';

import 'controller/member_ship_controller.dart';

class MemberShipPage extends StatelessWidget {
  final Customer customer;

  const MemberShipPage({Key key, this.customer}) : super(key: key);
  static void push({BuildContext context, Customer customer}) {
    Navigator.of(context).push(
        MaterialPageRoute(builder: (_) => MemberShipPage(customer: customer)));
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      title: "Đặc quyền thành viên",
      showCartBtn: false,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 50),
            height: 200,
            color: AppColor.primary.withOpacity(0.3),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    width: 150,
                    height: 150,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(AppAsset.membership),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  flex: 1,
                ),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text("ĐẶC QUYỀN", style: TextStyle(fontSize: 22)),
                        SizedBox(height: 5),
                        Text("dành cho thành viên",
                            style: TextStyle(fontSize: 15)),
                        SizedBox(height: 20),
                        Container(
                          height: 45,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            color: Colors.yellow,
                          ),
                          child: Center(
                            child: Text(
                                customer.rank == "" || customer.rank == null
                                    ? "Thành Viên"
                                    : memberRank(customer.rank)[0]),
                          ),
                        ),
                      ],
                    ),
                  ),
                  flex: 1,
                ),
              ],
            ),
          ),
          Expanded(
            child: MemberShipTab(
              index: customer.rank == "" || customer.rank == null
                  ? 1
                  : int.parse(memberRank(customer.rank)[1]),
            ),
          ),
        ],
      ),
    );
  }
}

List<String> memberRank(String typeCus) {
  String type;
  String index;
  // Color bgType;
  switch (typeCus) {
    case "LOYAL":
      type = "Thân Thiết";
      index = "1";
      break;
    case "SILVER":
      type = "Bạc";
      index = "2";
      break;
    case "GOLD":
      type = "Vàng";
      index = "3";
      break;

    case "DIAMOND":
      type = "Kim Cương";
      index = "4";
      break;

    case "PAINITE":
      type = "Painite";
      index = "5";
      break;

    case "RED_DIAMOND":
      type = "Kim Cương Đỏ";
      index = "6";
      break;
    case "":
      type = "Thành Viên";
      index = "7";
      break;
  }
  return [type, index];
}

class MemberShipTab extends StatefulWidget {
  final int index;

  const MemberShipTab({Key key, this.index}) : super(key: key);
  @override
  _MemberShipTabState createState() => _MemberShipTabState();
}

class _MemberShipTabState extends State<MemberShipTab>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    final _memberShipController =
        Provider.of<MemberShipController>(context, listen: false);

    return ValueListenableBuilder<List<MemberShipClass>>(
      valueListenable: _memberShipController.listMemberShip,
      builder: (_, state, __) {
        state.sort((first, second) =>
            first.requiredPoint.compareTo(second.requiredPoint));
        TabController tabController = TabController(
            length: state.length,
            vsync: this,
            initialIndex: (widget.index - 1) == 7 ? 1 : widget.index - 1);
        return DefaultTabController(
          length: state.length,
          child: Column(
            children: [
              TabBar(
                controller: tabController,
                indicatorColor: AppColor.primary,
                labelColor: AppColor.primary,
                unselectedLabelColor: Colors.grey[400],
                labelStyle: const TextStyle(),
                isScrollable: true,
                tabs: List.generate(
                  state.length,
                  (index) => Tab(
                    text: state[index].name,
                  ),
                ),
              ),
              Flexible(
                child: TabBarView(
                  controller: tabController,
                  children: List.generate(
                    state.length,
                    (index) => Container(
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            for (String e
                                in state[index].privileges.split('\n'))
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: 10),
                                child: Text(e),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
