import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/notifycation.model.dart';
import 'package:genie/src/pages/notification/notify_controller.dart';
import 'package:genie/src/pages/order_detail/order_detail_page.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';

import 'components/item_notify_page.dart';

class NotifyPage extends StatefulWidget {
  // final CustomerState
  const NotifyPage({
    Key key,
  }) : super(key: key);
  static void push(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => const NotifyPage()));
  }

  @override
  _NotifyPageState createState() => _NotifyPageState();
}

class _NotifyPageState extends State<NotifyPage>
    with AutomaticKeepAliveClientMixin {
  final _controller = NotifyController();

  @override
  void initState() {
    super.initState();
    _controller.stateSide.stream.listen((state) {
      OrderDetailPage.push(context,
          id: (state as StateSideGoOrderPage).orderId);
    });
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    // _controller.evenInit();
    return AppScaffold(
      title: 'Thông Báo',
      showBackBtn: false,
      child: DefaultTabController(
        length: 4,
        child: Column(
          children: [
            Container(
              width: double.infinity,
              padding: EdgeInsets.all(10),
              child: Center(
                child: TabBar(
                  isScrollable: true,
                  // indicatorSize: TabBarIndicatorSize.tab,
                  indicatorColor: AppColor.primary,
                  labelColor: AppColor.primary,
                  unselectedLabelColor: AppColor.separate,

                  onTap: (index) {
                    _seletctPage(index);
                  },
                  tabs: [
                    Tab(
                      text: "Đơn Hàng",
                    ),
                    Tab(
                      text: "Ưu Đãi",
                    ),
                    Tab(
                      text: "Chương Trình\nKhuyến Mãi",
                    ),
                    Tab(
                      text: "Cập Nhật\nGenie",
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Stack(
                children: [
                  _buildOffstageNavigator(0, context),
                  _buildOffstageNavigator(1, context),
                  _buildOffstageNavigator(2, context),
                  _buildOffstageNavigator(3, context),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  int _selectedIndex = 0;

  _seletctPage(_) {
    setState(() {
      _selectedIndex = _;
    });
  }

  Map<String, WidgetBuilder> _routeBuilders(int index, BuildContext context) {
    List<Widget> listPage = [
      TabNoti(
        typeNoti: ["type", "ORDER"],
        context: context,
      ),
      TabNoti(
        typeNoti: ["type", "CAMPAIGN"],
        context: context,
      ),
      // TabNoti(
      //   typeNoti: [
      //     "type",
      //     "MESSAGE",
      //     "WEBSITE",
      //     "SCREEN",
      //     "PRODUCT",
      //     "CATEGORY",
      //     "CAMPAIGN"
      //   ],
      //   context: context,
      // ),
      TabNoti(
        typeNoti: ["tab", "PROMOTION"],
        context: context,
      ),
      TabNoti(
        typeNoti: ["tab", "UPDATE"],
        context: context,
      ),
    ];
    return {
      '/': (context) {
        return listPage.elementAt(index);
      },
    };
  }

  Widget _buildOffstageNavigator(int index, BuildContext context) {
    var routeBuilders = _routeBuilders(index, context);
    return Offstage(
      offstage: _selectedIndex != index,
      child: Navigator(
        onGenerateRoute: (routeSettings) {
          return MaterialPageRoute(
            builder: (buildContext) =>
                routeBuilders[routeSettings.name](buildContext),
          );
        },
      ),
    );
  }
}

class TabNoti extends StatefulWidget {
  final dynamic typeNoti;
  final BuildContext context;

  const TabNoti({Key key, this.typeNoti, this.context}) : super(key: key);

  @override
  _TabNotiState createState() => _TabNotiState();
}

class _TabNotiState extends State<TabNoti> {
  @override
  Widget build(BuildContext context) {
    ScrollController _scrollController = new ScrollController();
    NotifyController _controller = NotifyController();
    _controller.evenInit(parameter: widget.typeNoti);
    return StreamBuilder<ListState>(
        stream: _controller.stateList.stream,
        builder: (_, ss) {
          if (ss?.data == null)
            return const Padding(
                padding: EdgeInsets.only(top: 40),
                child: Align(
                    alignment: Alignment.topCenter,
                    child: CupertinoActivityIndicator()));

          if (ss.data is ListEmptyState) return const SizedBox.shrink();

          List<NotifcationModel> stateNotify =
              (ss.data as ListLoadState<NotifcationModel>).data;
          if (stateNotify.length == 0)
            return Center(
              child: Text("Không có thông báo!"),
            );
          return ListView.builder(
            controller: _scrollController
              ..addListener(() {
                if (_scrollController.position.pixels ==
                    _scrollController.position.maxScrollExtent) {
                  _controller.eventLoadMore(parameter: widget.typeNoti);
                }
              }),
            itemCount: stateNotify.length,
            itemBuilder: (_, index) => GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                _controller.eventClick(
                  index,
                  stateNotify[index],
                  context: widget.context,
                );
              },
              child: NotifyItem(
                isRead: stateNotify[index].seen,
                createdDate: appConvertDateTime(stateNotify[index].createdAt,
                    format: 'hh:mm - dd/MM/yyyy'),
                subTitle: stateNotify[index].body,
                imageUrl: stateNotify[index].image,
                title: stateNotify[index].title,
              ),
            ),
          );
        });
  }
}
