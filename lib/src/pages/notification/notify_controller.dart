import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:genie/src/components/app_webview.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/controllers/auth_controller.dart';
import 'package:genie/src/models/notifycation.model.dart';
import 'package:genie/src/pages/category/category_page.dart';
import 'package:genie/src/pages/default_screen/default_screen_page.dart';
import 'package:genie/src/pages/detail_product/detail_product_page.dart';
import 'package:genie/src/pages/order_detail/order_detail_page.dart';
import 'package:genie/src/pages/voucher_detail/voucher_detail.dart';
import 'package:genie/src/repositories/notification.repo.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:provider/provider.dart';

class NotifyController extends InitLoadMoreSearchController<NotifcationModel> {
  NotifyController._(InitListProtocol init, LoadMoreProtocol loadMore)
      : super(
            initProtocol: init,
            loadMoreProtocol: loadMore,
            hashLoadingInit: false);

  factory NotifyController() {
    final repo = NotificationRepository();
    return NotifyController._(repo, repo);
  }

  final stateSide = StreamController<StateSideNotify>();

  void eventClick(int index, NotifcationModel item, {BuildContext context}) {
    AppClient.instance.execute('''
    mutation {
      readNotification(notificationId: "${item.id}") {
        id
      }
    }
    ''').then((value) {
      Provider.of<AuthController>(context, listen: false).getCustomer();
    });
    eventUpdateItem(index, item..seen = true);
    if (item.type == "ORDER") {
      stateSide.add(StateSideGoOrderPage(orderId: item.orderId));
      OrderDetailPage.push(context, id: item.orderId);
    }
    if (item.type == "WEBSITE") {
      AppWebView.push(context: context, title: item.body, url: item.link);
    }
    if (item.type == "PRODUCT") {
      DetailProductPage.push(context: context, productId: item.productId);
    }
    if (item.type == "SCREEN") {
      DefaultScreenPage.push(
          context: context, screenId: item.screen.id, title: item.screen.name);
    }
    if (item.type == "CATEGORY") {
      CategoryPage.push(context,
          categoryId: item.categoryId, title: item?.category?.name ?? "");
    }
    if (item.type == "CAMPAIGN") {
      VoucherDetail.push(
          context: context,
          title: item.title,
          subTitle: item.campaign.name,
          imageCampaign: item.image,
          campaign: item.campaign);
    }
  }
}

abstract class StateSideNotify {}

class StateSideGoOrderPage extends StateSideNotify {
  final orderId;
  StateSideGoOrderPage({@required this.orderId});
}
