import 'package:flutter/material.dart';
import 'package:genie/src/utils/app_fontSize.dart';

class NotifyItem extends StatelessWidget {
  const NotifyItem({
    Key key,
    @required this.isRead,
    @required this.subTitle,
    @required this.createdDate,
    this.title,
    @required this.imageUrl,
  }) : super(key: key);

  final bool isRead;
  final String subTitle;
  final String createdDate;
  final String title;
  final String imageUrl;

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: isRead ? Colors.white : const Color(0x4164A8A7),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 26, vertical: 18),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            imageUrl == null
                ? const SizedBox(width: 62, height: 62)
                : Image.network(
                    imageUrl,
                    width: 62,
                    height: 62,
                  ),
            const SizedBox(
              width: 15,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: const TextStyle(
                      color: Color(0xff64A8A7),
                      fontSize: titleSize,
                    ),
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  RichText(
                    text: TextSpan(
                      children: _convertText(subTitle),
                    ),
                  ),
                  const SizedBox(
                    height: 9,
                  ),
                  Text(
                    createdDate,
                    textAlign: TextAlign.end,
                    style: TextStyle(
                      color: Colors.grey.withOpacity(0.7),
                      fontSize: 13,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<TextSpan> _convertText(String text) {
    var listString = text.split(" ").toList();
    List<TextSpan> listTextSpan = [];

    for (int i = 0; i < listString.length; i++) {
      if (listString[i].contains('#')) {
        listTextSpan.add(
          TextSpan(
            text: listString[i],
            style: const TextStyle(
              color: Color(0xff64A8A7),
              fontSize: subTitleSize,
            ),
          ),
        );
      } else {
        listTextSpan.add(
          TextSpan(
            text: listString[i],
            style: const TextStyle(
              color: Colors.black,
              fontSize: subTitleSize,
            ),
          ),
        );
      }
      listTextSpan.add(
        const TextSpan(
          text: " ",
        ),
      );
    }
    return listTextSpan;
  }
}
