import 'package:flutter/material.dart';
import 'package:genie/src/models/order_item.model.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';

class ItemProductOrder extends StatelessWidget {
  final OrderItem orderItem;

  const ItemProductOrder({Key key, @required this.orderItem}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 26, vertical: 14),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            DecoratedBox(
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                  border: Border.all(width: 1, color: Colors.grey[300])),
              child: Padding(
                padding: const EdgeInsets.all(1.25),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                  child: orderItem.product.listImage == null ||
                          orderItem.product.listImage.isEmpty ||
                          orderItem.product.listImage[0] == null
                      ? const SizedBox.shrink()
                      : Image.network(orderItem.product.listImage[0],
                          width: 94, height: 94),
                ),
              ),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: Column(
                children: [
                  Text(orderItem?.product?.name ?? ''),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      "x${orderItem?.quantity ?? ''}",
                      style: const TextStyle(
                        color: Colors.grey,
                        fontSize: 13,
                      ),
                      textAlign: TextAlign.right,
                    ),
                  ),
                  const SizedBox(
                    height: 13,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      if (orderItem?.hasSale != null && orderItem.hasSale)
                        Text(
                          appCurrency(orderItem?.basePrice),
                          style: const TextStyle(
                            fontSize: 10,
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                      if (orderItem?.hasSale != null && orderItem.hasSale)
                        const SizedBox(
                          width: 10,
                        ),
                      Text(
                        appCurrency(orderItem?.salePrice),
                        style: const TextStyle(
                          color: AppColor.primary,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      SizedBox(
        width: double.infinity,
        height: 1,
        child: ColoredBox(
          color: Colors.grey[300],
        ),
      )
    ]);
  }
}
