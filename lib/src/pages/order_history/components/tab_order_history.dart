import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/order.model.dart';
import 'package:genie/src/models/order_status.model.dart';
import 'package:genie/src/pages/order_detail/order_detail_page.dart';
import 'package:genie/src/pages/order_history/controller/tab_order_history_controller.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';

class TabOrder extends StatefulWidget {
  final OrderStatus status;

  const TabOrder({Key key, @required this.status}) : super(key: key);

  @override
  _TabOrderState createState() => _TabOrderState();
}

class _TabOrderState extends State<TabOrder>
    with AutomaticKeepAliveClientMixin<TabOrder> {
  TabOrderHistoryContrroller _contrroller;

  @override
  void initState() {
    _contrroller = TabOrderHistoryContrroller();
    _contrroller.evenInit(parameter: widget.status);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return StreamBuilder<ListState>(
      stream: _contrroller.stateList.stream,
      builder: (_, ss) {
        print(ss.data);
        if (ss?.data == null)
          return const Padding(
              padding: EdgeInsets.only(top: 40),
              child: CupertinoActivityIndicator());

        if (ss.data is ListEmptyState) return const SizedBox.shrink();

        final List<Order> stateOrder = (ss.data as ListLoadState<Order>).data;

        return ListView.separated(
            separatorBuilder: (_, index) => SizedBox(
                  height: 10,
                  child: ColoredBox(
                    color: Colors.grey[200],
                  ),
                ),
            itemCount: stateOrder.length,
            itemBuilder: (_, index) => GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    OrderDetailPage.push(context, id: stateOrder[index].id);
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 26, vertical: 16),
                    child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Mã Đơn Hàng: ${stateOrder[index].code}",
                                style: const TextStyle(
                                  color: AppColor.primary,
                                ),
                              ),
                              const SizedBox(
                                height: 12,
                              ),
                              Text(
                                "Tổng sản phẩm: ${stateOrder[index].itemCount ?? ''} sản phẩm",
                                style: const TextStyle(fontSize: 15),
                              ),
                              const SizedBox(
                                height: 9,
                              ),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    const TextSpan(
                                      text: "Tổng Tiền: ",
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.black),
                                    ),
                                    TextSpan(
                                      text:
                                          appCurrency(stateOrder[index].amount),
                                      style: const TextStyle(
                                        color: AppColor.primary,
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        const Icon(
                          Icons.arrow_forward_ios_rounded,
                          size: 18,
                          color: Colors.grey,
                        ),
                      ],
                    ),
                  ),
                ));
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
