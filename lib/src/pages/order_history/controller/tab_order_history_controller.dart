import 'package:flutter/cupertino.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/order.model.dart';
import 'package:genie/src/repositories/order.repo.dart';

class TabOrderHistoryContrroller extends InitLoadMoreSearchController<Order> {
  TabOrderHistoryContrroller._(
      {@required InitListProtocol initProtocol,
      @required LoadMoreProtocol loadMoreProtocol})
      : super(
            initProtocol: initProtocol,
            loadMoreProtocol: loadMoreProtocol,
            hashLoadingInit: true) {}

  factory TabOrderHistoryContrroller() {
    final repo = OrderHistoryRepository();
    return TabOrderHistoryContrroller._(
        initProtocol: repo, loadMoreProtocol: repo);
  }
}
