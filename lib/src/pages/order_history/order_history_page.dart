import 'package:flutter/material.dart';
import 'package:genie/src/components/app_floating_contact.dart';
import 'package:genie/src/components/app_navbar.dart';
import 'package:genie/src/components/custom_app_bar.dart';
import 'package:genie/src/controllers/auth_controller.dart';
import 'package:genie/src/models/order_status.model.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:provider/provider.dart';

import 'components/tab_order_history.dart';

class OrderHistoryPage extends StatelessWidget {
  static void push({
    BuildContext context,
    List<String> listTitlePage = const [
      'Chờ xử lý',
      'Đang giao',
      'Đã giao',
      'Trả hàng',
      'Đã hủy'
    ],
  }) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => OrderHistoryPage(
              mode: _Mode.fromProfile,
              listTitlePage: listTitlePage,
            )));
  }

// When hit back, the page will route to dashboard
  static void pushNew({
    BuildContext context,
    List<String> listTitlePage = const [
      'Chờ xử lý',
      'Đang giao',
      'Đã giao',
      'Trả hàng',
      'Đã hủy'
    ],
  }) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (_) => OrderHistoryPage(
                  mode: _Mode.fromSuccessPay,
                  listTitlePage: listTitlePage,
                )),
        (_) => false);
    Provider.of<AuthController>(context, listen: false).getCustomer();
  }

  final _Mode mode;
  final List<String> listTitlePage;

  const OrderHistoryPage({
    this.mode,
    this.listTitlePage = const [
      'Chờ xử lý',
      'Đang giao',
      'Đã giao',
      'Trả hàng',
      'Đã hủy'
    ],
  });

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        appBar: CustomAppBar(
          showCartBtn: false,
          onCustomBackTap: () {
            if (mode == _Mode.fromSuccessPay) {
              AppNavBar.push(context);
            } else {
              Navigator.of(context).pop();
            }
          },
          title: 'Đơn mua',
          bottom: TabBar(
            indicatorColor: AppColor.primary,
            labelColor: AppColor.primary,
            unselectedLabelColor: Colors.grey[400],
            labelStyle: const TextStyle(),
            isScrollable: true,
            // tabs: [
            //   Tab(
            //     child: SizedBox(
            //       child: Text("Chờ xử lý"),
            //     ),
            //   ),
            //   Tab(
            //     child: SizedBox(
            //       child: Text("Đang giao"),
            //     ),
            //   ),
            //   Tab(
            //     child: SizedBox(
            //       child: Text("Đã giao"),
            //     ),
            //   ),
            //   Tab(
            //     child: SizedBox(
            //       child: Text("Trả hàng"),
            //     ),
            //   ),
            //   Tab(
            //     child: SizedBox(
            //       child: Text("Đã hủy"),
            //     ),
            //   ),
            // ],
            //
            tabs: List.generate(
              listTitlePage.length,
              (index) {
                return Tab(
                  child: SizedBox(
                    child: Text(
                      listTitlePage[index],
                      style: TextStyle(color: AppColor.primary),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
        body: const TabBarView(
          children: [
            TabOrder(
              key: ValueKey(1),
              status: OrderStatus.pending,
            ),
            TabOrder(
              key: ValueKey(2),
              status: OrderStatus.delivering,
            ),
            TabOrder(
              key: ValueKey(3),
              status: OrderStatus.approve,
            ),
            TabOrder(
              key: ValueKey(4),
              status: OrderStatus.returned,
            ),
            TabOrder(
              key: ValueKey(5),
              status: OrderStatus.canceled,
            ),
          ],
        ),
        floatingActionButton: AppFloatingContact(),
      ),
    );
  }
}

enum _Mode { fromProfile, fromSuccessPay }
