import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/models/payment_method.model.dart';
import 'package:genie/src/pages/order_history/order_history_page.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/size_config.dart';

class PaymentSuccessPage extends StatelessWidget {
  static void push(BuildContext context, {@required PaymentMethod method}) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (_) => PaymentSuccessPage(method)),
        (_) => false);
  }

  const PaymentSuccessPage(this.paymentMethod);

  final PaymentMethod paymentMethod;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        body: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: DecoratedBox(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage(AppAsset.loginbg),
                fit: BoxFit.fill,
              ),
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 0.0803571 * SizeConfig.heightDevice,
                  ),
                  SizedBox(
                    height: 80,
                    child: Image.asset(AppAsset.logo),
                  ),
                  const SizedBox(
                    height: 33,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 26,
                      ),
                      child: DecoratedBox(
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Column(
                          children: [
                            const SizedBox(
                              height: 40,
                            ),
                            Image.asset(
                              AppAsset.paySuccessIc,
                              width: 87,
                              height: 87,
                            ),
                            const SizedBox(
                              height: 30,
                            ),
                            const Text(
                              'Đặt hàng\nthành công',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 23),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            const Text(
                              'Cám ơn bạn đã mua hàng tại Genie!',
                              style:
                                  TextStyle(fontSize: 13, color: Colors.grey),
                            ),
                            const SizedBox(
                              height: 13,
                            ),
                            if (paymentMethod == PaymentMethod.bankTransfer)
                              const Padding(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                child: Text(
                                  'Chúng tôi sẽ vận chuyển đơn hàng ngay sau khi xác nhận chuyển khoản thành công',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      height: 1.4,
                                      fontSize: 13,
                                      color: Colors.grey),
                                ),
                              ),
                            const SizedBox(
                              height: 27,
                            ),
                            if (paymentMethod == PaymentMethod.bankTransfer)
                              SizedBox(
                                  width: double.infinity,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 47),
                                    child: AppBtn(
                                        onTap: () {
                                          showDialog(
                                            context: context,
                                            barrierDismissible: true,
                                            builder: (BuildContext context) {
                                              return AlertDialog(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10)),
                                                title: const Text(
                                                    'Thông tin chuyển khoản'),
                                                content: SizedBox(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.5,
                                                    child:
                                                        FutureBuilder<Response>(
                                                      future: AppClient.instance
                                                          .execute('''
                                                          query {
                                                            getOneSettingByKey(key: "APP_BANK_TRANSFER") {
                                                                name
                                                                key
                                                                value
                                                            }
                                                          }
                                                      '''),
                                                      builder: (_, snapshot) {
                                                        if (snapshot?.data ==
                                                                null ||
                                                            snapshot.data.data[
                                                                    'getOneSettingByKey'] ==
                                                                null ||
                                                            snapshot.data.data[
                                                                        'getOneSettingByKey']
                                                                    ['value'] ==
                                                                null)
                                                          return const SizedBox
                                                              .shrink();

                                                        final note = snapshot
                                                                    .data.data[
                                                                'getOneSettingByKey']
                                                            ['value'];

                                                        return Text(note);
                                                      },
                                                    )),
                                              );
                                            },
                                          );
                                        },
                                        title: 'Thông tin chuyển khoản'),
                                  )),
                            if (paymentMethod == PaymentMethod.bankTransfer)
                              const SizedBox(
                                height: 13,
                              ),
                            SizedBox(
                              width: double.infinity,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 47),
                                child: AppBtn(
                                  onTap: () {
                                    OrderHistoryPage.pushNew(context: context);
                                  },
                                  title: 'Lịch sử đơn hàng',
                                  color: Colors.white,
                                  textColor: AppColor.primary,
                                  borderColor: AppColor.primary,
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 40,
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
