import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../components/app_scaffold.dart';
import '../../components/init_loadmore_search/init_loadmore_search_controller.dart';
import '../../components/list_block/components/list_product_grid_block_item.dart';
import '../../components/refresh_scroll_view.dart';
import '../../models/category.model.dart';
import '../../models/product.model.dart';
import '../../repositories/product.repo.dart';
import '../../utils/app_color.dart';
import 'store_controller.dart';

class StorePage extends StatefulWidget {
  const StorePage({
    Key key,
  }) : super(key: key);
  static void push(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => const StorePage()));
  }

  @override
  _StorePageState createState() => _StorePageState();
}

class _StorePageState extends State<StorePage>
    with AutomaticKeepAliveClientMixin {
  final productProtocol = ProductGridPageRepositoryGetAll();

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final controller = StoreController(
        initProtocol: productProtocol, loadMoreProtocol: productProtocol);
    controller.loadAllCategories();
    return AppScaffold(
      title: "Cửa hàng",
      showSearchBtn: true,
      showBackBtn: false,
      showContact: true,
      child: ChangeNotifierProvider(
        create: (_) => CurrentCategory(),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            FilterBarStore(
              controller: controller,
            ),
            Expanded(
              child: GridProductStore(
                controller: controller,
              ),
              flex: 1,
            )
          ],
        ),
      ),
    );
  }
}

class GridProductStore extends StatelessWidget {
  final StoreController controller;
  const GridProductStore({
    Key key,
    this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // ScrollController _scrollController = new ScrollController();
    CurrentCategory categoryId =
        Provider.of<CurrentCategory>(context, listen: false);
    return ValueListenableBuilder(
        valueListenable: categoryId.category,
        builder: (_, state, __) {
          return RefreshScrollView(
              onRefresh: () async {
                if (state == "" || state == null)
                  controller.evenInit();
                else
                  controller.evenInit(parameter: state);
              },
              onLoadMore: () async {
                if (state == "" || state == null)
                  controller.eventLoadMore();
                else
                  controller.eventLoadMore(parameter: state);
              },
              child: StreamBuilder<ListState>(
                stream: controller.stateList,
                builder: (_, ss) {
                  if (ss?.data == null) return const SizedBox.shrink();
                  if (ss.data is ListEmptyState)
                    return const Center(
                        child: Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Text('Dữ liệu bị trống'),
                    ));
                  final ListLoadState<Product> stateProduct = ss.data;
                  return Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: SingleChildScrollView(
                      child: ListProductGridBlockItem(
                          items: stateProduct.data
                              .map((e) => e.blockItem)
                              .toList()),
                    ),
                  );
                },
              ));
        });
  }
}

class SortBarStore extends StatelessWidget {
  const SortBarStore({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      // color: Colors.red.withOpacity(0.3),
      height: 50,
      child: Stack(
        children: [
          Positioned(
            left: 0,
            child: Row(
              children: [
                const Icon(Icons.import_export_rounded),
                const Text("Sắp xếp")
              ],
            ),
          ),
          Positioned(
            right: 0,
            child: Row(
              children: [
                const Icon(Icons.filter_alt_outlined),
                const Text("Bộ lọc")
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class FilterBarStore extends StatefulWidget {
  final StoreController controller;
  // final String type;
  const FilterBarStore({
    Key key,
    this.controller,
    // this.type,
  }) : super(key: key);

  @override
  _FilterBarStoreState createState() => _FilterBarStoreState();
}

class _FilterBarStoreState extends State<FilterBarStore> {
  String option;
  bool isUpdate = false;
  List<Category> _listCate = [];
  Category cateAllProd = Category(id: "0", name: "Tất cả sản phẩm");
  @override
  void initState() {
    _listCate.add(cateAllProd);
    fetchData();

    option = _listCate[0].id;
    super.initState();
  }

  fetchData() {
    return widget.controller.loadAllCategories().then((value) {
      setState(() {
        value.forEach((e) {
          _listCate.add(e);
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final categoryId = Provider.of<CurrentCategory>(context, listen: false);
    return Container(
      height: 80,
      width: double.infinity,
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: List.generate(_listCate.length, (index) {
          return Container(
            margin: const EdgeInsets.only(right: 20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: option == (_listCate[index].id)
                  ? AppColor.primary
                  : AppColor.separate,
            ),
            child: FlatButton(
              onPressed: () {
                setState(() {
                  option = (_listCate[index].id);
                });
                if (_listCate[index].id == "0") {
                  widget.controller.evenInit();
                  widget.controller.eventLoadMore();
                  categoryId.filterCategory("", context);
                } else {
                  widget.controller.evenInit(parameter: _listCate[index].id);
                  widget.controller
                      .eventLoadMore(parameter: _listCate[index].id);
                  categoryId.filterCategory(_listCate[index].id, context);
                }
              },
              child: Text(
                (_listCate[index].name ?? ""),
                style: TextStyle(
                    color: option == (_listCate[index].id)
                        ? Colors.white
                        : Colors.black),
              ),
            ),
          );
        }),
      ),
    );
  }
}

class CurrentCategory extends ChangeNotifier {
  ValueNotifier<String> category = ValueNotifier(null);
  CurrentCategory();

  filterCategory(String categoryId, BuildContext context) {
    final _categoryId = Provider.of<CurrentCategory>(context, listen: false);
    _categoryId.category.value = categoryId;
    _categoryId.category.notifyListeners();
  }
}
