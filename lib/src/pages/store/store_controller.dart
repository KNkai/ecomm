import 'package:flutter/cupertino.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/repositories/store.repo.dart';

class StoreController extends InitLoadMoreSearchController<Product> {
  StoreController(
      {@required InitListProtocol initProtocol,
      @required LoadMoreProtocol loadMoreProtocol})
      : super(initProtocol: initProtocol, loadMoreProtocol: loadMoreProtocol) {
    this.evenInit();
  }
  final storeRepository = StoreRepository();

  Future loadAllCategories() {
    return storeRepository.getAllShopCategory().then((value) {
      return value;
    });
  }
}
