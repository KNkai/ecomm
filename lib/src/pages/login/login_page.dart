import 'package:flutter/material.dart';
import 'package:genie/src/controllers/auth_controller.dart';
import 'package:genie/src/pages/login/components/otp_login_widget.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:provider/provider.dart';

import 'components/phone_login_widget.dart';

class LoginPage extends StatefulWidget {
  static void push(
    BuildContext context, {
    @required VoidCallback onLoginSuccess,
  }) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => const LoginPage()))
        .then((value) {
      if (value != null && value) {
        onLoginSuccess();
      }
    });
  }

  const LoginPage();

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _controller = PageController(initialPage: 0, keepPage: false);
  final _heightWhite = 429.0;
  String _phone;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        body: Stack(
          children: [
            SizedBox(
              height: size.height,
              child: DecoratedBox(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(AppAsset.loginbg),
                    fit: BoxFit.fill,
                  ),
                ),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: size.height / 4,
                        padding: EdgeInsets.only(top: size.height / 8),
                        width: double.infinity,
                        child: Image.asset(AppAsset.logo),
                      ),
                      const SizedBox(
                        height: 33,
                      ),
                      SizedBox(
                        height: _heightWhite,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 37,
                          ),
                          child: DecoratedBox(
                            decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 36, horizontal: 16),
                              child: PageView(
                                controller: _controller,
                                physics: const NeverScrollableScrollPhysics(),
                                children: [
                                  PhoneLoginWidget(
                                    onPhone: (phone) {
                                      _phone = phone;
                                    },
                                    controller: _controller,
                                  ),
                                  OtpLoginWidget(
                                    onSuccessLogin: () {
                                      // AppNavBar.push(context);
                                      Navigator.of(context).pop();
                                      var auth = Provider.of<AuthController>(
                                          context,
                                          listen: false);
                                      auth.getCustomer();
                                    },
                                    onInitPhone: () {
                                      return _phone;
                                    },
                                    controller: _controller,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              left: 20,
              top: 50,
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: const Icon(Icons.highlight_off_rounded,
                    size: 40, color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}
