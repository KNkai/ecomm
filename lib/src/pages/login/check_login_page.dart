import 'package:flutter/material.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/utils/app_color.dart';

class CheckLoginPage extends StatelessWidget {
  static void push(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => CheckLoginPage()));
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      title: "Tài Khoản",
      showAppbar: true,
      child: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: const Text(
                "Bạn Chưa Đăng Nhập",
                style: TextStyle(
                  fontSize: 27,
                ),
              ),
            ),
            SizedBox(
              child: Text(
                "Vui lòng đăng nhập để tiếp tục",
                style: TextStyle(
                    color: Colors.grey.withOpacity(0.7), fontSize: 15),
              ),
            ),
            Container(
              height: 60,
              margin: EdgeInsets.symmetric(
                  vertical: 30,
                  horizontal: MediaQuery.of(context).size.width / 4),
              decoration: BoxDecoration(
                color: AppColor.primary,
                borderRadius: BorderRadius.circular(30),
              ),
              child: Center(
                child: FlatButton(
                  onPressed: () {},
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    width: double.infinity,
                    height: double.infinity,
                    child: const Center(
                      child: Text(
                        "Đăng Nhập",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
