import 'package:flutter/material.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/utils/util.dart';

import 'checkbox_custom.dart';

class PhoneLoginWidget extends StatefulWidget {
  final PageController controller;
  final void Function(String) onPhone;

  const PhoneLoginWidget({Key key, this.controller, @required this.onPhone})
      : super(key: key);

  @override
  _PhoneLoginWidgetState createState() => _PhoneLoginWidgetState();
}

class _PhoneLoginWidgetState extends State<PhoneLoginWidget> {
  bool isCheckBox = false;
  final _controller = TextEditingController();
  final _node = FocusNode();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const Center(
            child: Text(
              "Xin Chào",
              style: TextStyle(fontSize: 35, fontWeight: FontWeight.w300),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 10),
            child: const Center(
              child: Text(
                "Vui Lòng Đăng Nhập",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.grey,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 40),
            child: TextField(
              focusNode: _node,
              controller: _controller,
              decoration: const InputDecoration(
                labelText: "Số Điện Thoại",
              ),
              keyboardType: TextInputType.phone,
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 10),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      isCheckBox = !isCheckBox;
                    });
                  },
                  child: CheckBoxCustom(
                    isCheckBox: isCheckBox,
                  ),
                ),
                const SizedBox(width: 10),
                const Flexible(
                  child: Text(
                    "Tôi đồng ý với những quy định và điều kiện của ứng dụng Genie",
                    style: TextStyle(
                      fontWeight: FontWeight.w300,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 41,
          ),
          AppBtn(
            onTap: () {
              if (isCheckBox && !appCheckEmptyString(_controller.text)) {
                _node.unfocus();
                widget.onPhone(_controller.text);
                widget.controller.nextPage(
                    duration: const Duration(milliseconds: 500), curve: Curves.ease);
              }
            },
            title: isCheckBox ? "Đồng Ý" : "Đăng Nhập",
          )
        ],
      ),
    );
  }
}
