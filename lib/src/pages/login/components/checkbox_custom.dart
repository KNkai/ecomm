import 'package:flutter/material.dart';
import 'package:genie/src/utils/app_color.dart';

class CheckBoxCustom extends StatelessWidget {
  final bool isCheckBox;

  const CheckBoxCustom({Key key, @required this.isCheckBox}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 26,
      height: 26,
      decoration: BoxDecoration(
        color: AppColor.primary.withOpacity(0.23),
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          color: AppColor.primary,
        ),
      ),
      child: Center(
        child: isCheckBox
            ? const Icon(
                Icons.check,
                color: AppColor.primary,
                size: 25,
              )
            : null,
      ),
    );
  }
}
