import 'dart:async';

import 'package:flutter/material.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/controllers/auth_controller.dart';
import 'package:genie/src/pages/login/otp_login_widget_controller.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/constants.dart';
import 'package:genie/src/utils/util.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';

class OtpLoginWidget extends StatefulWidget {
  final PageController controller;
  final String Function() onInitPhone;
  final VoidCallback onSuccessLogin;
  const OtpLoginWidget(
      {Key key,
      this.controller,
      @required this.onInitPhone,
      @required this.onSuccessLogin})
      : super(key: key);

  @override
  _OtpLoginWidgetState createState() => _OtpLoginWidgetState();
}

class _OtpLoginWidgetState extends State<OtpLoginWidget> {
  final _controller = OtpLoginWidgetController();
  final _pinController = TextEditingController();
  String phone;

  // have to place staeful because the reCAPCHA makes widget rebuild.
  @override
  void initState() {
    phone = widget.onInitPhone();
    _controller.eventLoginPhone(phone);
    _controller.stateSide.stream.listen((state) {
      if (state is StateSideShowMsg)
        WarningDialog.show(context, state.msg, 'OK', title: 'Thông báo');
      else
        widget.onSuccessLogin();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var authCtrl = Provider.of<AuthController>(context, listen: false);
    return SingleChildScrollView(
      child: Column(
        children: [
          const Center(
            child: Text(
              "Xác Nhận",
              style: TextStyle(fontSize: 35, fontWeight: FontWeight.w300),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
            child: Center(
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: <TextSpan>[
                    const TextSpan(
                      text:
                          "Nhận mã code 6 chữ số mà chúng tôi đã gửi đến bạn qua số ",
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.grey,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                    TextSpan(
                      text: phone,
                      style: const TextStyle(
                        fontSize: 15,
                        color: AppColor.primary,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),

          // //verify otp
          Container(
            margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
            width: double.infinity,
            height: 60,
            child: PinCodeTextField(
              appContext: context,
              pastedTextStyle: TextStyle(
                color: Colors.green.shade600,
                fontWeight: FontWeight.bold,
              ),
              length: 6,
              keyboardType: TextInputType.number,
              controller: _pinController,
              pinTheme: PinTheme(
                shape: PinCodeFieldShape.underline,
                borderRadius: BorderRadius.circular(5),
                fieldHeight: 60,
                fieldWidth: MediaQuery.of(context).size.width / 12,
                inactiveColor: Colors.black,
                activeColor: AppColor.primary,
                selectedColor: AppColor.primary,
              ),
              animationType: AnimationType.fade,
              showCursor: false,
              animationDuration: const Duration(milliseconds: 300),
              textStyle: const TextStyle(
                fontSize: 20,
                height: 1.6,
                color: AppColor.primary,
              ),
              backgroundColor: Colors.white,
              onCompleted: (v) {
                _controller.eventConfirmOtp(v, (firebaseToken) {
                  authCtrl.loginWithFirebaseToken(firebaseToken);
                  widget.onSuccessLogin();
                });
              },
              onChanged: (value) {},
            ),
          ),
          _Resend(
            onRestart: () {
              _controller.eventLoginPhone(phone);
            },
          ),
          const SizedBox(
            height: 27,
          ),
          ValueListenableBuilder(
            valueListenable: _controller.stateLoadingConfirm,
            builder: (_, loading, child) {
              if (loading) return child;
              return AppBtn(
                onTap: () {
                  _controller.eventConfirmOtp(_pinController.text,
                      (firebaseToken) {
                    authCtrl.loginWithFirebaseToken(firebaseToken);
                    widget.onSuccessLogin();
                  });
                },
                title: 'Xác nhận',
              );
            },
            child: kLoadingSpinner,
          ),
          const SizedBox(
            height: 27,
          ),
          Center(
            child: GestureDetector(
              child: const Text(
                "Hoặc Thay Đổi Số Điện Thoại",
                style: TextStyle(
                  color: AppColor.primary,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onTap: () {
                widget.controller.previousPage(
                    duration: const Duration(milliseconds: 500),
                    curve: Curves.easeOut);
              },
            ),
          ),
        ],
      ),
    );
  }
}

class _Resend extends StatefulWidget {
  final VoidCallback onRestart;
  const _Resend({@required this.onRestart});
  @override
  __ResendState createState() => __ResendState();
}

class __ResendState extends State<_Resend> {
  static const int _maxTime = 60;
  bool hasError = false;
  String currentText = "";
  Timer _timer;
  int _start = _maxTime;
  bool isStart = false;

  _countTimer() {
    const oneSec = Duration(seconds: 1);

    _timer = new Timer.periodic(oneSec, (timer) {
      if (_start == 0) {
        setState(() {
          isStart = true;
          _timer.cancel();
          _start = _maxTime;
        });
      } else {
        setState(() {
          isStart = false;
          _start--;
        });
      }
    });
    print(_start);
  }

  @override
  void initState() {
    _countTimer();
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            "Chưa Nhận Được Mã Code ",
            style: TextStyle(
              fontSize: 13,
              color: Colors.grey,
              fontWeight: FontWeight.w300,
            ),
          ),
          GestureDetector(
            child: Text(
              "Gửi lại${_start == _maxTime ? '' : ' ' + _start.toString() + 's'}",
              style: TextStyle(
                color: !isStart ? Colors.grey : AppColor.primary,
                decoration: TextDecoration.underline,
              ),
            ),
            onTap: () {
              if (_start == _maxTime) {
                _countTimer();
                widget.onRestart();
              }
            },
          ),
        ],
      ),
    );
  }
}
