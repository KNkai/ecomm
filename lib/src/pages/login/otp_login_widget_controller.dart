import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class OtpLoginWidgetController {
  final stateSide = StreamController<StateSideLogin>();
  final stateLoadingConfirm = ValueNotifier<bool>(false);
  String _verificationId;

  void eventLoginPhone(String phone) async {
    phone = _cleanPhoneNumber(phone);
    await FirebaseAuth.instance.verifyPhoneNumber(
      timeout: const Duration(seconds: 20),
      phoneNumber: phone,
      verificationCompleted: (PhoneAuthCredential credential) {},
      verificationFailed: (FirebaseAuthException e) {
        // https://github.com/firebase/firebase-js-sdk/blob/eff049b75d9455921b14baeeda5119a04eb157f4/packages-exp/auth-exp/src/core/errors.ts
        switch (e.code) {
          case 'invalid-phone-number':
            stateSide.add(StateSideShowMsg(msg: 'Số điện thoại không hợp lệ'));
            return;
          case 'quota-exceeded':
            stateSide.add(StateSideShowMsg(
                msg:
                    'Đăng nhập nhiều lần không thành công vui lòng thử lại sau'));
            return;
          case 'web-context-cancelled':
            return;
          default:
            stateSide.add(
                StateSideShowMsg(msg: 'Có lỗi xảy ra. Mã: ${e?.code ?? ''}'));
        }
        stateLoadingConfirm.value = false;
      },
      codeSent: (String verificationId, int resendToken) {
        _verificationId = verificationId;
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        stateLoadingConfirm.value = false;
      },
    );
  }

  Future<String> eventConfirmOtp(String otp, Function(String) callback) async {
    if (_verificationId != null && otp.length == 6) {
      stateLoadingConfirm.value = true;
      try {
        final userCredential = await FirebaseAuth.instance.signInWithCredential(
            PhoneAuthProvider.credential(
                verificationId: _verificationId, smsCode: otp));
        return await userCredential.user.getIdToken(true).then((res) {
          callback(res);
          return res;
        });
      } catch (e) {
        if (e is FirebaseAuthException) {
          switch (e.code) {
            case 'invalid-verification-code':
              stateSide.add(StateSideShowMsg(msg: 'Mã OTP không hợp lệ'));
              return null;
            case 'code-expired':
              stateSide.add(
                  StateSideShowMsg(msg: 'Mã OTP hết hạn vui lòng thử lại'));
              return null;
            case 'quota-exceeded':
              stateSide.add(StateSideShowMsg(
                  msg:
                      'Đăng nhập nhiều lần không thành công vui lòng thử lại sau'));
              return null;
            default:
              stateSide.add(StateSideShowMsg(msg: e.message));
              return null;
          }
        }
      } finally {
        stateLoadingConfirm.value = false;
        return null;
      }
    }
    return null;
  }

  String _cleanPhoneNumber(String phone) {
    if (phone.codeUnitAt(0) == 48) {
      // zero
      phone = phone.replaceRange(0, 1, '+84');
    } else if (phone.codeUnitAt(0) == 56 &&
        phone.codeUnitAt(1) == 52 &&
        phone.length == 11) {
      phone = '+$phone';
    } else if (phone.length == 9) {
      phone = '+84$phone';
    }
    return phone;
  }

  void dispose() {
    stateSide.close();
    stateLoadingConfirm.dispose();
  }
}

abstract class StateSideLogin {}

class StateSideShowMsg extends StateSideLogin {
  final String msg;
  StateSideShowMsg({@required this.msg});
}

class StateSideSuccessLogin extends StateSideLogin {}
