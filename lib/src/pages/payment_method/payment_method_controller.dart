import 'package:genie/src/models/payment_method.model.dart';

abstract class PaymentMethodState {}

class PaymentMethodStateLoading extends PaymentMethodState {}

class LoadedPaymentMethodSuccess extends PaymentMethodState {
  final List<PaymentMethodModel> listPaymentMethod;

  LoadedPaymentMethodSuccess({this.listPaymentMethod});
}

class SelectedPaymentMethod extends PaymentMethodState {
  final PaymentMethodModel paymentMethodModel;

  SelectedPaymentMethod({this.paymentMethodModel});
}

class LoadedPaymentMethodFailed extends PaymentMethodState {}

class PaymentMethodController {
  updateStatePaymentMethod() {}
}
