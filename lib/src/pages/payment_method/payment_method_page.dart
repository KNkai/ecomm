import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/components/app_checkbox.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/components/bottom_action_container.dart';
import 'package:genie/src/models/payment_method.model.dart';
import 'package:genie/src/utils/app_color.dart';
/*
class PaymentMethodPage extends StatefulWidget {
  static void push(BuildContext context,
      {@required PaymentMethod defaultPaymentMethod,
      @required Function(PaymentMethod) onReturn}) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => PaymentMethodPage(defaultPaymentMethod, onReturn)));
  }

  const PaymentMethodPage(this.defaultPaymentMethod, this.onReturn);

  final PaymentMethod defaultPaymentMethod;
  final void Function(PaymentMethod) onReturn;

  @override
  _PaymentMethodPageState createState() => _PaymentMethodPageState();
}

class _PaymentMethodPageState extends State<PaymentMethodPage> {
  PaymentMethod _paymentMethod;

  @override
  void initState() {
    _paymentMethod = widget.defaultPaymentMethod;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
        title: 'Chọn phương thức thanh toán',
        showCartBtn: false,
        child: BottomActionContainer(
            child: Padding(
                padding:
                    const EdgeInsets.only(left: 26, top: 15, bottom: 15, right: 10),
                child: Column(
                  children: [
                    AppCheckBox(
                        onTap: () {
                          if (_paymentMethod != PaymentMethod.cod)
                            setState(() {
                              _paymentMethod = PaymentMethod.cod;
                            });
                        },
                        select: _paymentMethod == PaymentMethod.cod,
                        child: Text(PaymentMethod.cod.value,
                            style: _paymentMethod == PaymentMethod.cod
                                ? const TextStyle(color: AppColor.primary)
                                : null),
                        disable: false),
                    const SizedBox(
                      height: 20,
                    ),
                    AppCheckBox(
                        onTap: () {
                          if (_paymentMethod != PaymentMethod.bankTransfer)
                            setState(() {
                              _paymentMethod = PaymentMethod.bankTransfer;
                            });
                        },
                        select: _paymentMethod == PaymentMethod.bankTransfer,
                        child: Text(PaymentMethod.bankTransfer.title,
                            style: _paymentMethod == PaymentMethod.bankTransfer
                                ? const TextStyle(color: AppColor.primary)
                                : null),
                        disable: false),
                  ],
                )),
            bottomChild: AppBtn(
              title: 'Chọn',
              onTap: () {
                if (_paymentMethod != widget.defaultPaymentMethod)
                  widget.onReturn(_paymentMethod);
                Navigator.of(context).pop();
              },
            )));
  }
}
*/

class PaymentMethodPage extends StatefulWidget {
  static void push(BuildContext context,
      {@required PaymentMethodModel defaultPaymentMethod,
      @required List<PaymentMethodModel> listPaymentMethod,
      @required Function(PaymentMethodModel) onReturn}) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => PaymentMethodPage(
              defaultPaymentMethod: defaultPaymentMethod,
              listPaymentMethod: listPaymentMethod,
              onReturn: onReturn,
            )));
  }

  const PaymentMethodPage(
      {this.defaultPaymentMethod, this.onReturn, this.listPaymentMethod});

  final PaymentMethodModel defaultPaymentMethod;
  final List<PaymentMethodModel> listPaymentMethod;
  final void Function(PaymentMethodModel) onReturn;

  @override
  _PaymentMethodPageState createState() => _PaymentMethodPageState();
}

class _PaymentMethodPageState extends State<PaymentMethodPage> {
  PaymentMethodModel _paymentMethod;

  @override
  void initState() {
    _paymentMethod = widget.defaultPaymentMethod;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
        title: 'Chọn phương thức thanh toán',
        showCartBtn: false,
        child: BottomActionContainer(
            child: Padding(
                padding: const EdgeInsets.only(
                    left: 26, top: 15, bottom: 15, right: 10),
                child: Column(
                    children:
                        List.generate(widget.listPaymentMethod.length, (index) {
                  return AppCheckBox(
                      onTap: () {
                        if (_paymentMethod != widget.listPaymentMethod[index])
                          setState(() {
                            _paymentMethod = widget.listPaymentMethod[index];
                          });
                      },
                      select: _paymentMethod == widget.listPaymentMethod[index],
                      child: Text(widget.listPaymentMethod[index].label,
                          style:
                              _paymentMethod == widget.listPaymentMethod[index]
                                  ? const TextStyle(color: AppColor.primary)
                                  : null),
                      disable: false);
                  // const SizedBox(
                  //   height: 20,
                  // );
                }))),
            bottomChild: AppBtn(
              title: 'Chọn',
              onTap: () {
                if (_paymentMethod != widget.defaultPaymentMethod)
                  widget.onReturn(_paymentMethod);
                Navigator.of(context).pop();
              },
            )));
  }
}
