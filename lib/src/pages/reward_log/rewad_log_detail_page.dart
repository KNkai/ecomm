import 'package:flutter/material.dart';
import 'package:genie/src/components/app_delay.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/components/big_ticket.dart';
import 'package:genie/src/models/reward_log.model.dart';
import 'package:genie/src/utils/util.dart';

class RewardLogDetailPage extends StatelessWidget {
  static void go({@required BuildContext context, @required RewardLog log}) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => AppDelay(
            child: RewardLogDetailPage(log),
            starter: AppScaffold(
              title: 'Chi tiết quà tặng',
              child: SizedBox.shrink(),
              showCartBtn: false,
            ))));
  }

  final RewardLog log;
  RewardLogDetailPage(this.log);
  @override
  Widget build(BuildContext context) {
    final item = log.reward;
    return AppScaffold(
        title: 'Chi tiết quà tặng',
        showCartBtn: false,
        child: Padding(
          child: ListView(
            children: [
              BigTicket(
                  urlImage: item.image,
                  smallTitle:
                      'Hạn dùng đến ${appConvertDateTime(item.startAt)} - ${appConvertDateTime(item.endAt)}',
                  title: item.name,
                  mode: BigTicketMode.none),
              const SizedBox(
                height: 32,
              ),
              Text(
                'Tên phần quà',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(item.name ?? ''),
              const SizedBox(
                height: 29,
              ),
              Text(
                'Số điểm đã đổi',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text((item.point != null ? item.point.toString() : '') + " điểm"),
              const SizedBox(
                height: 29,
              ),
              Text(
                'Mô tả',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text((item.content ?? '')),
            ],
          ),
          padding: EdgeInsets.symmetric(horizontal: 26, vertical: 14),
        ));
  }
}
