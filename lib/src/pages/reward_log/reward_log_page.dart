import 'package:flutter/material.dart';
import 'package:genie/src/components/app_delay.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/components/big_ticket.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/reward_log.model.dart';
import 'package:genie/src/pages/reward_log/reward_log_controller.dart';
import 'package:genie/src/pages/voucher_detail/voucher_detail.dart';
import 'package:genie/src/utils/util.dart';

class RewardLogPage extends StatelessWidget {
  RewardLogPage();
  static void go(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => AppDelay(
            child: RewardLogPage(),
            starter: AppScaffold(
                showCartBtn: false,
                title: 'Lịch sử đổi quà',
                child: SizedBox.shrink()))));
  }

  final _controller = RewardLogController();

  @override
  Widget build(BuildContext context) {
    _controller.evenInit();
    return AppScaffold(
        showCartBtn: false,
        title: 'Lịch sử đổi quà',
        child: StreamBuilder<ListState>(
          stream: _controller.stateList,
          builder: (_, snapshot) {
            if (snapshot?.data == null || snapshot.data is ListEmptyState)
              return SizedBox.shrink();

            final loaded = snapshot.data as ListLoadState<RewardLog>;

            return ListView.separated(
                padding: EdgeInsets.symmetric(horizontal: 26, vertical: 14),
                itemCount: loaded.data.length,
                separatorBuilder: (_, __) {
                  return SizedBox(
                    height: 14,
                  );
                },
                itemBuilder: (_, index) {
                  final item = loaded.data[index].reward;
                  return GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      VoucherDetail.push(
                          context: context,
                          title: item?.name,
                          subTitle: item?.campaign?.name,
                          imageCampaign: item?.image,
                          campaign: item?.campaign);
                      // RewardLogDetailPage.go(
                      //     context: context, log: loaded.data[index]);
                    },
                    child: BigTicket(
                        urlImage: item.image,
                        smallTitle:
                            'Hạn dùng đến ${appConvertDateTime(item.startAt)} - ${appConvertDateTime(item.endAt)}',
                        title: item.name,
                        mode: BigTicketMode.none),
                  );
                });
          },
        ));
  }
}
