import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/reward_log.model.dart';
import 'package:genie/src/repositories/reward_log.repo.dart';

class RewardLogController extends InitLoadMoreSearchController<RewardLog> {
  RewardLogController._(InitListProtocol initListProtocol)
      : super(initProtocol: initListProtocol);

  factory RewardLogController() {
    return RewardLogController._(RewardLogRepository());
  }
}
