import 'package:flutter/material.dart';
import 'package:genie/src/components/block_container.dart';
import 'package:genie/src/components/product_stack/component/product_item.dart';

import '../../components/app_scaffold.dart';
import '../../components/init_loadmore_search/init_loadmore_search_controller.dart';
import '../../components/refresh_scroll_view.dart';
import '../../models/product.model.dart';
import '../../repositories/favorite.repo.dart';
import 'favorite_controller.dart';

class FavoritePage extends StatelessWidget {
  static void push({
    BuildContext context,
  }) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => FavoritePage()));
  }

  @override
  Widget build(BuildContext context) {
    final favoriteRepository = FavoriteStackRepository();
    final controller = FavoriteController(
        initProtocol: favoriteRepository, loadMoreProtocol: favoriteRepository);

    controller.evenInit();

    return AppScaffold(
        title: "Danh sách yêu thích",
        child: RefreshScrollView(
          onRefresh: () async {
            controller.evenInit();
          },
          onLoadMore: () async {
            controller.eventLoadMore();
          },
          child: StreamBuilder<ListState>(
            stream: controller.stateList,
            builder: (_, ss) {
              if (ss?.data == null) return const SizedBox.shrink();

              if (ss.data is ListEmptyState)
                return const Center(
                    child: Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Text('Dữ liệu bị trống'),
                ));

              final ListLoadState<FavoriteProduct> stateProduct = ss.data;
              return BlockContainer(
                title: "",
                child: ListView.separated(
                    padding: const EdgeInsets.only(left: 26, right: 26),
                    itemCount: stateProduct.data.length,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    separatorBuilder: (context, index) =>
                        Divider(color: Colors.grey[100], thickness: 3),
                    itemBuilder: (_, index) {
                      return ProductItem(
                          favouriteController: controller,
                          isFavourite: true,
                          item: stateProduct.data[index].product.blockItem,
                          onDelete: () async {
                            controller.delFavouriteProdFromList(
                                stateProduct.data[index].id);
                            controller.evenInit();
                          });
                    }),
              );
            },
          ),
        ));
  }
}
