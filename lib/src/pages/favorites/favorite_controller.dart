import 'package:flutter/foundation.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/repositories/favorite.repo.dart';

class FavoriteController extends InitLoadMoreSearchController<FavoriteProduct>
    with ChangeNotifier {
  FavoriteController(
      {InitListProtocol initProtocol, LoadMoreProtocol loadMoreProtocol})
      : super(initProtocol: initProtocol, loadMoreProtocol: loadMoreProtocol);

  FavoriteRepository favoriteRepository = FavoriteRepository();
  FavoriteStackRepository favoriteStackRepository = FavoriteStackRepository();
  ValueNotifier<FavoriteProduct> check = ValueNotifier(null);
  delFavouriteProd() {
    if (check.value != null) {
      favoriteRepository.deleteOneFavouriteProd(check.value.id);
      checkFavoritesProd(id: check.value.productId);
    }
  }

  delFavouriteProdFromList(id) {
    favoriteRepository.deleteOneFavouriteProd(id);
  }

  checkFavoritesProd({String id}) async {
    check.value = await favoriteStackRepository.checkFavoritesProd(id: id);
    check.notifyListeners();
  }

  addFavouriteProd(String id) {
    favoriteRepository.addFavoriteProduct(id);
    checkFavoritesProd(id: id);
  }
}
