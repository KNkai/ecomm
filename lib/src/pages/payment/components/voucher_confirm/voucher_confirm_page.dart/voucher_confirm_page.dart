import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:genie/src/components/app_grey.dart';
import 'package:genie/src/components/app_loading.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/components/big_ticket.dart';
import 'package:genie/src/models/create_order_input.model.dart';
import 'package:genie/src/models/evoucher.model.dart';
import 'package:genie/src/pages/payment/components/voucher_confirm/voucher_confirm_controller.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';

class VoucherConfirmPage extends StatelessWidget {
  final CreateOrderInput createOrderInput;
  static void go({
    @required BuildContext context,
    @required VoidCallback imBack,
    @required VoucherConfirmController controller,
    CreateOrderInput createOrderInput,
  }) {
    Navigator.of(context)
        .push(MaterialPageRoute(
            builder: (_) => VoucherConfirmPage(
                  controller: controller,
                  createOrderInput: createOrderInput,
                )))
        .then((value) {
      imBack();
    });
  }

  VoucherConfirmPage({this.controller, this.createOrderInput});

  final _codeController = TextEditingController();
  final VoucherConfirmController controller;

  @override
  Widget build(BuildContext context) {
    List<ValueNotifier<bool>> _stateSelectVouchers;
    controller.eventInit(reset: true);
    controller.stateSide.stream.listen((state) {
      if (state is StateSideChooseSelect) {
        _stateSelectVouchers[state.index].value =
            !_stateSelectVouchers[state.index].value;
      } else if (state is StateSideShowMSg) {
        WarningDialog.show(context, state.msg, 'OK', title: 'Thông báo');
        _codeController.clear();
      }
    });

    return ValueListenableBuilder<bool>(
      valueListenable: controller.stateLoading,
      builder: (_, loading, child) {
        return AppLoading(
          loading: loading,
          child: child,
        );
      },
      child: AppScaffold(
          title: 'Chọn mã ưu đãi',
          showCartBtn: false,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            const AppGray(
              left: 'Chọn hoặc Nhập mã ưu đãi',
            ),
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 26),
              child: DecoratedBox(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 0.0)),
                child: Padding(
                  padding: const EdgeInsets.only(left: 26, right: 13),
                  child: Row(
                    children: [
                      Expanded(
                          child: TextField(
                        maxLines: 1,
                        controller: _codeController,
                        cursorColor: Colors.grey,
                        inputFormatters: [
                          _UpperCaseTextFormatter(),
                        ],
                        decoration: InputDecoration(
                          focusedBorder: InputBorder.none,
                          hintText: 'Nhập mã ưu đãi',
                          hintStyle:
                              TextStyle(fontSize: 14, color: Color(0xff9B9B9B)),
                          enabledBorder: InputBorder.none,
                          border: InputBorder.none,
                          labelStyle: TextStyle(color: Colors.green),
                        ),
                      )),
                      GestureDetector(
                        onTap: () {
                          controller.eventAddNewVoucher(_codeController.text);
                        },
                        behavior: HitTestBehavior.opaque,
                        child: const Text(
                          'Áp dụng',
                          style:
                              TextStyle(color: AppColor.primary, fontSize: 14),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            Expanded(
              child: ValueListenableBuilder<StateVouchers>(
                  valueListenable: controller.stateVouchers,
                  builder: (_, state, child) {
                    if (state is StateVouchersInit) return SizedBox.shrink();

                    if (state is StateVouchersEmtpy) return child;

                    final list = (state as StateVouchersLoaded).list;

                    _stateSelectVouchers = List.generate(
                        list.length,
                        (index) => ValueNotifier<bool>(controller
                            .mapSelectVoucher
                            .containsValue((list[index].data is Evoucher)
                                ? list[index].data.id
                                : false)));

                    return ListView.separated(
                      itemCount: list.length,
                      padding: EdgeInsets.only(bottom: 20),
                      separatorBuilder: (__, _) => const SizedBox(
                        height: 14,
                      ),
                      itemBuilder: (_, c) {
                        if (list[c].type == EvoucherModelType.title)
                          return Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 26),
                            child: Text(list[c]?.data ?? '',
                                style: TextStyle(
                                    fontSize: 15, color: AppColor.primary)),
                          );

                        return ValueListenableBuilder<bool>(
                          valueListenable: _stateSelectVouchers[c],
                          builder: (_, select, __) {
                            return GestureDetector(
                              onTap: () {
                                controller.eventChooseVoucher(c, list[c]);
                                controller.checkErrorVoucher(
                                    createOrderInput, list, () {
                                  _stateSelectVouchers.forEach((e) {
                                    e.value = false;
                                  });
                                });
                              },
                              behavior: HitTestBehavior.opaque,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 26),
                                child: BigTicket(
                                    select: select,
                                    mode: BigTicketMode.select,
                                    urlImage: (list[c].data as Evoucher)
                                        ?.campaign
                                        ?.image,
                                    smallTitle: 'Hạn dùng đến: ' +
                                        appConvertDateTime(
                                            (list[c].data as Evoucher)
                                                ?.campaign
                                                ?.endAt),
                                    title: (list[c].data as Evoucher)
                                            ?.campaign
                                            ?.name ??
                                        ''),
                              ),
                            );
                          },
                        );
                      },
                    );
                  },
                  child: Center(
                    child: Text('Không có mã voucher'),
                  )),
            ),
          ])),
    );
  }
}

class _UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text?.toUpperCase(),
      selection: newValue.selection,
    );
  }
}
