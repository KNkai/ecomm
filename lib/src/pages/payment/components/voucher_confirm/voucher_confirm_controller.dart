import 'dart:async';
import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:genie/src/models/create_order_input.model.dart';
import 'package:genie/src/models/evoucher.model.dart';
import 'package:genie/src/models/payment_method.model.dart';
import 'package:genie/src/repositories/evoucher.repo.dart';
import 'package:genie/src/repositories/order.repo.dart';

class VoucherConfirmController {
  final stateLoading = ValueNotifier(false);
  final _repo = EvoucherRepository();
  final _repoOrder = OrderRepository();
  final _mapSelectVoucher = HashMap<String, String>(); // Title, EvoucherID
  final _mapLoadedVouchers = HashMap<String, List<Evoucher>>();
  final stateSelectVouchers = ValueNotifier<List<Evoucher>>(null);

  StreamController<StateVoucherSide> stateSide;
  ValueNotifier<StateVouchers> stateVouchers;

  HashMap<String, String> get mapSelectVoucher =>
      _mapSelectVoucher; // type and id

  void eventInit({bool loading = false, bool reset = false}) {
    if (reset) {
      stateSide?.close();
      stateSide = StreamController<StateVoucherSide>();

      stateVouchers?.dispose();
      stateVouchers = ValueNotifier<StateVouchers>(StateVouchersInit());
    }
    _mapLoadedVouchers.clear();

    if (loading) stateLoading.value = true;
    _repo.getAllEvoucher().then((value) {
      try {
        if (value != null && value is List<EvoucherModel>) {
          stateVouchers.value = StateVouchersLoaded(list: value);
        } else
          stateVouchers.value = StateVouchersEmtpy();
      } finally {
        if (loading) stateLoading.value = false;
      }
    });
  }

  checkErrorVoucher(CreateOrderInput createOrderInput,
      List<EvoucherModel> listEvoucherModel, Function onError) {
    CreateOrderInput _createOrderInput = CreateOrderInput();
    _createOrderInput = createOrderInput;
    _createOrderInput.evoucherIds = [];
    _createOrderInput.paymentMethod = PaymentMethodModel(label: "", value: "");
    final list = (stateVouchers.value as StateVouchersLoaded)
        .list
        .where(
          (e) =>
              (e.data is Evoucher) &&
              _mapSelectVoucher.values.contains(e.data.id),
        )
        .map((e) => e.data as Evoucher)
        .toList();
    list.forEach(
      (e) {
        _createOrderInput?.evoucherIds?.add(e.id);
      },
    );

    _repoOrder.generateDraftOrder(_createOrderInput).then(
      (responseOrder) {
        print(responseOrder);
        if (responseOrder is List<String>) {
          if (responseOrder[1].contains('voucher')) {
            stateSide.add(StateSideShowMSg(msg: responseOrder[0]));
            onError();
            _mapSelectVoucher.clear();
          }
        }
      },
    );
  }

  void eventChooseVoucher(int newIndex, EvoucherModel model) {
    if (model.data is Evoucher) {
      final voucher = (model.data as Evoucher);
      final oldIdSelect = _mapSelectVoucher[voucher.groupTitle];
      if (oldIdSelect == null) {
        _mapSelectVoucher[voucher.groupTitle] = model.data.id;
        stateSide.add(StateSideChooseSelect(index: newIndex));
      } else {
        if (model.data.id != oldIdSelect) {
          final listVouchers =
              (stateVouchers.value as StateVouchersLoaded).list;
          listVouchers.length;
          int oldIndex = 0;
          for (int c = 0; c < listVouchers.length; c++) {
            if ((listVouchers[c].data is Evoucher) &&
                listVouchers[c].data.id == oldIdSelect) {
              oldIndex = c;
              break;
            }
          }
          stateSide.add(StateSideChooseSelect(
              index: oldIndex)); // unselect old voucher same type
          stateSide.add(StateSideChooseSelect(index: newIndex));
          _mapSelectVoucher[voucher.groupTitle] = model.data.id;
          ;
        } else {
          stateSide.add(StateSideChooseSelect(index: newIndex));
          _mapSelectVoucher.remove(voucher.groupTitle);
        }
      }
    }
  }

  void eventReloadAfterOpenPage() {
    if (stateVouchers.value is StateVouchersLoaded) {
      final list = (stateVouchers.value as StateVouchersLoaded)
          .list
          .where((e) =>
              (e.data is Evoucher) &&
              _mapSelectVoucher.values.contains(e.data.id))
          .map((e) => e.data as Evoucher)
          .toList();
      stateSelectVouchers.value = list.isEmpty ? null : list;
      return;
    }
    stateSelectVouchers.value = null;
  }

  void eventAddNewVoucher(String code) {
    _repo.takeEVoucherByCode(code).then((value) {
      if (value != null) {
        if (value is Evoucher) {
          stateSide.add(StateSideShowMSg(msg: 'Chọn voucher thành công'));
          eventInit(loading: true);
        } else if (value is String) {
          stateSide.add(StateSideShowMSg(msg: value));
        }
      } else {
        stateSide.add(StateSideShowMSg(msg: 'Chọn voucher không thành công'));
      }
    });
  }

  void dispose() {
    stateVouchers.dispose();
    stateLoading.dispose();
    stateSelectVouchers.dispose();
  }
}

abstract class StateVouchers {}

class StateVouchersLoaded extends StateVouchers {
  final List<EvoucherModel> list;
  StateVouchersLoaded({@required this.list});

  @override
  bool operator ==(other) => false;
}
// class StateVouchersLoaded extends StateVouchers {
//   final List<EvoucherModel> list;
//   StateVouchersLoaded({@required this.list});

//   @override
//   bool operator ==(other) => false;
// }

class StateVouchersEmtpy extends StateVouchers {}

class StateVouchersInit extends StateVouchers {}

enum EvoucherModelType { title, item }

abstract class StateVoucherSide {}

class StateSideChooseSelect extends StateVoucherSide {
  final int index;
  StateSideChooseSelect({@required this.index});
}

class StateSideShowMSg extends StateVoucherSide {
  final String msg;
  StateSideShowMSg({@required this.msg});
}
