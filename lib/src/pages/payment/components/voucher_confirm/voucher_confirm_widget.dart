import 'dart:async';

import 'package:flutter/material.dart';
import 'package:genie/src/components/small_ticket.dart';
import 'package:genie/src/models/create_order_input.model.dart';
import 'package:genie/src/models/evoucher.model.dart';
import 'package:genie/src/pages/detail_product/components/detail_discount_component.dart';
import 'package:genie/src/pages/payment/components/voucher_confirm/voucher_confirm_controller.dart';
import 'package:genie/src/pages/payment/components/voucher_confirm/voucher_confirm_page.dart/voucher_confirm_page.dart';
import 'package:genie/src/pages/payment/payment_controller.dart';
import 'package:genie/src/repositories/profile.repo.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:provider/provider.dart';

class VoucherConfirmWidget extends StatelessWidget {
  final VoucherConfirmController controller;
  final CreateOrderInput createOrderInput;
  const VoucherConfirmWidget(this.controller, this.createOrderInput);

  @override
  Widget build(BuildContext context) {
    // final controller = VoucherConfirmController();
    final paymentController =
        Provider.of<PaymentController>(context, listen: false);
    final _profileRepository = ProfileRepository();
    final side = StreamController<List<String>>();
    side.stream.listen((event) {
      paymentController.eventUpdateSelectVoucher(event);
    });
    return Padding(
      padding: const EdgeInsets.only(right: 18, left: 26, top: 14, bottom: 14),
      child: Row(
        children: [
          const Text('Voucher'),
          Expanded(
            child: Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    VoucherConfirmPage.go(
                        createOrderInput: createOrderInput,
                        context: context,
                        imBack: () {
                          controller.eventReloadAfterOpenPage();
                        },
                        controller: controller);
                  },
                  child: ValueListenableBuilder<List<Evoucher>>(
                      valueListenable: controller.stateSelectVouchers,
                      builder: (_, list, __) {
                        if (list == null || list.isEmpty) {
                          side.add(null);
                          return DecoratedBox(
                              child: Padding(
                                padding: EdgeInsets.all(7),
                                child: Text('Nhập mã ưu đãi',
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.grey)),
                              ),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 1, color: Colors.grey[300])));
                        }
                        side.add(list.map((e) => e.id).toList());
                        return FutureBuilder<dynamic>(
                            future: _profileRepository
                                .getOneSettingByKey('PROMOTION_COLOR'),
                            builder: (context, snapshot) {
                              return Wrap(
                                spacing: 5,
                                runSpacing: 5,
                                children: list
                                    .map((voucher) => SmallTicket(
                                          color: snapshot.data == null
                                              ? AppColor.primary
                                              : HexColor(
                                                  snapshot?.data['value'] ??
                                                      '000000'),
                                          title: voucher.campaign.code,
                                        ))
                                    // SmallTicket(title: voucher.campaign.code))
                                    .toList(),
                              );
                            });
                      }),
                )),
          ),
          const SizedBox(
            width: 10,
          ),
          Icon(
            Icons.arrow_forward_ios,
            color: Colors.grey[400],
            size: 15,
          )
        ],
      ),
    );
  }
}
