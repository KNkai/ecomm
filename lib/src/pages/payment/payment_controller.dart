import 'dart:async';

import 'package:collection/collection.dart';
import 'package:flutter/widgets.dart';
import 'package:genie/src/helper/cart_data.dart';
import 'package:genie/src/models/create_order_input.model.dart';
import 'package:genie/src/models/delivery_address.model.dart';
import 'package:genie/src/models/delivery_method.model.dart';
import 'package:genie/src/models/order.model.dart';
import 'package:genie/src/models/payment_method.model.dart';
import 'package:genie/src/pages/order_detail/order_detail_page.dart';
import 'package:genie/src/pages/payment_method/payment_method_controller.dart';
import 'package:genie/src/repositories/delivery_address.repo.dart';
import 'package:genie/src/repositories/delivery_method.repo.dart';
import 'package:genie/src/repositories/order.repo.dart';
import 'package:genie/src/repositories/payment_method.repo.dart';
import 'package:genie/src/utils/util.dart';
import 'package:momo_vn/momo_vn.dart';

class PaymentController {
  PaymentController({@required this.cartData});

  final CartData cartData;
  final _repoDeliveryAddress = DeliveryAddressRepository();
  final _paymentMethodRepo = PaymentMethodRepository();
  final _deliveryMethodRepo = DeliveryMethodRepository();
  final _repoOrder = OrderRepository();
  final stateDefaultAddress = StreamController<StateDefaultAddress>();
  final statePaymentMethod = ValueNotifier<PaymentMethod>(PaymentMethod.cod);
  final stateShipMethods = ValueNotifier<List<ModelShipMethod>>(null);
  final statePayment = ValueNotifier<StatePayment>(StatePaymentLoading());
  final stateLoadingUpdate = ValueNotifier<bool>(false);
  final stateSide = ValueNotifier<StateSidePayment>(null);
  final statePaymentMethodModel = StreamController<PaymentMethodState>();
  final _equality = ListEquality();

  List<PaymentMethodModel> listPaymentMethod = [];

  String _note;
  DeliveryAddress _choosenAddress;
  PaymentMethodModel _choosePaymentMethod;
  ModelShipMethod _choosenShipMethod;
  List<String> _choosenVouchers;

  MomoVn _momoPay;
  PaymentResponse _momoPaymentResult;
  String _payment_status;
  String _currentOrderId;
  BuildContext _paymentContext;

  List<String> get _getChoosenVouchers {
    return _choosenVouchers == null ? null : _choosenVouchers.toList();
  }

  void dispose() {
    stateDefaultAddress.close();
    statePaymentMethod.dispose();
    stateShipMethods.dispose();
    statePayment.dispose();
    stateLoadingUpdate.dispose();
    stateSide.dispose();
    statePaymentMethodModel.close();
  }

  void eventUpdateSelectVoucher(List<String> vouchers) {
    final sort = vouchers == null ? null : (vouchers..sort());
    bool run = (sort == null && _choosenVouchers != null) ||
        (sort != null && _choosenVouchers == null) ||
        ((sort != null && _choosenVouchers != null) &&
            (!_equality.equals(sort, _choosenVouchers)));
    if (run) {
      _choosenVouchers = sort;
      stateLoadingUpdate.value = true;
      _repoOrder
          .generateDraftOrder(CreateOrderInput(
              provinceId: _choosenAddress?.provinceId,
              wardId: _choosenAddress?.wardId,
              customerName: _choosenAddress?.name,
              customerPhone: _choosenAddress?.phone,
              address: _choosenAddress?.address,
              paymentMethod: _choosePaymentMethod,
              evoucherIds: _getChoosenVouchers,
              shipMethod: _choosenShipMethod.method.value,
              districtId: _choosenAddress?.districtId,
              items: cartData.mapCart.entries.map((e) => e.value).toList()))
          .then((responseOrder) {
        try {
          _onResponseOrder(
              responseOrder: responseOrder,
              onNull: () {
                stateSide.value = StateSidePaymentShowMsg(
                    msg: 'Có lỗi xảy ra vui lòng thử lại');
              },
              onOrder: (responseOrder) {
                statePayment.value = StatePaymentSuccess(order: responseOrder);
              });
        } finally {
          stateLoadingUpdate.value = false;
        }
      });
    }
  }

  // void eventPay() {
  //   if (_choosenAddress == null) {
  //     stateSide.value =
  //         StateSidePaymentShowMsg(msg: 'Chưa có địa chỉ giao hàng');
  //   } else if (_choosePaymentMethod == null) {
  //     stateSide.value =
  //         StateSidePaymentShowMsg(msg: 'Chưa chọn phương thức thanh toán');
  //   } else if (_choosenShipMethod == null) {
  //     stateSide.value =
  //         StateSidePaymentShowMsg(msg: 'Chưa chọn nhà vận chuyển');
  //   } else {
  //     stateLoadingUpdate.value = true;
  //     _repoOrder
  //         .createOrder(CreateOrderInput(
  //             evoucherIds: _getChoosenVouchers,
  //             provinceId: _choosenAddress.provinceId,
  //             wardId: _choosenAddress.wardId,
  //             customerName: _choosenAddress.name,
  //             customerPhone: _choosenAddress.phone,
  //             address: _choosenAddress.address,
  //             paymentMethod: _choosePaymentMethod,
  //             shipMethod: _choosenShipMethod.method.value,
  //             districtId: _choosenAddress.districtId,
  //             note: _note,
  //             items: cartData.mapCart.entries.map((e) => e.value).toList()))
  //         .then((responseOrder) {
  //       try {
  //         _onResponseOrder(
  //             responseOrder: responseOrder,
  //             onNull: () {
  //               stateSide.value = StateSidePaymentShowMsg(
  //                   msg: 'Có lỗi xảy ra khi thanh toán vui lòng thử lại');
  //             },
  //             onOrder: (responesOrder) {
  //               stateSide.value = StateSidePaymentSuccessPay(
  //                   method: statePaymentMethod.value);
  //             });
  //       } finally {
  //         stateLoadingUpdate.value = false;
  //       }
  //     });
  //   }
  // }
  //
  void eventPay(BuildContext context) {
    _paymentContext = context;
    if (_choosenAddress == null) {
      stateSide.value =
          StateSidePaymentShowMsg(msg: 'Chưa có địa chỉ giao hàng');
    } else if (_choosePaymentMethod == null) {
      stateSide.value =
          StateSidePaymentShowMsg(msg: 'Chưa chọn phương thức thanh toán');
    } else if (_choosenShipMethod == null) {
      stateSide.value =
          StateSidePaymentShowMsg(msg: 'Chưa chọn nhà vận chuyển');
    } else {
      stateLoadingUpdate.value = true;
      if (_choosePaymentMethod.value == 'MOMO') {
        _paymentByMoMo(context);
      } else {
        _repoOrder
            .createOrder(CreateOrderInput(
                evoucherIds: _getChoosenVouchers,
                provinceId: _choosenAddress.provinceId,
                wardId: _choosenAddress.wardId,
                customerName: _choosenAddress.name,
                customerPhone: _choosenAddress.phone,
                address: _choosenAddress.address,
                paymentMethod: _choosePaymentMethod,
                shipMethod: _choosenShipMethod.method.value,
                districtId: _choosenAddress.districtId,
                note: _note,
                items: cartData.mapCart.entries.map((e) => e.value).toList()))
            .then((responseOrder) {
          try {
            _onResponseOrder(
                responseOrder: responseOrder,
                onNull: () {
                  stateSide.value = StateSidePaymentShowMsg(
                      msg: 'Có lỗi xảy ra khi thanh toán vui lòng thử lại');
                },
                onOrder: (responesOrder) {
                  stateSide.value = StateSidePaymentSuccessPay(
                      method: statePaymentMethod.value);
                });
          } finally {
            stateLoadingUpdate.value = false;
          }
        });
      }
    }
  }

  _paymentByMoMo(BuildContext context) async {
    _momoPay = MomoVn();
    _momoPay.on(MomoVn.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _momoPay.on(MomoVn.EVENT_PAYMENT_ERROR, _handlePaymentError);
    var responseOrder = await _repoOrder.createOrder(CreateOrderInput(
        evoucherIds: _getChoosenVouchers,
        provinceId: _choosenAddress.provinceId,
        wardId: _choosenAddress.wardId,
        customerName: _choosenAddress.name,
        customerPhone: _choosenAddress.phone,
        address: _choosenAddress.address,
        paymentMethod: _choosePaymentMethod,
        shipMethod: _choosenShipMethod.method.value,
        districtId: _choosenAddress.districtId,
        note: _note,
        items: cartData.mapCart.entries.map((e) => e.value).toList()));
    if (responseOrder != null && responseOrder is Order) {
      _currentOrderId = responseOrder.id;
      print("extra: ${responseOrder.paymentInfo.extra}");
      MomoPaymentInfo options = MomoPaymentInfo(
        merchantname: responseOrder.paymentInfo.merchantnamelabel,
        appScheme: responseOrder.paymentInfo.appScheme,
        merchantcode: responseOrder.paymentInfo.merchantcode,
        amount: responseOrder.paymentInfo.amount,
        orderId: responseOrder.paymentInfo.orderId,
        orderLabel: responseOrder.paymentInfo.orderLabel,
        merchantnamelabel: responseOrder.paymentInfo.merchantnamelabel,
        fee: responseOrder.paymentInfo.fee,
        description: responseOrder.paymentInfo.description,
        username: responseOrder.paymentInfo.username,
        partner: responseOrder.paymentInfo.partner,
        extra: responseOrder.paymentInfo.extra,
        isTestMode: responseOrder.paymentInfo.isTestMode,
      );
      try {
        // if (responseOrder.paymentInfo.amount < 5000000 &&
        //     responseOrder.paymentInfo.amount > 0) {
        //   _momoPay.open(options);
        // } else {
        //   stateSide.value = StateSidePaymentShowMsg(
        //       msg: 'Thanh toán bằng MOMO không được vượt quá 5,000,000đ');
        // }
        _momoPay.open(options);
      } catch (e) {
        print('xay ra loi luc tahnh toan $e');
      } finally {
        stateLoadingUpdate.value = false;
        CartData.getCtl(context).eventReset();
      }
    }
  }

  void _handlePaymentSuccess(PaymentResponse response) {
    _momoPaymentResult = response;
    if (_currentOrderId != null) {
      _repoOrder
          .payMomo(_currentOrderId, response.token, response.phonenumber)
          .then((value) {
        // print("in ra gia tri tai day ne: ${value.id}");
        // print("in ra gia tri tai day ne: ${value.address}");
        // print("in ra gia tri tai day ne: ${value.amount}");
        // print("in ra gia tri tai day ne: ${value.customerName}");
        // print("in ra gia tri tai day ne: ${value.customerPhone}");
        if (_paymentContext != null) {
          if (value != null) {
            OrderDetailPage.pushNew(context: _paymentContext, id: value.id);
            showLoaderDialog(_paymentContext, value.id, "Thanh toán thành công",
                isPushNew: false);
          } else {
            OrderDetailPage.pushNew(
                context: _paymentContext, id: _currentOrderId);
            showLoaderDialog(
                _paymentContext, _currentOrderId, "Thanh toán thất bại",
                isPushNew: false);
          }
        }
      }).catchError((e) {
        // print("in ra gia tri tai day ne nhe: ${e}");
        return null;
      });
    }
  }

  void _handlePaymentError(PaymentResponse response) {
    _momoPaymentResult = response;
    stateLoadingUpdate.value = false;
    if (_paymentContext != null) {
      showLoaderDialog(_paymentContext, _currentOrderId,
          "Thanh toán thất bại vui lòng đợi...");
    }
  }

  void eventInit({bool addLoading = false}) async {
    if (addLoading) statePayment.value = StatePaymentLoading();
    listPaymentMethod = await _paymentMethodRepo.getAllPayMentMethod();
    _choosePaymentMethod = listPaymentMethod[0];

    stateShipMethods.value = await _deliveryMethodRepo.getAllDeliveryMethod();
    _choosenShipMethod = stateShipMethods.value[0]..select = true;

    final responseDefaultAddress =
        await _repoDeliveryAddress.getDefaultDeliveryAddress();
    _generateDraftOrderWithAddress(responseDefaultAddress);
  }

  void eventLoadAddress() async {
    final responseDefaultAddress =
        await _repoDeliveryAddress.getDefaultDeliveryAddress();
    _generateDraftOrderWithAddress(responseDefaultAddress);
  }

  void eventChangePaymentMethod(PaymentMethodModel code) {
    statePaymentMethodModel
        .add(SelectedPaymentMethod(paymentMethodModel: code));
    _choosePaymentMethod = code;
  }

  void eventLoadPaymentMethod({bool addLoding = false}) {
    if (addLoding) statePaymentMethodModel.add(PaymentMethodStateLoading());
    _choosePaymentMethod = null;
    _paymentMethodRepo.getAllPayMentMethod().then((value) {
      if (value != null && value is List<PaymentMethodModel>) {
        _choosePaymentMethod = value[0];
        statePaymentMethodModel
            .add(LoadedPaymentMethodSuccess(listPaymentMethod: value));
        statePaymentMethodModel.add(
            SelectedPaymentMethod(paymentMethodModel: _choosePaymentMethod));
      } else
        statePaymentMethodModel.add(LoadedPaymentMethodFailed());
    });
  }

  void eventChangeShipMethod(int index, ModelShipMethod shipMethod) {
    stateLoadingUpdate.value = true;
    _repoOrder
        .generateDraftOrder(CreateOrderInput(
            evoucherIds: _getChoosenVouchers,
            provinceId: _choosenAddress?.provinceId,
            wardId: _choosenAddress?.wardId,
            customerName: _choosenAddress?.name,
            customerPhone: _choosenAddress?.phone,
            address: _choosenAddress?.address,
            paymentMethod: _choosePaymentMethod,
            shipMethod: shipMethod.method.value,
            districtId: _choosenAddress?.districtId,
            items: cartData.mapCart.entries.map((e) => e.value).toList()))
        .then((responseOrder) {
      try {
        _onResponseOrder(
            responseOrder: responseOrder,
            onOrder: (responseOrder) {
              final list =
                  stateShipMethods.value.map((e) => e..select = false).toList();
              list[index] = shipMethod..select = true;
              _choosenShipMethod = shipMethod;
              stateShipMethods.value = list;

              statePayment.value = StatePaymentSuccess(order: responseOrder);
            },
            onNull: () {
              stateSide.value = StateSidePaymentShowMsg(
                  msg: 'Có lỗi xảy ra vui lòng thử lại');
            });
      } finally {
        stateLoadingUpdate.value = false;
      }
    });
  }

  void _onResponseOrder(
      {@required dynamic responseOrder,
      @required VoidCallback onNull,
      @required void Function(Order) onOrder}) {
    if (responseOrder == null) {
      onNull();
    } else {
      if (responseOrder is Order) {
        onOrder(responseOrder);
      } else if (responseOrder is List<String>) {
        // invalid

        stateSide.value = StateSidePaymentShowMsg(
            msg: "${responseOrder[0]}.${responseOrder[1]}");
      }
    }
  }

  void _generateDraftOrderWithAddress(responseDefaultAddress) async {
    if (responseDefaultAddress == null) {
      _choosenAddress = null;
      statePayment.value = StatePaymentFailed();
    } else {
      var responseOrder;
      if (responseDefaultAddress is DeliveryAddress) {
        _choosenAddress = responseDefaultAddress;
      } else {
        // EmptyDeliveryAddress
        _choosenAddress = null;
      }

      responseOrder = await _repoOrder.generateDraftOrder(CreateOrderInput(
          evoucherIds: _getChoosenVouchers,
          paymentMethod: _choosePaymentMethod,
          shipMethod: _choosenShipMethod.method.value,
          provinceId: _choosenAddress?.provinceId,
          wardId: _choosenAddress?.wardId,
          customerName: _choosenAddress?.name,
          customerPhone: _choosenAddress?.phone,
          address: _choosenAddress?.address,
          districtId: _choosenAddress?.districtId,
          items: cartData.mapCart.entries.map((e) => e.value).toList()));

      if (responseOrder != null && responseOrder is Order) {
        if (responseDefaultAddress is DeliveryAddress) {
          stateDefaultAddress.add(
            StateDefaultAddressLoaded(
              fullAddress: appFullAddress(
                address: responseDefaultAddress.address,
                ward: responseDefaultAddress.ward,
                district: responseDefaultAddress.district,
                province: responseDefaultAddress.province,
              ),
              phone: responseDefaultAddress.phone,
              name: responseDefaultAddress.name,
            ),
          );
        } else if (responseDefaultAddress is EmptyDeliveryAddress) {
          stateDefaultAddress.add(StateDefaultAddressEmpty());
        }

        statePaymentMethodModel.add(
            LoadedPaymentMethodSuccess(listPaymentMethod: listPaymentMethod));
        statePaymentMethodModel.add(
            SelectedPaymentMethod(paymentMethodModel: listPaymentMethod[0]));

        statePayment.value = StatePaymentSuccess(order: responseOrder);
      } else if (responseOrder != null && responseOrder is List<String>) {
        //invalid
        stateSide.value = StateSidePaymentShowMsg(msg: responseOrder[0]);
      } else
        stateDefaultAddress.add(StateDefaultAddressFailed());
    }
  }

  void eventChangeNote(String note) {
    _note = note;
  }
}

abstract class StateSidePayment {}

class StateSidePaymentShowMsg extends StateSidePayment {
  final String msg;
  StateSidePaymentShowMsg({@required this.msg});
}

class StateSidePaymentSuccessPay extends StateSidePayment {
  final PaymentMethod method;
  StateSidePaymentSuccessPay({@required this.method});
}

abstract class StatePayment {}

class StatePaymentFailed extends StatePayment {}

class StatePaymentSuccess extends StatePayment {
  final Order order;

  StatePaymentSuccess({this.order});
}

class StatePaymentLoading extends StatePayment {}

abstract class StateDefaultAddress {}

class StateDefaultAddressLoaded extends StateDefaultAddress {
  final String fullAddress;
  final String phone;
  final String name;

  StateDefaultAddressLoaded(
      {@required this.fullAddress, @required this.phone, @required this.name});
}

class StateDefaultAddressFailed extends StateDefaultAddress {}

class StateDefaultAddressEmpty extends StateDefaultAddress {}

class StateDefaultAddressLoading extends StateDefaultAddress {}

class ModelShipMethod {
  bool select;
  final DeliveryMethod method;
  ModelShipMethod({this.method, this.select = false});
}
