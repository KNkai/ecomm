import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:genie/src/components/error_text.dart';
import 'package:genie/src/components/final_info_item.dart';
import 'package:genie/src/models/create_order_input.model.dart';
import 'package:genie/src/models/discount_log.model.dart';
import 'package:genie/src/pages/payment/components/voucher_confirm/voucher_confirm_widget.dart';
import 'package:genie/src/pages/payment_method/payment_method_controller.dart';
import 'package:genie/src/pages/payment_method/payment_method_page.dart';
import 'package:provider/provider.dart';

import '../../components/app_btn.dart';
import '../../components/app_checkbox.dart';
import '../../components/app_grey.dart';
import '../../components/app_loading.dart';
import '../../components/app_scaffold.dart';
import '../../components/bottom_action_container.dart';
import '../../helper/auth.dart';
import '../../helper/cart_data.dart';
import '../../models/order_item.model.dart';
import '../../utils/app_color.dart';
import '../../utils/util.dart';
import '../delivery_address/delivery_address_page.dart';
import '../order_history/components/item_product_order.dart';
import '../payment_success/payment_success_page.dart';
import 'components/voucher_confirm/voucher_confirm_controller.dart';
import 'payment_controller.dart';

class PaymentPage extends StatefulWidget {
  static void go(BuildContext context) {
    Auth.checkExistUser(
        context: context,
        onLoginSuccess: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => PaymentPage()));
        });
  }

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  bool isFocusTextfiled = false;
  @override
  Widget build(BuildContext context) {
    final cartData = CartData.getCtl(context);
    final controller = PaymentController(cartData: cartData);
    final voucherController = VoucherConfirmController();
    controller.stateSide.addListener(() {
      if (controller.stateSide.value is StateSidePaymentSuccessPay) {
        final method =
            (controller.stateSide.value as StateSidePaymentSuccessPay).method;
        CartData.getCtl(context).eventReset();
        PaymentSuccessPage.push(context, method: method);
      } else if (controller.stateSide.value is StateSidePaymentShowMsg) {
        final msg = (controller.stateSide.value as StateSidePaymentShowMsg).msg;
        //check condition voucher duplicate
        WarningDialog.show(context, "${msg.split('.')[0]}.", 'OK',
            title: 'Thông báo');
      }
    });

    controller.eventInit();
    return Provider<PaymentController>(
      create: (_) => controller,
      dispose: (_, con) => con.dispose(),
      child: ValueListenableBuilder(
        valueListenable: controller.stateLoadingUpdate,
        builder: (_, loadingUpdate, child) {
          return AppLoading(
            child: child,
            loading: loadingUpdate,
          );
        },
        child: AppScaffold(
            isPaddingContact: true,
            showCartBtn: false,
            title: 'Thanh toán',
            child: BottomActionContainer(
              bottomChild: AppBtn(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                title: 'Xác nhận',
                onTap: () {
                  isFocusTextfiled
                      ? SystemChannels.textInput
                          .invokeMethod('TextInput.hide')
                          .then((value) {
                          isFocusTextfiled = false;
                        })
                      : controller.eventPay(context);
                },
              ),
              child: SingleChildScrollView(
                child: ValueListenableBuilder<StatePayment>(
                  valueListenable: controller.statePayment,
                  builder: (_, value, __) {
                    if (value is StatePaymentLoading)
                      return const Padding(
                          padding: EdgeInsets.only(top: 40),
                          child: CupertinoActivityIndicator());

                    if (value is StatePaymentFailed)
                      return ErrorText(
                          onClick: () =>
                              controller.eventInit(addLoading: true));

                    final StatePaymentSuccess paymentLoaded = value;

                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AppGray(
                          onRightTap: () {
                            DeliveryAddressPage.push(context, onRefresh: () {
                              controller.eventLoadAddress();
                            });
                          },
                          left: 'Thông tin giao hàng',
                          right: 'Thay đổi',
                        ),
                        StreamBuilder<StateDefaultAddress>(
                            stream: controller.stateDefaultAddress.stream,
                            builder: (_, ss) {
                              if (ss.data == null ||
                                  ss.data is StateDefaultAddressLoading)
                                return const Padding(
                                  padding: EdgeInsets.symmetric(vertical: 10),
                                  child: Center(
                                    child: CupertinoActivityIndicator(),
                                  ),
                                );

                              if (ss.data is StateDefaultAddressFailed)
                                return ErrorText(
                                    onClick: () =>
                                        controller.eventLoadAddress());

                              if (ss.data is StateDefaultAddressEmpty) {
                                return GestureDetector(
                                  behavior: HitTestBehavior.opaque,
                                  onTap: () {
                                    DeliveryAddressPage.push(context,
                                        onRefresh: () {
                                      controller.eventLoadAddress();
                                    });
                                  },
                                  child: const Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 26, vertical: 18),
                                    child: Text(
                                      '+ Tạo địa chỉ giao hàng',
                                      style: TextStyle(color: AppColor.primary),
                                    ),
                                  ),
                                );
                              }

                              final defaultDelivery =
                                  ss.data as StateDefaultAddressLoaded;

                              return Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 26, vertical: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      defaultDelivery.name ?? '',
                                      style: const TextStyle(
                                        color: AppColor.primary,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Text(defaultDelivery.phone ?? ''),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(defaultDelivery.fullAddress),
                                  ],
                                ),
                              );
                            }),
                        const AppGray(
                          left: 'Giỏ hàng của bạn',
                        ),
                        Column(
                          children: [
                            for (OrderItem orderItem
                                in paymentLoaded.order.items)
                              ItemProductOrder(orderItem: orderItem)
                          ],
                        ),
                        const AppGray(
                          left: 'Nhà vận chuyển',
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 26, vertical: 5),
                          child: ValueListenableBuilder<List<ModelShipMethod>>(
                            valueListenable: controller.stateShipMethods,
                            builder: (_, shipMethods, __) {
                              if (shipMethods == null || shipMethods.isEmpty)
                                return SizedBox.shrink();
                              return Column(
                                children: [
                                  for (int index = 0;
                                      index < shipMethods.length;
                                      index++) ...[
                                    AppCheckBox(
                                      onTap: () {
                                        controller.eventChangeShipMethod(
                                            index, shipMethods[index]);
                                      },
                                      disable: false,
                                      select: shipMethods[index].select,
                                      child: _CheckBoxTextDeliveryService(
                                        select: shipMethods[index].select,
                                        title:
                                            shipMethods[index]?.method?.label ??
                                                '',
                                      ),
                                    ),
                                    const Divider(),
                                  ]
                                ],
                              );
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 26, vertical: 5),
                          child: TextField(
                            scrollPadding: EdgeInsets.zero,
                            minLines: 2,
                            maxLines: 2,
                            onChanged: (changed) {
                              controller.eventChangeNote(changed);
                              isFocusTextfiled = true;
                            },
                            cursorColor: Colors.grey,
                            decoration: InputDecoration(
                              focusedBorder: InputBorder.none,
                              hintText:
                                  ' + Ghi chú cho đơn hàng: \n+ Thời gian giao hàng mong muốn.',
                              hintStyle: TextStyle(
                                  fontSize: 13, color: Colors.grey[400]),
                              enabledBorder: InputBorder.none,
                              border: const OutlineInputBorder(),
                            ),
                          ),
                        ),
                        AppGray(
                          left:
                              'Tổng tiền (${cartData.mapCart.length} sản phẩm)',
                          right: appCurrency(paymentLoaded.order.subtotal),
                          rightStyle: const TextStyle(color: AppColor.primary),
                        ),
                        SizedBox(
                          height: 4,
                          width: double.infinity,
                          child: ColoredBox(
                            color: Colors.grey[100],
                          ),
                        ),
                        VoucherConfirmWidget(
                          voucherController,
                          CreateOrderInput(
                            shipMethod: paymentLoaded.order.shipMethod,
                            paymentMethod: null,
                            items: cartData.mapCart.values.toList(),
                            customerName: paymentLoaded.order.customerName,
                            provinceId: paymentLoaded.order.provinceId,
                            wardId: paymentLoaded.order.wardId,
                            address: paymentLoaded.order.address,
                            districtId: paymentLoaded.order.districtId,
                          ),
                        ),
                        if (paymentLoaded?.order?.offerItems != null &&
                            paymentLoaded.order.offerItems.isNotEmpty)
                          Padding(
                              padding: const EdgeInsets.only(
                                  right: 18, left: 26, top: 14, bottom: 14),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Quà tặng kèm theo'),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      children: [
                                        for (int c = 0;
                                            c <
                                                paymentLoaded
                                                    .order.offerItems.length;
                                            c++)
                                          Row(
                                            children: [
                                              SizedBox(
                                                width: 111,
                                                child: DecoratedBox(
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color:
                                                              Colors.grey[200],
                                                          width: 0.8)),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      paymentLoaded
                                                                      ?.order
                                                                      ?.offerItems[
                                                                          c]
                                                                      ?.product
                                                                      ?.listImage !=
                                                                  null ||
                                                              paymentLoaded
                                                                  .order
                                                                  .offerItems[c]
                                                                  .product
                                                                  .listImage
                                                                  .isEmpty
                                                          ? Image.network(
                                                              paymentLoaded
                                                                  .order
                                                                  .offerItems[c]
                                                                  .product
                                                                  .listImage[0],
                                                              fit: BoxFit.cover,
                                                              width: 111,
                                                              height: 111)
                                                          : SizedBox(
                                                              width: 111,
                                                              height: 111),
                                                      const SizedBox(
                                                        height: 10,
                                                      ),
                                                      Padding(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 5),
                                                        child: Text(
                                                            paymentLoaded
                                                                    ?.order
                                                                    ?.offerItems[
                                                                        c]
                                                                    ?.product
                                                                    ?.name ??
                                                                '',
                                                            style: TextStyle(
                                                                fontSize: 12)),
                                                      ),
                                                      const SizedBox(
                                                        height: 10,
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                horizontal: 8),
                                                        child: Divider(
                                                          thickness: 0.8,
                                                          color:
                                                              Colors.grey[200],
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        height: 5,
                                                      ),
                                                      Padding(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 5),
                                                        child: Text(
                                                          appCurrency(
                                                              paymentLoaded
                                                                  ?.order
                                                                  ?.offerItems[
                                                                      c]
                                                                  ?.basePrice),
                                                          style: const TextStyle(
                                                              color: AppColor
                                                                  .primary,
                                                              fontSize: 12),
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        height: 9,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              const Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 12),
                                                child: Icon(
                                                  Icons.add,
                                                  size: 20,
                                                  color: Color(0xfff9b9b9b),
                                                ),
                                              )
                                            ],
                                          )
                                      ],
                                    ),
                                  )
                                ],
                              )),
                        StreamBuilder<PaymentMethodState>(
                          stream: controller.statePaymentMethodModel.stream,
                          builder: (_, ss) {
                            if (ss.data == null ||
                                ss.data is PaymentMethodStateLoading)
                              return const Padding(
                                padding: EdgeInsets.symmetric(vertical: 10),
                                child: Center(
                                  child: CupertinoActivityIndicator(),
                                ),
                              );
                            if (ss.data is LoadedPaymentMethodFailed)
                              return ErrorText(
                                  onClick: () => controller
                                      .eventLoadPaymentMethod(addLoding: true));

                            final defaultPaymentMethod =
                                ss.data as SelectedPaymentMethod;

                            // final listPaymenMethod =
                            //     ss.data as LoadedPaymentMethodSuccess;

                            return GestureDetector(
                              behavior: HitTestBehavior.opaque,
                              onTap: () {
                                PaymentMethodPage.push(
                                  context,
                                  defaultPaymentMethod:
                                      defaultPaymentMethod.paymentMethodModel,
                                  onReturn: (paymentCode) {
                                    controller
                                        .eventChangePaymentMethod(paymentCode);
                                  },
                                  listPaymentMethod:
                                      controller.listPaymentMethod,
                                );
                              },
                              child: Padding(
                                  padding: const EdgeInsets.only(
                                      right: 18, left: 26, top: 14, bottom: 14),
                                  child: Row(
                                    children: [
                                      const Text('Phương thức thanh toán'),
                                      const SizedBox(
                                        width: 30,
                                      ),
                                      Expanded(
                                        child: Text(
                                            defaultPaymentMethod
                                                .paymentMethodModel.label,
                                            textAlign: TextAlign.right,
                                            style: const TextStyle(
                                                color: AppColor.primary)),
                                      ),
                                      const SizedBox(
                                        width: 7,
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.grey[400],
                                        size: 15,
                                      )
                                    ],
                                  )),
                            );
                          },
                        ),
                        ColoredBox(
                            color: AppColor.primary,
                            child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 18, bottom: 18, left: 26, right: 16),
                                child: Column(
                                  children: [
                                    FinalInfoItem(
                                        title: 'Tổng tiền hàng',
                                        value: appCurrency(
                                            paymentLoaded.order.subtotal)),
                                    FinalInfoItem(
                                      title: 'Phí vận chuyển',
                                      value: appCurrency(
                                          paymentLoaded.order.shipfee),
                                    ),
                                    if (paymentLoaded.order?.discountLogs !=
                                        null)
                                      ...paymentLoaded.order.discountLogs
                                          .where((element) =>
                                              element.type ==
                                                  DiscountLogType.shipFee ||
                                              element.type ==
                                                  DiscountLogType.bonusPoint ||
                                              element.type ==
                                                  DiscountLogType.subTotal)
                                          .map((e) => FinalInfoItem(
                                                title:
                                                    'Voucher ${e.type.title}',
                                                minus: e.type ==
                                                        DiscountLogType
                                                            .bonusPoint
                                                    ? false
                                                    : true,
                                                value: e.type ==
                                                        DiscountLogType
                                                            .bonusPoint
                                                    ? e.value == null
                                                        ? ''
                                                        : e.value.toString()
                                                    : appCurrency(e.value),
                                              ))
                                  ],
                                ))),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 26, right: 24, top: 18),
                          child: Row(
                            children: [
                              const Text('Tổng Thanh Toán',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              Expanded(
                                child: Text(
                                    appCurrency(paymentLoaded.order.amount),
                                    textAlign: TextAlign.right,
                                    style: const TextStyle(
                                        color: AppColor.primary,
                                        fontWeight: FontWeight.bold)),
                              )
                            ],
                          ),
                        ),
                        paymentLoaded.order.rewardPoint != 0
                            ? Padding(
                                padding: const EdgeInsets.only(
                                    left: 26, right: 24, top: 18),
                                child: Row(
                                  children: [
                                    const Text('Điểm thưởng',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                    Expanded(
                                      child: Text(
                                          appConvertStringDouble(
                                              paymentLoaded.order.rewardPoint),
                                          textAlign: TextAlign.right,
                                          style: const TextStyle(
                                              color: AppColor.primary,
                                              fontWeight: FontWeight.bold)),
                                    )
                                  ],
                                ),
                              )
                            : SizedBox(),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 26, right: 24, top: 18),
                          child: Row(
                            children: [
                              const Text('Điểm tích luỹ',
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              Expanded(
                                child: Text(
                                    appConvertStringDouble(
                                        paymentLoaded.order.cumulativePoint),
                                    textAlign: TextAlign.right,
                                    style: const TextStyle(
                                        color: AppColor.primary,
                                        fontWeight: FontWeight.bold)),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        )
                      ],
                    );
                  },
                ),
              ),
            )),
      ),
    );
  }
}

class _CheckBoxTextDeliveryService extends StatelessWidget {
  final String title;
  // final DateTimeRange range;
  final bool select;
  final bool disable;
  const _CheckBoxTextDeliveryService(
      {@required this.select,
      @required this.title,
      // @required this.range,
      this.disable = false});
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title ?? '',
            style: select && !disable
                ? const TextStyle(color: AppColor.primary)
                : disable
                    ? TextStyle(color: Colors.grey[350])
                    : null),
        // const SizedBox(
        //   height: 5,
        // ),
        // Text(
        //   'Nhận hàng dự kiến khoảng ${appConvertDateTime(range.start, format: "dd/MM")} đến ${appConvertDateTime(range.end, format: "dd/MM")}',
        //   style: TextStyle(
        //       color: disable ? Colors.grey[350] : Colors.grey, fontSize: 12),
        // ),
      ],
    );
  }
}
