import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/app_webview.dart';
import 'package:genie/src/pages/member_ship_page/controller/member_ship_controller.dart';
import 'package:genie/src/pages/purchased_later/purchase_later_page.dart';
import 'package:genie/src/pages/reward_log/reward_log_page.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:provider/provider.dart';

import '../../components/app_scaffold.dart';
import '../../components/app_version.dart';
import '../favorites/favorite_page.dart';
import '../order_history/order_history_page.dart';
import '../voucher/voucher_page.dart';
import 'components/detail_profile_page.dart';
import 'components/info_profile_page.dart';
import 'controllers/profile_controller.dart';
import 'setup_profile_page.dart';

class UserPage extends StatefulWidget {
  const UserPage({Key key}) : super(key: key);
  static void push(
      {BuildContext context, MemberShipController memberShipController}) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => const UserPage()));
  }

  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  String pathImgLocal;
  ProfileController _profileController;

  @override
  Widget build(BuildContext context) {
    _profileController = Provider.of<ProfileController>(context, listen: false);

    return AppScaffold(
      showBackBtn: false,
      showContact: true,
      title: 'Tài Khoản',
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InfoUser(),
            Divider(
              thickness: 10,
              color: const Color(0xff9B9B9B).withOpacity(0.3),
            ),
            SizedBox(
              child: Column(
                children: [
                  ItemListView(
                    titleItem: "Lịch Sử Đơn Hàng",
                    iconUrl: "assets/icon_profile/lichsudonhang.png",
                    isTrailing: true,
                    afterClick: () {
                      OrderHistoryPage.push(context: context);
                    },
                  ),
                  ItemListView(
                    titleItem: "Ví Voucher",
                    iconUrl: "assets/icon_profile/voucher.png",
                    isTrailing: true,
                    trailingWidget: Container(
                      child: Wrap(
                        children: [
                          Container(
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              color: AppColor.primary,
                              shape: BoxShape.circle,
                            ),
                            child: ValueListenableBuilder<int>(
                                valueListenable: _profileController.allEvoucher,
                                builder: (_, countEvoucher, __) {
                                  return Text(
                                    "${countEvoucher ?? 0}",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 11),
                                  );
                                }),
                          ),
                          SizedBox(width: 5),
                          ValueListenableBuilder<int>(
                              valueListenable:
                                  _profileController.allEvoucherExpired,
                              builder: (_, countExpired, __) {
                                return countExpired == 0 || countExpired == null
                                    ? const SizedBox()
                                    : Container(
                                        padding: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                          color: Colors.purple.withOpacity(0.7),
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                        child: Text(
                                          "Sắp hết: ${countExpired ?? 0}",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 11),
                                        ),
                                      );
                              }),
                        ],
                      ),
                    ),
                    afterClick: () {
                      VoucherPage.go(context: context);
                    },
                  ),
                  ItemListView(
                    titleItem: 'Lịch sử đổi quà',
                    iconUrl: "assets/icon_profile/reward.png",
                    isTrailing: true,
                    afterClick: () {
                      RewardLogPage.go(context);
                    },
                  ),
                  ItemListView(
                    titleItem: "Thiết Lập Tài Khoản",
                    iconUrl: "assets/icon_profile/thietlaptaikhoan.png",
                    isTrailing: true,
                    afterClick: () {
                      SetupProfilePage.push(context: context);
                    },
                  ),
                  ItemListView(
                    titleItem: "Danh Sách Sản Phẩm Yêu Thích",
                    iconUrl: "assets/icon_profile/danhsachsanpham.png",
                    isTrailing: true,
                    afterClick: () {
                      FavoritePage.push(context: context);
                    },
                  ),
                  ItemListView(
                    titleItem: "Danh Sách Sản Phẩm Mua Sau",
                    iconUrl: "assets/icon_profile/danhsachsanpham.png",
                    isTrailing: true,
                    afterClick: () {
                      PurchasedLaterPage.push(context: context);
                    },
                  ),
                  ValueListenableBuilder(
                      valueListenable: _profileController.linkFaq,
                      builder: (_, faqLink, __) {
                        return ItemListView(
                          titleItem: "Hỏi và Đáp (FAQ)",
                          iconUrl: "assets/icon_profile/faq.png",
                          isTrailing: false,
                          afterClick: () {
                            AppWebView.push(
                                context: context,
                                title: "Hỏi và Đáp (FAQ)",
                                url: "${faqLink}");
                          },
                        );
                      }),
                  // ValueListenableBuilder(
                  //     valueListenable: _profileController.chatLinkNotifer,
                  //     builder: (_, chatLink, __) {
                  //       return ItemListView(
                  //         titleItem: "Chat Với Genie",
                  //         iconUrl: AppAsset.chat,
                  //         isTrailing: false,
                  //         afterClick: () {
                  //           openMessenger(chatLink);
                  //         },
                  //       );
                  //     }),
                  // ValueListenableBuilder(
                  //     valueListenable: _profileController.hotlineNotifer,
                  //     builder: (_, hotline, __) {
                  //       return ItemListView(
                  //         titleItem: "Hotline",
                  //         iconUrl: AppAsset.hotline,
                  //         isTrailing: false,
                  //         afterClick: () {
                  //           makePhoneCall('tel:$hotline');
                  //         },
                  //       );
                  //     }),
                ],
              ),
            ),
            AppVersion(),
          ],
        ),
      ),
    );
  }
}
