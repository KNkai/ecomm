import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../components/app_grey.dart';
import '../../components/app_navbar.dart';
import '../../components/app_scaffold.dart';
import '../../controllers/auth_controller.dart';
import 'components/avatar_profile.dart';
import 'components/detail_setup_profile_page.dart';

class SetupProfilePage extends StatelessWidget {
  // final _controller = SetupProfileController();

  SetupProfilePage({Key key}) : super(key: key);

  static void push({BuildContext context}) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => SetupProfilePage()));
  }

  @override
  Widget build(BuildContext context) {
    var authCtrl = Provider.of<AuthController>(context, listen: false);
    return ValueListenableBuilder(
        valueListenable: authCtrl.customerNotifer,
        builder: (_, customer, __) {
          return AppScaffold(
            title: "Thiết lập tài khoản",
            showCartBtn: false,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    height: 100,
                    child: AvatarProfile(isProfile: false, customer: customer),
                  ),
                  const AppGray(left: "Thông tin chi tiết"),
                  DetailSetupProfile(customer: customer),
                  SizedBox(
                    child: Container(
                      color: Colors.grey.withOpacity(0.3),
                    ),
                    height: 10,
                  ),
                  SizedBox(
                    width: double.infinity,
                    height: 70,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FlatButton(
                          onPressed: () async {
                            await authCtrl.logout();
                            AppNavBar.push(context);
                            // _controller.eventLogOut();
                          },
                          child: const Text(
                            "Đăng xuất",
                            style: TextStyle(
                                color: Colors.red, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
