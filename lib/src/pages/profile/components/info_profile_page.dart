import 'package:flutter/material.dart';
import 'package:genie/src/models/membership.model.dart';
import 'package:genie/src/pages/member_ship_page/controller/member_ship_controller.dart';
import 'package:genie/src/pages/reward/reward_page.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:provider/provider.dart';

import '../../../controllers/auth_controller.dart';
import '../../../utils/app_color.dart';
import 'avatar_profile.dart';
import 'timeline_profile.dart';
import 'title_timeline_profile.dart';

class InfoUser extends StatelessWidget {
  const InfoUser({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AuthController authCtrl =
        Provider.of<AuthController>(context, listen: false);
    var memberShipController =
        Provider.of<MemberShipController>(context, listen: false);
    return ValueListenableBuilder(
        valueListenable: authCtrl.customerNotifer,
        builder: (_, customer, __) {
          return Container(
            padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
            height: 250,
            child: Column(
              children: [
                Expanded(
                  flex: 2,
                  child: AvatarProfile(isProfile: true, customer: customer),
                ),
                Expanded(
                  flex: 3,
                  child: Container(
                    height: 133,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border:
                          Border.all(color: AppColor.primary.withOpacity(0.3)),
                    ),
                    width: double.infinity,
                    margin: const EdgeInsets.only(
                      left: 10,
                      right: 10,
                      top: 15,
                    ),
                    padding: const EdgeInsets.only(top: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          flex: 1,
                          child: Container(
                            padding: const EdgeInsets.only(
                              left: 20,
                              right: 10,
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Text(
                                        "Điểm thưởng",
                                        style: TextStyle(
                                          color: Color(0xff9B9B9B),
                                          fontSize: 13,
                                        ),
                                      ),
                                      Text(
                                        "${customer?.rewardPoint} điểm",
                                        style: const TextStyle(
                                            fontSize: 17,
                                            color: AppColor.primary),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    height: double.infinity,
                                    decoration: BoxDecoration(
                                      color: AppColor.primary,
                                      borderRadius: BorderRadius.circular(30),
                                    ),
                                    child: GestureDetector(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Text(
                                            "Đổi quà",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 13,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 3,
                                          ),
                                          Image.asset(
                                            AppAsset.reward,
                                            fit: BoxFit.fill,
                                            height: 22,
                                            width: 22,
                                          )
                                        ],
                                      ),
                                      onTap: () {
                                        RewardPage.push(
                                            context: context,
                                            rewardPoint: customer?.rewardPoint);
                                      },
                                    ),
                                  ),
                                ),
                                const VerticalDivider(
                                  thickness: 1,
                                ),
                                Expanded(
                                  child: Container(
                                    padding: const EdgeInsets.only(left: 20),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        const Text(
                                          "Tổng điểm",
                                          style: TextStyle(
                                            color: Color(0xff9B9B9B),
                                            fontSize: 13,
                                          ),
                                        ),
                                        Text(
                                          "${customer?.cumulativePoint} điểm",
                                          style: const TextStyle(fontSize: 17),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: ValueListenableBuilder<List<MemberShipClass>>(
                            valueListenable:
                                memberShipController.listMemberShip,
                            builder: (_, data, __) {
                              if (data == null) {
                                return const SizedBox();
                              } else {
                                data?.sort((a, b) =>
                                    a.requiredPoint.compareTo(b.requiredPoint));
                                final _indexOfRank = memberShipController
                                    .getPosition(data, customer?.rank);
                                return Container(
                                  width: double.infinity,
                                  padding: const EdgeInsets.only(top: 20),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        width: double.infinity,
                                        child: TimeLine(
                                          type: customer?.rank,
                                          listMemberShip: data,
                                          indexOfRank: _indexOfRank,
                                        ),
                                      ),
                                      TitleTimeLine(
                                        type: customer?.rank,
                                        indexOfRank: _indexOfRank,
                                        listMemberShip: data,
                                      ),
                                    ],
                                  ),
                                );
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
