import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/models/province_district_ward.model.dart';
import 'package:genie/src/pages/delivery_address/component/address_vn.dart';
import 'package:genie/src/pages/delivery_address/controller/create_delivery_address_controller.dart';
import 'package:genie/src/utils/util.dart';

class DeliveryAddressSetupProfile extends StatelessWidget {
  static void push({
    BuildContext context,
  }) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => DeliveryAddressSetupProfile()));
  }

  final _controller = CreateDeliveryAddressController();
  @override
  Widget build(BuildContext context) {
    _controller.stateSide.stream.listen((event) {
      if (event is StateSideShowMsg)
        WarningDialog.show(context, event.msg, 'OK', title: 'Thông báo');
      else
        Navigator.of(context).pop(true);
    });

    return AppScaffold(
      title: "Địa chỉ mặc định",
      showCartBtn: false,
      child: Column(
        children: [
          AddressVn(
            onData: (String detail, Province p, District d, Ward w) {
              _controller.eventUpdateFullAddress(detail, p, d, w);
            },
          ),
          SizedBox(
            height: 20,
          ),
          AppBtn(
              title: 'Tạo mới',
              onTap: () {
                _controller.eventCreatedSetupProfile(context);
              })
        ],
      ),
    );
  }
}
