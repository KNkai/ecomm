import 'dart:core';

import 'package:flutter/material.dart';
import 'package:genie/src/models/membership.model.dart';
import 'package:genie/src/pages/member_ship_page/controller/member_ship_controller.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:provider/provider.dart';

class TimeLine extends StatelessWidget {
  final String type;
  final List<MemberShipClass> listMemberShip;
  final int indexOfRank;

  const TimeLine(
      {Key key,
      @required this.type,
      @required this.listMemberShip,
      @required this.indexOfRank})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    int _indexOfRank;
    final _memberShipController =
        Provider.of<MemberShipController>(context, listen: false);
    return ValueListenableBuilder<List<MemberShipClass>>(
      valueListenable: _memberShipController.listMemberShip,
      builder: (_, memberShipClass, __) {
        if (memberShipClass != null) {
          memberShipClass
              ?.sort((a, b) => a.requiredPoint.compareTo(b.requiredPoint));
          _indexOfRank =
              _memberShipController.getPosition(memberShipClass, this.type);
          return SizedBox(
            child: CustomPaint(
                painter: DrawTimeLine(
                    type: type,
                    indexOfRank: _indexOfRank,
                    listMemberShip: memberShipClass)),
          );
        } else
          return SizedBox();
      },
    );
  }
}

class DrawTimeLine extends CustomPainter {
  final List<MemberShipClass> listMemberShip;
  final int indexOfRank;
  final String type;

  DrawTimeLine(
      {@required this.indexOfRank, @required this.listMemberShip, this.type});
  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint();
    paint.strokeCap = StrokeCap.round;
    paint.strokeWidth = 5;
    int positionCurrent = 0;

    if (indexOfRank < listMemberShip.length - 2 || indexOfRank == null)
      positionCurrent = 1;
    else if (indexOfRank == listMemberShip.length - 2)
      positionCurrent = 2;
    else if (indexOfRank == listMemberShip.length - 1) positionCurrent = 3;
    _drawBaseLine(canvas, size, paint);

    _drawDashedLine(canvas, size, paint, positionCurrent);

    _drawPointInLine(canvas, size, paint);

    _drawArrowPoint(canvas, size, paint, positionCurrent);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }

  void _drawBaseLine(Canvas canvas, Size size, Paint paint) {
    double startX = 10;
    double y = size.height / 2;

    paint.color = Colors.grey.withOpacity(0.3);
    canvas.drawLine(Offset(startX, y), Offset(size.width - 10, y), paint);
  }

  void _drawDashedLine(Canvas canvas, Size size, Paint paint, int posotion) {
    // Chage to your preferred size

    double startX = 10;
    double y = size.height / 2;
    paint.color = AppColor.primary;

    canvas.drawLine(
        Offset(startX, y), Offset((size.width / 4) * posotion, y), paint);
  }

  void _drawPointInLine(Canvas canvas, Size size, Paint paint) {
    double startX = size.width / 4;
    double y = size.height / 2;

    paint.color = AppColor.primary;
    paint.strokeWidth = 1;

    for (int i = 0; i < 4; i++) {
      canvas.drawCircle(Offset(startX, y), 5, paint);
      startX = (size.width / 4) * (1 + i);
    }
  }

  void _drawArrowPoint(Canvas canvas, Size size, Paint paint, int position) {
    double startX = ((size.width / 4) * position) - 10;
    double y = (size.height / 2) - 25;

    final icon = Icons.location_on;
    TextPainter textPainter = TextPainter(textDirection: TextDirection.ltr);
    textPainter.text = TextSpan(
      text: String.fromCharCode(icon.codePoint),
      style: TextStyle(
        color: AppColor.primary,
        fontSize: 20,
        fontFamily: icon.fontFamily,
        package:
            icon.fontPackage, // This line is mandatory for external icon packs
      ),
    );
    textPainter.layout();
    textPainter.paint(canvas, Offset(startX, y));
  }
}
