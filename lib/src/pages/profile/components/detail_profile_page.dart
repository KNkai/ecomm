import 'package:flutter/material.dart';

class ItemListView extends StatelessWidget {
  final String iconUrl;
  final String titleItem;
  final Widget trailingWidget;
  final Function afterClick;
  final bool isTrailing;

  const ItemListView(
      {Key key,
      this.iconUrl,
      this.titleItem,
      this.afterClick,
      this.isTrailing,
      this.trailingWidget})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
              width: 1, color: const Color(0xff9B9B9B).withOpacity(0.3)),
        ),
      ),
      padding: const EdgeInsets.only(
        left: 20,
        right: 20,
      ),
      child: ListTile(
        leading: Container(
          width: 35,
          height: 35,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              image: DecorationImage(
                image: AssetImage(iconUrl),
                fit: BoxFit.fill,
              )),
        ),
        title: Text(titleItem),
        trailing: isTrailing
            ? Wrap(
                children: [
                  trailingWidget == null ? SizedBox() : trailingWidget,
                  Icon(Icons.arrow_forward_ios,
                      color: const Color(0xff9B9B9B).withOpacity(0.3)),
                ],
              )
            : null,
        onTap: afterClick,
      ),
    );
  }
}
