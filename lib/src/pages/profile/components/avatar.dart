import 'dart:io';

import 'package:flutter/material.dart';
import 'package:genie/src/pages/profile/controllers/avatar_controller.dart';
import 'package:genie/src/pages/profile/setup_profile_page.dart';
import 'package:genie/src/utils/app_asset.dart';

class Avatar extends StatefulWidget {
  final String imageLink;
  final AvatarController controller;
  final bool isUserPage;
  const Avatar(
      {Key key,
      @required this.imageLink,
      this.controller,
      this.isUserPage = true})
      : super(key: key);

  @override
  _AvatarState createState() => _AvatarState();
}

class _AvatarState extends State<Avatar> {
  AvatarController controller;
  @override
  void initState() {
    controller = widget.controller;
    if (controller == null) {
      controller = new AvatarController();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: ValueListenableBuilder<AvatarState>(
        valueListenable: controller.avatarState,
        builder: (_, state, __) {
          if (state is AvatarStateError) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  controller = widget.controller;
                });
              },
              child: const CircleAvatar(
                backgroundImage: AssetImage(AppAsset.errorPlaceHolder),
                maxRadius: 45,
                backgroundColor: Colors.grey,
              ),
            );
          } else if (state is AvatarStateLocal) {
            File _f = File(state.path);
            return CircleAvatar(
              backgroundImage: FileImage(_f),
              maxRadius: 45,
              backgroundColor: Colors.grey,
            );
          } else {
            return widget.imageLink != null
                ? GestureDetector(
                    onTap: () {
                      Size sizeAvatar;
                      Image image = Image.network(widget?.imageLink);
                      image.image.resolve(ImageConfiguration()).addListener(
                        ImageStreamListener(
                          (ImageInfo image, bool synchronousCall) {
                            var myImage = image?.image;
                            sizeAvatar = Size(myImage?.width?.toDouble(),
                                myImage?.height?.toDouble());
                          },
                        ),
                      );

                      widget.isUserPage
                          ? SetupProfilePage.push(context: context)
                          : Navigator.push(
                              context,
                              PageRouteBuilder(
                                opaque: false,
                                barrierDismissible: true,
                                pageBuilder: (context, _, __) {
                                  return AlertDialog(
                                    backgroundColor: Colors.transparent,
                                    elevation: 0,
                                    content: new Hero(
                                      tag: "avatar+${widget?.imageLink}",
                                      child: Container(
                                        width: sizeAvatar?.width,
                                        height: 400,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          image: DecorationImage(
                                            image: NetworkImage(
                                              widget?.imageLink,
                                            ),
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            );
                    },
                    child: Hero(
                      tag: "avatar+${widget?.imageLink}",
                      child: CircleAvatar(
                        backgroundImage: new NetworkImage(widget?.imageLink),
                        maxRadius: 45,
                        backgroundColor: Colors.grey,
                      ),
                    ),
                  )
                : const CircleAvatar(
                    backgroundImage: AssetImage(AppAsset.errorPlaceHolder),
                    maxRadius: 45,
                    backgroundColor: Colors.grey);
          }
        },
      ),
    );
  }
}
