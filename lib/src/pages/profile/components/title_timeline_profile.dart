import 'package:flutter/material.dart';
import 'package:genie/src/models/membership.model.dart';
import 'package:genie/src/pages/member_ship_page/controller/member_ship_controller.dart';
import 'package:genie/src/utils/util.dart';
import 'package:provider/provider.dart';

class TitleTimeLine extends StatelessWidget {
  final String type;
  final List<MemberShipClass> listMemberShip;
  final int indexOfRank;

  const TitleTimeLine(
      {Key key, @required this.type, this.listMemberShip, this.indexOfRank})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    int _indexOfRank = indexOfRank;
    bool checkOrientation =
        MediaQuery.of(context).orientation == Orientation.portrait;
    var memberShipController =
        Provider.of<MemberShipController>(context, listen: false);
    return Container(
      width: double.infinity,
      padding: checkOrientation
          ? const EdgeInsets.symmetric(
              horizontal: 18,
              vertical: 10,
            )
          : const EdgeInsets.symmetric(
              horizontal: 50,
              vertical: 10,
            ),
      child: ValueListenableBuilder<List<MemberShipClass>>(
        valueListenable: memberShipController.listMemberShip,
        builder: (_, memberShipClass, __) {
          if (memberShipClass != null) {
            memberShipClass
                ?.sort((a, b) => a.requiredPoint.compareTo(b.requiredPoint));
            _indexOfRank =
                memberShipController.getPosition(memberShipClass, this.type);
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: List.generate(
                3,
                (index) {
                  if (_indexOfRank >= memberShipClass.length - 3) {
                    _indexOfRank = index + memberShipClass.length - 3;
                    // _indexOfRank = i;
                  } else
                    _indexOfRank = _indexOfRank + index;
                  return itemTitle(
                      title: memberShipClass[_indexOfRank]?.name,
                      colorText: Color(
                        convertColor(memberShipClass[_indexOfRank]?.color),
                      ),
                      point: "${memberShipClass[_indexOfRank]?.requiredPoint}");
                },
              ),
            );
          } else
            return const SizedBox();
        },
      ),
    );
  }

  Expanded itemTitle({String title, Color colorText, String point}) {
    return Expanded(
      flex: 1,
      child: SizedBox(
        width: double.infinity,
        child: Center(
          child: Text(
            "${title ?? null}\n${point == null ? "0" : point}",
            style: TextStyle(fontSize: 10, color: colorText ?? null),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }

  //
  // int getIndex(int position, int length) {
  //   int _index = 0;
  //   if (length - (position + 1) == 1)
  //     _index = 1;
  //   else if (length - (position + 1) == 0)
  //     _index = 2;
  //   else
  //     _index = 0;
  //   return _index;
  // }
}
