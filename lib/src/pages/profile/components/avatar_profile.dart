import 'package:flutter/material.dart';
import 'package:genie/src/pages/member_ship_page/member_ship_page.dart';
import 'package:provider/provider.dart';

import '../../../controllers/auth_controller.dart';
import '../../../models/customer.model.dart';
import '../controllers/avatar_controller.dart';
import 'avatar.dart';

class AvatarProfile extends StatelessWidget {
  final bool isProfile;
  final Customer customer;

  const AvatarProfile(
      {Key key, @required this.isProfile, @required this.customer})
      : super(key: key);

  // File cameraFile;
  @override
  Widget build(BuildContext context) {
    var authCtrl = Provider.of<AuthController>(context, listen: false);
    var avatarCtrl = new AvatarController(onUpdateImage: (newAvatar) {
      authCtrl.updateCustomerProfile({"avatar": newAvatar});
    });
    final String avatarLink = customer?.avatar;
    avatarLink == null
        ? () {}
        : avatarCtrl.initAvatar(
            customer,
            false,
          );
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Row(
        children: [
          Expanded(
            child: Avatar(
              isUserPage: isProfile,
              imageLink: customer?.avatar,
              controller: avatarCtrl,
            ),
            flex: 1,
          ),
          Expanded(
            child: Container(
              padding: const EdgeInsets.only(left: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Text(
                      customer?.name == null ? "" : customer?.name,
                      style: const TextStyle(
                          fontSize: 22, fontWeight: FontWeight.w400),
                    ),
                    flex: 1,
                  ),
                  Expanded(
                    child: isProfile
                        ? _avatarProfile(customer?.rank, context)
                        : _avatarSetUpProfile(context, avatarCtrl),
                    flex: 1,
                  ),
                ],
              ),
              width: double.infinity,
            ),
            flex: 3,
          ),
        ],
      ),
    );
  }

  _avatarProfile(String typeCus, context) {
    String type;
    // Color bgType;
    switch (typeCus) {
      case "LOYAL":
        type = "Thân Thiết";
        break;
      case "SILVER":
        type = "Bạc";
        break;
      case "GOLD":
        type = "Vàng";
        break;

      case "DIAMOND":
        type = "Kim Cương";
        break;

      case "PAINITE":
        type = "Painite";
        break;

      case "RED_DIAMOND":
        type = "Kim Cương Đỏ";
        break;
      case "":
        type = "Thành Viên";
        break;
    }
    return GestureDetector(
      child: Container(
        width: 200,
        height: 32,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(typeCus == null ? "Thành Viên" : type),
            // Text(typeCus == null ? "Thành Viên" : type),
          ],
        ),
        decoration: BoxDecoration(
          color: Colors.yellow,
          borderRadius: BorderRadius.circular(30),
        ),
      ),
      onTap: () {
        MemberShipPage.push(context: context, customer: customer);
      },
    );
  }

  _avatarSetUpProfile(BuildContext context, AvatarController controller) {
    return SizedBox(
      child: FlatButton(
        child: const SizedBox(
          width: double.infinity,
          child: Text(
            "Đổi Avatar",
            style: TextStyle(fontSize: 17, color: Color(0xff64A8A7)),
            textAlign: TextAlign.start,
          ),
        ),
        onPressed: () {
          controller.imagePicker(context: context);
        },
      ),
    );
  }
}
