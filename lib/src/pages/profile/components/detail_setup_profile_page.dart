import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/controllers/auth_controller.dart';
import 'package:genie/src/models/customer.model.dart';
import 'package:genie/src/pages/payment/payment_controller.dart';
import 'package:genie/src/pages/profile/controllers/profile_controller.dart';
import 'package:genie/src/pages/profile/controllers/setup_profile_controller.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';
import 'package:provider/provider.dart';

import 'delivery_address_setup_profile.dart';

class DetailSetupProfile extends StatelessWidget {
  final Customer customer;

  DetailSetupProfile({Key key, @required this.customer}) : super(key: key);
  final controller = SetupProfileController();
  @override
  Widget build(BuildContext context) {
    controller.eventInit();
    var authCtrl = Provider.of<AuthController>(context, listen: false);
    return SizedBox(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ItemDetailUser(
              title: "Họ Và Tên",
              data: customer?.name,
              onChanged: (value) => {
                    authCtrl.updateCustomerProfile({"name": value})
                  }),
          Row(
            children: [
              Expanded(
                flex: 5,
                child: ShowDatePicker(
                  dob: customer?.birthday,
                  disabled: customer?.birthday != null,
                  onChanged: (value) {
                    if (customer?.birthday == null) {
                      authCtrl.updateCustomerProfile({"birthday": value});
                    }
                  },
                ),
              ),
              Expanded(
                flex: 3,
                child: ChooseGender(
                  onChange: (value) {
                    authCtrl.updateCustomerProfile({"gender": value});
                  },
                  genderType: customer?.gender,
                ),
              ),
            ],
          ),
          ItemDetailUser(
            title: "Số Điện Thoại",
            keyboardType: TextInputType.phone,
            data: customer?.phone,
            disabled: true,
          ),
          ItemDetailUser(
            title: "Email Của Bạn",
            keyboardType: TextInputType.emailAddress,
            data: customer?.email,
            disabled: customer?.email != null,
            onChanged: (value) => {
              authCtrl.updateCustomerProfile({"email": value})
            },
          ),
          (customer?.address == null ||
                  customer?.provinceId == null ||
                  customer?.wardId == null ||
                  customer?.districtId == null)
              ? GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    DeliveryAddressSetupProfile.push(context: context);
                  },
                  child: const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 26, vertical: 18),
                    child: Text(
                      '+ Tạo địa chỉ mặc định',
                      style: TextStyle(color: AppColor.primary),
                    ),
                  ),
                )
              : DeliveryDefault(
                  customer: customer,
                  // defaultDelivery: defaultDelivery,
                  onChange: () {
                    DeliveryAddressSetupProfile.push(context: context);
                  },
                ),
        ],
      ),
    );
  }
}

class ItemDetailUser extends StatelessWidget {
  final String title;
  final TextInputType keyboardType;
  final String data;
  final Function(String) onChanged;
  final bool disabled;

  const ItemDetailUser({
    Key key,
    this.title,
    this.keyboardType,
    this.data,
    this.onChanged,
    this.disabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController _data = TextEditingController();
    _data.text = data;

    return Container(
      height: 70,
      padding: const EdgeInsets.only(
        top: 15,
        left: 20,
        right: 20,
        bottom: 10,
      ),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1, color: Colors.grey.withOpacity(0.3)),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Text(
              title ?? '',
              style: TextStyle(
                color: Colors.black.withOpacity(0.5),
                fontSize: 13,
              ),
            ),
          ),
          Expanded(
            child: TextField(
                enabled: !disabled,
                controller: _data,
                textAlign: TextAlign.left,
                keyboardType: keyboardType,
                decoration: const InputDecoration(
                  border: InputBorder.none,
                ),
                style: const TextStyle(fontSize: 17),
                onSubmitted: (data) {
                  if (onChanged != null) onChanged(data);
                }),
          )
        ],
      ),
    );
  }
}

class ShowDatePicker extends StatefulWidget {
  final ProfileController profileController;
  final DateTime dob;
  final Function(String) onChanged;
  final bool disabled;

  const ShowDatePicker(
      {Key key,
      this.dob,
      this.profileController,
      this.onChanged,
      this.disabled = false})
      : super(key: key);
  @override
  _ShowDatePickerState createState() => _ShowDatePickerState();
}

class _ShowDatePickerState extends State<ShowDatePicker> {
  DateTime _selectedDate;
  @override
  void initState() {
    _selectedDate = widget.dob;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 70,
      padding: const EdgeInsets.only(
        top: 15,
        left: 20,
        right: 20,
        bottom: 10,
      ),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1, color: Colors.grey.withOpacity(0.3)),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: Text(
              "Ngày Sinh",
              style: TextStyle(
                color: Colors.black.withOpacity(0.5),
                fontSize: 13,
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                if (widget.disabled) return;
                setState(() {
                  _showdate(context);
                });
              },
              child: SizedBox(
                width: double.infinity,
                height: 100,
                child: Text(_selectedDate == null
                    ? ""
                    : appConvertDateTime(_selectedDate)),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _showdate(BuildContext context) async {
    final DateTime selectedDate = await showDatePicker(
      context: context,
      initialDate: _selectedDate == null ? DateTime.now() : _selectedDate,
      firstDate: DateTime(1850),
      lastDate: DateTime(2100),
      // currentDate: _selectedDate == null ? DateTime.now() : _selectedDate,
    );
    if (selectedDate != null && selectedDate != _selectedDate)
      setState(() {
        _selectedDate = selectedDate;
        if (widget.onChanged != null) {
          widget.onChanged(selectedDate.toString());
        }
      });
  }
}

class ChooseGender extends StatefulWidget {
  final String genderType;
  final Function(String) onChange;

  const ChooseGender({Key key, this.genderType, this.onChange})
      : super(key: key);

  @override
  _ChooseGenderState createState() => _ChooseGenderState();
}

class _ChooseGenderState extends State<ChooseGender> {
  @override
  Widget build(BuildContext context) {
    Item selectedUser = (widget?.genderType == null
        ? users[0]
        : users.where((element) => element.type == widget?.genderType).single);
    return Container(
      width: double.infinity,
      height: 70,
      padding: const EdgeInsets.only(
        top: 15,
        left: 20,
        right: 20,
        bottom: 10,
      ),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1, color: Colors.grey.withOpacity(0.3)),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: Text(
              "Giới Tính",
              style: TextStyle(
                color: Colors.black.withOpacity(0.5),
                fontSize: 13,
              ),
            ),
          ),
          Expanded(
            child: DropdownButtonHideUnderline(
              child: DropdownButton<Item>(
                hint: Text("Select item"),
                value: selectedUser,
                onChanged: (Item value) {
                  setState(() {
                    selectedUser = value;
                    widget.onChange(selectedUser?.type);
                  });
                },
                isExpanded: true,
                items: users.map((Item user) {
                  return DropdownMenuItem<Item>(
                    value: user,
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          user?.name,
                          style: TextStyle(color: Colors.black),
                        ),
                      ],
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Item {
  const Item({this.name, this.type});
  final String name;
  final String type;
}

List<Item> users = <Item>[
  const Item(
    name: 'Nam',
    type: "male",
  ),
  const Item(
    name: 'Nữ',
    type: "female",
  ),
  const Item(
    name: 'Khác',
    type: "other",
  ),
];

class DeliveryDefault extends StatelessWidget {
  final StateDefaultAddressLoaded defaultDelivery;
  final Function onChange;
  final Customer customer;

  const DeliveryDefault(
      {Key key, this.defaultDelivery, this.onChange, this.customer})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      // height: 200,
      padding: const EdgeInsets.only(
        top: 15,
        left: 20,
        right: 20,
        bottom: 10,
      ),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1, color: Colors.grey.withOpacity(0.3)),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                flex: 5,
                child: Text(
                  "Địa Chỉ Mặc Định",
                  style: TextStyle(
                    color: Colors.black.withOpacity(0.5),
                    fontSize: 13,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    onChange();
                  },
                  child: Text(
                    "Thay Đổi",
                    style: TextStyle(
                      color: AppColor.primary,
                      fontSize: 13,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  customer?.name ?? '',
                  style: const TextStyle(
                    color: AppColor.primary,
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(customer?.phone ?? ''),
                const SizedBox(
                  height: 5,
                ),
                Text(appFullAddress(
                  address: customer?.address,
                  district: customer?.district,
                  province: customer?.province,
                  ward: customer?.ward,
                )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
