import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:genie/src/models/delivery_address.model.dart';
import 'package:genie/src/pages/payment/payment_controller.dart';
import 'package:genie/src/repositories/delivery_address.repo.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/utils/app_key.dart';
import 'package:genie/src/utils/util.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SetupProfileController {
  final stateSideSetupProfile = StreamController();
  final stateDefaultAddress = StreamController<StateDefaultAddress>();
  DeliveryAddress _choosenAddress;
  final _repoDeliveryAddress = DeliveryAddressRepository();

  void eventLogOut() {
    stateSideSetupProfile.add(StateSideLoading(loading: true));
    getUniqueDeviceId().then((deviceId) {
      AppClient.instance.execute('''
    mutation {
    unRegisDevice(deviceId: "$deviceId")
}
    ''').then((result) {
        // if (result.data['unRegisDevice'] != null &&
        //     result.data['unRegisDevice']) {
        //   // Delete xToken to app.
        //   AppClient.instance.unInstallToken();

        //   // Delete xToken to secure storage.
        //   FlutterSecureStorage().delete(key: AppKey.xToken);

        //   stateSideSetupProfile.add(StateSideLoading(loading: false));
        //   stateSideSetupProfile.add(StateSideLogOutSuccess());
        // } else {
        //   stateSideSetupProfile.add(StateSideLoading(loading: false));
        //   stateSideSetupProfile
        //       .add(StateSideShowMsg(msg: 'Có lỗi xảy ra vui lòng thử lại'));
        // }

        AppClient.instance.unInstallToken();

        // Delete xToken to secure storage.
        const FlutterSecureStorage().delete(key: AppKey.xToken);

        // Delete customerId from local
        SharedPreferences.getInstance().then((instance) {
          instance.remove(
            AppKey.customerId,
          );
        });

        stateSideSetupProfile.add(StateSideLoading(loading: false));
        stateSideSetupProfile.add(StateSideLogOutSuccess());
      }).catchError((_) {
        stateSideSetupProfile.add(StateSideLoading(loading: false));
        stateSideSetupProfile
            .add(StateSideShowMsg(msg: 'Có lỗi xảy ra vui lòng thử lại'));
      });
    });
  }

  void eventInit() async {
    final responseDefaultAddress =
        await _repoDeliveryAddress.getDefaultDeliveryAddress();
    _getDefaultAddress(responseDefaultAddress);
  }

  void eventLoadAddress() async {
    final responseDefaultAddress =
        await _repoDeliveryAddress.getDefaultDeliveryAddress();
    _getDefaultAddress(responseDefaultAddress);
  }

  void _getDefaultAddress(responseDefaultAddress) async {
    if (responseDefaultAddress == null) {
      _choosenAddress = null;
    } else {
      if (responseDefaultAddress is DeliveryAddress) {
        _choosenAddress = responseDefaultAddress;
      } else {
        // EmptyDeliveryAddress
        _choosenAddress = null;
      }

      if (responseDefaultAddress is DeliveryAddress) {
        stateDefaultAddress.add(
          StateDefaultAddressLoaded(
            fullAddress: appFullAddress(
              address: responseDefaultAddress.address,
              ward: responseDefaultAddress.ward,
              district: responseDefaultAddress.district,
              province: responseDefaultAddress.province,
            ),
            phone: responseDefaultAddress.phone,
            name: responseDefaultAddress.name,
          ),
        );
      } else if (responseDefaultAddress is EmptyDeliveryAddress) {
        stateDefaultAddress.add(StateDefaultAddressEmpty());
      }
    }
  }
}

abstract class StateSideSetupProfile {}

class StateSideShowMsg extends StateSideSetupProfile {
  final String msg;
  StateSideShowMsg({@required this.msg});
}

class StateSideLogOutSuccess extends StateSideSetupProfile {}

class StateSideLoading extends StateSideSetupProfile {
  final bool loading;
  StateSideLoading({@required this.loading});
}
