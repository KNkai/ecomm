import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:genie/src/models/customer.model.dart';
import 'package:genie/src/utils/util.dart';
import 'package:image_picker/image_picker.dart';
import 'package:imgur/imgur.dart' as ig;
import 'package:path/path.dart' as path;
import 'package:permission_handler/permission_handler.dart';

abstract class AvatarState {}

class AvatarStateLocal extends AvatarState {
  final String path;

  AvatarStateLocal(this.path);
}

class AvatarStateNetwork extends AvatarState {
  final String url;

  AvatarStateNetwork(this.url);
}

class AvatarStateError extends AvatarState {}

class AvatarController extends ChangeNotifier {
  final avatarState = ValueNotifier<AvatarState>(AvatarStateError());
  String pathTestAvatar;
  File avatarFileCache = null;
  bool checkAvatarFile = false;
  final Function(String) onUpdateImage;

  AvatarController({this.onUpdateImage});

  loadAvatar({String type, String data}) {
    switch (type) {
      case "network":
        avatarState.value = AvatarStateNetwork(data);
        break;
      case "local":
        avatarState.value = AvatarStateLocal(data);
        break;
      case "error":
        avatarState.value = AvatarStateError();
    }
  }

  initAvatar(Customer customer, bool isUpdate, {String pathImgLocal}) {
    if (customer.avatar == null) {
      loadAvatar(type: "error", data: "");
    } else {
      if (isUpdate) {
        loadAvatar(type: "local", data: pathImgLocal);
      } else {
        loadAvatar(type: "network", data: customer.avatar);
      }
    }
  }

  Future<String> selectFromCamera() async {
    // ignore: deprecated_member_use
    avatarFileCache = await ImagePicker.pickImage(
      source: ImageSource.camera,
      imageQuality: 100,
      maxWidth: 480,
      maxHeight: 640,
    );

    pathTestAvatar = avatarFileCache.path;
    String dir = path.dirname(avatarFileCache.path);
    String newPath = path.join(dir, 'avatar.jpg');
    avatarFileCache.renameSync(newPath);
    avatarFileCache = File(newPath);
    return avatarFileCache.path;
  }

  Future<String> selectFromGallery() async {
    // ignore: deprecated_member_use
    avatarFileCache = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 100,
      maxWidth: 480,
      maxHeight: 640,
    );
    pathTestAvatar = avatarFileCache.path;
    String dir = path.dirname(avatarFileCache.path);
    String newPath = path.join(dir, 'avatar.jpg');
    avatarFileCache.renameSync(newPath);
    avatarFileCache = File(newPath);
    return avatarFileCache.path;
  }

  Future eventUpdateImage(String path) async {
    // loadAvatar("local", path);
    final imgurService =
        ig.Imgur(ig.Authentication.fromClientId('317d13ba79b0825')).image;
    if (path != null) {
      await FlutterExifRotation.rotateImage(path: path).then((file) {
        return imgurService.uploadImage(imageFile: file).then((value) {
          if (value.link != null && onUpdateImage != null) {
            onUpdateImage(value.link);
          }
        });
      });
    }
    avatarState.value = AvatarStateLocal(path);
  }

  void imagePicker(
      {BuildContext context,
      Function(String path) onCameraPick,
      Function(String path) onGalleryPick,
      String title}) {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      elevation: 0,
      context: context,
      builder: (_) {
        return Material(
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 4),
            width: double.infinity,
            height: 200,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title ?? 'Chọn ảnh từ điện thoại',
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, color: Color(0xff696969)),
                ),
                const SizedBox(
                  height: 12,
                ),
                const Divider(height: 1, color: Colors.grey),
                const SizedBox(
                  height: 16,
                ),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    onCustomPersionRequest(
                        permission: Permission.photos,
                        onGranted: () {
                          Navigator.of(context).pop();
                          selectFromCamera()
                              .then((path) => eventUpdateImage(path));
                        });
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: const [
                      Icon(
                        Icons.add_a_photo,
                        color: Color(0xff696969),
                      ),
                      SizedBox(
                        width: 13,
                      ),
                      Text(
                        'Camera',
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    onCustomPersionRequest(
                        permission: Permission.photos,
                        onGranted: () {
                          Navigator.of(context).pop();
                          selectFromGallery()
                              .then((path) => eventUpdateImage(path));
                        });
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: const [
                      Icon(
                        Icons.apps,
                        color: Color(0xff696969),
                      ),
                      SizedBox(
                        width: 13,
                      ),
                      Text(
                        'Thư viện',
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ],
            ),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(8),
                topLeft: Radius.circular(8),
              ),
            ),
            padding: const EdgeInsets.all(15),
          ),
        );
      },
    );
  }
}
