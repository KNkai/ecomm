import 'dart:async';

import 'package:flutter/material.dart';
import 'package:genie/src/models/customer.model.dart';
import 'package:genie/src/repositories/evoucher.repo.dart';
import 'package:genie/src/repositories/profile.repo.dart';

abstract class StateDetail {}

class StateDetailSuccessLoaded extends StateDetail {
  final Customer customer;

  StateDetailSuccessLoaded({@required this.customer});
}

class StateDetailFaileLoaded extends StateDetail {}

class StateDetailUpload extends StateDetail {
  final bool isUpdate;

  StateDetailUpload({this.isUpdate});
}

class ProfileController extends ChangeNotifier {
  // CustomerRepository _customerRepository = CustomerRepository();
  ProfileRepository _profileRepository = ProfileRepository();
  final BuildContext context;
  final _voucherRepo = EvoucherRepository();
  final stateLoadingDetail = ValueNotifier<bool>(true);
  final stateDetail = StreamController<StateDetail>();
  final stateSetupProfileLoading = ValueNotifier<bool>(false);
  final stateSideSetupProfile = StreamController();
  ValueNotifier<String> chatLinkNotifer = new ValueNotifier(null);
  ValueNotifier<String> hotlineNotifer = new ValueNotifier(null);
  ValueNotifier<String> linkFaq = new ValueNotifier(null);
  ValueNotifier<int> allEvoucher = new ValueNotifier(null);
  ValueNotifier<int> allEvoucherExpired = new ValueNotifier(null);

  ProfileController(this.context) {
    chatSetting();
    hotlineSetting();
    getLinkFaq();
    loadVoucher();
  }

  Future<dynamic> chatSetting() {
    return _profileRepository.getOneSettingByKey('APP_CHAT_LINK').then((value) {
      chatLinkNotifer.value = value['value'];
      chatLinkNotifer.notifyListeners();
      return value['value'];
    });
  }

  Future<dynamic> getLinkFaq() {
    return _profileRepository.getOneSettingByKey('FAQ_LINK').then((value) {
      linkFaq.value = value['value'];
      linkFaq.notifyListeners();
      return value['value'];
    });
  }

  loadVoucher() {
    int _countEvoucher = 0;
    int _countEvoucherExpired = 0;
    _voucherRepo.getAllEvoucherByGroup().then((value) {
      if (value != null) {
        value.forEach((e) {
          _countEvoucher += e.listEvoucher.length;
          e.listEvoucher.forEach((c) {
            if (c.almostExpired) _countEvoucherExpired++;
          });
        });
        allEvoucher.value = _countEvoucher;
        allEvoucher.notifyListeners();
        allEvoucherExpired.value = _countEvoucherExpired;
        allEvoucherExpired.notifyListeners();
      }
    });
  }

  Future<dynamic> hotlineSetting() {
    return _profileRepository.getOneSettingByKey('APP_HOTLINE').then((value) {
      hotlineNotifer.value = value['value'];
      hotlineNotifer.notifyListeners();
      return value['value'];
    });
  }

  Future<dynamic> redDimondSetting() {
    return _profileRepository
        .getOneSettingByKey('MEMBER_RANK_RED_DIAMOND')
        .then((value) {
      if (value != null) {
        return value['value'];
      } else {
        return 'null';
      }
    });
  }

  Future<dynamic> painiteSetting() {
    return _profileRepository
        .getOneSettingByKey('MEMBER_RANK_PAINITE')
        .then((value) {
      if (value != null) {
        return value['value'];
      } else {
        return 'null';
      }
    });
  }

  Future<dynamic> diamondSetting() {
    return _profileRepository
        .getOneSettingByKey('MEMBER_RANK_DIAMOND')
        .then((value) {
      if (value != null) {
        return value['value'];
      } else {
        return 'null';
      }
    });
  }

  Future<dynamic> goldSetting() {
    return _profileRepository
        .getOneSettingByKey('MEMBER_RANK_GOLD')
        .then((value) {
      if (value != null) {
        return value['value'];
      } else {
        return 'null';
      }
    });
  }

  Future<dynamic> loyalSetting() {
    return _profileRepository
        .getOneSettingByKey('MEMBER_RANK_LOYAL')
        .then((value) {
      return value['value'];
    });
  }

  Future<dynamic> vipSetting() {
    return _profileRepository
        .getOneSettingByKey('MEMBER_RANK_VIP')
        .then((value) {
      if (value != null) {
        return value['value'];
      } else {
        return 'null';
      }
    });
  }

  Future uploadAvatar(String imgUrl) {
    return _profileRepository.upLoadAvatar(imgUrl).catchError((onError) {
      print(onError);
    });
  }
}
