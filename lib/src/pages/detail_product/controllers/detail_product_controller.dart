import 'dart:async';
import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/repositories/product.repo.dart';
import 'package:genie/src/repositories/profile.repo.dart';

class DetailProductController {
  final stateDetail = StreamController<StateDetail>();
  final stateAttributes =
      StreamController<HashMap<String, HashMap<String, ModelValueType>>>();
  final stateAttributeSideEffect =
      StreamController<StateRollbackAttributeEffect>();
  final stateDetailSideEffect = StreamController<StateDetailSideEffect>();
  final stateQuantity = ValueNotifier<int>(1);
  final stateLoadingDetail = ValueNotifier<bool>(true);
  final _detailRepo = ProductRepository();
  final _mapAttributes = HashMap<String, HashMap<String, ModelValueType>>();
  final mapSelectAttribute = LinkedHashMap<String, String>();
  final _mapKeysUnions = HashMap<String, List<String>>();
  final _profileRepository = ProfileRepository();
  ValueNotifier<String> color = new ValueNotifier(null);

  bool _attributeCondition = false;
  Product _product;
  String _note;
  String get note => _note;
  bool get isSafeToOpenCart => _product != null;
  voucherColor() async {
    final value =
        await _profileRepository.getOneSettingByKey('PROMOTION_COLOR');
    color = value['value'];
  }

  void eventGoCart({bool openCart = false}) {
    if (_attributeCondition && isSafeToOpenCart) {
      stateDetailSideEffect.add(StateSideAddCart(
          openCart: openCart,
          price: _product.getSalePrice,
          quantity: stateQuantity.value,
          productId: _product.id,
          name: _product?.name,
          note: _note,
          image: _product.listImage.isEmpty ? null : _product.listImage[0]));
    }
  }

  void eventChangeNote(String note) {
    _note = note;
  }

  void eventChangeProductQuantity({bool add = true}) {
    if (add) {
      stateQuantity.value++;
    } else if (stateQuantity.value > 1) {
      stateQuantity.value--;
    }
  }

  void eventLoad({@required String productId}) {
    stateLoadingDetail.value = true;
    _detailRepo.getOneProductDetail(productId: productId).then((value) {
      try {
        if (value == null)
          _updateDetailState(StateDetailFaileLoaded());
        else
          _updateDetailState(StateDetailSuccessLoaded(product: value));
      } finally {
        stateLoadingDetail.value = false;
      }
    });
  }

  LinkedHashMap<String, String> eventLoadAttributes(
      List<ProductAttribute> attributes, String mainProductId) {
    attributes.forEach((d) {
      mapSelectAttribute[d.attribute.name] = null;
      final _mapValueAttribute = HashMap<String, ModelValueType>();
      d.values.forEach((e) {
        _mapValueAttribute[e.value] =
            ModelValueType(productIds: e.productIds, value: e.value);
        for (String productId in e.productIds) {
          if (mainProductId == productId) {
            mapSelectAttribute[d.attribute.name] = e.value;
            break;
          }
        }
      });

      _mapAttributes[d.attribute.name] = _mapValueAttribute;
    });

    _attributeCondition = mapSelectAttribute.length == 0 ? true : false; // init

    _convertHidden(init: true);

    stateAttributes.add(_mapAttributes);

    return mapSelectAttribute;
  }

  void eventSelectAttributeVallue(
      String attributeName, String attributeValue, bool select) {
    if (select)
      mapSelectAttribute[attributeName] = attributeValue;
    else
      mapSelectAttribute[attributeName] = null;

    _convertHidden();

    stateAttributes.add(_mapAttributes);

    // _print();
  }

  void _updateDetailState(StateDetail state) {
    if (state is StateDetailSuccessLoaded) {
      _product = state.product;
    } else {
      _product = null;
    }
    stateDetail.add(state);
  }

  void _convertHidden({bool init = false}) {
    if (_selectAttribute() == 0) {
      _resetMapAttribute();
      // _print();
    } else {
      List<String> fullUnion = _findFullUnion();
      _mapAttributes.forEach((attributeKey, attributeValue) {
        if (mapSelectAttribute[attributeKey] != null) {
          _convertHiddenSelectAttribute(attributeKey);
        } else {
          _convertHiddenSingleAttribute(attributeKey, fullUnion);
        }
      });

      _attributeCondition = fullUnion.length == 1;

      if (fullUnion.length == 1 && !init) {
        // implies select all attribute

        _detailRepo.getOneProductDetail(productId: fullUnion[0]).then((value) {
          stateLoadingDetail.value = true;
          try {
            if (value == null)
              stateAttributeSideEffect.add(StateRollbackAttributeEffect());
            else
              _updateDetailState(StateDetailSuccessLoaded(product: value));
          } finally {
            stateLoadingDetail.value = false;
          }
        });
      }
    }
  }

  void _convertHiddenSelectAttribute(
    String choosenAttributeKey,
  ) {
    if (_selectAttribute() == 1) return;

    String concat = '';
    final List<List<String>> tempList = [];

    mapSelectAttribute.forEach((selectKey, selectValue) {
      if (selectKey != choosenAttributeKey && selectValue != null) {
        concat += selectValue;
        tempList.add(_mapAttributes[selectKey][selectValue].productIds);
      }
    });
    List<String> union;
    union = _mapKeysUnions[concat];
    if (union == null) {
      union = _findUnion(tempList);
      _mapKeysUnions[concat] = union;
    }

    _convertHiddenSingleAttribute(choosenAttributeKey, union);
  }

  void _convertHiddenSingleAttribute(
      String choosenAttributeKey, List<String> union) {
    for (MapEntry<String, ModelValueType> attributeValue
        in _mapAttributes[choosenAttributeKey].entries) {
      var e = _hasUnion(attributeValue.value.productIds, union);

      attributeValue.value.hidden = !e;
    }
  }

  int _selectAttribute() {
    int count = 0;
    mapSelectAttribute.forEach((key, value) {
      if (value != null) count++;
    });
    return count;
  }

  List<String> _findFullUnion() {
    final List<List<String>> lists = [];
    String concat = '';
    List<String> result;
    mapSelectAttribute.forEach((key, value) {
      if (value != null) {
        concat += value;
        lists.add(_mapAttributes[key][value].productIds);
      }
    });
    final map = _mapKeysUnions[concat];
    if (map != null) {
      result = map;
    } else {
      result = _findUnion(lists);
      _mapKeysUnions[concat] = result;
    }
    return result;
  }

  List<String> _findUnion(List<List<String>> listConstraints) {
    List<String> union = listConstraints[0];
    for (int c = 1; c < listConstraints.length; c++) {
      union = _pairUnion(union, listConstraints[c]);
    }

    return union;
  }

  List<String> _pairUnion(List<String> list1, List<String> list2) {
    final List<String> result = [];
    list1.forEach((element1) {
      list2.forEach((element2) {
        if (element1 == element2) result.add(element1);
      });
    });
    return result;
  }

  bool _hasUnion(List<String> list1, List<String> list2) {
    bool has = false;
    for (String id1 in list1) {
      for (String id2 in list2) {
        if (id1 == id2) {
          has = true;
          break;
        }
      }
      if (has) break;
    }
    return has;
  }

  void _resetMapAttribute() {
    _mapAttributes.forEach((key, valule) {
      valule.forEach((key2, _) {
        _mapAttributes[key][key2].hidden = false;
      });
    });
  }

  // void _print() {
  //   _mapAttributes.forEach((key, value) {
  //     print('Attribute Key: $key\n');
  //     value.forEach((attributeValueKey, modelValueType) {
  //       print(
  //           'Attribute Name: $attributeValueKey hidden: ${modelValueType.hidden}');
  //     });
  //     print('===');
  //   });
  // }

  void dispose() {
    stateDetail.close();
    stateAttributes.close();
    stateLoadingDetail.dispose();
    stateAttributeSideEffect.close();
    stateQuantity.dispose();
  }
}

abstract class StateDetail {}

class StateDetailSuccessLoaded extends StateDetail {
  final Product product;

  StateDetailSuccessLoaded({@required this.product});
}

class StateDetailFaileLoaded extends StateDetail {}

abstract class StateDetailSideEffect {}

class StateSideWarning extends StateDetailSideEffect {
  final String msg;
  StateSideWarning({@required this.msg});
}

class StateSideAddCart extends StateDetailSideEffect {
  final String productId;
  final String note;
  final int quantity;
  final double price;
  final String image;
  final String name;
  final bool openCart;
  StateSideAddCart(
      {@required this.productId,
      @required this.price,
      @required this.note,
      @required this.openCart,
      @required this.quantity,
      @required this.name,
      @required this.image});
}

class StateRollbackAttributeEffect {}

class ModelAttribute {
  final String name;
  final ModelValueType valueType;
  ModelAttribute({@required this.name, @required this.valueType});
}

class ModelValueType {
  bool hidden;
  final String value;
  final List<String> productIds;
  ModelValueType(
      {@required this.value, this.hidden = false, @required this.productIds});
}

class ProductMasterModel extends ChangeNotifier {
  List<ProductMaster> listProductMaster;

  ProductMasterModel({this.listProductMaster});

  loadListProductMaster(Product product) {
    listProductMaster = product?.productMaster?.sharedProducts?.toList();
  }
}
