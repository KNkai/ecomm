import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/components/app_delay.dart';
import 'package:genie/src/components/app_loading.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/components/bottom_action_container.dart';
import 'package:genie/src/helper/cart_data.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/pages/cart/cart_page.dart';
import 'package:genie/src/pages/detail_product/components/detail_adjust_amount.dart';
import 'package:genie/src/pages/detail_product/components/detail_adjust_amount_popup.dart';
import 'package:genie/src/pages/detail_product/controllers/detail_product_controller.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';
import 'package:provider/provider.dart';

import 'components/detail_attributes.dart';
import 'components/detail_discount_component.dart';
import 'components/detail_product_image.dart';
import 'components/detail_rating.dart';
import 'components/detail_tabs_content.dart';

// class DetailProductPage extends StatefulWidget {
//   DetailProductPage({Key key, @required this.productId}) : super(key: key);

//   static void push(
//       {@required BuildContext context, @required String productId}) {
//     Navigator.of(context).push(MaterialPageRoute(
//         builder: (_) => DetailProductPage(productId: productId)));
//   }

//   static void pushReplacement(
//       {@required BuildContext context, @required String productId}) {
//     Navigator.of(context).pushReplacement(MaterialPageRoute(
//         builder: (_) => DetailProductPage(productId: productId)));
//   }

//   final String productId;

//   @override
//   _DetailProductPageState createState() => _DetailProductPageState();
// }

// class _DetailProductPageState extends State<DetailProductPage>
//     with TickerProviderStateMixin {
//   final addToCartBtn = GlobalKey();

//   final _streamTabIndex = ValueNotifier<int>(0);

//   // AnimationController _animationController;
//   // // Animation<double> animation;

//   // AnimationController controller;
//   // Animation<double> animationRadiusIn;
//   // Animation<double> animationRadiusOut;

//   // final double initialRadius = 55.0;
//   // double radius = 0.0;

//   @override
//   void initState() {
//     // _animationController = AnimationController(
//     //   vsync: this,
//     //   duration: Duration(milliseconds: 3000),
//     // );
//     // animation = Tween(begin: 0.0, end: 500.0).animate(_animationController);
//     // runListString(listString);
//     // _animationController.repeat(reverse: true);

//     // controller =
//     //     AnimationController(vsync: this, duration: Duration(seconds: 5));

//     // animationRadiusIn = Tween<double>(begin: 1.0, end: 0.5).animate(
//     //     CurvedAnimation(
//     //         parent: controller,
//     //         curve: Interval(0.75, 1.0, curve: Curves.elasticIn)));
//     // animationRadiusOut = Tween<double>(begin: 0.5, end: 1.0).animate(
//     //     CurvedAnimation(
//     //         parent: controller,
//     //         curve: Interval(0.0, 0.25, curve: Curves.elasticOut)));

//     // controller.addListener(() {
//     //   setState(() {
//     //     if (controller.value >= 0.75 && controller.value <= 1.0) {
//     //       radius = animationRadiusIn.value * initialRadius;
//     //     } else if (controller.value >= 0.0 && controller.value <= 0.25) {
//     //       radius = animationRadiusOut.value * initialRadius;
//     //     }
//     //   });
//     // });

//     // controller.repeat();
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     Product productTemp;

//     return AppDelay(
//         child: Provider(
//           create: (context) => DetailProductController(),
//           dispose: (_, value) {
//             value.dispose();
//           },
//           child: Builder(
//             builder: (context) {
//               final controller =
//                   Provider.of<DetailProductController>(context, listen: false);
//               final listProductMasterController =
//                   Provider.of<ProductMasterModel>(context, listen: false);

//               controller.stateDetailSideEffect.stream.listen((state) {
//                 if (state is StateSideWarning)
//                   WarningDialog.show(context, state.msg, 'OK');
//                 else if (state is StateSideAddCart) {
//                   CartData.getCtl(context).eventAddProudct(
//                       productId: state.productId,
//                       quantity: state.quantity,
//                       note: state.note);

//                   if (state.openCart) {
//                     CartPage.push(context);
//                   }
//                 }
//               });

//               return AppScaffold(
//                   isPaddingContact: true,
//                   title: "Chi Tiết",
//                   child: ValueListenableBuilder(
//                     valueListenable: controller.stateLoadingDetail,
//                     builder: (_, loading, child) {
//                       return AppLoading(
//                         child: child,
//                         loading: loading,
//                       );
//                     },
//                     child: BottomActionContainer(
//                         child: StreamBuilder<StateDetail>(
//                           stream: controller.stateDetail.stream,
//                           builder: (context, snapshot) {
//                             if (snapshot?.data == null) // init
//                               controller.eventLoad(productId: widget.productId);

//                             if (snapshot.data is StateDetailFaileLoaded)
//                               return ConstrainedBox(
//                                 constraints: const BoxConstraints.expand(),
//                                 child: Column(
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: [
//                                     const Text('Có lỗi xảy ra'),
//                                     const SizedBox(
//                                       height: 30,
//                                     ),
//                                     GestureDetector(
//                                       onTap: () {
//                                         controller.eventLoad(
//                                             productId: widget.productId);
//                                         // controller.stateDetail
//                                       },
//                                       child: const Icon(Icons.refresh),
//                                     )
//                                   ],
//                                 ),
//                               );

//                             Product product;
//                             if (snapshot.data != null &&
//                                 snapshot.data is StateDetailSuccessLoaded) {
//                               product =
//                                   (snapshot.data as StateDetailSuccessLoaded)
//                                       .product;
//                               productTemp = product;
//                             }
//                             listProductMasterController
//                                 .loadListProductMaster(product);
//                             return Padding(
//                               padding:
//                                   const EdgeInsets.only(left: 20, right: 20),
//                               child: NestedScrollView(
//                                 headerSliverBuilder: (_, __) {
//                                   return [
//                                     SliverToBoxAdapter(
//                                       child: Column(
//                                         crossAxisAlignment:
//                                             CrossAxisAlignment.start,
//                                         children: [
//                                           const SizedBox(height: 20),
//                                           DetailProductImage(product: product),
//                                           const SizedBox(height: 20),
//                                           ProductInfoAndPricing(
//                                               product: product),
//                                           const SizedBox(height: 10),
//                                           DetailAttributes(
//                                             product: product,
//                                           ),
//                                           const SizedBox(height: 10),
//                                           const DetailAdjustAmount(),
//                                           const SizedBox(height: 10),
//                                           DetailDiscountComponent(
//                                             product: product,
//                                           ),
//                                           const Divider(thickness: 0.8),
//                                           DetailRating(
//                                             product: product,
//                                           ),
//                                           const Divider(thickness: 0.8),
//                                           const SizedBox(height: 10),
//                                         ],
//                                       ),
//                                     ),
//                                     SliverToBoxAdapter(
//                                       child: DetailTabs(
//                                         isFAQ: product?.productMaster?.faq !=
//                                                 null &&
//                                             product
//                                                 .productMaster.faq.isNotEmpty,
//                                         isDescription:
//                                             product?.description != null &&
//                                                 product.description.isNotEmpty,
//                                         onIndex: (index) {
//                                           _streamTabIndex.value = index;
//                                         },
//                                       ),
//                                     ),
//                                     const SliverToBoxAdapter(
//                                         child: SizedBox(
//                                       height: 10,
//                                     )),
//                                   ];
//                                 },
//                                 body: DetailTabsContent(
//                                   product: product,
//                                   isFAQ: product?.productMaster?.faq != null &&
//                                       product.productMaster.faq.isNotEmpty,
//                                   isDescription: product?.description != null &&
//                                       product.description.isNotEmpty,
//                                   streamIndexChange: _streamTabIndex,
//                                 ),
//                               ),
//                             );
//                           },
//                         ),
//                         bottomChild: Row(
//                           mainAxisAlignment: MainAxisAlignment.center,
//                           children: [
//                             const SizedBox(width: 20),
//                             Expanded(
//                               child: AppBtn(
//                                 borderColor: AppColor.primary,
//                                 color: Colors.white,
//                                 textColor: AppColor.primary,
//                                 onTap: () {
//                                   controller.eventGoCart(openCart: true);
//                                   controller.stateQuantity.value = 1;
//                                 },
//                                 title: 'Mua ngay',
//                               ),
//                             ),
//                             const SizedBox(width: 20),
//                             Expanded(
//                               key: addToCartBtn,
//                               child: AppBtn(
//                                 onTap: () {
//                                   if (controller.isSafeToOpenCart)
//                                     appShowModalBottomSheet(
//                                       context: context,
//                                       onBtnTap: () {
//                                         // controller.eventGoCart();
//                                         // controller.stateQuantity.value = 1;
//                                         // return Transform.translate(
//                                         //   offset: Offset(radius, radius),
//                                         //   child: Image.asset(
//                                         //     AppAsset.cart,
//                                         //     width: 30,
//                                         //     height: 30,
//                                         //   ),
//                                         // );
//                                         print(addToCartBtn.globalPaintBounds);
//                                       },
//                                       child: Column(
//                                         children: [
//                                           Padding(
//                                             padding: const EdgeInsets.symmetric(
//                                                 vertical: 10),
//                                             child: DetailAdjustAmountPopUp(
//                                               controller: controller,
//                                             ),
//                                           ),
//                                           const SizedBox(height: 10),
//                                           TextField(
//                                             controller: TextEditingController(
//                                                 text: controller.note),
//                                             minLines: 2,
//                                             maxLines: 2,
//                                             cursorColor: Colors.grey,
//                                             onChanged: (note) {
//                                               controller.eventChangeNote(note);
//                                             },
//                                             decoration: InputDecoration(
//                                               focusedBorder:
//                                                   const OutlineInputBorder(
//                                                 borderSide: BorderSide(
//                                                     color: Colors.grey,
//                                                     width: 0.0),
//                                               ),
//                                               hintText: 'Ghi chú',
//                                               hintStyle: TextStyle(
//                                                   fontSize: 13,
//                                                   color: Colors.grey[400]),
//                                               enabledBorder:
//                                                   const OutlineInputBorder(
//                                                 borderSide: BorderSide(
//                                                     color: Colors.grey,
//                                                     width: 0.0),
//                                               ),
//                                               border:
//                                                   const OutlineInputBorder(),
//                                               labelStyle: const TextStyle(
//                                                   color: Colors.green),
//                                             ),
//                                           ),
//                                         ],
//                                       ),
//                                       title: nameProductSelect(
//                                         name: productTemp?.productMaster?.name,
//                                         attr: controller
//                                             .mapSelectAttribute.values
//                                             .toString(),
//                                       ),
//                                     );
//                                 },
//                                 title: 'Thêm vào giỏ',
//                               ),
//                             ),
//                             const SizedBox(width: 20),
//                           ],
//                         )),
//                   ));
//             },
//           ),
//         ),
//         starter: AppScaffold(title: 'Chi Tiết', child: SizedBox.shrink()));
//   }

//   String nameProductSelect({String name, String attr}) {
//     return name + " - " + attr.substring(1, attr.length - 1);
//   }
// }

// class ProductInfoAndPricing extends StatelessWidget {
//   final Product product;
//   const ProductInfoAndPricing({
//     Key key,
//     @required this.product,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     final firstCategory = product?.productMaster?.categories == null ||
//             product.productMaster.categories.isEmpty ||
//             product.productMaster.categories[0] == null ||
//             product.productMaster.categories[0].name == null
//         ? ''
//         : product.productMaster.categories[0].name;
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         Text(firstCategory),
//         const SizedBox(height: 8),
//         Text(
//           product?.name ?? "",
//           style: const TextStyle(fontSize: 21),
//         ),
//         const SizedBox(height: 10),
//         Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Text(
//               appCurrency(product?.getSalePrice),
//               style: const TextStyle(
//                   color: AppColor.primary,
//                   fontSize: 22,
//                   fontWeight: FontWeight.bold),
//             ),
//             if (product?.hasSale != null && product.hasSale) ...[
//               const SizedBox(height: 3),
//               Text(
//                 appCurrency(product?.basePrice),
//                 style: const TextStyle(
//                   fontSize: 13,
//                   decoration: TextDecoration.lineThrough,
//                 ),
//               )
//             ]
//           ],
//         )
//       ],
//     );
//   }
// }
class DetailProductPage extends StatelessWidget {
  DetailProductPage({Key key, @required this.productId}) : super(key: key);

  static void push(
      {@required BuildContext context, @required String productId}) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => DetailProductPage(productId: productId)));
  }

  static void pushReplacement(
      {@required BuildContext context, @required String productId}) {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (_) => DetailProductPage(productId: productId)));
  }

  final String productId;
  final _streamTabIndex = ValueNotifier<int>(0);

  @override
  Widget build(BuildContext context) {
    Product productTemp;
    return AppDelay(
        child: Provider(
          create: (context) => DetailProductController(),
          dispose: (_, value) {
            value.dispose();
          },
          child: Builder(
            builder: (context) {
              final controller =
                  Provider.of<DetailProductController>(context, listen: false);
              final listProductMasterController =
                  Provider.of<ProductMasterModel>(context, listen: false);

              controller.stateDetailSideEffect.stream.listen((state) {
                if (state is StateSideWarning)
                  WarningDialog.show(context, state.msg, 'OK');
                else if (state is StateSideAddCart) {
                  CartData.getCtl(context).eventAddProudct(
                      productId: state.productId,
                      quantity: state.quantity,
                      note: state.note);

                  if (state.openCart) {
                    CartPage.push(context);
                  }
                }
              });

              return AppScaffold(
                  isPaddingContact: true,
                  title: "Chi Tiết",
                  child: ValueListenableBuilder(
                    valueListenable: controller.stateLoadingDetail,
                    builder: (_, loading, child) {
                      return AppLoading(
                        child: child,
                        loading: loading,
                      );
                    },
                    child: BottomActionContainer(
                        child: StreamBuilder<StateDetail>(
                          stream: controller.stateDetail.stream,
                          builder: (context, snapshot) {
                            if (snapshot?.data == null) // init
                              controller.eventLoad(productId: productId);
                            if (snapshot.data is StateDetailFaileLoaded)
                              return ConstrainedBox(
                                constraints: const BoxConstraints.expand(),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Text('Có lỗi xảy ra'),
                                    const SizedBox(
                                      height: 30,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        controller.eventLoad(
                                            productId: productId);
                                        // controller.stateDetail
                                      },
                                      child: const Icon(Icons.refresh),
                                    )
                                  ],
                                ),
                              );

                            Product product;
                            if (snapshot.data != null &&
                                snapshot.data is StateDetailSuccessLoaded) {
                              product =
                                  (snapshot.data as StateDetailSuccessLoaded)
                                      .product;
                              productTemp = product;
                            }
                            listProductMasterController
                                .loadListProductMaster(product);
                            return Padding(
                              padding:
                                  const EdgeInsets.only(left: 20, right: 20),
                              child: NestedScrollView(
                                headerSliverBuilder: (_, __) {
                                  return [
                                    SliverToBoxAdapter(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const SizedBox(height: 20),
                                          DetailProductImage(product: product),
                                          const SizedBox(height: 20),
                                          ProductInfoAndPricing(
                                              product: product),
                                          const SizedBox(height: 10),
                                          DetailAttributes(
                                            product: product,
                                          ),
                                          const SizedBox(height: 10),
                                          const DetailAdjustAmount(),
                                          const SizedBox(height: 10),
                                          DetailDiscountComponent(
                                            product: product,
                                          ),
                                          const Divider(thickness: 0.8),
                                          DetailRating(
                                            product: product,
                                          ),
                                          const Divider(thickness: 0.8),
                                          const SizedBox(height: 10),
                                        ],
                                      ),
                                    ),
                                    SliverToBoxAdapter(
                                      child: DetailTabs(
                                        isFAQ: product?.productMaster?.faq !=
                                                null &&
                                            product
                                                .productMaster.faq.isNotEmpty,
                                        isDescription:
                                            product?.description != null &&
                                                product.description.isNotEmpty,
                                        onIndex: (index) {
                                          _streamTabIndex.value = index;
                                        },
                                      ),
                                    ),
                                    const SliverToBoxAdapter(
                                        child: SizedBox(
                                      height: 10,
                                    )),
                                  ];
                                },
                                body: DetailTabsContent(
                                  product: product,
                                  isFAQ: product?.productMaster?.faq != null &&
                                      product.productMaster.faq.isNotEmpty,
                                  isDescription: product?.description != null &&
                                      product.description.isNotEmpty,
                                  streamIndexChange: _streamTabIndex,
                                ),
                              ),
                            );
                          },
                        ),
                        bottomChild: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const SizedBox(width: 20),
                            Expanded(
                              child: AppBtn(
                                borderColor: AppColor.primary,
                                color: Colors.white,
                                textColor: AppColor.primary,
                                onTap: () {
                                  controller.eventGoCart(openCart: true);
                                  controller.stateQuantity.value = 1;
                                },
                                title: 'Mua ngay',
                              ),
                            ),
                            const SizedBox(width: 20),
                            Expanded(
                              child: AppBtn(
                                onTap: () {
                                  if (controller.isSafeToOpenCart)
                                    appShowModalBottomSheet(
                                      context: context,
                                      onBtnTap: () {
                                        controller.eventGoCart();
                                        controller.stateQuantity.value = 1;
                                      },
                                      child: Column(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 10),
                                            child: DetailAdjustAmountPopUp(
                                              controller: controller,
                                            ),
                                          ),
                                          const SizedBox(height: 10),
                                          TextField(
                                            controller: TextEditingController(
                                                text: controller.note),
                                            minLines: 2,
                                            maxLines: 2,
                                            cursorColor: Colors.grey,
                                            onChanged: (note) {
                                              controller.eventChangeNote(note);
                                            },
                                            decoration: InputDecoration(
                                              focusedBorder:
                                                  const OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.grey,
                                                    width: 0.0),
                                              ),
                                              hintText: 'Ghi chú',
                                              hintStyle: TextStyle(
                                                  fontSize: 13,
                                                  color: Colors.grey[400]),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.grey,
                                                    width: 0.0),
                                              ),
                                              border:
                                                  const OutlineInputBorder(),
                                              labelStyle: const TextStyle(
                                                  color: Colors.green),
                                            ),
                                          ),
                                        ],
                                      ),
                                      title: nameProductSelect(
                                        name: productTemp?.productMaster?.name,
                                        attr: controller
                                            .mapSelectAttribute.values
                                            .toString(),
                                      ),
                                    );
                                },
                                title: 'Thêm vào giỏ',
                              ),
                            ),
                            const SizedBox(width: 20),
                          ],
                        )),
                  ));
            },
          ),
        ),
        starter: AppScaffold(title: 'Chi Tiết', child: SizedBox.shrink()));
  }

  String nameProductSelect({String name, String attr}) {
    return name + " - " + attr.substring(1, attr.length - 1);
  }
}

class ProductInfoAndPricing extends StatelessWidget {
  final Product product;
  const ProductInfoAndPricing({
    Key key,
    @required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final firstCategory = product?.productMaster?.categories == null ||
            product.productMaster.categories.isEmpty ||
            product.productMaster.categories[0] == null ||
            product.productMaster.categories[0].name == null
        ? ''
        : product.productMaster.categories[0].name;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(firstCategory),
        const SizedBox(height: 8),
        Text(
          product?.name ?? "",
          style: const TextStyle(fontSize: 21),
        ),
        const SizedBox(height: 10),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              appCurrency(product?.getSalePrice),
              style: const TextStyle(
                  color: AppColor.primary,
                  fontSize: 22,
                  fontWeight: FontWeight.bold),
            ),
            if (product?.hasSale != null && product.hasSale) ...[
              const SizedBox(height: 3),
              Text(
                appCurrency(product?.basePrice),
                style: const TextStyle(
                  fontSize: 13,
                  decoration: TextDecoration.lineThrough,
                ),
              )
            ]
          ],
        )
      ],
    );
  }
}
