import 'package:flutter/material.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/helper/auth.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/pages/detail_product/components/detail_tab_rating/detail_tab_rating_controller.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';
import 'package:provider/provider.dart';

import '../detail_list_product_master.dart';
import 'detail_tab_comments.dart';

class DetailTabRating extends StatefulWidget {
  final Product product;
  const DetailTabRating({@required this.product});
  @override
  _DetailTabRatingState createState() => _DetailTabRatingState();
}

class _DetailTabRatingState extends State<DetailTabRating>
    with AutomaticKeepAliveClientMixin {
  final _controller = DetailTabRatingController();

  @override
  void initState() {
    _controller.stateSide.stream.listen((event) {
      WarningDialog.show(context, event.msg, 'OK', title: 'Thông báo');
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Provider(
      create: (_) => _controller,
      child: SingleChildScrollView(
        child: Column(
          children: [
            ValueListenableBuilder<bool>(
              valueListenable: _controller.stateLockRating,
              builder: (_, lock, __) {
                return IgnorePointer(
                  ignoring: lock,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Đánh giá của bạn',
                        style: TextStyle(fontSize: 15),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      _RatingButtons(
                        onRating: (rating) {
                          _controller.eventUpdateRating(rating);
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      if (!lock)
                        const Text(
                          'Viết nhận xét',
                          style: TextStyle(fontSize: 15),
                        ),
                      if (!lock)
                        const SizedBox(
                          height: 15,
                        ),
                      if (!lock)
                        TextField(
                          minLines: 4,
                          maxLines: 10,
                          cursorColor: Colors.grey,
                          onChanged: (msg) {
                            _controller.eventUpdateMsg(msg);
                          },
                          decoration: InputDecoration(
                            focusedBorder: const OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.grey, width: 0.0),
                            ),
                            hintText: 'Nhập nhận xét của bạn',
                            hintStyle: TextStyle(
                                fontSize: 13, color: Colors.grey[400]),
                            enabledBorder: const OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.grey, width: 0.0),
                            ),
                            border: const OutlineInputBorder(),
                            labelStyle: const TextStyle(color: Colors.green),
                          ),
                        ),
                      if (!lock)
                        const SizedBox(
                          height: 18,
                        ),
                      if (!lock)
                        Align(
                          alignment: Alignment.center,
                          child: AppBtn(
                            title: 'Gửi nhận xét',
                            onTap: () {
                              Auth.checkExistUser(
                                  context: context,
                                  onLoginSuccess: () {
                                    _controller.eventSendRating(widget.product);
                                  });
                            },
                          ),
                        ),
                      const SizedBox(
                        height: 18,
                      ),
                      const Divider(
                        thickness: 0.8,
                      ),
                    ],
                  ),
                );
              },
            ),
            DetailTabComments(
              productMasterId: widget.product.productMasterId,
            ),
            DetailListProductMaster(),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _RatingButtons extends StatefulWidget {
  final void Function(int) onRating;

  const _RatingButtons({@required this.onRating});

  @override
  __RatingButtonsState createState() => __RatingButtonsState();
}

class __RatingButtonsState extends State<_RatingButtons> {
  int _index = -1;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
        _StarSingleBtn(
          select: _index >= 1,
          onTap: () {
            _onRating(1);
          },
        ),
        _StarSingleBtn(
          select: _index >= 2,
          onTap: () {
            _onRating(2);
          },
        ),
        _StarSingleBtn(
          select: _index >= 3,
          onTap: () {
            _onRating(3);
          },
        ),
        _StarSingleBtn(
          select: _index >= 4,
          onTap: () {
            _onRating(4);
          },
        ),
        _StarSingleBtn(
          select: _index == 5,
          onTap: () {
            _onRating(5);
          },
        ),
      ]),
    );
  }

  void _onRating(int index) {
    setState(() {
      _index = index;
    });
    widget.onRating(_index);
  }
}

class _StarSingleBtn extends StatelessWidget {
  final void Function() onTap;
  final bool select;
  const _StarSingleBtn({@required this.onTap, this.select = false});
  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.opaque,
      child: Align(
        alignment: Alignment.centerLeft,
        child: Icon(
          Icons.star,
          color: select ? AppColor.primary : Colors.grey[300],
          size: 30,
        ),
      ),
    ));
  }
}
