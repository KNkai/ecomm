import 'package:flutter/material.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/rating_log.model.dart';
import 'package:genie/src/models/rating_replay.model.dart';
import 'package:genie/src/pages/detail_product/components/detail_tab_rating/detail_tab_rating_controller.dart';
import 'package:genie/src/pages/detail_product/components/star_rating.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';
import 'package:provider/provider.dart';
import 'package:readmore/readmore.dart';

class DetailTabComments extends StatelessWidget {
  DetailTabComments({@required this.productMasterId});
  final String productMasterId;

  @override
  Widget build(BuildContext context) {
    final controller =
        Provider.of<DetailTabRatingController>(context, listen: false);
    controller.evenInit(parameter: productMasterId);

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: Column(
        children: [
          Row(
            children: [
              const Expanded(
                  child: Text(
                'Ý KIẾN ĐÁNH GIÁ',
                style: TextStyle(fontWeight: FontWeight.bold),
              )),
              ValueListenableBuilder<int>(
                  valueListenable: controller.stateTotalComments,
                  builder: (_, value, __) {
                    return Text(
                      '${value ?? 0} đánh giá',
                      style: const TextStyle(color: Colors.grey),
                    );
                  })
            ],
          ),
          const SizedBox(
            height: 15,
          ),
          StreamBuilder<ListState>(
            stream: controller.stateList.stream,
            builder: (_, ss) {
              if (ss?.data == null || ss.data is ListEmptyState)
                return const SizedBox.shrink();

              final ListLoadState<RatingLog> loadedState = ss.data;
              return Column(
                children: loadedState.data.reversed.map(
                  (e) {
                    return _Comment(ratingLog: e);
                  },
                ).toList(),
              );
            },
          ),
          const SizedBox(
            height: 20,
          ),
          ValueListenableBuilder<bool>(
            valueListenable: controller.stateLoadMoreBtn,
            builder: (_, show, child) {
              if (show) return child;
              return const SizedBox.shrink();
            },
            child: GestureDetector(
                onTap: () {
                  controller.eventLoadMore(parameter: productMasterId);
                },
                child: const Text(
                  'Xem thêm đánh giá',
                  style: TextStyle(
                      color: AppColor.primary,
                      decoration: TextDecoration.underline),
                )),
          )
        ],
      ),
    );
  }
}

class _Comment extends StatelessWidget {
  final RatingLog ratingLog;
  const _Comment({@required this.ratingLog});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(children: [
            SizedBox(
              width: 30,
              height: 30,
              child: (ratingLog?.customer?.avatar != null)
                  ? DecoratedBox(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.grey[300],
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(ratingLog.customer.avatar))),
                    )
                  : DecoratedBox(
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        shape: BoxShape.circle,
                      ),
                    ),
            ),
            const SizedBox(
              width: 10,
            ),
            Text(ratingLog?.customer?.name ?? ''),
          ]),
          const SizedBox(
            height: 5,
          ),
          Row(
            children: [
              StarRating(
                value: ratingLog?.rating ?? 0,
                size: 24,
                color: AppColor.primary,
              ),
              const SizedBox(
                width: 10,
              ),
              Text(
                appConvertDateTime(ratingLog?.updatedAt,
                    format: 'dd/MM/yyyy HH:mm'),
                style: TextStyle(fontSize: 13),
              )
            ],
          ),
          const SizedBox(
            height: 8,
          ),
          ReadMoreText(
            ratingLog?.message ?? '',
            trimCollapsedText: ' Xem thêm',
            trimExpandedText: ' Rút gọn',
          ),
          _Responses(
            replies: ratingLog?.replies,
          ),
        ],
      ),
    );
  }
}

class _Responses extends StatelessWidget {
  final List<RatingReplay> replies;

  const _Responses({@required this.replies});
  @override
  Widget build(BuildContext context) {
    if (replies == null || replies.isEmpty) return const SizedBox.shrink();

    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 16),
      child: Column(
        children: replies.map((e) {
          if (e == null) return const SizedBox.shrink();

          return Padding(
            padding: const EdgeInsets.only(top: 10),
            child: DecoratedBox(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(children: [
                        SizedBox(
                          width: 30,
                          height: 30,
                          child: DecoratedBox(
                            decoration: (e?.user?.avatar != null)
                                ? BoxDecoration(
                                    color: Colors.grey[300],
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: NetworkImage(e.user.avatar)))
                                : BoxDecoration(
                                    color: Colors.grey[300],
                                    shape: BoxShape.circle,
                                  ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Flexible(
                            child: Text(
                          'Genie',
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        )),
                        const SizedBox(
                          width: 10,
                        ),
                        Text(
                          e?.user?.role ?? '',
                          style: const TextStyle(color: AppColor.primary),
                        ),
                      ]),
                      const SizedBox(
                        height: 10,
                      ),
                      ReadMoreText(
                        e?.message ?? '',
                        trimCollapsedText: ' Xem thêm',
                        trimExpandedText: ' Rút gọn',
                      ),
                    ],
                  ),
                ),
                decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Colors.grey[300]),
                    color: Colors.grey[50])),
          );
        }).toList(),
      ),
    );
  }
}
