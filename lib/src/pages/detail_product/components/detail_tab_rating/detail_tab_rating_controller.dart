import 'dart:async';

import 'package:flutter/material.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/models/rating_log.model.dart';
import 'package:genie/src/repositories/rating_log.repo.dart';
import 'package:genie/src/utils/app_key.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DetailTabRatingController
    extends InitLoadMoreSearchController<RatingLog> {
  final stateSide = StreamController<StateSideTabShowMsg>();
  final stateLockRating = ValueNotifier(false);
  final _repo = RatingLogRepository();
  final stateTotalComments = ValueNotifier<int>(null);
  final stateLoadMoreBtn = ValueNotifier<bool>(true);

  int _rating;
  String _msg;

  DetailTabRatingController._(
      InitListProtocol initListProtocol, LoadMoreProtocol loadMoreProtocol)
      : super(
            hashLoadingInit: false,
            initProtocol: initListProtocol,
            loadMoreProtocol: loadMoreProtocol);

  factory DetailTabRatingController() {
    final _protocol = RatingLogRepository();
    return DetailTabRatingController._(_protocol, _protocol);
  }

  @override
  void onUpdateTotal(int total) {
    super.onUpdateTotal(total);

    if (stateTotalComments.value == null) stateTotalComments.value = total;
  }

  @override
  void onReachMaximumData() {
    stateLoadMoreBtn.value = false;
  }

  void eventUpdateRating(int rating) {
    _rating = rating;
  }

  void eventUpdateMsg(String msg) {
    _msg = msg;
  }

  void eventSendRating(Product product) {
    if (_msg == null || _msg.isEmpty) {
      stateSide.add(StateSideTabShowMsg(msg: 'Không được để trống đánh giá'));
    } else if (_rating == null || _rating == 0) {
      stateSide.add(StateSideTabShowMsg(msg: 'Không được để trống điểm'));
    } else {
      SharedPreferences.getInstance().then((instance) {
        final customerId = instance.getString(AppKey.customerId);
        _repo.writeRating(_msg, _rating, product.id, customerId);
      });
      stateSide.add(StateSideTabShowMsg(msg: 'Cám ơn bạn đã đánh giá!'));
      stateLockRating.value = true;
    }
  }

  @override
  void dispose() {
    super.dispose();
    stateLoadMoreBtn.dispose();
    stateSide.close();
    stateLockRating.dispose();
    stateTotalComments.dispose();
  }
}

class StateSideTabShowMsg {
  final String msg;
  StateSideTabShowMsg({@required this.msg});
}
