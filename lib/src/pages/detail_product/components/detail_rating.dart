import 'package:flutter/material.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/pages/detail_product/components/star_rating.dart';
import 'package:genie/src/pages/favorites/favorite_controller.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';

class DetailRating extends StatelessWidget {
  const DetailRating({@required this.product});

  final Product product;
  @override
  Widget build(BuildContext context) {
    FavoriteController favoriteController = FavoriteController();
    favoriteController.checkFavoritesProd(id: product?.id);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: [
          StarRating(
            color: AppColor.primary,
            size: 24,
            value: product?.rating ?? 0,
          ),
          const SizedBox(
            width: 8,
          ),
          Text(
            appConvertStringDouble(product?.rating) ?? "",
            style: const TextStyle(color: AppColor.primary),
          ),
          const SizedBox(
            width: 8,
          ),
          SizedBox(
              height: 18,
              width: 1.2,
              child: ColoredBox(color: Colors.grey[400])),
          const SizedBox(
            width: 8,
          ),
          Text(
            product?.saleQty == null
                ? ''
                : 'Đã bán: ' + product.saleQty.toString(),
            style: const TextStyle(color: AppColor.primary),
          ),
          ValueListenableBuilder<FavoriteProduct>(
            valueListenable: favoriteController.check,
            builder: (context, value, child) {
              return IconButton(
                icon: value != null
                    ? Image.asset(
                        AppAsset.ic_favorites,
                        fit: BoxFit.fitWidth,
                      )
                    : Image.asset(
                        AppAsset.ic_nonfavorites,
                        fit: BoxFit.fitWidth,
                      ),
                onPressed: () {
                  // print(check);
                  value != null
                      ? favoriteController.delFavouriteProd()
                      : favoriteController.addFavouriteProd(product?.id);
                },
              );
            },
          )
        ],
      ),
    );
  }
}
