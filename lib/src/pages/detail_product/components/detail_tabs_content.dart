import 'package:flutter/material.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/pages/detail_product/components/detail_tab_faq.dart';
import 'package:genie/src/utils/app_color.dart';

import 'detail_tab_info.dart';
import 'detail_tab_rating/detail_tab_rating.dart';

class DetailTabsContent extends StatefulWidget {
  final ValueNotifier<int> streamIndexChange;
  final bool isDescription;
  final bool isFAQ;
  final Product product;

  const DetailTabsContent(
      {@required this.product,
      @required this.streamIndexChange,
      @required this.isDescription,
      @required this.isFAQ});

  @override
  _DetailTabsContentState createState() => _DetailTabsContentState();
}

class _DetailTabsContentState extends State<DetailTabsContent> {
  final _pageController = PageController();

  @override
  void dispose() {
    widget.streamIndexChange.dispose();
    widget.streamIndexChange.removeListener(_onStreamIndexChange);
    _pageController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    widget.streamIndexChange.addListener(_onStreamIndexChange);
    super.initState();
  }

  void _onStreamIndexChange() {
    _pageController.jumpToPage(widget.streamIndexChange.value);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.product == null) return SizedBox.shrink();
    return PageView(
      physics: const NeverScrollableScrollPhysics(),
      controller: _pageController,
      children: [
        // All tab pages must attach keep-state mechanism with AutomaticKeepAliveClientMixin
        if (widget.isDescription)
          SingleChildScrollView(
              child: DetailTabInfo(
            value: widget?.product?.description,
          )),
        DetailTabRating(
          product: widget?.product,
        ),
        if (widget.isFAQ)
          SingleChildScrollView(
              child: DetailTabFaq(
            product: widget.product,
          )),
      ],
    );
  }
}

class DetailTabs extends StatefulWidget {
  final void Function(int) onIndex;
  final bool isDescription;
  final bool isFAQ;

  const DetailTabs(
      {@required this.onIndex,
      @required this.isDescription,
      // @required this.isRatingLog,
      @required this.isFAQ});
  @override
  _DetailTabsState createState() => _DetailTabsState();
}

class _DetailTabsState extends State<DetailTabs> {
  int _index = 0;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!widget.isDescription && widget.isFAQ)
      return Row(children: [
        _TabItem(
          onTap: () {
            changeToPage(0);
          },
          title: 'Đánh giá',
          select: 0 == _index,
        ),
        const SizedBox(
          width: 20,
        ),
        _TabItem(
          onTap: () {
            changeToPage(1);
          },
          select: 1 == _index,
          title: 'FAQ',
        )
      ]);

    if (!widget.isDescription && !widget.isFAQ)
      return Align(
        alignment: Alignment.centerLeft,
        child: _TabItem(
          onTap: () {},
          select: true,
          title: 'Đánh giá',
        ),
      );

    if (widget.isDescription && !widget.isFAQ)
      return Row(children: [
        _TabItem(
          onTap: () {
            changeToPage(0);
          },
          title: 'Thông tin',
          select: 0 == _index,
        ),
        const SizedBox(
          width: 20,
        ),
        _TabItem(
          onTap: () {
            changeToPage(1);
          },
          title: 'Đánh giá',
          select: 1 == _index,
        ),
      ]);

    return Row(children: [
      _TabItem(
        onTap: () {
          changeToPage(0);
        },
        title: 'Thông tin',
        select: 0 == _index,
      ),
      const SizedBox(
        width: 20,
      ),
      _TabItem(
        onTap: () {
          changeToPage(1);
        },
        title: 'Đánh giá',
        select: 1 == _index,
      ),
      const SizedBox(
        width: 20,
      ),
      _TabItem(
        onTap: () {
          changeToPage(2);
        },
        select: 2 == _index,
        title: 'FAQ',
      )
    ]);
  }

  void changeToPage(int index) {
    if (index != _index) {
      setState(() {
        _index = index;
      });
      widget.onIndex(index);
    }
  }
}

class _TabItem extends StatelessWidget {
  final bool select;
  final String title;
  final void Function() onTap;
  const _TabItem({@required this.onTap, this.select = false, this.title});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: this.onTap,
      child: Column(
        children: [
          Text(
            title,
            style: TextStyle(
                fontSize: 18, color: select ? Colors.black : Colors.grey),
          ),
          const SizedBox(
            height: 6,
          ),
          SizedBox(
            width: 70,
            height: 2,
            child: ColoredBox(
              color: select ? AppColor.primary : Colors.transparent,
            ),
          )
        ],
      ),
    );
  }
}
