import 'package:flutter/material.dart';
import 'package:genie/src/components/ribbon.dart';
import 'package:genie/src/pages/detail_product/controllers/detail_product_controller.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';
import 'package:provider/provider.dart';

import '../detail_product_page.dart';

class DetailListProductMaster extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final listProductMasterController =
        Provider.of<ProductMasterModel>(context, listen: false);
    if (listProductMasterController.listProductMaster != null) {
      return Column(
        children: [
          DetailListProductMasterTitle(),
          SizedBox(
            height: ((MediaQuery.of(context).size.width / 3) + 20) * 2,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: List.generate(
                listProductMasterController.listProductMaster?.length,
                (index) {
                  return Container(
                    margin: EdgeInsets.only(right: 14, top: 17, bottom: 20),
                    width: (MediaQuery.of(context).size.width / 3) + 30,
                    child: GestureDetector(
                      onTap: () {
                        DetailProductPage.pushReplacement(
                            context: context,
                            productId: listProductMasterController
                                .listProductMaster[index]?.product?.id);
                      },
                      child: Card(
                        elevation: 10,
                        child: Ribbon(
                          showRibbon: listProductMasterController
                              .listProductMaster[index]?.product?.hasSale,
                          title: "Giảm",
                          content: listProductMasterController
                              .listProductMaster[index]
                              ?.product
                              ?.getSaleContent,
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: Container(
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: NetworkImage(
                                            listProductMasterController
                                                .listProductMaster[index]
                                                ?.listImage[0]),
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  ),
                                  flex: 1,
                                ),
                                Expanded(
                                  child: Column(
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 10),
                                          child: Text(
                                            listProductMasterController
                                                .listProductMaster[index]?.name,
                                            maxLines: 3,
                                            textAlign: TextAlign.center,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontSize: 15,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Divider(
                                        thickness: 1,
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Row(
                                          children: [
                                            Expanded(
                                              flex: 3,
                                              child: Text(
                                                appCurrency(
                                                    listProductMasterController
                                                        .listProductMaster[
                                                            index]
                                                        ?.product
                                                        ?.getSalePrice),
                                                style: TextStyle(
                                                  color: AppColor.primary,
                                                  fontSize: 13,
                                                ),
                                                maxLines: 1,
                                                overflow: TextOverflow.fade,
                                              ),
                                            ),
                                            Expanded(
                                              flex: 2,
                                              child: Text(
                                                appCurrency(
                                                    listProductMasterController
                                                        .listProductMaster[
                                                            index]
                                                        ?.product
                                                        ?.basePrice),
                                                style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 9,
                                                  decoration: TextDecoration
                                                      .lineThrough,
                                                ),
                                                textAlign: TextAlign.end,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      );
    }
    return const SizedBox();
  }
}

class DetailListProductMasterTitle extends StatelessWidget {
  const DetailListProductMasterTitle({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 30,
      child: Text(
        "Sản phẩm liên quan",
        textAlign: TextAlign.start,
        style: TextStyle(
          fontSize: 21,
        ),
      ),
    );
  }
}
