import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/ribbon.dart';
import 'package:genie/src/pages/youtube/youtube_page.dart';
import 'package:genie/src/utils/app_color.dart';

import '../../../models/product.model.dart';
import '../../../utils/app_asset.dart';
import '../../../utils/size_config.dart';

class DetailProductImage extends StatelessWidget {
  final _borderRadius = BorderRadius.circular(10);
  final _imageSize = 0.64492 * SizeConfig.widthDevice;
  final _controller = _DetailProductImageController();
  final Product product;

  DetailProductImage({Key key, @required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final smallImageSize = 0.3501945525 * _imageSize;
    _controller.eventLoad(product);
    return SizedBox(
      height: _imageSize + smallImageSize,
      child: ValueListenableBuilder<String>(
        valueListenable: _controller.stateMainImage,
        builder: (_, mainImage, __) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: double.infinity,
                child: Stack(
                  children: [
                    Container(
                      height: _imageSize,
                      width: double.infinity,
                      child: FadeInImage.assetNetwork(
                          fit: BoxFit.cover,
                          placeholder: AppAsset.errorPlaceHolder,
                          // width: _imageSize,
                          // height: _imageSize,
                          image: mainImage ?? ''),
                    ),
                    product?.hasSale == null
                        ? Container()
                        : product.hasSale
                            ? Positioned.fill(
                                child: CustomPaint(
                                  painter: RibbonPainter(
                                      isRight: true,
                                      title: "Giảm",
                                      content: product?.getSaleContent ?? ""),
                                ),
                              )
                            : Container(),
                    FutureBuilder<String>(
                      future: _controller.stateYoutubeOpen.future,
                      builder: (_, youtubeLink) {
                        if (youtubeLink?.data == null)
                          return const SizedBox.shrink();
                        return Positioned(
                          left: 8,
                          top: 8,
                          child: GestureDetector(
                            onTap: () {
                              YoutubePage.go(
                                  context: context, link: youtubeLink.data);
                            },
                            child: DecoratedBox(
                                child: const Padding(
                                  child: Text(
                                    'Xem video',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 13, vertical: 9),
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    color: AppColor.primary)),
                          ),
                        );
                      },
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[200],
                        spreadRadius: 5,
                        blurRadius: 10,
                        offset:
                            const Offset(0, 10), // changes position of shadow
                      )
                    ],
                    borderRadius: _borderRadius),
              ),
              Expanded(
                child: ValueListenableBuilder<_StateListImages>(
                  valueListenable: _controller.stateImages,
                  builder: (_, state, __) {
                    if (state is _StateListImagesEmpty)
                      return const SizedBox.shrink();

                    final _StateListImagesLoaded staeLoaded = (state);
                    return ListView.separated(
                      scrollDirection: Axis.horizontal,
                      separatorBuilder: (_, __) {
                        return const SizedBox(
                          height: 15,
                        );
                      },
                      itemCount: staeLoaded.images.length,
                      itemBuilder: (_, index) {
                        return GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () {
                            _controller
                                .eventSelectImage(staeLoaded.images[index]);
                          },
                          child: DecoratedBox(
                            decoration: BoxDecoration(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(8)),
                                border: Border.all(
                                    width: 2,
                                    color: mainImage == staeLoaded.images[index]
                                        ? AppColor.primary
                                        : Colors.transparent)),
                            child: Padding(
                              padding: const EdgeInsets.all(1.25),
                              child: ClipRRect(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(8)),
                                child: FadeInImage.assetNetwork(
                                    placeholder: AppAsset.errorPlaceHolder,
                                    width: smallImageSize,
                                    fit: BoxFit.cover,
                                    height: smallImageSize,
                                    image: staeLoaded.images[index]),
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  },
                ),
              )
            ],
          );
        },
      ),
    );
  }
}

class _DetailProductImageController {
  final stateMainImage = ValueNotifier<String>('');
  final stateYoutubeOpen = Completer<String>();
  final ValueNotifier<_StateListImages> stateImages =
      ValueNotifier(_StateListImagesEmpty());

  _DetailProductImageController();

  void eventLoad(Product product) {
    if (product != null) {
      if (product.youtubeLink != null && product.youtubeLink.isNotEmpty) {
        stateYoutubeOpen.complete(product.youtubeLink);
      }
      stateMainImage.value =
          product.listImage.isEmpty ? null : product.listImage[0];
      stateImages.value = _StateListImagesLoaded(images: product.listImage);
    }

// // MOCK
    // stateMainImage.value =
    //     'https://fontmeme.com/images/golden-state-warriors-current.png';
    // stateImages.value = _StateListImagesLoaded(images: [
    //   'https://cdn2.iconfinder.com/data/icons/metro-ui-icon-set/512/Reddit.png',
    //   'https://fontmeme.com/images/golden-state-warriors-current.png',
    //   'https://i.pinimg.com/originals/e3/d8/ef/e3d8efd1d7e6efc623af783ef91c3666.jpg',
    //   'xhttps://ftw.usatoday.com/wp-content/uploads/sites/90/2020/03/1280px-philadelphia_76ers_logo.svg_.png?w=1000',
    //   'https://target.scene7.com/is/image/Target/GUEST_63e782b8-c65a-43de-97b8-e171c9eea54f?wid=488&hei=488&fmt=pjpeg',
    //   'https://img.pngio.com/los-angeles-clippers-logo-transparent-png-stickpng-clippers-logo-png-400_400.png',
    //   'https://spng.pngfind.com/pngs/s/17-171333_cleveland-cavaliers-logo-cavs-vector-eps-free-download.png',
    // ]);
  }

  void eventSelectImage(String link) {
    if (link != null) {
      stateMainImage.value = link;
    }
  }
}

abstract class _StateListImages {}

class _StateListImagesLoaded extends _StateListImages {
  final List<String> images;
  _StateListImagesLoaded({@required this.images});
}

class _StateListImagesEmpty extends _StateListImages {}
