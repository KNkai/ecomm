import 'package:flutter/material.dart';
import 'package:genie/src/components/big_ticket.dart';
import 'package:genie/src/components/small_ticket.dart';
import 'package:genie/src/helper/auth.dart';
import 'package:genie/src/models/campaign.model.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/repositories/product.repo.dart';
import 'package:genie/src/repositories/profile.repo.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';

class DetailDiscountComponent extends StatelessWidget {
  final Product product;

  const DetailDiscountComponent({@required this.product});
  @override
  Widget build(BuildContext context) {
    final _profileRepository = ProfileRepository();
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        if (product?.campaigns != null && product.campaigns.isNotEmpty)
          Auth.checkExistUser(
              context: context,
              onLoginSuccess: () {
                appShowModalBottomSheet(
                    context: context,
                    title: 'Chọn mã ưu đãi',
                    child: _VoucherWidget(product.id));
              });
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Align(
            alignment: Alignment.centerLeft,
            child: Row(
              children: [
                Text(
                  '${product?.campaigns == null ? '' : product.campaigns.length.toString()} mã giảm giá',
                ),
                const SizedBox(
                  width: 5,
                ),
                if (product?.campaigns != null)
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Wrap(
                        spacing: 5,
                        runSpacing: 5,
                        children: (product.campaigns.length < 2
                                ? product.campaigns.length == 0
                                    ? product.campaigns.getRange(0, 0)
                                    : product.campaigns.getRange(0, 1)
                                : product?.campaigns?.getRange(0, 2))
                            .map((e) {
                          return FutureBuilder<dynamic>(
                              future: _profileRepository
                                  .getOneSettingByKey('PROMOTION_COLOR'),
                              builder: (context, snapshot) {
                                return SmallTicket(
                                  color: snapshot.data == null
                                      ? AppColor.primary
                                      : HexColor(
                                          snapshot?.data['value'] ?? '000000'),
                                  title: e.code,
                                );
                              });
                        }).toList(),
                      ),
                    ),
                  ),
                const SizedBox(
                  width: 5,
                ),
                Icon(
                  Icons.arrow_forward_ios_rounded,
                  color: Color(0xff9B9B9B),
                ),
              ],
            )),
      ),
    );
  }
}

class _VoucherWidget extends StatelessWidget {
  _VoucherWidget(this.productId);
  final String productId;
  final _controller = _VoucherWidgetController();

  @override
  Widget build(BuildContext context) {
    _controller.eventInit(productId);
    return SizedBox(
      height: 300,
      child: ValueListenableBuilder<List<Campaign>>(
        valueListenable: _controller.stateVouchers,
        builder: (_, campaigns, ___) {
          if (campaigns == null || campaigns.isEmpty) return SizedBox.shrink();

          return ListView.separated(
              padding: EdgeInsets.only(top: 30, left: 1, right: 1),
              itemCount: campaigns.length,
              separatorBuilder: (_, __) {
                return const SizedBox(
                  height: 25,
                );
              },
              itemBuilder: (_, index) {
                return BigTicket(
                    mode: BigTicketMode.save,
                    onSave: () {
                      AppClient.instance.executeWithError('''
                            mutation { takeEvoucher(campaignId: "${campaigns[index].id}") { id } }
                            ''').then((value) {
                        if (value.errors != null &&
                            value.errors.isNotEmpty &&
                            value.errors[0] != null &&
                            value.errors[0].message != null) {
                          WarningDialog.show(
                              context, value.errors[0].message, 'OK',
                              title: 'Thông báo');
                        }
                      });
                    },
                    isSave: campaigns[index].taken,
                    urlImage: campaigns[index].image,
                    smallTitle:
                        'Hạn dùng đến ${appConvertDateTime(campaigns[index].endAt)}',
                    title: campaigns[index].name);
              });
        },
      ),
    );
  }
}

class _VoucherWidgetController {
  final stateVouchers = ValueNotifier<List<Campaign>>(null);
  final _repo = ProductRepository();

  void eventInit(String productId) {
    _repo.getCampaigns(productId: productId).then((value) {
      if (value != null && value is List<Campaign>) {
        if (value.isNotEmpty) {
          stateVouchers.value = value;
        } else {
          stateVouchers.value = null;
        }
      }
    });
  }
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
