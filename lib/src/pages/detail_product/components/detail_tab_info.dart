import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import 'detail_list_product_master.dart';

class DetailTabInfo extends StatefulWidget {
  final String value;
  const DetailTabInfo({@required this.value});

  @override
  _DetailTabInfoState createState() => _DetailTabInfoState();
}

class _DetailTabInfoState extends State<DetailTabInfo>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return widget.value == null || widget.value.isEmpty
        ? const SizedBox.shrink()
        : Column(
            children: [
              Html(data: widget.value),
              DetailListProductMaster(),
            ],
          );
  }
}
