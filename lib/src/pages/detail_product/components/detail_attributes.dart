import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/pages/detail_product/controllers/detail_product_controller.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:provider/provider.dart';

class DetailAttributes extends StatefulWidget {
  final Product product;

  const DetailAttributes({@required this.product});

  @override
  _DetailAttributesState createState() => _DetailAttributesState();
}

class _DetailAttributesState extends State<DetailAttributes> {
  DetailProductController _controller;
  _SingleAttributeController _attributeController;
  LinkedHashMap<String, String> _selectAttribute;
  List<ProductAttribute> _attributes;
  @override
  void initState() {
    super.initState();
    _controller = Provider.of<DetailProductController>(context, listen: false);

    _controller.stateAttributeSideEffect.stream.listen((state) {
      _attributeController?.rollBack();
    });
  }

  @override
  void didUpdateWidget(covariant DetailAttributes oldWidget) {
    super.didUpdateWidget(oldWidget);
    _attributes = widget.product?.productMaster?.attributes;
    if (_attributes != null)
      _selectAttribute =
          _controller.eventLoadAttributes(_attributes, widget.product.id);
  }

  @override
  Widget build(BuildContext context) {
    if (_attributes == null || _attributes.isEmpty)
      return const SizedBox.shrink();

    return StreamBuilder<HashMap<String, HashMap<String, ModelValueType>>>(
        stream: _controller.stateAttributes.stream,
        builder: (_, ss) {
          if (ss?.data == null) return const SizedBox.shrink();

          return Column(
            children: [
              const Divider(
                thickness: 0.8,
              ),
              for (MapEntry<String, HashMap<String, ModelValueType>> a
                  in ss.data.entries)
                Column(
                  children: [
                    _SingleAttribute(
                      onController: (c) {
                        _attributeController = c;
                      },
                      controller: _controller,
                      name: a.key,
                      modelValueType: a.value,
                      initSelectAttribute: _selectAttribute,
                    ),
                  ],
                )
            ],
          );
        });
  }
}

class _SingleAttribute extends StatefulWidget {
  final String name;
  final HashMap<String, ModelValueType> modelValueType;
  final LinkedHashMap<String, String> initSelectAttribute;
  final DetailProductController controller;
  final void Function(_SingleAttributeController controller) onController;
  const _SingleAttribute(
      {@required this.name,
      @required this.modelValueType,
      @required this.controller,
      @required this.onController,
      @required this.initSelectAttribute});

  @override
  __SingleAttributeState createState() => __SingleAttributeState();
}

class __SingleAttributeState extends State<_SingleAttribute>
    with _SingleAttributeController {
  String _attributeValueSelect;
  String _rollbackAttributeValueSelect;
  @override
  void initState() {
    widget.onController(this);
    _attributeValueSelect = widget.initSelectAttribute[widget.name];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final detailProductController =
        Provider.of<DetailProductController>(context, listen: false);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Text(
              '${widget?.name ?? ''}:',
              style: const TextStyle(fontSize: 15),
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          Expanded(
              flex: 5,
              child: Wrap(
                spacing: 16,
                runSpacing: 10,
                children: [
                  for (MapEntry<String, ModelValueType> entry
                      in widget.modelValueType.entries)
                    GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        if (!entry.value.hidden) {
                          if (_attributeValueSelect != entry.value.value) {
                            setState(() {
                              _rollbackAttributeValueSelect =
                                  _attributeValueSelect;

                              _attributeValueSelect = entry.value.value;
                            });
                          } else
                            setState(() {
                              _rollbackAttributeValueSelect =
                                  _attributeValueSelect;

                              _attributeValueSelect = null;
                            });
                          widget.controller.eventSelectAttributeVallue(
                              widget.name,
                              entry.value.value,
                              _attributeValueSelect != null &&
                                  _attributeValueSelect == entry.value.value);
                        }
                        detailProductController.stateQuantity.value = 1;
                      },
                      child: _Item(
                          hidden: entry.value.hidden,
                          title: entry.value.value ?? '',
                          select: _attributeValueSelect != null &&
                              _attributeValueSelect == entry.value.value),
                    ),
                ],
              ))
        ],
      ),
    );
  }

  @override
  void rollBack() {
    setState(() {
      _attributeValueSelect = _rollbackAttributeValueSelect;
      _rollbackAttributeValueSelect = null;
    });
  }
}

class _Item extends StatelessWidget {
  final bool select;
  final bool hidden;
  final String title;
  const _Item({this.select = false, @required this.title, this.hidden = false});

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
        child: Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              title,
              style: TextStyle(
                  color: select
                      ? Colors.white
                      : Colors.black.withOpacity(hidden ? 0.3 : 1)),
            )),
        decoration: BoxDecoration(
            border: select
                ? null
                : Border.all(
                    width: 1,
                    color: AppColor.primary.withOpacity(hidden ? 0.3 : 1)),
            color: select ? AppColor.primary : Colors.white,
            borderRadius: const BorderRadius.all(Radius.circular(8))));
  }
}

abstract class _SingleAttributeController {
  void rollBack();
}
