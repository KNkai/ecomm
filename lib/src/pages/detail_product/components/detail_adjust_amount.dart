import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:genie/src/pages/detail_product/controllers/detail_product_controller.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:provider/provider.dart';

class DetailAdjustAmount extends StatelessWidget {
  const DetailAdjustAmount();

  @override
  Widget build(BuildContext context) {
    final controller =
        Provider.of<DetailProductController>(context, listen: false);
    return Row(
      children: [
        const Expanded(flex: 2, child: Text('Số lượng:')),
        Expanded(
            flex: 5,
            child: Row(
              children: [
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    _down(controller);
                  },
                  child: const DecoratedBox(
                    decoration: BoxDecoration(
                        color: AppColor.primary, shape: BoxShape.circle),
                    child: Icon(
                      Icons.remove,
                      color: Colors.white,
                    ),
                  ),
                ),
                ValueListenableBuilder(
                    valueListenable: controller.stateQuantity,
                    builder: (_, quantity, __) {
                      return SizedBox(
                          width: 40,
                          child: Text(
                            quantity.toString(),
                            textAlign: TextAlign.center,
                            style: const TextStyle(fontSize: 15),
                          ));
                    }),
                GestureDetector(
                  onTap: () {
                    _up(controller);
                  },
                  child: const DecoratedBox(
                    decoration: BoxDecoration(
                        color: AppColor.primary, shape: BoxShape.circle),
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ))
      ],
    );
  }

  void _down(DetailProductController controller) {
    controller.eventChangeProductQuantity(add: false);
  }
}

void _up(DetailProductController controller) {
  controller.eventChangeProductQuantity();
}
