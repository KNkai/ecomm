import 'package:flutter/material.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/pages/detail_product/components/detail_list_product_master.dart';
import 'package:genie/src/utils/app_color.dart';

class DetailTabFaq extends StatelessWidget {
  final Product product;
  const DetailTabFaq({@required this.product});
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      for (int c = 0; c < product.productMaster.faq.length; c++)
        _Item(
            question: product.productMaster.faq[c]?.question ?? '',
            answer: product.productMaster.faq[c]?.answer ?? ''),
      DetailListProductMaster(),
    ]);
  }
}

class _Item extends StatefulWidget {
  final String question;
  final String answer;
  const _Item({@required this.question, @required this.answer});

  @override
  __ItemState createState() => __ItemState();
}

class __ItemState extends State<_Item> {
  bool _isExpand = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: DecoratedBox(
        decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colors.grey[300])),
        child: Column(
          children: [
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                setState(() {
                  _isExpand = !_isExpand;
                });
              },
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                        padding:
                            const EdgeInsets.only(left: 8, bottom: 8, top: 8),
                        child: Text('Hỏi: ${widget.question}')),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: Icon(
                      _isExpand ? Icons.expand_less : Icons.expand_more,
                      color: Colors.grey,
                    ),
                  )
                ],
              ),
            ),
            if (_isExpand)
              SizedBox(
                width: double.infinity,
                height: 1,
                child: ColoredBox(color: Colors.grey[300]),
              ),
            if (_isExpand)
              SizedBox(
                width: double.infinity,
                child: ColoredBox(
                    color: AppColor.primary.withOpacity(0.1),
                    child: Padding(
                        padding: const EdgeInsets.all(8),
                        child: Text('Trả lời:\n${widget.answer}'))),
              )
          ],
        ),
      ),
    );
  }
}
