import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:genie/src/pages/detail_product/controllers/detail_product_controller.dart';
import 'package:genie/src/utils/app_color.dart';

class DetailAdjustAmountPopUp extends StatelessWidget {
  final DetailProductController controller;
  const DetailAdjustAmountPopUp({@required this.controller});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {
            _down(controller);
          },
          child: const DecoratedBox(
            decoration:
                BoxDecoration(color: AppColor.primary, shape: BoxShape.circle),
            child: Icon(
              Icons.remove,
              size: 23,
              color: Colors.white,
            ),
          ),
        ),
        ValueListenableBuilder(
            valueListenable: controller.stateQuantity,
            builder: (_, quantity, __) {
              return SizedBox(
                width: 70,
                child: Text(quantity.toString(),
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontSize: 22)),
              );
            }),
        GestureDetector(
          onTap: () {
            _up(controller);
          },
          child: const DecoratedBox(
            decoration:
                BoxDecoration(color: AppColor.primary, shape: BoxShape.circle),
            child: Icon(
              Icons.add,
              size: 23,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }

  void _down(DetailProductController controller) {
    controller.eventChangeProductQuantity(add: false);
  }
}

void _up(DetailProductController controller) {
  controller.eventChangeProductQuantity();
}
