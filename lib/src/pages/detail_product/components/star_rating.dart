import 'package:flutter/material.dart';

class StarRating extends StatelessWidget {
  final double value;
  final Color color;
  final double size;
  const StarRating(
      {@required this.value, this.color = Colors.amber, this.size = 13});
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _GradientStart(
          value: value,
          color: color,
          pivotValue: 1,
          size: size,
        ),
        _GradientStart(
          value: value,
          color: color,
          pivotValue: 2,
          size: size,
        ),
        _GradientStart(
          value: value,
          color: color,
          pivotValue: 3,
          size: size,
        ),
        _GradientStart(
          value: value,
          pivotValue: 4,
          color: color,
          size: size,
        ),
        _GradientStart(
          value: value,
          pivotValue: 5,
          color: color,
          size: size,
        ),
      ],
    );
  }
}

class _GradientStart extends StatelessWidget {
  final double value;
  final Color color;
  final int pivotValue;
  final double size;
  const _GradientStart(
      {@required this.value, @required this.pivotValue, this.color, this.size});

  @override
  Widget build(BuildContext context) {
    if (value < pivotValue && value > pivotValue - 1)
      return ShaderMask(
        child: Icon(
          Icons.star,
          color: Colors.white,
          size: size,
        ),
        shaderCallback: (Rect bounds) {
          final c = pivotValue > 1 ? 1 - (pivotValue % value) : value;
          final Rect rect = Rect.fromLTRB(0, 0, size, size);
          final gradient = LinearGradient(
              stops: [c, c],
              colors: [color, Colors.grey[300]],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight);
          return gradient.createShader(rect);
        },
      );

    if (value >= pivotValue)
      return Icon(
        Icons.star,
        color: color,
        size: size,
      );

    return Icon(
      Icons.star,
      color: Colors.grey[300],
      size: size,
    );
  }
}
