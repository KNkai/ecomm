import 'package:flutter/material.dart';
import 'package:genie/src/components/app_delay.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/components/list_block/controller/list_block_controller.dart';
import 'package:genie/src/components/list_block/list_block.dart';
import 'package:genie/src/components/refresh_scroll_view.dart';
import 'package:genie/src/utils/util.dart';

class DefaultScreenPage extends StatelessWidget {
  static void push({BuildContext context, String screenId, String title}) {
    bool lock = false;
    if (!lock) {
      lock = true;
      Navigator.of(context).push(MaterialPageRoute(
          builder: (_) => AppDelay(
                starter: AppScaffold(
                  child: SizedBox.shrink(),
                  title: title,
                ),
                child: DefaultScreenPage(
                    title: title,
                    controller: ListBlockController(screenId: screenId)),
              )));
    }
  }

  final ListBlockController controller;
  final String title;

  const DefaultScreenPage(
      {Key key, @required this.controller, @required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    controller.eventLoad();
    return AppScaffold(
        title: title,
        child: RefreshScrollView(
          onRefresh: () async {
            await controller.eventLoad();
            setPopup(context, controller?.screen, force: true);
          },
          child: ChildColumnListBlock(
              mode: ListBlockMode.alone, controller: controller),
        ));
  }
}
