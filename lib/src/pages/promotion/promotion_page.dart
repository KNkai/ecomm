import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/utils/util.dart';

import '../../components/app_scaffold.dart';
import '../../components/list_block/controller/promotion_block_controller.dart';
import '../../components/list_block/list_block.dart';
import '../../components/product_stack/product_stack_controller.dart';
import '../../components/refresh_scroll_view.dart';

class PromotionPage extends StatefulWidget {
  const PromotionPage({Key key}) : super(key: key);
  static void push(BuildContext context, VoidCallback reFresh) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => const PromotionPage()));
  }

  @override
  _PromotionPageState createState() => _PromotionPageState();
}

class _PromotionPageState extends State<PromotionPage>
    with AutomaticKeepAliveClientMixin {
  final _listBlockCtrl = PromotionBlockController();
  final productStackCtrl = ProductStackController();

  @override
  void initState() {
    super.initState();
    productStackCtrl.evenInit();
    _loadListBlockCtrl();
  }

  void _loadListBlockCtrl() async {
    await _listBlockCtrl.eventLoad();
    setPopup(context, _listBlockCtrl?.screen);
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return AppScaffold(
        showBackBtn: false,
        showContact: true,
        title: 'Ưu Đãi',
        child: RefreshScrollView(
          onRefresh: () async {
            await _listBlockCtrl.eventLoad();
            productStackCtrl.evenInit();
            setPopup(context, _listBlockCtrl?.screen, force: true);
          },
          onLoadMore: () async {
            productStackCtrl.eventLoadMore();
          },
          child: Padding(
            padding: EdgeInsets.only(bottom: 30),
            child: ChildColumnListBlock(
              mode: ListBlockMode.alone,
              controller: _listBlockCtrl,
            ),
          ),
        ));
  }
}
