import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/models/province_district_ward.model.dart';
import 'package:genie/src/pages/delivery_address/component/item_create_delivery_address.dart';
import 'package:genie/src/repositories/province_district_ward.repo.dart';

class AddressVn extends StatefulWidget {
  final ValueNotifier<AddressVnValues> controller;
  final String defaultDetail;
  final void Function(String detail, Province p, District d, Ward w) onData;
  final bool hasAddressText;

  const AddressVn(
      {this.defaultDetail,
      this.controller,
      this.hasAddressText = true,
      @required this.onData});

  @override
  _AddressVnState createState() => _AddressVnState();
}

class _AddressVnState extends State<AddressVn> {
  _Controller _controller;
  TextEditingController _ctl;
  @override
  void initState() {
    _controller = _Controller(
        widget?.controller?.value?.province,
        widget?.controller?.value?.district,
        widget?.controller?.value?.ward,
        widget?.controller?.value?.address);
    _controller.eventInit();
    _ctl = TextEditingController(text: widget.defaultDetail);
    widget.controller?.addListener(_valuesListeners);
    _controller.stateSide.stream.listen((onData) {
      if (onData is _StateSideAlert) {
        showWarningDialog(context, onData.msg, 'OK', title: 'Thông báo');
      } else if (onData is _StateSideOpenList) {
        _showDialogl1(onData.type, onData.list);
      } else if (onData is _StateSideCallbackData) {
        widget.onData(
            onData.detail, onData.province, onData.district, onData.ward);
      } else if (onData is _StateSideUpdateDetailView) {
        _ctl.text = onData.detail;
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    widget.controller?.removeListener(_valuesListeners);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: _controller.stateLoading,
      builder: (_, ss, ___) {
        if (ss) return const LinearProgressIndicator();

        return Column(
          children: <Widget>[
            if (widget.hasAddressText)
              ItemCreateDeliveryAddress(
                  isPadding: false,
                  first: 'Địa chỉ',
                  second: TextFormField(
                    controller: _ctl,
                    onChanged: (value) {
                      _controller.eventUpdateDetail(value);
                    },
                    decoration: const InputDecoration(border: InputBorder.none),
                  )),
            ItemCreateDeliveryAddress(
                arrow: true,
                first: 'Thành Phố',
                onTap: () {
                  _controller.eventOpenProvines();
                },
                second: ValueListenableBuilder<Province>(
                    valueListenable: _controller.stateSelectProvine,
                    builder: (_, ss, __) {
                      if (ss?.name == null) return const Text('Chọn Thành Phố');
                      return Text(ss.name);
                    })),
            ItemCreateDeliveryAddress(
                first: 'Quận/ Huyện',
                arrow: true,
                onTap: () {
                  _controller.eventOpenDistricts();
                },
                second: ValueListenableBuilder<District>(
                    valueListenable: _controller.stateSelectDistrict,
                    builder: (_, ss, __) {
                      if (ss?.name == null)
                        return const Text('Chọn Quận/ Huyện');
                      return Text(ss.name);
                    })),
            ItemCreateDeliveryAddress(
                arrow: true,
                first: 'Phường/ Xã',
                onTap: () {
                  _controller.eventOpenWards();
                },
                second: ValueListenableBuilder<Ward>(
                    valueListenable: _controller.stateSelectWard,
                    builder: (_, ss, __) {
                      if (ss?.name == null)
                        return const Text('Chọn Phường/ Xã');
                      return Text(ss.name);
                    }))
          ],
        );
      },
    );
  }

  String _textType(OpenType type) {
    switch (type) {
      case OpenType.provine:
        return 'Chọn Thành Phố';
      case OpenType.district:
        return 'Chọn Quận/ Huyện';
      case OpenType.ward:
        return 'Chọn Phường/ Xã';
    }

    return '';
  }

  void _valuesListeners() {
    final fullAddress = widget.controller.value;
    _controller.eventInitAgain(fullAddress.province, fullAddress.district,
        fullAddress.ward, fullAddress.address);
  }

  void _showDialogl1(OpenType type, List<dynamic> list) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        // return alert dialog object
        return AlertDialog(
          title: new Text(_textType(type)),
          content: SizedBox(
            height: MediaQuery.of(context).size.height * 0.7,
            width: MediaQuery.of(context).size.width * 0.7,
            child: ListView.builder(
                itemCount: list.length,
                itemBuilder: (_, index) {
                  return ListTile(
                    onTap: () {
                      if (type == OpenType.provine) {
                        _controller.eventSelectProvine(list[index]);
                      } else if (type == OpenType.district) {
                        _controller.eventSelectDistrict(list[index]);
                      } else {
                        _controller.eventSelectWard(list[index]);
                      }
                      Navigator.pop(context);
                    },
                    title: Text(
                      list[index].name,
                    ),
                  );
                }),
          ),
        );
      },
    );
  }

  void showWarningDialog(
      BuildContext context, String content, String cancelText,
      {String title,
      Function onCloseDialog,
      String onActionText,
      Function onTapAction}) {
    Widget alert;

    Widget _titleWidget({TextAlign textAlign = TextAlign.start}) =>
        title != null
            ? Text(
                title,
                textAlign: textAlign,
              )
            : null;
    if (content != null && content.isNotEmpty) {
      if (Platform.isAndroid) {
        // If it has plenty of buttons, it will stack them on vertical way.
        // If title isn't supplied, height of this alert will smaller than the one has title.
        alert = AlertDialog(
          title: _titleWidget(),
          content: Text(
            content,
            textAlign: TextAlign.start,
          ),
          actions: [
            FlatButton(
              child: Text(cancelText),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            onActionText != null && onTapAction != null
                ? FlatButton(
                    child: Text(onActionText),
                    onPressed: () {
                      onTapAction();
                    },
                  )
                : const SizedBox.shrink()
          ],
        );
      } else {
        // Almost similiar with Cupertino style.
        // If title isn't supplied, height of this alert will smaller than the one has title.
        var listAction = <Widget>[];
        listAction.add(CupertinoDialogAction(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            cancelText,
          ),
        ));
        if (onActionText != null && onTapAction != null)
          listAction.add(CupertinoDialogAction(
            onPressed: () {
              onTapAction();
            },
            child: Text(
              onActionText,
            ),
          ));

        alert = CupertinoAlertDialog(
          title: Padding(
            padding: const EdgeInsets.only(
              bottom: 10,
            ),
            child: _titleWidget(textAlign: TextAlign.center),
          ),
          content: Text(
            content,
            textAlign: TextAlign.start,
          ),
          actions: listAction,
        );
      }

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      ).then((value) {
        if (onCloseDialog != null) {
          onCloseDialog();
        }
      });
    }
  }
}

class _Controller {
  final stateLoading = ValueNotifier<bool>(false);
  final stateNumber = ValueNotifier<String>('');
  final stateSide = StreamController<_StateSide>();
  final ValueNotifier<Province> stateSelectProvine;
  final ValueNotifier<District> stateSelectDistrict;
  final ValueNotifier<Ward> stateSelectWard;
  final _repo = ProvinceDistrictWardRepository();

  List<Province> _listProvince;
  List<District> _listDistricts;
  List<Ward> _listWard;
  String _detail;

  _Controller(Province defaultProvince, District defaultDistrict,
      Ward defaultWard, String detail)
      : this.stateSelectProvine = ValueNotifier<Province>(defaultProvince),
        this.stateSelectDistrict = ValueNotifier<District>(defaultDistrict),
        this._detail = detail,
        this.stateSelectWard = ValueNotifier<Ward>(defaultWard);

  void eventInitAgain(Province defaultProvince, District defaultDistrict,
      Ward defaultWard, String detail) {
    stateSelectProvine.value = defaultProvince;
    stateSelectDistrict.value = defaultDistrict;
    stateSelectWard.value = defaultWard;
    stateSide.add(_StateSideUpdateDetailView(detail));
    _init();
  }

  void eventInit() async {
    stateSide.add(_StateSideUpdateDetailView(_detail));
    _init();
  }

  void eventUpdateDetail(String detail) {
    _detail = detail;
    _callbackData();
  }

  void eventOpenProvines() {
    _openList(_listProvince, OpenType.provine);
  }

  void eventOpenDistricts() {
    _openList(_listDistricts, OpenType.district);
  }

  void eventOpenWards() {
    _openList(_listWard, OpenType.ward);
  }

  void _callbackData() {
    stateSide.add(_StateSideCallbackData(_detail, stateSelectProvine.value,
        stateSelectDistrict.value, stateSelectWard.value));
  }

  void _init() async {
    stateLoading.value = true;
    try {
      final provinces = await _repo.getAllProvince();
      _listProvince = (provinces);

      if (stateSelectProvine.value != null) {
        for (int c = 0; c < provinces.length; c++) {
          if (provinces[c].name == stateSelectProvine.value.name) {
            _listDistricts = await _repo.getAllDistrict(provinces[c].id);
            break;
          }
        }
      }

      if (stateSelectDistrict.value?.name != null &&
          _listDistricts != null &&
          _listDistricts.isNotEmpty) {
        for (int c = 0; c < _listDistricts.length; c++) {
          if (_listDistricts[c].name == stateSelectDistrict.value.name) {
            _listWard = await _repo.getAllWard(_listDistricts[c].id);
            break;
          }
        }
      }
    } finally {
      stateLoading.value = false;
    }
  }

  void _openList(List<dynamic> list, OpenType type) {
    if (list == null || list.isEmpty) {
      if (type == OpenType.provine)
        stateSide.add(_StateSideAlert('Dữ liệu trống'));
      else if (type == OpenType.district)
        stateSide.add(_StateSideAlert('Hãy chọn tỉnh trước'));
      else
        stateSide.add(_StateSideAlert('Hãy chọn quận trước'));
    } else {
      stateSide.add(_StateSideOpenList(list, type));
    }
  }

  void eventSelectProvine(Province province) {
    stateSelectProvine.value = province;

    _listDistricts = null;
    _listWard = null;
    stateSelectDistrict.value = null;
    stateSelectWard.value = null;
    _callbackData();

    _repo.getAllDistrict(province.id).then((onValue) {
      _listDistricts = (onValue);
    });
  }

  void eventSelectDistrict(District district) {
    stateSelectDistrict.value = district;
    _listWard = null;
    stateSelectWard.value = null;
    _callbackData();

    _repo.getAllWard(district.id).then((onValue) {
      _listWard = (onValue);
    });
  }

  void eventSelectWard(Ward ward) {
    stateSelectWard.value = ward;
    _callbackData();
  }

  dispose() {
    stateSelectProvine.dispose();
    stateSide.close();
    stateLoading.dispose();
    stateNumber.dispose();
  }
}

enum OpenType { provine, district, ward }

class _StateSide {}

class _StateSideAlert extends _StateSide {
  final String msg;
  _StateSideAlert(this.msg);
}

class _StateSideOpenList extends _StateSide {
  final List<dynamic> list;
  final OpenType type;
  _StateSideOpenList(this.list, this.type);
}

class _StateSideUpdateDetailView extends _StateSide {
  final String detail;
  _StateSideUpdateDetailView(this.detail);
}

class _StateSideCallbackData extends _StateSide {
  final String detail;
  final Province province;
  final District district;
  final Ward ward;
  _StateSideCallbackData(this.detail, this.province, this.district, this.ward);
}

class AddressVnValues {
  final String address;
  final Province province;
  final District district;
  final Ward ward;
  AddressVnValues(this.address, this.province, this.district, this.ward);
}
