import 'package:flutter/material.dart';

class ItemCreateDeliveryAddress extends StatelessWidget {
  final String first;
  final Widget second;
  final bool arrow;
  final bool isPadding;
  final VoidCallback onTap;
  const ItemCreateDeliveryAddress(
      {@required this.first,
      @required this.second,
      this.arrow = false,
      this.onTap,
      this.isPadding = true});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) onTap();
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 19,
          ),
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 26),
              child: Text(
                first,
                style: TextStyle(fontSize: 12, color: Colors.grey[400]),
              )),
          if (isPadding)
            const SizedBox(
              height: 7,
            ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 26),
            child: Row(children: [
              Expanded(child: second),
              if (arrow)
                Icon(
                  Icons.keyboard_arrow_down,
                  color: Colors.grey[400],
                )
            ]),
          ),
          if (isPadding)
            const SizedBox(
              height: 9,
            ),
          SizedBox(
            width: double.infinity,
            height: .8,
            child: ColoredBox(
              color: Colors.grey[300],
            ),
          ),
        ],
      ),
    );
  }
}
