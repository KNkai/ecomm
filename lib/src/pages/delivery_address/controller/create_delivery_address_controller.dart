import 'dart:async';

import 'package:flutter/material.dart';
import 'package:genie/src/controllers/auth_controller.dart';
import 'package:genie/src/models/province_district_ward.model.dart';
import 'package:genie/src/repositories/delivery_address.repo.dart';
import 'package:genie/src/utils/util.dart';
import 'package:provider/provider.dart';

class CreateDeliveryAddressController {
  final stateSide = StreamController<StateSideCreateDelivery>();
  final stateSelectDefault = ValueNotifier(false);

  Province _province;
  District _district;
  Ward _ward;
  String _detail;

  final _repo = DeliveryAddressRepository();

  void eventUpdateFullAddress(
      String detail, Province province, District district, Ward ward) {
    _province = province;
    _district = district;
    _ward = ward;
    _detail = detail;
  }

  void eventChangeMakeDefault() {
    stateSelectDefault.value = !stateSelectDefault.value;
  }

  void eventCreate(String name, String phone) {
    if (appCheckEmptyString(name)) {
      stateSide.add(StateSideShowMsg(msg: 'Tên không được bỏ trống'));
    } else if (appCheckEmptyString(phone)) {
      stateSide.add(StateSideShowMsg(msg: 'Số điện thoại không được bỏ trống'));
    } else if (!checkPhoneNumberFormat(phone)) {
      stateSide.add(StateSideShowMsg(msg: 'Số điện thoại không hợp lệ'));
    } else if (appCheckEmptyString(_detail)) {
      stateSide.add(StateSideShowMsg(msg: 'Địa chỉ không được bỏ trống'));
    } else if (_province == null) {
      stateSide.add(StateSideShowMsg(msg: 'Xin hãy chọn Thành Phố'));
    } else if (_district == null) {
      stateSide.add(StateSideShowMsg(msg: 'Xin hãy chọn Quận/ Huyện'));
    } else if (_ward == null) {
      stateSide.add(StateSideShowMsg(msg: 'Xin hãy chọn Phường/ Xã'));
    } else {
      _repo
          .createDeliveryAddress(
              name: name,
              phone: phone,
              address: _detail,
              provinceId: _province.id,
              districtId: _district.id,
              wardId: _ward.id,
              isDefault: stateSelectDefault.value)
          .then((value) {
        if (value)
          stateSide.add(StateSidePop());
        else
          stateSide
              .add(StateSideShowMsg(msg: 'Có lổi xảy ra vui lòng thử lại'));
      });
    }
  }

  void eventCreatedSetupProfile(BuildContext context) {
    var authCtrl = Provider.of<AuthController>(context, listen: false);
    if (appCheckEmptyString(_detail)) {
      stateSide.add(StateSideShowMsg(msg: 'Địa chỉ không được bỏ trống'));
    } else if (_province == null) {
      stateSide.add(StateSideShowMsg(msg: 'Xin hãy chọn Thành Phố'));
    } else if (_district == null) {
      stateSide.add(StateSideShowMsg(msg: 'Xin hãy chọn Quận/ Huyện'));
    } else if (_ward == null) {
      stateSide.add(StateSideShowMsg(msg: 'Xin hãy chọn Phường/ Xã'));
    } else {
      authCtrl.updateCustomerProfile({
        'address': _detail,
        'provinceId': _province?.id,
        'districtId': _district?.id,
        'wardId': _ward?.id,
      });
      authCtrl.getCustomer();
      Navigator.of(context).pop();
    }
  }
}

abstract class StateSideCreateDelivery {}

class StateSideShowMsg extends StateSideCreateDelivery {
  final String msg;
  StateSideShowMsg({@required this.msg});
}

class StateSidePop extends StateSideCreateDelivery {}
