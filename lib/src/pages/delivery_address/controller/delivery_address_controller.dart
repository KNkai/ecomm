import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:genie/src/models/delivery_address.model.dart';
import 'package:genie/src/repositories/delivery_address.repo.dart';

class DeliveryAddressController {
  final _repo = DeliveryAddressRepository();
  final stateDelivery = StreamController<StateDelivery>();
  final stateSelectDefault = ValueNotifier(-1);

  List<DeliveryAddress> _data;

  void eventInit() {
    _repo.getAllDeliveryAddress().then((value) {
      if (value != null) {
        _data = value;
        int c = 0;
        for (DeliveryAddress address in _data) {
          if (address.isDefault) {
            stateSelectDefault.value = c;
            break;
          }
          c++;
        }
        stateDelivery.add(StateDeliverySuccess(data: value));
      } else
        stateDelivery.add(StateDeliveryFailed());
    });
  }

  void eventDeleteAddress(int index) {
    final one = _data[index];
    _repo.deleteOne(id: one.id);
    _data.removeAt(index);

    if (index == stateSelectDefault.value) stateSelectDefault.value = -1;

    stateDelivery.add(StateDeliverySuccess(data: _data));
  }

  void eventSelectDefault(int index) {
    stateSelectDefault.value = index;
  }

  Future<void> eventAgreeNewDefault() async {
    if (stateSelectDefault.value != -1) {
      await _repo.udpate(
          id: _data[stateSelectDefault.value].id,
          variables: {'isDefault': true});
    }
  }
}

abstract class StateDelivery {}

class StateDeliveryFailed extends StateDelivery {}

class StateDeliverySuccess extends StateDelivery {
  final List<DeliveryAddress> data;
  StateDeliverySuccess({@required this.data});
}
