import 'package:flutter/material.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/components/app_checkbox.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/components/bottom_action_container.dart';
import 'package:genie/src/pages/delivery_address/controller/create_delivery_address_controller.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';

import 'component/address_vn.dart';
import 'component/item_create_delivery_address.dart';

class CreateDeliveryAddreess extends StatelessWidget {
  static void push(BuildContext context, {@required VoidCallback onRefresh}) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => CreateDeliveryAddreess()))
        .then((value) {
      if (value != null && value) onRefresh();
    });
  }

  final _controllerName = TextEditingController();
  final _controllerPhone = TextEditingController();
  final _controller = CreateDeliveryAddressController();

  @override
  Widget build(BuildContext context) {
    _controller.stateSide.stream.listen((event) {
      if (event is StateSideShowMsg)
        WarningDialog.show(context, event.msg, 'OK', title: 'Thông báo');
      else
        Navigator.of(context).pop(true);
    });

    return AppScaffold(
        showCartBtn: false,
        title: 'Tạo địa chỉ giao hàng',
        child: BottomActionContainer(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 16,
                ),
                SizedBox(
                  width: double.infinity,
                  height: .8,
                  child: ColoredBox(
                    color: Colors.grey[300],
                  ),
                ),
                ItemCreateDeliveryAddress(
                  first: 'Họ và Tên',
                  isPadding: false,
                  second: TextField(
                    controller: _controllerName,
                    scrollPadding: EdgeInsets.zero,
                    minLines: 1,
                    maxLines: 1,
                    cursorColor: Colors.grey,
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.zero,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                ItemCreateDeliveryAddress(
                  first: 'Số điện thoai',
                  isPadding: false,
                  second: TextField(
                    controller: _controllerPhone,
                    scrollPadding: EdgeInsets.zero,
                    minLines: 1,
                    maxLines: 1,
                    keyboardType: TextInputType.number,
                    cursorColor: Colors.grey,
                    decoration: const InputDecoration(
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                AddressVn(
                  onData: (detail, p, d, w) {
                    _controller.eventUpdateFullAddress(detail, p, d, w);
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 26,
                    ),
                    child: ValueListenableBuilder(
                      valueListenable: _controller.stateSelectDefault,
                      builder: (_, select, child) => AppCheckBox(
                        onTap: () {
                          _controller.eventChangeMakeDefault();
                        },
                        select: select,
                        disable: false,
                        child: child,
                      ),
                      child: const Text(
                        'Chọn làm địa chỉ mặc định',
                        style: TextStyle(color: AppColor.primary),
                      ),
                    )),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
          bottomChild: AppBtn(
              title: 'Tạo mới',
              onTap: () {
                _controller.eventCreate(
                    _controllerName.text, _controllerPhone.text);
              }),
        ));
  }
}
