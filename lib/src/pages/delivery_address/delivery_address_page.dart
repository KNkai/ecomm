import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/components/app_checkbox.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/components/bottom_action_container.dart';
import 'package:genie/src/pages/delivery_address/controller/delivery_address_controller.dart';
import 'package:genie/src/pages/delivery_address/create_delivery_address_page.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/app_fontSize.dart';
import 'package:genie/src/utils/util.dart';

class DeliveryAddressPage extends StatelessWidget {
  static void push(BuildContext context, {@required VoidCallback onRefresh}) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => DeliveryAddressPage()))
        .then((value) {
      onRefresh();
    });
  }

  final _controller = DeliveryAddressController();

  @override
  Widget build(BuildContext context) {
    _controller.eventInit();

    return AppScaffold(
        title: 'Địa chỉ giao hàng',
        showCartBtn: false,
        child: BottomActionContainer(
            child: StreamBuilder<StateDelivery>(
              stream: _controller.stateDelivery.stream,
              builder: (_, ss) {
                if (ss?.data == null)
                  return ConstrainedBox(
                    constraints: const BoxConstraints.expand(),
                    child: const Center(
                      child: CupertinoActivityIndicator(),
                    ),
                  );

                if (ss.data is StateDeliveryFailed)
                  return ConstrainedBox(
                    constraints: const BoxConstraints.expand(),
                    child: const Center(
                      child: Text('Có lỗi xảy ra'),
                    ),
                  );

                final StateDeliverySuccess stateLoaded = ss.data;

                return ValueListenableBuilder(
                  valueListenable: _controller.stateSelectDefault,
                  builder: (_, selectIndex, __) {
                    return ListView.separated(
                        separatorBuilder: (_, __) {
                          return SizedBox(
                            height: 4,
                            width: double.infinity,
                            child: ColoredBox(
                              color: Colors.grey[100],
                            ),
                          );
                        },
                        itemCount: stateLoaded.data.length + 1,
                        itemBuilder: (_, index) {
                          if (index == stateLoaded.data.length)
                            return GestureDetector(
                              behavior: HitTestBehavior.opaque,
                              onTap: () {
                                CreateDeliveryAddreess.push(context,
                                    onRefresh: () {
                                  _controller.eventInit();
                                });
                              },
                              child: const Padding(
                                padding: EdgeInsets.symmetric(vertical: 23),
                                child: Text(
                                  'Tạo địa chỉ mới',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: AppColor.primary),
                                ),
                              ),
                            );

                          final address = stateLoaded.data[index];
                          return Padding(
                            padding: const EdgeInsets.only(
                                left: 26, top: 15, bottom: 15, right: 10),
                            child: AppCheckBox(
                                onTap: () {
                                  _controller.eventSelectDefault(index);
                                },
                                select: index == selectIndex,
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(address.name ?? '',
                                              style: const TextStyle(
                                                  color: AppColor.primary)),
                                          const SizedBox(height: 9),
                                          Text(address.phone ?? ''),
                                          const SizedBox(height: 7),
                                          Text(
                                            appFullAddress(
                                                address: address.address,
                                                ward: address.ward,
                                                district: address.district,
                                                province: address.province),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) =>
                                                _showAlertDialog(
                                                    context: context,
                                                    func: () {
                                                      _controller
                                                          .eventDeleteAddress(
                                                              index);
                                                    }));
                                      },
                                      child: Icon(
                                        Icons.delete,
                                        size: 20,
                                        color: Colors.grey[300],
                                      ),
                                    )
                                  ],
                                ),
                                disable: false),
                          );
                        });
                  },
                );
              },
            ),
            bottomChild: AppBtn(
              title: 'Giao đến địa chỉ này',
              onTap: () async {
                await _controller.eventAgreeNewDefault();
                Navigator.of(context).pop();
              },
            )));
  }

  _showAlertDialog({BuildContext context, Function func}) {
    return AlertDialog(
      contentPadding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Center(
              child: Image.asset(
                AppAsset.trash,
                height: 87,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: const Center(
                child: Text(
              "Xóa địa chỉ",
              style: TextStyle(fontSize: titleSize + 5),
            )),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: AppBtn(
                  onTap: () {
                    func();
                    Navigator.of(context).pop();
                  },
                  title: "Xóa",
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: AppBtn(
                  borderColor: AppColor.primary,
                  color: Colors.white,
                  textColor: Colors.black,
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  title: "Hủy",
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
