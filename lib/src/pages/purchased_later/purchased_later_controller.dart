import 'package:flutter/cupertino.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/repositories/purchased_later.repo.dart';

class PurchasedLaterController
    extends InitLoadMoreSearchController<PurchasedLaterProduct> {
  PurchasedLaterController(
      {@required InitListProtocol initProtocol,
      @required LoadMoreProtocol loadMoreProtocol})
      : super(initProtocol: initProtocol, loadMoreProtocol: loadMoreProtocol);

  PurchasedLaterRepository purchaseLaterRepository = PurchasedLaterRepository();
  delPurchasedLaterProd(String id) {
    purchaseLaterRepository.deleteOnePurchasedLaterProd(id);
  }
}
