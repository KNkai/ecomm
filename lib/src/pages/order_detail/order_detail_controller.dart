import 'package:flutter/material.dart';
import 'package:genie/src/models/order.model.dart';
import 'package:genie/src/repositories/order.repo.dart';
import 'package:genie/src/utils/util.dart';
import 'package:momo_vn/momo_vn.dart';

class OrderDetailController {
  final stateOrder = ValueNotifier<StateOrderDetail>(StateOrderDetailLoading());

  final _repo = OrderRepository();

  Order _orderCurrent;
  BuildContext _currentContext;

  // void eventInit(String id, {bool addLoading = false}) {
  //   if (addLoading) stateOrder.value = StateOrderDetailLoading();
  //   _repo.getOrderDetail(id).then((result) {
  //     if (result != null && result is Order) {
  //       stateOrder.value = StateOrderDetailSuccess(order: result);
  //     } else
  //       stateOrder.value = StateOrderDetailFailed();
  //   });
  // }

  void eventInit(String id, {bool addLoading = false, BuildContext context}) {
    _currentContext = context;
    if (addLoading) stateOrder.value = StateOrderDetailLoading();
    _repo.getOrderDetail(id).then((result) {
      if (result != null && result is Order) {
        stateOrder.value = StateOrderDetailSuccess(order: result);
        _orderCurrent = result;
      } else
        stateOrder.value = StateOrderDetailFailed();
    });
  }

  updateRate({int rate, String id, String rateCmt}) async {
    return await _repo
        .rateOrder(id: id, rate: rate, rateComt: rateCmt)
        .then((value) => eventInit(
              id,
              addLoading: true,
            ));
  }

  cancelOrderPending(String id) {
    _repo.cancelOrder(id);
  }

  MomoVn _momoPay;

  void _handlePaymentSuccess(PaymentResponse response) {
    _repo
        .payMomo(_orderCurrent.id, response.token, response.phonenumber)
        .then((value) {
      if (value != null) {
        showLoaderDialog(
            _currentContext, _orderCurrent.id, "Thanh toán thành công",
            isPushNew: false);
        eventInit(_orderCurrent.id, context: _currentContext);
      } else {
        showLoaderDialog(
            _currentContext, _orderCurrent.id, "Thanh toán thất bại",
            isPushNew: false);
      }
      return value;
    }).catchError((e) {
      print(e);
      return null;
    });
  }

  void _handlePaymentError(PaymentResponse response) {
    showLoaderDialog(_currentContext, _orderCurrent.id,
        "Thanh toán thất bại vui lòng đợi...",
        isPushNew: false);
    eventInit(_orderCurrent.id, context: _currentContext);
  }

  void paymentMomo() {
    _momoPay = MomoVn();
    _momoPay.on(MomoVn.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _momoPay.on(MomoVn.EVENT_PAYMENT_ERROR, _handlePaymentError);
    if (_orderCurrent != null) {
      MomoPaymentInfo options = MomoPaymentInfo(
        merchantname: _orderCurrent.paymentInfo.merchantnamelabel,
        appScheme: _orderCurrent.paymentInfo.appScheme,
        merchantcode: _orderCurrent.paymentInfo.merchantcode,
        amount: _orderCurrent.paymentInfo.amount,
        orderId: _orderCurrent.paymentInfo.orderId,
        orderLabel: _orderCurrent.paymentInfo.orderLabel,
        merchantnamelabel: _orderCurrent.paymentInfo.merchantnamelabel,
        fee: _orderCurrent.paymentInfo.fee,
        description: _orderCurrent.paymentInfo.description,
        username: _orderCurrent.paymentInfo.username,
        partner: _orderCurrent.paymentInfo.partner,
        extra: _orderCurrent.paymentInfo.extra,
        isTestMode: _orderCurrent.paymentInfo.isTestMode,
      );
      try {
        _momoPay.open(options);
      } catch (e) {
        debugPrint(e);
      }
    }
  }
}

abstract class StateOrderDetail {}

class StateOrderDetailSuccess extends StateOrderDetail {
  final Order order;
  StateOrderDetailSuccess({this.order});
}

class StateOrderDetailFailed extends StateOrderDetail {}

class StateOrderDetailLoading extends StateOrderDetail {}
