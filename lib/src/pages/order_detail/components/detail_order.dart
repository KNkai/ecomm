import 'package:flutter/material.dart';
import 'package:genie/src/components/app_grey.dart';
import 'package:genie/src/components/final_info_item.dart';
import 'package:genie/src/models/discount_log.model.dart';
import 'package:genie/src/models/order.model.dart';
import 'package:genie/src/pages/order_history/components/item_product_order.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';

class DetailOrder extends StatelessWidget {
  final Order order;

  const DetailOrder({Key key, @required this.order}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        AppGray(
          left: "ID Đơn hàng",
          right: order?.code,
          rightStyle: const TextStyle(fontSize: 17, color: AppColor.primary),
        ),
        ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: order.rawItems.length,
            itemBuilder: (_, index) =>
                ItemProductOrder(orderItem: order.rawItems[index])),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 26, vertical: 16),
            child: Column(
              children: [
                FinalInfoItem(
                    color: Colors.grey,
                    title: 'Tổng tiền hàng',
                    value: appCurrency(order?.subtotal)),
                FinalInfoItem(
                  color: Colors.grey,
                  title: 'Phí vận chuyển',
                  value: appCurrency(order?.shipfee),
                ),
                FinalInfoItem(
                  color: Colors.grey,
                  title: 'Điểm thưởng',
                  value: "${order?.rewardPoint}",
                ),
                FinalInfoItem(
                  color: Colors.grey,
                  title: 'Điểm tích lũy',
                  value: "${order?.cumulativePoint}",
                ),
                if (order?.discountLogs != null)
                  ...order.discountLogs
                      .where((element) =>
                          element.type == DiscountLogType.shipFee ||
                          element.type == DiscountLogType.bonusPoint ||
                          element.type == DiscountLogType.subTotal)
                      .map((e) => FinalInfoItem(
                            color: Colors.grey,
                            title: 'Voucher ${e.type.title}',
                            minus: e.type == DiscountLogType.bonusPoint
                                ? false
                                : true,
                            value: e.type == DiscountLogType.bonusPoint
                                ? e.value == null
                                    ? ''
                                    : e.value.toString()
                                : appCurrency(e.value),
                          )),
              ],
            )),
        AppGray(
          left: "Thanh Toán",
          right: appCurrency((order.amount)),
          rightStyle: const TextStyle(color: AppColor.primary, fontSize: 17),
          barColor: AppColor.primary.withOpacity(0.3),
        ),
      ],
    );
  }
}
