import 'package:flutter/material.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/pages/order_history/order_history_page.dart';

import '../order_detail_controller.dart';

class BottomDialogCancel extends StatelessWidget {
  final String id;
  final BuildContext orderContext;
  const BottomDialogCancel({
    Key key,
    this.id,
    this.orderContext,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _controller = OrderDetailController();
    return Container(
      padding: EdgeInsets.all(20),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Text('Xác Nhận hủy đơn hàng'.toUpperCase()),
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: AppBtn(
                  onTap: () {
                    _controller.cancelOrderPending(id);
                    OrderHistoryPage.pushNew(context: context);
                    showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        content: Container(
                          child: Text('Đã hủy đơn hàng thành công'),
                        ),
                      ),
                    );
                  },
                  title: 'Xác nhận'),
            ),
          ],
        ),
      ),
    );
  }
}
