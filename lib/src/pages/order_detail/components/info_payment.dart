import 'package:flutter/material.dart';
import 'package:genie/src/services/app_client.dart';
import 'package:genie/src/utils/app_color.dart';

class InfoPayment extends StatefulWidget {
  @override
  _InfoPaymentState createState() => _InfoPaymentState();
}

class _InfoPaymentState extends State<InfoPayment> {
  bool _isExpand = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 26, right: 26, bottom: 20),
      width: double.infinity,
      child: Column(
        children: [
          GestureDetector(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  width: 1,
                  color: AppColor.primary,
                  style: BorderStyle.solid,
                ),
                borderRadius: _isExpand
                    ? const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      )
                    : BorderRadius.circular(10),
              ),
              child: Row(
                children: [
                  const Expanded(
                    child: Icon(
                      Icons.credit_card,
                      color: AppColor.primary,
                    ),
                    flex: 1,
                  ),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8, vertical: 17),
                      child: const Text(
                        "Thông tin chuyển khoản",
                        style: TextStyle(
                            fontWeight: FontWeight.w400, fontSize: 17),
                      ),
                    ),
                    flex: 8,
                  ),
                  Expanded(
                    child: Icon(
                      !_isExpand
                          ? Icons.keyboard_arrow_down
                          : Icons.keyboard_arrow_up,
                      color: Colors.grey,
                    ),
                    flex: 1,
                  ),
                ],
              ),
            ),
            onTap: () {
              setState(() {
                _isExpand ? _isExpand = false : _isExpand = true;
                print(_isExpand);
              });
            },
          ),
          Visibility(
            visible: _isExpand,
            child: Container(
                width: double.infinity,
                padding:
                    const EdgeInsets.symmetric(horizontal: 19, vertical: 10),
                decoration: _isExpand
                    ? BoxDecoration(
                        border: Border.all(
                          width: 1,
                          color: AppColor.primary,
                          style: BorderStyle.solid,
                        ),
                        borderRadius: const BorderRadius.only(
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10),
                        ),
                      )
                    : const BoxDecoration(),
                child: FutureBuilder<Response>(
                  future: AppClient.instance.execute('''
                                                          query {
                                                            getOneSettingByKey(key: "APP_BANK_TRANSFER") {
                                                                name
                                                                key
                                                                value
                                                            }
                                                          }
                                                      '''),
                  builder: (_, snapshot) {
                    if (snapshot?.data == null ||
                        snapshot.data.data['getOneSettingByKey'] == null ||
                        snapshot.data.data['getOneSettingByKey']['value'] ==
                            null) return const SizedBox.shrink();
                    final note =
                        snapshot.data.data['getOneSettingByKey']['value'];
                    return Text(note);
                  },
                )),
          )
        ],
      ),
    );
  }
}
