import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/pages/order_detail/order_detail_controller.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/size_config.dart';
import 'package:intl/intl.dart';

class BottomRatingBar extends StatefulWidget {
  final int rate;
  final DateTime rateAt;
  final String rateCmt;
  final String idOrder;
  final OrderDetailController controller;
  // final VoidCallback onSubmit;
  const BottomRatingBar({
    Key key,
    this.rate,
    this.rateAt,
    this.rateCmt,
    this.controller,
    this.idOrder,
    // this.onSubmit,
  }) : super(key: key);

  @override
  _BottomRatingBarState createState() => _BottomRatingBarState();
}

class _BottomRatingBarState extends State<BottomRatingBar> {
  TextEditingController _ratingController = TextEditingController();
  int _rating;
  DateTime _rateAt;
  @override
  void initState() {
    if (widget.rateAt != null) {
      _rating = widget.rate;
      _rateAt = widget.rateAt;
      _ratingController.text = widget.rateCmt;
    } else {
      _rating = 0;
      _ratingController.text = '';
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.heightDevice / 2,
      child: SingleChildScrollView(
        child: IgnorePointer(
          // ignoring: false,
          ignoring: widget.rateAt != null,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RatingBar.builder(
                  initialRating: _rating.toDouble(),
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: false,
                  itemCount: 5,
                  itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (rating) {
                    _rating = rating.toInt();
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Container(
                  width: double.infinity,
                  child: _rateAt == null || _rateAt == ""
                      ? Text(
                          'Chưa rating',
                          style: TextStyle(color: Colors.grey, fontSize: 11),
                        )
                      : Text(
                          "Ngày đánh giá: ${DateFormat('dd/MM/yyyy').format(_rateAt)}",
                          style: TextStyle(color: Colors.grey, fontSize: 11),
                        ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                child: TextField(
                  scrollPadding: EdgeInsets.zero,
                  maxLines: 1,
                  onSubmitted: (changed) async {
                    await SystemChannels.textInput
                        .invokeMethod('TextInput.hide');
                  },
                  cursorColor: Colors.grey,
                  controller: _ratingController,
                  decoration: InputDecoration(
                    focusedBorder: InputBorder.none,
                    hintText: 'Phản hồi đơn hàng...',
                    hintStyle: TextStyle(fontSize: 13, color: Colors.grey[400]),
                    enabledBorder: InputBorder.none,
                    border: const OutlineInputBorder(),
                  ),
                ),
              ),
              AppBtn(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                title: widget.rateAt == null
                    ? 'Xác nhận'
                    : 'Đơn hàng đã đc đánh giá',
                color: widget.rateAt == null
                    ? AppColor.primary
                    : AppColor.separate,
                onTap: () {
                  widget.controller.updateRate(
                    id: widget.idOrder,
                    rate: _rating,
                    rateCmt: _ratingController.text ?? "",
                  );

                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
