import 'package:flutter/material.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/helper/cart_data.dart';
import 'package:genie/src/models/order.model.dart';
import 'package:genie/src/pages/cart/cart_page.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/size_config.dart';
import 'package:genie/src/utils/util.dart';

class BottomDialogReBuy extends StatefulWidget {
  final Order order;
  const BottomDialogReBuy({
    Key key,
    this.order,
  }) : super(key: key);

  @override
  _BottomDialogReBuyState createState() => _BottomDialogReBuyState();
}

class _BottomDialogReBuyState extends State<BottomDialogReBuy> {
  Order _orderTemp;
  @override
  void initState() {
    _orderTemp = widget.order;
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      constraints: BoxConstraints(maxHeight: SizeConfig.heightDevice * (2 / 3)),
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
      child: SingleChildScrollView(
        child: Column(
          children: [
            for (int i = 0; i < _orderTemp.rawItems.length; i++)
              SizedBox(
                height: 70,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Row(
                    children: [
                      DecoratedBox(
                        decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                            border:
                                Border.all(width: 1, color: Colors.grey[300])),
                        child: Padding(
                          padding: const EdgeInsets.all(1.25),
                          child: ClipRRect(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                            child: _orderTemp.rawItems[i].product.listImage ==
                                        null ||
                                    _orderTemp.rawItems[i].product.listImage
                                        .isEmpty ||
                                    _orderTemp
                                            .rawItems[i].product.listImage[0] ==
                                        null
                                ? const SizedBox.shrink()
                                : Image.network(
                                    _orderTemp.rawItems[i].product.listImage[0],
                                    width: 60,
                                    height: 60),
                          ),
                        ),
                      ),
                      const SizedBox(width: 5),
                      Flexible(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              _orderTemp.rawItems[i].product.name,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 3),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  if (_orderTemp.rawItems[i]?.hasSale != null &&
                                      _orderTemp.rawItems[i].hasSale)
                                    Text(
                                      appCurrency(
                                          _orderTemp.rawItems[i]?.basePrice),
                                      style: const TextStyle(
                                        fontSize: 10,
                                        decoration: TextDecoration.lineThrough,
                                      ),
                                    ),
                                  if (_orderTemp.rawItems[i]?.hasSale != null &&
                                      _orderTemp.rawItems[i].hasSale)
                                    const SizedBox(
                                      width: 10,
                                    ),
                                  Text(
                                    appCurrency(
                                        _orderTemp.rawItems[i].salePrice *
                                            _orderTemp.rawItems[i].quantity),
                                    style: const TextStyle(
                                      color: AppColor.primary,
                                    ),
                                  ),
                                  Spacer(),
                                  _orderTemp.rawItems[i].product.allowSale
                                      ? Row(
                                          children: [
                                            Row(
                                              children: [
                                                GestureDetector(
                                                  behavior:
                                                      HitTestBehavior.opaque,
                                                  onTap: () {
                                                    if (_orderTemp.rawItems[i]
                                                            .quantity >
                                                        1) {
                                                      _orderTemp.rawItems[i]
                                                          .quantity--;
                                                      setState(() {});
                                                    }
                                                  },
                                                  child: const DecoratedBox(
                                                    decoration: BoxDecoration(
                                                        color: AppColor.primary,
                                                        shape: BoxShape.circle),
                                                    child: Icon(
                                                      Icons.remove,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(
                                                    width: 40,
                                                    child: Text(
                                                      "${_orderTemp.rawItems[i].quantity}",
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: const TextStyle(
                                                          fontSize: 15),
                                                    )),
                                                GestureDetector(
                                                  onTap: () {
                                                    _orderTemp
                                                        .rawItems[i].quantity++;
                                                    setState(() {});
                                                  },
                                                  child: const DecoratedBox(
                                                    decoration: BoxDecoration(
                                                        color: AppColor.primary,
                                                        shape: BoxShape.circle),
                                                    child: Icon(
                                                      Icons.add,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                )
                                              ],
                                            )
                                          ],
                                        )
                                      : Text(
                                          "Sản phẩm đã hết",
                                          style: TextStyle(color: Colors.red),
                                        ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  AppBtn(
                      onTap: () {
                        int _count = 0;
                        _orderTemp.rawItems.forEach((e) {
                          if (e.product.allowSale) {
                            _count++;
                            CartData.getCtl(context).eventAddProudct(
                                productId: e.product.id,
                                quantity: e.quantity,
                                note: e.product.note);
                          }
                        });
                        Navigator.of(context).pop();
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            content: Container(
                              child: Text(_count == 0
                                  ? 'Không có sản phẩm nào được thêm vào'
                                  : 'Đã thêm sản phẩm vào giỏ hàng'),
                            ),
                          ),
                        );
                        Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (_) => CartPage()));
                      },
                      title: 'Xác nhận'),
                  AppBtn(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      title: 'Hủy'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
