import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/components/app_grey.dart';
import 'package:genie/src/components/app_navbar.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/components/error_text.dart';
import 'package:genie/src/models/order.model.dart';
import 'package:genie/src/models/order_status.model.dart';
import 'package:genie/src/models/payment_method.model.dart';
import 'package:genie/src/pages/detail_product/components/star_rating.dart';
import 'package:genie/src/pages/order_detail/order_detail_controller.dart';
import 'package:genie/src/pages/profile/controllers/profile_controller.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';
import 'package:provider/provider.dart';

import 'components/bottom_dialog_cancel.dart';
import 'components/bottom_dialog_rating_bar.dart';
import 'components/bottom_dialog_rebuy.dart';
import 'components/detail_order.dart';
import 'components/info_payment.dart';

class OrderDetailPage extends StatelessWidget {
  final String id;
  final bool backNew;
  final _controller = OrderDetailController();

  OrderDetailPage({
    Key key,
    @required this.backNew,
    @required this.id,
  }) : super(key: key);

  static void push(
    BuildContext context, {
    @required String id,
  }) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => OrderDetailPage(
          backNew: false,
          id: id,
        ),
      ),
    );
  }

  // When hit back, the page will route to dashboard
  static void pushNew({@required BuildContext context, @required id}) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (_) => OrderDetailPage(
                  id: id,
                  backNew: true,
                )),
        (_) => false);
  }

  @override
  Widget build(BuildContext context) {
    ProfileController profileController =
        Provider.of<ProfileController>(context);
    _controller.eventInit(id, context: context);
    return AppScaffold(
      title: 'Thông tin đơn hàng',
      showCartBtn: true,
      onCustomonBackTap: () {
        if (backNew) {
          AppNavBar.push(context);
        } else {
          Navigator.of(context).pop();
        }
      },
      child: ValueListenableBuilder<StateOrderDetail>(
        valueListenable: _controller.stateOrder,
        builder: (_, state, loading) {
          if (state is StateOrderDetailLoading) return loading;

          if (state is StateOrderDetailFailed)
            return ErrorText(
                onClick: () => _controller.eventInit(id, addLoading: true));

          final order = (state as StateOrderDetailSuccess).order;

          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const AppGray(
                  left: "Tình trạng đơn hàng",
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 8),
                  child: Text(
                    order.status.title,
                    style: const TextStyle(
                        color: AppColor.primary, fontWeight: FontWeight.w300),
                  ),
                ),
                const AppGray(
                  left: "Trạng thái thanh toán",
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        order.paymentStatus.title,
                        style: TextStyle(
                            color: AppColor.primary,
                            fontWeight: FontWeight.w300),
                      ),
                      order?.paymentInfo?.method == "MOMO" &&
                              order?.paymentStatus?.value == "PENDING"
                          ? ElevatedButton(
                              onPressed: () {
                                _controller.paymentMomo();
                              },
                              style: ElevatedButton.styleFrom(
                                elevation: 5,
                                primary: AppColor.primary,
                              ),
                              child: Text(
                                'Thanh toán lại',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            )
                          : const SizedBox(),
                    ],
                  ),
                ),
                const AppGray(
                  left: "Thông tin giao hàng",
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 26, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        order.customerName ?? '',
                        style: const TextStyle(
                          color: AppColor.primary,
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(order.customerPhone ?? ''),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(appFullAddress(
                        address: order.address,
                        ward: order.ward,
                        district: order.district,
                        province: order.province,
                      )),
                    ],
                  ),
                ),
                const AppGray(
                  left: "Thông tin vận chuyển",
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 12, horizontal: 26),
                  child: Text(
                    "Đơn vị vận chuyển: ${order.shipMethodText}",
                    overflow: TextOverflow.fade,
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  height: 1,
                  child: ColoredBox(
                    color: Colors.grey[300],
                  ),
                ),
                DetailOrder(
                  order: order,
                ),
                AppGray(
                  left: "Phương Thức Thanh Toán",
                  right: order.paymentMethod.title,
                  rightStyle:
                      const TextStyle(color: AppColor.primary, fontSize: 17),
                  barColor: Colors.white,
                ),
                if (order.status == OrderStatus.approve)
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 12, horizontal: 26),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: InkWell(
                            child: StarRating(
                              value: order?.rate?.toDouble() ?? 0,
                              size: 30,
                            ),
                            onTap: () => showModalBottomSheet(
                              // isScrollControlled: true,
                              context: context,
                              builder: (_) => Material(
                                  child: Padding(
                                padding: EdgeInsets.only(
                                    bottom: MediaQuery.of(context)
                                        .viewInsets
                                        .bottom),
                                child: BottomRatingBar(
                                  controller: _controller,
                                  idOrder: order?.id,
                                  rate: order?.rate,
                                  rateAt: order?.rateAt,
                                  rateCmt: order?.rateComment,
                                  // onSubmit: () => _controller
                                  //     .eventInit(order?.id, addLoading: true),
                                ),
                              )),
                            ),
                          ),
                        ),
                        AppBtn(
                            onTap: () => showModalBottomSheet(
                                context: context,
                                builder: (_) {
                                  return BottomDialogReBuy(
                                    order: order,
                                  );
                                }),
                            title: "Mua Lại")
                      ],
                    ),
                  ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 26),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Container(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          decoration: BoxDecoration(
                            border:
                                Border.all(width: 1, color: AppColor.primary),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: FlatButton(
                            onPressed: () {
                              profileController
                                  .chatSetting()
                                  .then((value) => openMessenger(value));
                            },
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    width: 35,
                                    height: 35,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        image: const DecorationImage(
                                          image: AssetImage(AppAsset.chat),
                                          fit: BoxFit.fill,
                                        )),
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                const Expanded(
                                  flex: 3,
                                  child: SizedBox(
                                    child: Text(
                                      'Chat với genie',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 17),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          decoration: BoxDecoration(
                            border:
                                Border.all(width: 1, color: AppColor.primary),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: FlatButton(
                            onPressed: () {
                              profileController
                                  .hotlineSetting()
                                  .then((value) => makePhoneCall("tel:$value"));
                            },
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    width: 35,
                                    height: 35,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        image: const DecorationImage(
                                          image: AssetImage(AppAsset.hotline),
                                          fit: BoxFit.fill,
                                        )),
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                const Expanded(
                                  flex: 3,
                                  child: SizedBox(
                                    child: Text(
                                      'Hotline',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 17),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                InfoPayment(),
                if (order.status == OrderStatus.pending)
                  // AppGray(
                  //   right: "Hủy Đơn",
                  //   rightStyle:
                  //       const TextStyle(color: AppColor.primary, fontSize: 17),
                  //   barColor: Colors.white,
                  //   onRightTap: () {

                  //     );
                  //   },
                  // ),
                  SizedBox(
                    width: double.infinity,
                    child: Center(
                      child: AppBtn(
                        onTap: () {
                          showModalBottomSheet(
                            context: context,
                            builder: (_) => BottomDialogCancel(
                              id: id,
                              orderContext: _,
                            ),
                          );
                        },
                        title: "Hủy Đơn",
                        color: Colors.red,
                        textColor: Colors.white,
                      ),
                    ),
                  ),
                const SizedBox(height: 20),
              ],
            ),
          );
        },
        child: const Padding(
            padding: EdgeInsets.only(top: 40),
            child: Align(
                alignment: Alignment.topCenter,
                child: CupertinoActivityIndicator())),
      ),
    );
  }
}
