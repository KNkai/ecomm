import 'package:flutter/material.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/components/list_block/controller/list_block_controller.dart';
import 'package:genie/src/models/screen.model.dart';
import 'package:genie/src/pages/dashboard/dashboard_page.dart';
import 'package:genie/src/pages/default_screen/default_screen_page.dart';
import 'package:genie/src/pages/promotion/promotion_page.dart';
import 'package:genie/src/pages/review_screen/review_screen_controller.dart';

class ReviewScreenPage extends StatefulWidget {
  static void go(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => ReviewScreenPage()));
  }

  @override
  _ReviewScreenPageState createState() => _ReviewScreenPageState();
}

class _ReviewScreenPageState extends State<ReviewScreenPage> {
  final _controller = ReviewScreenController();
  Widget defaultScreenPage;
  Future _loadScreen;
  @override
  void initState() {
    _loadScreen = _controller.screenRepo.getAllScreen();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      showAppbar: false,
      showContact: false,
      child: Column(
        children: [
          FutureBuilder<List<Screen>>(
              future: _loadScreen,
              builder: (context, snapshot) {
                if (snapshot.data == null) return const SizedBox.shrink();
                defaultScreenPage = new DefaultScreenPage(
                  controller:
                      ListBlockController(screenId: snapshot.data[0].id),
                  title: snapshot.data[0].name,
                );
                return Container(
                  height: 80,
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: List.generate(
                        snapshot.data.length,
                        (i) => Container(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: InkWell(
                            child: Text(snapshot.data[i].name),
                            onTap: () {
                              if (snapshot.data[i].isHome) {
                                defaultScreenPage = DashboardPage();
                              } else if (snapshot.data[i].isPromotion) {
                                defaultScreenPage = PromotionPage();
                              } else {
                                defaultScreenPage = new DefaultScreenPage(
                                  controller: ListBlockController(
                                      screenId: snapshot.data[i].id),
                                  title: snapshot.data[i].name,
                                );
                              }

                              setState(() {});
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              }),
          Expanded(
              child: defaultScreenPage == null
                  ? const SizedBox(
                      child: Center(
                        child: Text('Click Chọn màn hình'),
                      ),
                    )
                  : defaultScreenPage),
        ],
      ),
    );
  }
}
