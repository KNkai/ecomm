import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/components/big_ticket.dart';
import 'package:genie/src/controllers/auth_controller.dart';
import 'package:genie/src/models/campaign.model.dart';
import 'package:genie/src/models/customer.model.dart';
import 'package:genie/src/models/reward.model.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:provider/provider.dart';

class VoucherDetail extends StatelessWidget {
  // final BlockItem item;
  final String title, subTitle, imageCampaign;
  final bool isRewardPage;
  final Campaign campaign;
  final Reward reward;
  final bool canChange;
  const VoucherDetail({
    Key key,
    this.title,
    this.subTitle,
    this.imageCampaign,
    this.campaign,
    this.isRewardPage = false,
    this.reward = null,
    this.canChange = true,
  }) : super(key: key);

  static void push({
    @required BuildContext context,
    @required final String title,
    @required subTitle,
    @required imageCampaign,
    @required final Campaign campaign,
    bool isRewardPage = false,
    bool canChange = true,
    Reward reward = null,
  }) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => VoucherDetail(
              title: title,
              subTitle: subTitle,
              imageCampaign: imageCampaign,
              campaign: campaign,
              isRewardPage: isRewardPage,
              reward: reward,
              canChange: canChange,
            )));
  }

  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<AuthController>(context, listen: false);
    return AppScaffold(
      title: "Chi tiết ưu đãi",
      showCartBtn: false,
      showButtonBottom: isRewardPage,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            child: Material(
              type: MaterialType.transparency,
              child: BigTicket(
                title: title,
                smallTitle: subTitle,
                urlImage: imageCampaign,
                mode: BigTicketMode.none,
              ),
            ),
          ),
          Flexible(
            child: ListView(
              children: List.generate(
                campaign.detail.length,
                (index) => ListDescriptionVoucherDetail(
                  title: campaign.detail[index].title,
                  description: campaign.detail[index].content,
                ),
              ),
            ),
          ),
          const SizedBox(height: 30),
          if (isRewardPage)
            _BtnGetVoucher(
              auth: auth,
              canChange: canChange,
              rewardId: reward.id,
            ),
        ],
      ),
    );
  }
}

class _BtnGetVoucher extends StatelessWidget {
  final AuthController auth;
  final bool canChange;
  final String rewardId;

  const _BtnGetVoucher(
      {Key key,
      @required this.auth,
      @required this.canChange,
      @required this.rewardId})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Customer>(
        valueListenable: auth.customerNotifer,
        builder: (_, state, __) {
          if (!canChange) {
            return const SizedBox();
          }
          return Container(
            decoration: BoxDecoration(
              color: AppColor.primary,
              borderRadius: BorderRadius.circular(30),
            ),
            margin: EdgeInsets.only(bottom: 20),
            child: FlatButton(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: Text(
                  "Đổi ưu đãi",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              onPressed: () {
                showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: (_) {
                      return AlertDialog(
                        content: Container(
                          width: MediaQuery.of(context).size.width / 2,
                          height: MediaQuery.of(context).size.width / 2 + 100,
                          color: Colors.white,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                child: Center(
                                  child: Text(
                                    "Đổi thành công",
                                    style: TextStyle(
                                        color: AppColor.primary, fontSize: 22),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                flex: 1,
                              ),
                              Expanded(
                                flex: 4,
                                child: Image.asset(
                                  AppAsset.reward_primary,
                                  fit: BoxFit.fitWidth,
                                  width: 120,
                                  height: 120,
                                ),
                              ),
                              Expanded(
                                child: SizedBox(
                                  width: double.infinity,
                                  child: SizedBox(
                                    width: 60,
                                    height: 30,
                                    child: AppBtn(
                                        onTap: () {
                                          auth.getReward(rewardId);
                                          Navigator.of(context).popUntil(
                                              (route) => route.isFirst);
                                        },
                                        title: "Xác nhận"),
                                  ),
                                ),
                                flex: 1,
                              )
                            ],
                          ),
                        ),
                      );
                    });
              },
            ),
          );
        });
  }
}

class ListDescriptionVoucherDetail extends StatelessWidget {
  final String title, description;
  const ListDescriptionVoucherDetail({
    Key key,
    @required this.title,
    @required this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Html(
            data: description,
          ),
        ],
      ),
    );
  }
}
