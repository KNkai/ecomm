import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/app_bloc_profile.dart';
import 'package:genie/src/components/app_floating_contact.dart';
import 'package:genie/src/components/app_version.dart';
import 'package:genie/src/components/list_block/list_block.dart';
import 'package:genie/src/components/main_navigation_bar.dart';
import 'package:genie/src/components/product_stack/product_stack.dart';
import 'package:genie/src/components/refresh_scroll_view.dart';
import 'package:genie/src/utils/util.dart';

import '../../components/list_block/controller/home_block_controller.dart';
import '../../components/product_stack/product_stack_controller.dart';
import '../../repositories/product.repo.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key key}) : super(key: key);
  static void push(
    BuildContext context,
  ) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => const DashboardPage()));
  }

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage>
    with AutomaticKeepAliveClientMixin {
  final _listBlockCtrl = HomeBlockController();
  final productStackProtocol = ProductStackRepository();
  final _productStackCtrl = ProductStackController();

  @override
  void initState() {
    super.initState();
    _productStackCtrl.evenInit();
    _loadListBlockCtrl();
  }

  void _loadListBlockCtrl() async {
    await _listBlockCtrl.eventLoad();
    setPopup(context, _listBlockCtrl?.screen);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return RefreshScrollView(
        onRefresh: () async {
          await _listBlockCtrl.eventLoad();
          _productStackCtrl.evenInit();
          setPopup(context, _listBlockCtrl?.screen, force: true);
        },
        onLoadMore: () async {
          _productStackCtrl.eventLoadMore();
        },
        child: FutureBuilder<bool>(
            future: _listBlockCtrl.isShowSuggest(),
            builder: (context, snapshot) {
              return Stack(
                children: [
                  Positioned.fill(
                    child: CustomScrollView(
                      slivers: [
                        SliverToBoxAdapter(
                          child: const MainNavigationBar(),
                        ),
                        SliverToBoxAdapter(
                          child: const AppBlocProfile(),
                        ),
                        ChildColumnListBlock(
                          mode: ListBlockMode.company,
                          controller: _listBlockCtrl,
                        ),
                        snapshot.data ?? false
                            ? SliverToBoxAdapter(
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      left: 26, bottom: 16, top: 20),
                                  child: Text(
                                    'Gợi ý cho bạn',
                                    style: TextStyle(fontSize: 21),
                                  ),
                                ),
                              )
                            : SliverToBoxAdapter(),
                        snapshot.data ?? false
                            ? ProductStack(
                                controller: _productStackCtrl,
                              )
                            : SliverToBoxAdapter(),
                        SliverToBoxAdapter(child: AppVersion()),
                        SliverToBoxAdapter(
                            child: SizedBox(
                          height: 10,
                        )),
                      ],
                    ),
                  ),
                  Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    child: Container(
                      color: Colors.white,
                      child: const MainNavigationBar(),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: AppFloatingContact(),
                    ),
                  )
                ],
              );
            }));
  }

  @override
  bool get wantKeepAlive => true;
}
