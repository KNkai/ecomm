import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:genie/src/components/app_grey.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/components/big_ticket.dart';
import 'package:genie/src/models/campaign.model.dart';
import 'package:genie/src/models/evoucher.model.dart';
import 'package:genie/src/pages/payment/components/voucher_confirm/voucher_confirm_controller.dart';
import 'package:genie/src/pages/voucher/components/expansion_tile_custom.dart';
import 'package:genie/src/pages/voucher/voucher_controller.dart';
import 'package:genie/src/pages/voucher_detail/voucher_detail.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/util.dart';

class VoucherPage extends StatefulWidget {
  static void go({
    @required BuildContext context,
  }) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => VoucherPage()))
        .then((value) {});
  }

  VoucherPage();

  @override
  _VoucherPageState createState() => _VoucherPageState();
}

class _VoucherPageState extends State<VoucherPage> {
  final _codeController = TextEditingController();
  TextEditingController errorVoucher = TextEditingController();

  final _controller = VoucherController();

  @override
  Widget build(BuildContext context) {
    _controller.eventInit();
    return AppScaffold(
        title: 'Ví Voucher',
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          const AppGray(
            left: 'Chọn hoặc Nhập mã ưu đãi',
          ),
          const SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 26),
            child: DecoratedBox(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 0.0)),
              child: Padding(
                padding: const EdgeInsets.only(left: 26, right: 13),
                child: Row(
                  children: [
                    Expanded(
                        child: TextField(
                      maxLines: 1,
                      controller: _codeController,
                      cursorColor: Colors.grey,
                      inputFormatters: [
                        _UpperCaseTextFormatter(),
                      ],
                      decoration: InputDecoration(
                        focusedBorder: InputBorder.none,
                        hintText: 'Nhập mã ưu đãi',
                        hintStyle:
                            TextStyle(fontSize: 14, color: Color(0xff9B9B9B)),
                        enabledBorder: InputBorder.none,
                        border: InputBorder.none,
                        labelStyle: TextStyle(color: Colors.green),
                      ),
                    )),
                    GestureDetector(
                      onTap: () {
                        _controller.eventAddNewVoucher(_codeController.text,
                            (value) {
                          _codeController.clear();
                          if (value != null) {
                            setState(() {
                              errorVoucher.text = value;
                            });
                            return null;
                          } else {
                            setState(() {
                              errorVoucher.clear();
                            });
                          }
                          return showDialog(
                              context: context,
                              builder: (_) {
                                return AlertDialog(
                                  content: SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    height:
                                        MediaQuery.of(context).size.width / 4,
                                    child: Center(
                                      child: Text(
                                        "Thêm ưu đãi thành công",
                                        style:
                                            TextStyle(color: AppColor.primary),
                                      ),
                                    ),
                                  ),
                                );
                              });
                        });
                      },
                      behavior: HitTestBehavior.opaque,
                      child: const Text(
                        'Áp dụng',
                        style: TextStyle(color: AppColor.primary, fontSize: 14),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 26, vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ValueListenableBuilder<StateVouchersProfile>(
                    valueListenable: _controller.stateVouchers,
                    builder: (_, state, child) {
                      if (state is StateVouchersProfileInit)
                        return SizedBox.shrink();

                      if (state is StateVouchersProfileEmtpy)
                        return const SizedBox();

                      final list = (state as StateVouchersProfileLoaded).list;
                      return Text(
                        "Bạn đang có ${list.where((e) => e.data is Evoucher).length} voucher",
                        style: TextStyle(color: AppColor.primary, fontSize: 13),
                      );
                    }),
                Text(
                  errorVoucher.text,
                  style: TextStyle(color: Colors.red, fontSize: 13),
                ),
              ],
            ),
          ),
          ListVoucherProfile(controller: _controller),
        ]));
  }
}

class ListVoucherProfile extends StatelessWidget {
  final VoucherController controller;

  const ListVoucherProfile({Key key, this.controller}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ValueListenableBuilder<StateVouchersProfile>(
          valueListenable: controller.stateVouchers,
          builder: (_, state, child) {
            if (state is StateVouchersProfileInit) return SizedBox.shrink();

            if (state is StateVouchersProfileEmtpy) return child;

            final list = (state as StateVouchersProfileLoaded).list;
            final listGroupEvoucher =
                (state as StateVouchersProfileLoaded).listGroupEvoucher;

            int count = 0;
            List<Widget> listVoucherCurrent = [];
            return ListView(
                scrollDirection: Axis.vertical,
                children: List.generate(list.length, (c) {
                  if (list[c].type == EvoucherModelType.title) {
                    count = 0;
                    listVoucherCurrent.clear();
                    int _countExEvoucher = 0;
                    listGroupEvoucher.forEach((e) {
                      if (list[c].data.toString() == e.title) {
                        e.listEvoucher.forEach((element) {
                          if (element.almostExpired) {
                            _countExEvoucher++;
                          }
                        });
                      }
                    });
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 26),
                      child: Row(
                        children: [
                          Text((list[c].data.toString()) ?? '',
                              style: TextStyle(
                                  fontSize: 15, color: AppColor.primary)),
                          SizedBox(width: 10),
                          Text(
                              _countExEvoucher == 0
                                  ? ""
                                  : "(${_countExEvoucher} Voucher sắp hết hạn)",
                              style: TextStyle(
                                  fontSize: 13,
                                  color: Colors.grey.withOpacity(0.7))),
                        ],
                      ),
                    );
                  } else {
                    final campaign = (list[c].data as Evoucher)?.campaign;
                    final smallTitle = 'Hạn dùng đến: ' +
                        appConvertDateTime(
                            (list[c].data as Evoucher)?.expiredAt,
                            format: "dd/MM/yyyy HH:mm");
                    final name = campaign?.name ?? '';
                    if (count <= 1) {
                      count++;
                      return VoucherItem(
                          name: name,
                          smallTitle: smallTitle,
                          campaign: campaign);
                    } else {
                      listVoucherCurrent.add(VoucherItem(
                          name: name,
                          smallTitle: smallTitle,
                          campaign: campaign));
                      if (c + 1 <= list.length) {
                        if (c == list.length - 1) {
                          return ExpansionTileCustom(
                            expandedAlignment: Alignment.center,
                            children: listVoucherCurrent.toList(),
                          );
                        } else {
                          if (list[c + 1].type == EvoucherModelType.title) {
                            return ExpansionTileCustom(
                              expandedAlignment: Alignment.center,
                              children: listVoucherCurrent.toList(),
                            );
                          } else {
                            return SizedBox();
                          }
                        }
                      } else {
                        return SizedBox();
                      }
                    }
                  }
                }));
          },
          child: Center(
            child: Text('Không có mã voucher'),
          )),
    );
  }
}

class VoucherItem extends StatelessWidget {
  const VoucherItem({
    Key key,
    @required this.name,
    @required this.smallTitle,
    @required this.campaign,
  }) : super(key: key);

  final String name;
  final String smallTitle;
  final Campaign campaign;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        VoucherDetail.push(
            context: context,
            title: name,
            subTitle: smallTitle,
            imageCampaign: campaign?.image ?? '',
            campaign: campaign);
      },
      behavior: HitTestBehavior.opaque,
      child: Padding(
        padding: const EdgeInsets.only(left: 26, right: 26, bottom: 14),
        child: BigTicket(
            mode: BigTicketMode.none,
            urlImage: campaign?.image ?? '',
            smallTitle: smallTitle,
            title: name),
      ),
    );
  }
}

class _UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text?.toUpperCase(),
      selection: newValue.selection,
    );
  }
}
