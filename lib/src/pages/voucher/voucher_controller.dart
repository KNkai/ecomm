import 'package:flutter/material.dart';
import 'package:genie/src/models/evoucher_group.model.dart';
import 'package:genie/src/repositories/evoucher.repo.dart';

class VoucherController {
  final _repo = EvoucherRepository();

  final stateVouchers =
      ValueNotifier<StateVouchersProfile>(StateVouchersProfileInit());

  void eventInit() {
    _repo.getAllEvoucher().then((value) {
      if (value != null && value is List<EvoucherModel>) {
        _repo.getAllEvoucherByGroup().then((c) {
          stateVouchers.value =
              StateVouchersProfileLoaded(list: value, listGroupEvoucher: c);
        });
      } else
        stateVouchers.value = StateVouchersProfileEmtpy();
    });
  }

  void eventAddNewVoucher(String code, Function(String) onComplete) {
    if (code != null && code.isNotEmpty)
      _repo.takeEVoucherByCode(code).then((value) {
        eventInit();
        onComplete((value is String) ? value : null);
      });
  }

  void dispose() {
    stateVouchers.dispose();
  }
}

abstract class StateVouchersProfile {}

class StateVouchersProfileLoaded extends StateVouchersProfile {
  final List<EvoucherModel> list;
  List<GroupEvoucher> listGroupEvoucher;
  StateVouchersProfileLoaded({@required this.list, this.listGroupEvoucher});

  @override
  bool operator ==(other) => false;
}

class StateVouchersProfileEmtpy extends StateVouchersProfile {}

class StateVouchersProfileInit extends StateVouchersProfile {}

abstract class StateVoucherSide {}

class StateSideChooseSelect extends StateVoucherSide {
  final int index;
  StateSideChooseSelect({@required this.index});
}

class StateSideShowMSg extends StateVoucherSide {
  final String msg;
  StateSideShowMSg({@required this.msg});
}
