import 'dart:async';

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:youtube_explode_dart/youtube_explode_dart.dart';

class YoutubePageController {
  final _yt = YoutubeExplode();
  final stateSide = StreamController<StateSideYoutube>();
  final stateVideoData = ValueNotifier<VideoPlayerController>(null);

  void eventOpenYoutube(String link) {
    _yt.videos.streamsClient
        .getManifest(Uri.parse(link).queryParameters['v'])
        .then((manifest) {
      final rawUri = (manifest.video.toList()
            ..sort(
                (a, b) => b.videoQuality.index.compareTo(a.videoQuality.index)))
          .first;
      try {
        final controller = VideoPlayerController.network(rawUri.url.toString());
        controller.initialize().then((_) {
          stateVideoData.value = controller;
        }).catchError((_) {
          stateSide.add(StateSideYoutubeMsg(msg: 'Có lỗi xảy ra xin thử lại'));
        });
      } catch (_) {
        stateSide.add(StateSideYoutubeMsg(msg: 'Có lỗi xảy ra xin thử lại'));
      }
    }).catchError((_) {
      stateSide.add(StateSideYoutubeMsg(msg: 'Có lỗi xảy ra xin thử lại'));
    });
  }

  void dispose() {
    stateVideoData.dispose();
    stateVideoData.value.dispose();
  }
}

abstract class StateSideYoutube {}

class StateSideYoutubeMsg extends StateSideYoutube {
  final String msg;
  StateSideYoutubeMsg({@required this.msg});
}
