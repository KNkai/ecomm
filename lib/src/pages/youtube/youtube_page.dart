import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:genie/src/pages/youtube/youtube_controller.dart';
import 'package:video_player/video_player.dart';

class YoutubePage extends StatefulWidget {
  static void go({@required BuildContext context, @required String link}) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => YoutubePage(link)));
  }

  final String link;

  const YoutubePage(this.link);

  @override
  _YoutubePageState createState() => _YoutubePageState();
}

class _YoutubePageState extends State<YoutubePage> {
  final _controller = YoutubePageController();

  @override
  void initState() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
    _controller.eventOpenYoutube(widget.link);
    _controller.stateSide.stream.listen((event) {
      final StateSideYoutubeMsg showMsgState = event;
      WarningDialog.show(context, showMsgState.msg, 'OK', title: 'Thông báo',
          onCloseDialog: () {
        _pop();
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        _pop();
        return false;
      },
      child: Scaffold(
        backgroundColor: Colors.black, //
        body: Stack(
          children: [
            ValueListenableBuilder<VideoPlayerController>(
              valueListenable: _controller.stateVideoData,
              builder: (context, videoController, child) {
                if (videoController == null) return child;

                videoController.play();
                return AspectRatio(
                  aspectRatio: MediaQuery.of(context).size.width /
                      MediaQuery.of(context).size.height,
                  child: VideoPlayer(videoController),
                );
              },
              child: ConstrainedBox(
                constraints: BoxConstraints.expand(),
                child: Center(
                  child: _CupertinoActivityIndicator(
                    radius: 20,
                  ),
                ),
              ),
            ),
            Positioned(
                right: 10,
                top: 20,
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    _pop();
                  },
                  child: const Icon(Icons.close, size: 32, color: Colors.green),
                ))
          ],
        ),
      ),
    );
  }

  void _pop() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    Navigator.of(context).pop();
  }
}

const _kDefaultIndicatorRadius = 10.0;

class _CupertinoActivityIndicator extends StatefulWidget {
  const _CupertinoActivityIndicator({
    Key key,
    this.animating = true,
    this.radius = _kDefaultIndicatorRadius,
  })  : assert(animating != null),
        assert(radius != null),
        assert(radius > 0.0),
        progress = 1.0,
        super(key: key);

  final bool animating;

  final double radius;

  final double progress;

  @override
  _CupertinoActivityIndicatorState createState() =>
      _CupertinoActivityIndicatorState();
}

class _CupertinoActivityIndicatorState
    extends State<_CupertinoActivityIndicator>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );

    if (widget.animating) {
      _controller.repeat();
    }
  }

  @override
  void didUpdateWidget(_CupertinoActivityIndicator oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.animating != oldWidget.animating) {
      if (widget.animating)
        _controller.repeat();
      else
        _controller.stop();
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.radius * 2,
      width: widget.radius * 2,
      child: CustomPaint(
        painter: _CupertinoActivityIndicatorPainter(
          position: _controller,
          activeColor: CupertinoDynamicColor.resolve(Colors.white, context),
          radius: widget.radius,
          progress: widget.progress,
        ),
      ),
    );
  }
}

const double _kTwoPI = pi * 2.0;

const int _partiallyRevealedAlpha = 147;

class _CupertinoActivityIndicatorPainter extends CustomPainter {
  _CupertinoActivityIndicatorPainter({
    @required this.position,
    @required this.activeColor,
    @required this.radius,
    @required this.progress,
  })  : alphaValues = [
          47,
          47,
          47,
          47,
          72,
          97,
          122,
          147,
        ],
        tickFundamentalRRect = RRect.fromLTRBXY(
          -radius / _kDefaultIndicatorRadius,
          -radius / (3.0),
          radius / _kDefaultIndicatorRadius,
          -radius,
          radius / _kDefaultIndicatorRadius,
          radius / _kDefaultIndicatorRadius,
        ),
        super(repaint: position);

  final Animation<double> position;
  final Color activeColor;
  final double radius;
  final double progress;

  final List<int> alphaValues;
  final RRect tickFundamentalRRect;

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint();
    final int tickCount = alphaValues.length;

    canvas.save();
    canvas.translate(size.width / 2.0, size.height / 2.0);

    final int activeTick = (tickCount * position.value).floor();

    for (int i = 0; i < tickCount * progress; ++i) {
      final int t = (i - activeTick) % tickCount;
      paint.color = activeColor
          .withAlpha(progress < 1 ? _partiallyRevealedAlpha : alphaValues[t]);
      canvas.drawRRect(tickFundamentalRRect, paint);
      canvas.rotate(_kTwoPI / tickCount);
    }

    canvas.restore();
  }

  @override
  bool shouldRepaint(_CupertinoActivityIndicatorPainter oldPainter) {
    return oldPainter.position != position ||
        oldPainter.activeColor != activeColor ||
        oldPainter.progress != progress;
  }
}

class WarningDialog {
  static bool _isShow = false;

  static void show(BuildContext context, String content, String btnText,
      {String title, Function onCloseDialog}) {
    if (!_isShow) {
      _isShow = true;
      Widget alert;
      Widget _titleWidget({TextAlign textAlign = TextAlign.start}) =>
          title != null
              ? Text(
                  title,
                  textAlign: textAlign,
                )
              : null;

      if (content != null && content.isNotEmpty) {
        if (Platform.isAndroid) {
          // If it has plenty of buttons, it will stack them on vertical way.
          // If title isn't supplied, height of this alert will smaller than the one has title.
          alert = AlertDialog(
            title: _titleWidget(),
            content: Text(
              content,
              textAlign: TextAlign.start,
            ),
            actions: [
              FlatButton(
                child: Text(btnText),
                onPressed: () {
                  _isShow = false;
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        } else {
          // Almost similiar with Cupertino style.
          // If title isn't supplied, height of this alert will smaller than the one has title.
          alert = CupertinoAlertDialog(
            title: Padding(
              padding: const EdgeInsets.only(
                bottom: 10,
              ),
              child: _titleWidget(textAlign: TextAlign.center),
            ),
            content: Text(
              content,
              textAlign: TextAlign.start,
            ),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () {
                  _isShow = false;
                  Navigator.of(context).pop();
                },
                child: Text(
                  btnText,
                ),
              )
            ],
          );
        }

        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert;
          },
        ).then((value) {
          _isShow = false;
          if (onCloseDialog != null) {
            onCloseDialog();
          }
        });
      }
    }
  }
}
