import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:genie/src/pages/cart/cart_controller.dart';
import 'package:genie/src/utils/app_color.dart';

class CartItemAdjustAmount extends StatelessWidget {
  CartItemAdjustAmount(
      {@required int quantity,
      @required this.productId,
      @required this.controller})
      : _stateQuantity = ValueNotifier<int>(quantity);

  final ValueNotifier<int> _stateQuantity;
  final String productId;
  final CartController controller;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {
            _down(controller);
          },
          child: const DecoratedBox(
            decoration:
                BoxDecoration(color: AppColor.primary, shape: BoxShape.circle),
            child: Icon(
              Icons.remove,
              size: 20,
              color: Colors.white,
            ),
          ),
        ),
        ValueListenableBuilder(
            valueListenable: _stateQuantity,
            builder: (_, quantity, __) {
              return SizedBox(
                width: 70,
                child: Text(quantity.toString(),
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontSize: 16)),
              );
            }),
        GestureDetector(
          onTap: () {
            _up(controller);
          },
          child: const DecoratedBox(
            decoration:
                BoxDecoration(color: AppColor.primary, shape: BoxShape.circle),
            child: Icon(
              Icons.add,
              size: 20,
              color: Colors.white,
            ),
          ),
        )
      ],
    );
  }

  void _down(CartController controller) {
    if (_stateQuantity.value > 1) {
      _stateQuantity.value--;
      controller.eventChangeProductQuantity(
          productId: productId, quantity: _stateQuantity.value);
      controller.eventReload();
    }
  }

  void _up(CartController controller) {
    _stateQuantity.value++;
    controller.eventChangeProductQuantity(
        productId: productId, quantity: _stateQuantity.value);
    controller.eventReload();
  }
}
