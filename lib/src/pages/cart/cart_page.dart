import 'package:flutter/material.dart';
import 'package:genie/src/components/app_btn.dart';
import 'package:genie/src/components/app_delay.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/components/bottom_action_container.dart';
import 'package:genie/src/helper/auth.dart';
import 'package:genie/src/helper/cart_data.dart';
import 'package:genie/src/pages/cart/cart_controller.dart';
import 'package:genie/src/pages/cart/component/cart_item_adjust_amount.dart';
import 'package:genie/src/pages/payment/payment_page.dart';
import 'package:genie/src/utils/app_asset.dart';
import 'package:genie/src/utils/app_color.dart';
import 'package:genie/src/utils/app_fontSize.dart';
import 'package:genie/src/utils/util.dart';
import 'package:provider/provider.dart';

class CartPage extends StatelessWidget {
  const CartPage();

  static void push(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => const CartPage()));
  }

  @override
  Widget build(BuildContext context) {
    return AppDelay(
      starter: AppScaffold(
        showCartBtn: false,
        title: 'Giỏ hàng',
        child: SizedBox.shrink(),
        isPaddingContact: true,
      ),
      child: Provider<CartController>(
        create: (_) => CartController(cartData: CartData.getCtl(context)),
        dispose: (_, con) => con.dispose(),
        child: Builder(
          builder: (contextX) {
            final controller =
                Provider.of<CartController>(contextX, listen: false);

            controller.eventInit();

            return ValueListenableBuilder<int>(
                valueListenable: controller.cartData.stateNumberCart,
                builder: (_, quantity, __) {
                  if (quantity == 0)
                    return const AppScaffold(
                        showCartBtn: false,
                        title: 'Giỏ hàng',
                        child: Center(
                          child: Text('Giỏ hàng trống'),
                        ));

                  controller.eventInit();

                  return AppScaffold(
                    showCartBtn: false,
                    title: 'Giỏ hàng',
                    child: BottomActionContainer(
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              SizedBox(
                                width: double.infinity,
                                child: ColoredBox(
                                  color: Colors.grey[200],
                                  child: Padding(
                                    child: Text(
                                        'Có ${controller.cartData.stateNumberCart.value} sản phẩm'),
                                    padding: const EdgeInsets.only(
                                        left: 26, top: 14, bottom: 16),
                                  ),
                                ),
                              ),
                              ValueListenableBuilder<List<ModelProduct>>(
                                  valueListenable: controller.stateCart,
                                  builder: (_, data, ___) {
                                    if (data == null) return SizedBox.shrink();
                                    print('in ra data: ${data.length}');
                                    return Column(
                                      children: [
                                        for (ModelProduct product in data)
                                          _Item(
                                            model: product,
                                            controller: controller,
                                          )
                                      ],
                                    );
                                  }),
                            ],
                          ),
                        ),
                        bottomChild: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const SizedBox(width: 26),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    'Tổng cộng',
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  const SizedBox(height: 5),
                                  ValueListenableBuilder<double>(
                                    valueListenable: controller.stateTotal,
                                    builder: (_, total, __) {
                                      return Text(
                                        appCurrency(total),
                                        style: const TextStyle(
                                            color: AppColor.primary),
                                      );
                                    },
                                  )
                                ],
                              ),
                            ),
                            const SizedBox(width: 20),
                            Expanded(
                              child: AppBtn(
                                onTap: () {
                                  PaymentPage.go(context);
                                },
                                title: 'Xác nhận',
                              ),
                            ),
                            const SizedBox(width: 26),
                          ],
                        )),
                  );
                });
          },
        ),
      ),
    );
  }
}

class _Item extends StatelessWidget {
  final ModelProduct model;
  final CartController controller;
  const _Item({
    @required this.model,
    @required this.controller,
  });
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: 100,
              height: 100,
              child: model?.product?.listImage == null ||
                      model.product.listImage.isEmpty
                  ? const SizedBox.shrink()
                  : Image.network(model?.product?.listImage[0]),
            ),
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () {
                              appShowModalBottomSheet(
                                  onExit: () {
                                    controller.eventReload();
                                  },
                                  context: context,
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10),
                                        child: CartItemAdjustAmount(
                                            controller: controller,
                                            quantity: model.quantity,
                                            productId: model.product.id),
                                      ),
                                      const SizedBox(height: 10),
                                      TextField(
                                        controller: TextEditingController(
                                            text: model.note),
                                        minLines: 2,
                                        maxLines: 2,
                                        cursorColor: Colors.grey,
                                        onChanged: (note) {
                                          controller.eventChangeProductNote(
                                              productId: model.product.id,
                                              note: note);
                                        },
                                        decoration: InputDecoration(
                                          focusedBorder:
                                              const OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.grey, width: 0.0),
                                          ),
                                          hintText: 'Ghi chú',
                                          hintStyle: TextStyle(
                                              fontSize: 13,
                                              color: Colors.grey[400]),
                                          enabledBorder:
                                              const OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.grey, width: 0.0),
                                          ),
                                          border: const OutlineInputBorder(),
                                          labelStyle: const TextStyle(
                                              color: Colors.green),
                                        ),
                                      ),
                                    ],
                                  ),
                                  title: model.product.name);
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  model?.product?.name ?? '',
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                model?.note == null
                                    ? Text(
                                        model?.note ?? '+ Thêm chú thích',
                                        style: const TextStyle(
                                            color: AppColor.primary,
                                            fontSize: 13),
                                      )
                                    : Text(
                                        model?.note,
                                        style: TextStyle(
                                            color: Colors.grey[500],
                                            fontSize: 13),
                                      )
                              ],
                            ),
                          ),
                        ),
                        GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  _showAlertDialog(
                                      context,
                                      () => controller.eventRemoveProduct(
                                            productId: model.product.id,
                                          )),
                            );
                          },
                          child: Center(
                              child: Icon(
                            Icons.delete,
                            color: Colors.grey[300],
                          )),
                        )
                      ],
                    ),
                    const Divider(),
                    Row(
                      children: [
                        CartItemAdjustAmount(
                          controller: controller,
                          productId: model.product.id,
                          quantity: model.quantity,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                appCurrency(model.product.getSalePrice),
                                style: const TextStyle(color: AppColor.primary),
                              ),
                              if (model.product.hasSale)
                                const SizedBox(
                                  height: 5,
                                ),
                              if (model.product.hasSale)
                                Text(
                                  appCurrency(model.product.basePrice),
                                  style: const TextStyle(
                                      decoration: TextDecoration.lineThrough,
                                      color: Colors.grey),
                                )
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
        Divider(
          thickness: 4,
          color: Colors.grey[100],
        ),
      ],
    );
  }

  _showAlertDialog(BuildContext context, Function func) {
    return AlertDialog(
      contentPadding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Center(
              child: Image.asset(
                AppAsset.trash,
                height: 87,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: const Center(
                child: Text(
              "Xóa sản phẩm",
              style: TextStyle(fontSize: titleSize + 5),
            )),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: const Center(
                child: Text(
              "Bạn có thể xem lại danh sách sản phẩm dành để mua sau ở trang tài khoản của bạn",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: subTitleSize),
            )),
          ),
          Container(
            padding: const EdgeInsets.only(top: 20),
            width: MediaQuery.of(context).size.width / 2,
            child: AppBtn(
              color: Colors.white,
              borderColor: AppColor.primary,
              textColor: AppColor.primary,
              onTap: () {
                Auth.checkExistUser(
                    context: context,
                    onLoginSuccess: () {
                      func();
                      controller.addProductToPurchasedLater(
                          model.product.id, model.product.name);
                      Navigator.of(context).pop();
                    });
              },
              title: "Để Dành Mua Sau",
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10),
            width: MediaQuery.of(context).size.width / 2,
            child: AppBtn(
              onTap: () {
                func();
                Navigator.of(context).pop();
              },
              title: "Xóa Sản Phẩm",
            ),
          ),
        ],
      ),
    );
  }
}
