import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/repositories/product.repo.dart';
import 'package:genie/src/repositories/purchased_later.repo.dart';

import '../../helper/cart_data.dart';

class CartController extends CartProtocol {
  final CartData cartData;
  final stateTotal = ValueNotifier<double>(0);
  final stateCart = ValueNotifier<List<ModelProduct>>(null);
  final LinkedHashMap<String, ModelCart> _mapCart;
  final _purchasedLaterRepository = PurchasedLaterRepository();
  final _repoProduct = ProductRepository();

  CartController({@required this.cartData}) : _mapCart = cartData.mapCart;

  void eventInit() {
    // calculate total
    double total = 0;
    _repoProduct
        .getProductsCart(ids: _mapCart.values.map((e) => e.productId).toList())
        .then((value) {
      if (value != null && value is List<Product>) {
        final List<ModelProduct> carts = List<ModelProduct>(value.length);
        for (int c = 0; c < value.length; c++) {
          final element = value[c];
          final cart = _mapCart[element.id];
          total += cart.quantity * element.getSalePrice;
          carts[c] = ModelProduct(element, cart.note, cart.quantity);
        }
        stateCart.value = carts;
        stateTotal.value = total ?? 0;
      }
    });
  }

  @override
  void eventChangeProductQuantity({String productId, int quantity}) {
    final oldQuantity = _mapCart[productId].quantity;
    cartData.eventChangeProductQuantity(
        productId: productId, quantity: quantity);
    if (oldQuantity != quantity) {
      final product = stateCart.value
          .firstWhere((element) => element.product.id == productId);

      // calcualte total
      if (quantity < oldQuantity) {
        stateTotal.value -=
            (oldQuantity - quantity) * product.product.getSalePrice;
      } else
        stateTotal.value +=
            (quantity - oldQuantity) * product.product.getSalePrice;
    }
  }

  @override
  void eventAddProudct(
      {String productId,
      int quantity,
      String note,
      double price,
      String name,
      String image}) {
    cartData.eventAddProudct(
      productId: productId,
      quantity: quantity,
      note: note,
    );
    // calculate total
    stateTotal.value += _mapCart[productId].quantity * price;
    eventReload();
  }

  @override
  void eventChangeProductNote({String productId, String note}) {
    cartData.eventChangeProductNote(productId: productId, note: note);
  }

  @override
  void eventRemoveProduct({String productId}) {
    // calculate total
    final product = stateCart.value
        .firstWhere((element) => element.product.id == productId);
    stateTotal.value -= product.quantity * product.product.getSalePrice;

    cartData.eventRemoveProduct(productId: productId);

    eventReload();
  }

  @override
  void eventReset() {
    cartData.eventReset();

    // calculate total
    stateTotal.value = 0;
  }

  void addProductToPurchasedLater(String idProduct, String productName) {
    _purchasedLaterRepository.addPurchasedLaterProduct(idProduct, productName);
  }

  void eventReload() {
    stateCart.value = _mapCart.values.map((value) {
      final product = stateCart.value
          .firstWhere((element) => element.product.id == value.productId);
      return ModelProduct(product.product, value.note, value.quantity);
    }).toList();
  }

  void dispose() {
    stateCart.dispose();
    stateTotal.dispose();
  }
}

class ModelCart {
  final String productId;
  int quantity;
  String note;

  ModelCart({
    this.productId,
    this.quantity,
    this.note,
  });

  factory ModelCart.fromJson(Map<String, dynamic> json) {
    return ModelCart(
      productId: json["productId"],
      quantity: json["quantity"] == null ? 0 : json["quantity"],
      note: json["note"],
    );
  }

  Map<String, dynamic> toJson() => {
        "productId": productId,
        "quantity": quantity,
        "note": note,
      };
}

class ModelProduct {
  final Product product;
  final String note;
  final int quantity;
  ModelProduct(this.product, this.note, this.quantity);
}
