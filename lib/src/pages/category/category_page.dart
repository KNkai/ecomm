import 'package:flutter/material.dart';

import '../../components/app_scaffold.dart';
import '../../components/init_loadmore_search/init_loadmore_search_controller.dart';
import '../../components/list_block/components/list_product_grid_block_item.dart';
import '../../components/refresh_scroll_view.dart';
import '../../models/product.model.dart';
import '../../repositories/product.repo.dart';
import 'category_controller.dart';

class CategoryPage extends StatelessWidget {
  static void push(BuildContext context,
      {@required String categoryId, @required String title}) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => CategoryPage(
              categoryId: categoryId,
              title: title,
            )));
  }

  final String categoryId;
  final String title;

  const CategoryPage({Key key, @required this.categoryId, @required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final productProtocol = ProductGridPageRepository();
    final controller = CategoryController(
        initProtocol: productProtocol, loadMoreProtocol: productProtocol);

    controller.evenInit(parameter: categoryId);
    return AppScaffold(
        title: title,
        showSearchBtn: true,
        onCustomonBackTap: () {
          Navigator.of(context).pop();
        },
        child: RefreshScrollView(
          onRefresh: () async {
            controller.evenInit(parameter: categoryId);
          },
          onLoadMore: () async {
            controller.eventLoadMore(parameter: categoryId);
          },
          child: StreamBuilder<ListState>(
            stream: controller.stateList,
            builder: (_, ss) {
              if (ss?.data == null) return const SizedBox.shrink();

              if (ss.data is ListEmptyState)
                return const Center(
                    child: Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Text('Dữ liệu bị trống'),
                ));

              final ListLoadState<Product> stateProduct = ss.data;
              return Padding(
                padding: const EdgeInsets.all(16.0),
                child: SingleChildScrollView(
                  child: ListProductGridBlockItem(
                      items:
                          stateProduct.data.map((e) => e.blockItem).toList()),
                ),
              );
            },
          ),
        ));
  }
}
