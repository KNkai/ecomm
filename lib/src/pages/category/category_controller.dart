import 'package:flutter/material.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/product.model.dart';

class CategoryController extends InitLoadMoreSearchController<Product> {
  CategoryController(
      {@required InitListProtocol initProtocol,
      @required LoadMoreProtocol loadMoreProtocol})
      : super(initProtocol: initProtocol, loadMoreProtocol: loadMoreProtocol);
}
