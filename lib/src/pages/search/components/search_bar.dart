import 'package:flutter/material.dart';
import 'package:genie/src/pages/search/controller/search_controller.dart';

import '../search_page.dart';

class SearchBar extends StatefulWidget {
  final KeyWordController controller;
  final String popularCampaign;
  final String textValue;
  final bool isEnable;
  final bool isSearchProduct;
  final BuildContext contextValue;
  final VoidCallback onTap;
  const SearchBar(
      {Key key,
      this.controller,
      this.popularCampaign,
      this.textValue,
      this.contextValue,
      this.isSearchProduct,
      this.isEnable,
      this.onTap})
      : super(key: key);

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  TextEditingController _productNameSearchController = TextEditingController();
  final focusSearch = FocusNode();
  bool _isFocus = false;

  @override
  void initState() {
    focusSearch
      ..addListener(() {
        _isFocus ? _isFocus == true : _isFocus == false;
        setState(() {});
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.textValue != null)
      _productNameSearchController.text = widget.textValue;
    // bool hasText = false;
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      child: Center(
        child: Column(
          children: [
            Material(
              elevation: 10,
              shadowColor: Colors.grey.withOpacity(0.5),
              borderRadius: BorderRadius.circular(5),
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.only(right: 20),
                child: Row(
                  children: [
                    Expanded(
                      flex: 10,
                      child: TextField(
                        autofocus: widget.isEnable,
                        focusNode: focusSearch,
                        onTap: () {
                          if (widget.isSearchProduct) {
                            widget.onTap();
                          }
                        },
                        onSubmitted: (value) =>
                            SearchPage.push(context: context, keyword: value),
                        onChanged: (_) {
                          if (_ != "") {
                            widget.controller.filterKeyword(_);
                          } else {
                            widget.controller.getRecentKeyWords();
                          }
                        },
                        controller: _productNameSearchController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(left: 20),
                          hintText: "${widget.popularCampaign}",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    focusSearch.dispose();
    super.dispose();
  }
}
