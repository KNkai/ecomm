import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:genie/src/components/app_scaffold.dart';
import 'package:genie/src/models/keyword.model.dart';
import 'package:genie/src/pages/search/components/search_bar.dart';
import 'package:genie/src/pages/search/controller/search_controller.dart';
import 'package:genie/src/pages/voucher/components/expansion_tile_custom.dart';
import 'package:provider/provider.dart';
import "package:genie/src/pages/search/search_page.dart";

class SearchPageKeyWord extends StatefulWidget {
  const SearchPageKeyWord({
    Key key,
  }) : super(key: key);
  static void push(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => ChangeNotifierProvider<KeyWordController>(
            create: (_) => KeyWordController(), child: SearchPageKeyWord())));
  }

  @override
  _SearchPageKeyWordState createState() => _SearchPageKeyWordState();
}

class _SearchPageKeyWordState extends State<SearchPageKeyWord> {
  final _codeController = TextEditingController();
  TextEditingController errorVoucher = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final _customerController =
        Provider.of<KeyWordController>(context, listen: false);

    return AppScaffold(
        title: 'Tìm Kiếm',
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          child: Center(
            child: Column(children: [
              ValueListenableBuilder<String>(
                  valueListenable:
                      Provider.of<KeyWordController>(context, listen: false)
                          .hintKeywords,
                  builder: (context, ss, __) {
                    print(ss);
                    return ss != null
                        ? SearchBar(
                            controller: _customerController,
                            popularCampaign: ss,
                            isSearchProduct: false,
                            isEnable: true,
                            // controller: controller,
                            // scrollController: _scrollController,
                          )
                        : const SizedBox.shrink();
                  }),
              ListHistoryKeyword(controller: _customerController),
            ]),
          ),
        ));
  }
}

class ListHistoryKeyword extends StatelessWidget {
  final KeyWordController controller;
  const ListHistoryKeyword({Key key, @required this.controller})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ValueListenableBuilder<List<KeyWordClass>>(
        valueListenable: controller.listAllKeywords,
        builder: (_, key, __) {
          return ValueListenableBuilder<List<String>>(
            valueListenable: controller.listRecentKeywords,
            builder: (_, cus, child) {
              if (cus == null) return child;
              List<KeyWordItem> listKeyWordCurrent = [];

              cus.forEach((e) {
                listKeyWordCurrent.add(KeyWordItem(name: e));
              });
              if (key != null) {
                key.sort((a, b) => a.count.compareTo(b.count));
                for (int i = 0; i < 2; i++) {
                  listKeyWordCurrent.add(KeyWordItem(name: key[i].keyWord));
                }
              }
              listKeyWordCurrent = listKeyWordCurrent.reversed.toList();
              return SingleChildScrollView(
                child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.fromLTRB(15, 0, 20, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: cus.length <= 2
                              ? List.generate(
                                  cus.length, (i) => KeyWordItem(name: cus[i]))
                              : [
                                  for (int i = 0; i < 2; i++)
                                    listKeyWordCurrent[i],
                                  ExpansionTileCustom(
                                    expandedCrossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: listKeyWordCurrent
                                        .getRange(2, listKeyWordCurrent.length)
                                        .toList(),
                                  ),
                                ]),
                    ],
                  ),
                ),
              );
            },
            child: Center(
              child: Text('Không có lịch sử tìm kiếm'),
            ),
          );
        },
      ),
    );
  }
}

class KeyWordItem extends StatelessWidget {
  const KeyWordItem({
    Key key,
    @required this.name,
  }) : super(key: key);

  final String name;

  @override
  Widget build(BuildContext context) {
    // final productProtocol = ProductGridPageRepositorySearch();
    // final _searchController = SearchController(
    //     initProtocol: productProtocol, loadMoreProtocol: productProtocol);
    // ScrollController _scrollController = new ScrollController();
    return GestureDetector(
      onTap: () {
        // if (name != "") {
        //   _searchController.evenInit(
        //     parameter: name,
        //   );
        //   // _scrollController
        //   //   ..addListener(() {
        //   //     if (_scrollController.position.pixels ==
        //   //         _scrollController.position.maxScrollExtent) {
        //   //       _searchController.eventLoadMore(parameter: name);
        //   //     }
        //   //   });
        //   _searchController.eventSearch(name);
        // } else {
        //   _searchController.evenInit(parameter: "null");
        //   _searchController.eventLoadMore(parameter: "null");
        // }
        SearchPage.push(context: context, keyword: name);
      },
      behavior: HitTestBehavior.opaque,
      child: Padding(
        padding: const EdgeInsets.only(left: 26, right: 26, bottom: 10),
        child: Text(
          name,
          style: TextStyle(fontSize: 16),
        ),
        // child: BigTicket(mode: BigTicketMode.none, title: name),
      ),
    );
  }
}

// class _UpperCaseTextFormatter extends TextInputFormatter {
//   @override
//   TextEditingValue formatEditUpdate(
//       TextEditingValue oldValue, TextEditingValue newValue) {
//     return TextEditingValue(
//       text: newValue.text?.toUpperCase(),
//       selection: newValue.selection,
//     );
//   }
// }
