import 'package:flutter/material.dart';

import '../../components/app_scaffold.dart';
import '../../components/init_loadmore_search/init_loadmore_search_controller.dart';
import '../../components/list_block/components/list_product_grid_block_item.dart';
import '../../models/product.model.dart';
import '../../repositories/product.repo.dart';
import 'components/search_bar.dart';
import 'controller/search_controller.dart';

class SearchPage extends StatelessWidget {
  final String keyword;
  final SearchController controller;
  final ScrollController scrollController;
  const SearchPage({
    Key key,
    @required this.keyword,
    this.controller,
    this.scrollController,
  }) : super(key: key);
  static void push({BuildContext context, String keyword}) {
    final productProtocol = ProductGridPageRepositorySearch();
    final controller = SearchController(
        initProtocol: productProtocol, loadMoreProtocol: productProtocol);
    ScrollController _scrollController = new ScrollController();
    // controller.evenInit(parameter: keyWord);
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => SearchPage(
          keyword: keyword,
          controller: controller,
          scrollController: _scrollController,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    controller.evenInit(parameter: keyword);
    return AppScaffold(
      title: "Tìm Kiếm",
      showCartBtn: false,
      child: Column(
        children: [
          SearchBar(
            contextValue: context,
            textValue: keyword,
            isSearchProduct: true,
            isEnable: false,
            onTap: () => Navigator.of(context).pop(),
          ),
          Flexible(
            child: SingleChildScrollView(
              controller: scrollController
                ..addListener(() {
                  if (scrollController.position.pixels ==
                      scrollController.position.maxScrollExtent) {
                    controller.eventLoadMore(parameter: keyword);
                  }
                }),
              child: StreamBuilder<ListState>(
                stream: controller.stateList,
                builder: (_, ss) {
                  if (ss?.data == null) {
                    return const SearchProductEmpty(
                      titleType: "Chưa có sản phẩm nào được tìm kiếm",
                    );
                  }
                  if (ss.data is ListEmptyState) {
                    return const SearchProductEmpty(
                      titleType: "Không tìm thấy sản phẩm tương ứng",
                    );
                  }
                  final ListLoadState<Product> stateProduct = ss.data;
                  return Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 20),
                        width: double.infinity,
                        child: Text(
                            "Tìm thấy ${stateProduct.data.length} sản phẩm"),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 20),
                        width: double.infinity,
                        child: ListProductGridBlockItem(
                            items: stateProduct.data
                                .map((e) => e.blockItem)
                                .toList()),
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class SearchProductEmpty extends StatelessWidget {
  final String titleType;
  const SearchProductEmpty({
    Key key,
    this.titleType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Text(titleType),
    ));
  }
}
