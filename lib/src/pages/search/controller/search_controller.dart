import 'package:flutter/material.dart';
import 'package:genie/src/components/init_loadmore_search/init_loadmore_search_controller.dart';
import 'package:genie/src/models/keyword.model.dart';
import 'package:genie/src/models/product.model.dart';
import 'package:genie/src/repositories/campaign.repo.dart';
import 'package:genie/src/repositories/customer.repo.dart';
import 'package:genie/src/repositories/keyword_repo.dart';

class SearchController extends InitLoadMoreSearchController<Product> {
  SearchController(
      {@required InitListProtocol initProtocol,
      @required LoadMoreProtocol loadMoreProtocol})
      : super(
          initProtocol: initProtocol,
          loadMoreProtocol: loadMoreProtocol,
        );
}

class KeyWordController extends ChangeNotifier {
  final _getMeCustomerRepo = CustomerRepository();
  final campaignRepo = CampaignRepository();
  final keywordRepo = KeyWordRepository();
  ValueNotifier<List<String>> listRecentKeywords = ValueNotifier(null);
  ValueNotifier<String> hintKeywords = ValueNotifier(null);
  ValueNotifier<List<KeyWordClass>> listAllKeywords = ValueNotifier(null);

  KeyWordController() {
    getPopularCampaign();
    getRecentKeyWords();
    getAllKeyWord();
  }

  getAllKeyWord() async {
    setKeyword(await keywordRepo.getAllKeyWord());
  }

  getPopularCampaign() async {
    hintKeywords.value = (await campaignRepo.getCampaignPopular())[0].code +
        " - " +
        (await campaignRepo.getCampaignPopular())[0].name;
    hintKeywords.notifyListeners();
  }

  getRecentKeyWords() async {
    await _getMeCustomerRepo.getCust().then((value) {
      setHistoryKeyword(value.recentKeywords);
    });
  }

  setHistoryKeyword(List<String> _list) {
    // if (listRecentKeywords.value == null) {
    //   listRecentKeywords.value = [];
    //   listRecentKeywords.value.insertAll(0, _list);
    // } else {
    //   listRecentKeywords.value.insertAll(0, _list);
    // }
    listRecentKeywords.value = _list;
    listRecentKeywords.notifyListeners();
    print(listRecentKeywords.value);
  }

  setKeyword(List<KeyWordClass> _listAll) {
    if (listAllKeywords.value == null) {
      listAllKeywords.value = [];
      _listAll.forEach((e) {
        listAllKeywords.value.add(e);
      });
    } else {
      _listAll.forEach((e) {
        listAllKeywords.value.add(e);
      });
    }

    listAllKeywords.notifyListeners();
    print(listAllKeywords.value);
  }

  filterKeyword(String searchWord) async {
    listRecentKeywords.value.clear();
    await getRecentKeyWords();
    if (listAllKeywords != null && listRecentKeywords != null) {
      listAllKeywords.value.forEach((e) {
        listRecentKeywords.value.add(e.keyWord);
      });
    }
    ;
    setHistoryKeyword(listRecentKeywords.value
        .where((e) => e.contains('$searchWord'))
        .toList());
  }
}
