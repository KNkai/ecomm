import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:genie/src/models/pagination.model.dart';
import 'package:genie/src/services/app_client.dart';

abstract class GraphQL<T> {
  T fromJson(Map<String, dynamic> json);
  String get modelName;

  Future<GraphQLList<T>> getAllFavorite(
      {@required String query, GraphQLQueryInput queryInput}) {
    final apiName = 'getAll$modelName';
    String _query = '''
    query GetAll(\$q: QueryGetListInput) {
           $apiName(q: \$q) {
    				data {
              $query
             }
      } pagination { limit page total } }
         }
    ''';

    return AppClient.instance
        .execute(_query,
            variables: queryInput == null ? {} : {'q': queryInput.toJson()})
        .then((response) {
      return GraphQLList(
        List<T>.from(_tranformJson(response.data[apiName]["data"])
            .map((x) => fromJson(x))),
        Pagination.fromJson(response.data[apiName]["pagination"]),
      );
    }).catchError((onError) {});
  }

  _tranformJson(json) {
    List<String> _json = [];
    for (final _temp in json) {
      _json.add(jsonEncode(_temp["product"]));
    }

    String _fullJson = "[";
    for (int i = 0; i < _json.length; i++) {
      if (i == _json.length - 1) {
        _fullJson += _json[i];
      } else {
        _fullJson += _json[i] + ",";
      }
    }
    _fullJson += "]";
    json = jsonDecode(_fullJson);
    return json;
  }

  Future<GraphQLList<T>> getAll(
      {@required String query, GraphQLQueryInput queryInput}) {
    final apiName = 'getAll$modelName';

    String _query = '''
        query GetAll(\$q: QueryGetListInput) {
          $apiName(q: \$q) { data { $query } pagination { limit page total } }
        }
        ''';

    // print(_query);
    return AppClient.instance
        .execute(_query,
            variables: queryInput == null ? {} : {'q': queryInput.toJson()})
        .then((response) {
      return GraphQLList(
          List<T>.from(response.data[apiName]["data"].map((x) => fromJson(x))),
          Pagination.fromJson(response.data[apiName]["pagination"]));
    });
  }

  Future<T> getOne(
      {@required String query,
      @required String id,
      GraphQLQueryInput queryInput}) {
    String _query = '''
       query GetOne {
          getOne$modelName(id: "$id") {  $query  }
        }
        ''';

    return AppClient.instance
        .execute(_query,
            variables: queryInput == null ? {} : {'q': queryInput.toJson()})
        .then((response) {
      return fromJson(response.data['getOne$modelName']);
    });
  }

  Future<T> create(
      {@required String query, @required Map<String, dynamic> variables}) {
    return AppClient.instance.execute('''
         mutation Create(\$d: Create${modelName}Input!) {
          create$modelName(data: \$d) {  $query  }
        }
        ''', variables: {'d': variables}).then((response) {
      return fromJson(response.data['create$modelName']);
    });
  }

  void deleteOne({@required String id}) {
    AppClient.instance.execute('''
         mutation {
          deleteOne$modelName(id: "$id") {id}
        }
        ''');
  }

  Future<void> udpate(
      {@required String id, @required Map<String, dynamic> variables}) {
    return AppClient.instance.execute('''
         mutation Update(\$q: UpdateDeliveryAddressInput!) {
          update$modelName(id: "$id", data: \$q) {id}
        }
        ''', variables: {'q': variables}).then((value) {
      return null;
    }).catchError((_) => null);
  }
}

class GraphQLList<T> {
  final List<T> data;
  final Pagination pagination;

  GraphQLList(this.data, this.pagination);
}

class GraphQLQueryInput {
  GraphQLQueryInput({
    this.limit = 20,
    this.page = 1,
    this.search,
    this.order,
    this.filter,
  });

  int limit;
  int page;
  String search;
  dynamic order;
  dynamic filter;

  Map<String, dynamic> toJson() => {
        "limit": limit == null ? null : limit,
        "page": page == null ? null : page,
        "search": search == null ? null : search,
        "order": order,
        "filter": filter,
      };
}

class FavoriteDataDemo {}
