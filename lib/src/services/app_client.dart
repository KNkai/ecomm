import 'dart:convert';

import 'package:dio/dio.dart' as d;
import 'package:flutter/cupertino.dart';

class AppClient {
  AppClient._();
  static final AppClient instance = AppClient._();

  d.Dio _dio;

  void init() {
    _dio = d.Dio(d.BaseOptions(
      baseUrl: 'https://genie.mcom.app/graphql',
      connectTimeout: 70000,
    ));
    // install later
  }

  void installToken({@required String xToken}) {
    _dio.options.headers['x-token'] = xToken;
  }

  void unInstallToken() {
    _dio.options.headers['x-token'] = null;
  }

  bool checkExistToken() => _dio.options.headers['x-token'] != null;

  Future<Response> execute(String query,
      {Map<String, dynamic> variables}) async {
    return await _post({
      'variables': variables == null ? {} : jsonEncode(variables),
      'query': query
    }, _dio)
        .first;
  }

  Future<Response> executeWithError(String query,
      {Map<String, dynamic> variables}) async {
    return await _postError({
      'variables': variables == null ? {} : jsonEncode(variables),
      'query': query
    }, _dio)
        .first;
  }

  Stream<Response> _postError(dynamic body, d.Dio dio) async* {
    final httpResponse = await dio.post("", data: body);

    Response response;

    try {
      response = Response(
          data: httpResponse.data['data'] as Map<String, dynamic>,
          errors: httpResponse.data['errors'] != null
              ? (httpResponse.data['errors'] as List)
                  .map((e) => GraphQLError(message: e['message']))
                  .toList()
              : null);
    } catch (e) {
      throw Exception("Parser exception in _postError function class Api");
    }

    yield Response(
      errors: response.errors,
      data: response.data,
    );
  }

  Stream<Response> _post(dynamic body, d.Dio dio) async* {
    final httpResponse = await dio.post("", data: body);

    Response response;

    try {
      response =
          Response(data: httpResponse.data['data'] as Map<String, dynamic>);
    } catch (e) {
      throw Exception("Parser exception in _post function class Api");
    }

    yield Response(
      data: response.data,
    );
  }
}

class Response {
  final List<GraphQLError> errors;

  final Map<String, dynamic> data;

  const Response({
    this.errors,
    this.data,
  });
}

class GraphQLError {
  final String message;

  GraphQLError({@required this.message});
}
